﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpDX.Toolkit;
using SharpDX.Toolkit.Graphics;
using SharpDX;

namespace TiledMap {
	public class Animation {
		TileSet TileSet { get; set; }
		private List<TileAnimation> TileAnimations { get; set; }
		float Timer;
		float maxTimer;
		int index;
		public Vector2 Position;
		float Opacity;

		public Animation(List<TileAnimation> ani, TileSet tileset, Vector2 position, float opacity) {
			TileAnimations = ani;
			Timer = 0f;
			maxTimer = ani[0].Duration / 1000f;
			index = 0;
			TileSet = tileset;
			Position = position;
			Opacity = opacity;
		}

		public void Update(GameTime gametime) {
			Timer += (float)gametime.ElapsedGameTime.TotalSeconds;

			if (Timer >= maxTimer) {
				index++;
				if (index >= TileAnimations.Count) {
					index = 0;
				}

				Timer = 0f;
				maxTimer = TileAnimations[index].Duration / 1000f;
			}
		}

		public void Draw(SpriteBatch spritebatch, Camera Cam, int x, int y) {
			Rectangle source = TileSet.GetRectangle(TileAnimations[index].TileID + 1, TileSet);

			//abschneiden des tiles was rechts oder unten über die Camera hinausragen würde
			if (y * TileSet.TileHeight - (int)Cam.Position.Y + TileSet.TileHeight > Cam.Height) {
				source.Height -= Convert.ToInt32((y * TileSet.TileHeight - (int)Cam.Position.Y + TileSet.TileHeight) - Cam.Height);
			}
			if (x * TileSet.TileWidth - (int)Cam.Position.X + TileSet.TileWidth > Cam.Width) {
				source.Width -= Convert.ToInt32((x * TileSet.TileWidth - (int)Cam.Position.X + TileSet.TileWidth) - Cam.Width);
			}

			spritebatch.Draw(TileSet.FullTileSet, Position, source, Color.White * Opacity);
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpDX;

namespace TiledMap {
	public class MapObject {
		public string Name { get; set; }
		public string Type { get; set; }
		public RectangleF Dimension {get; set;}
	}
}

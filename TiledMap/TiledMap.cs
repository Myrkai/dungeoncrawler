﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using SharpDX.Toolkit.Content;
using SharpDX.Toolkit.Graphics;
using System.IO;
using SharpDX;
using SharpDX.Toolkit;

namespace TiledMap {
	public class TiledMap {
		private List<TileSet> Tilesets { get; set; }
		private List<Layer<int>> Layers { get; set; }
		public Layer<bool> CollisitionLayer { get; private set; }
		public Camera Cam { get; private set; }
		private Dictionary<int, Animation> TileAnimations { get; set; }
		public List<MapObject> Events { get; set; }
		public ContentManager Content { get; private set; }

		/// <summary>
		/// Lädt eine komplette TiledMap-Datei im XML-Format.
		/// Beispiel: TiledMap map = new TiledMap(@"Content\TiledMaps\test.tmx", Content, @"Tilesets");
		/// </summary>
		public TiledMap(string path, ContentManager content, string TileSetPathFolder) {
			TileSetPathFolder = Initialize(path, content, TileSetPathFolder);
			Cam = new Camera(Vector2.Zero, Layers[0].Width * Tilesets[0].TileWidth, Layers[0].Height * Tilesets[0].TileHeight);
			Content = content;
		}

		public TiledMap(string path, ContentManager content, string TileSetPathFolder, Camera cam) {
			TileSetPathFolder = Initialize(path, content, TileSetPathFolder);
			Cam = cam;
			Content = content;
		}

		private string Initialize(string path, ContentManager content, string TileSetPathFolder) {
			Tilesets = new List<TileSet>();
			Layers = new List<Layer<int>>();
			TileAnimations = new Dictionary<int, Animation>();
			Events = new List<MapObject>();

			if (TileSetPathFolder.Substring(TileSetPathFolder.Length - 1) != @"\") {
				TileSetPathFolder += @"\";
			}

			//load Information throw XML-File
			XmlDocument doc = new XmlDocument();
			doc.Load(path);

			XmlNode mapNode = doc.GetElementsByTagName("map").Item(0);

			//Asserts
			if (mapNode.Attributes.GetNamedItem("orientation").Value != "orthogonal") {
				throw new Exception("Tiled-Maps müssen in der Ausrichtung orthogonal angelegt werden");
			}

			if (mapNode.Attributes.GetNamedItem("renderorder").Value != "right-down") {
				throw new Exception("Tiled-Maps müssen mit der Zählrichtung right-dows(rechts-unten) angelegt werden");
			}

			int mapWidth = Convert.ToInt32(mapNode.Attributes.GetNamedItem("width").Value);
			int mapHeight = Convert.ToInt32(mapNode.Attributes.GetNamedItem("height").Value);
			int tileWidth = Convert.ToInt32(mapNode.Attributes.GetNamedItem("tilewidth").Value);
			int tileHeight = Convert.ToInt32(mapNode.Attributes.GetNamedItem("tileheight").Value);

			//load TileSets
			foreach (XmlNode node in doc.GetElementsByTagName("tileset")) {
				int firstgid = Convert.ToInt32(node.Attributes.GetNamedItem("firstgid").Value);
				string name = node.Attributes.GetNamedItem("name").Value;
				int margin = 0;
				int spacing = 0;

				if (node.Attributes.GetNamedItem("spacing") != null) {
					spacing = Convert.ToInt32(node.Attributes.GetNamedItem("spacing").Value);
				}

				if (node.Attributes.GetNamedItem("margin") != null) {
					margin = Convert.ToInt32(node.Attributes.GetNamedItem("margin").Value);
				}

				if (node.Attributes.GetNamedItem("tilewidth").Value != tileWidth.ToString()) {
					throw new Exception("alle TileSets müssen pro Tile die gleiche Weite besitzen");
				}

				if (node.Attributes.GetNamedItem("tileheight").Value != tileHeight.ToString()) {
					throw new Exception("alle TileSets müssen pro Tile die gleiche Höhe besitzen");
				}

				XmlNode imageNode = node.SelectSingleNode("image");
				string source = imageNode.Attributes.GetNamedItem("source").Value;
				int imageWidth = Convert.ToInt32(imageNode.Attributes.GetNamedItem("width").Value);
				int imageHeight = Convert.ToInt32(imageNode.Attributes.GetNamedItem("height").Value);

				int lastgid = (int)Math.Floor((decimal)imageWidth / (decimal)tileWidth) * (int)Math.Floor((decimal)imageHeight / (decimal)tileHeight) + firstgid - 1;

				source = source.Substring(source.LastIndexOf(@"/") + 1);

				TileSet set = new TileSet(content.Load<Texture2D>(TileSetPathFolder + source), firstgid, lastgid, name, tileWidth, tileHeight, source);
				set.Margin = margin;
				set.Spacing = spacing;

				foreach (XmlNode tile in node.SelectNodes("tile")) {
					if (tile.HasChildNodes) {
						int id = Convert.ToInt32(tile.Attributes.GetNamedItem("id").Value);
						try {
							foreach (XmlNode frame in tile.SelectSingleNode("animation")) {
								int tileID = Convert.ToInt32(frame.Attributes.GetNamedItem("tileid").Value);
								int duration = Convert.ToInt32(frame.Attributes.GetNamedItem("duration").Value);

								if (!set.TileAnimation.ContainsKey(tileID)) {
									set.TileAnimation.Add(tileID, new TileAnimation() { ID = id, Duration = duration, TileID = tileID });
								}
							}
						} catch { }
					}
				}

				Tilesets.Add(set);
			}

			//load Layers & Data
			bool hasLoadCollisionLayer = false;
			foreach (XmlNode node in doc.GetElementsByTagName("layer")) {
				string name = node.Attributes.GetNamedItem("name").Value;
				float opacity = 1f;
				bool visible = true;

				//optionale Angaben
				if (node.Attributes.GetNamedItem("opacity") != null) {
					opacity = Convert.ToSingle(node.Attributes.GetNamedItem("opacity").Value.Replace(".", ","));
				}

				if (node.Attributes.GetNamedItem("visible") != null) {
					visible = node.Attributes.GetNamedItem("visible").Value == "0" ? false : true;
				}

				//Asserts
				if (node.Attributes.GetNamedItem("width").Value != mapWidth.ToString()) {
					throw new Exception("alle Layer müssen die gleiche Anzahl Kacheln in der Weite besitzen");
				}

				if (node.Attributes.GetNamedItem("height").Value != mapHeight.ToString()) {
					throw new Exception("alle Layer müssen die gleiche Anzahl Kacheln in der Höhe besitzen");
				}

				//Absicher das die Kollisionsebene nur genau 1 mal geladen wird
				if (name == "Walkable" && hasLoadCollisionLayer) {
					throw new Exception("Es darf nur eine Kollisionsebene pro Map existieren");
				} else if (name == "Walkable") { //laden der Kollisionsebene
					CollisitionLayer = new Layer<bool>(mapWidth, mapHeight, name, opacity, visible);
					hasLoadCollisionLayer = true;

					foreach (XmlNode tileNode in node.SelectSingleNode("data").SelectNodes("tile")) {
						bool walkable = Convert.ToInt32(tileNode.Attributes.GetNamedItem("gid").Value) > 0 ? false : true;
						CollisitionLayer.AddCellInformation(walkable);
					}
				} else { //laden der Tile's für jede einzelne Ebene
					Layer<int> layer = new Layer<int>(mapWidth, mapHeight, name, opacity, visible);
					Layers.Add(layer);

					foreach (XmlNode tileNode in node.SelectSingleNode("data").SelectNodes("tile")) {
						int gid = Convert.ToInt32(tileNode.Attributes.GetNamedItem("gid").Value);
						layer.AddCellInformation(gid);
					}
				}
			}

			bool hasLoadEventGroup = false;
			foreach (XmlNode node in doc.GetElementsByTagName("objectgroup")) {
				if (!hasLoadEventGroup) {
					hasLoadEventGroup = true;
					foreach (XmlNode objectNode in node.SelectNodes("object")) {
						string name = objectNode.Attributes.GetNamedItem("name").Value;
						string type = objectNode.Attributes.GetNamedItem("type").Value;
						RectangleF dimension = new RectangleF(Convert.ToSingle(objectNode.Attributes.GetNamedItem("x").Value.Replace(",", ".")),
															Convert.ToSingle(objectNode.Attributes.GetNamedItem("y").Value.Replace(",", ".")),
															Convert.ToSingle(objectNode.Attributes.GetNamedItem("width").Value.Replace(",", ".")),
															Convert.ToSingle(objectNode.Attributes.GetNamedItem("height").Value.Replace(",", ".")));

						Events.Add(new MapObject() { Name = name, Type = type, Dimension = dimension });
					}
				} else {
					throw new Exception("Es wird nur eine Events ObjectGruppe/ObjectLayer unterstützt.");
				}
			}

			//Absichern das die Kollisionsebene überhaupt geladen wurde
			if (!hasLoadCollisionLayer) {
				throw new Exception("Die Kollisionsebene wurde nicht geladen, es fehlt die Ebene mit dem Namen 'Walkable'");
			}

			return TileSetPathFolder;
		}

		public virtual void Draw(SpriteBatch spriteBatch) {
			int maxWidthCam = (int)Math.Floor((double)Cam.Width / Tilesets[0].TileWidth); //bis wohin wird die Map gerender
			int maxHeightCam = (int)Math.Floor((double)Cam.Height / Tilesets[0].TileHeight);

			int offsetX = (int)Math.Ceiling(Cam.Position.X / Tilesets[0].TileWidth); //von welcher zelle aus wird ei map gerender
			int offsetY = (int)Math.Ceiling(Cam.Position.Y / Tilesets[0].TileWidth);

			maxWidthCam += offsetX; //korrekt der maximalen weite, betrifft wenn der start größer 0 ist
			maxHeightCam += offsetY;

			for (int x = Math.Max(offsetX - 1, 0); x < Math.Min(maxWidthCam + 1, Layers[0].Width); x++) { //stellt sicher das links und oben auch gerendet wird
				for (int y = Math.Max(offsetY - 1, 0); y < Math.Min(maxHeightCam + 1, Layers[0].Height); y++) {
					foreach (Layer<int> layer in Layers.Where(l => l.Visible)) {
						int gid = layer.GetCell(x, y);
						if (gid > 0) { //Zellen in Tiled mit GID 0 sind transparent/blank/null
							TileSet set = GetTileSet(gid);
							//Draw Tile Animation
							if (set.TileAnimation.ContainsKey(gid)) {
								if (TileAnimations.ContainsKey(gid)) {
									TileAnimations[gid].Position = new Vector2(x * set.TileWidth - (int)Cam.Position.X, y * set.TileHeight - (int)Cam.Position.Y);
									if (x * set.TileWidth - (int)Cam.Position.X < Cam.Width && y * set.TileHeight - (int)Cam.Position.Y < Cam.Width) {
										TileAnimations[gid].Draw(spriteBatch, Cam, x, y);
									}
								} else {
									List<TileAnimation> tileAnimation = set.TileAnimation.Values.Where(s => s.ID == set.TileAnimation[gid].ID).ToList();
									TileAnimations.Add(gid, new Animation(tileAnimation, set, new Vector2(x * set.TileWidth - (int)Cam.Position.X, y * set.TileHeight - (int)Cam.Position.Y), layer.Opacity));
								}
							} else { //Draw Tile
								Rectangle source = set.GetRectangle(gid, set);

								//abschneiden des tiles was rechts oder unten über die Camera hinausragen würde
								if (y * set.TileHeight - (int)Cam.Position.Y + set.TileHeight > Cam.Height) {
									source.Height -= Convert.ToInt32((y * set.TileHeight - (int)Cam.Position.Y + set.TileHeight) - Cam.Height);
								}
								if (x * set.TileWidth - (int)Cam.Position.X + set.TileWidth > Cam.Width) {
									source.Width -= Convert.ToInt32((x * set.TileWidth - (int)Cam.Position.X + set.TileWidth) - Cam.Width);
								}

								//trotzdem nur rendern was sich innerhalb der Camera befindet und nicht rechts oder unterhalb... ja das hier ist nur eine schlechte Bug behebung. (ist mir egal!)
								if (x * set.TileWidth - (int)Cam.Position.X < Cam.Width && y * set.TileHeight - (int)Cam.Position.Y < Cam.Width) {
									spriteBatch.Draw(set.FullTileSet, new Vector2(x * set.TileWidth - (int)Cam.Position.X, y * set.TileHeight - (int)Cam.Position.Y), source, Color.White * layer.Opacity);
								}
							}
						}
					}
				}
			}
		}

		public virtual void Update(GameTime gametime) { 
			TileAnimations.Values.ToList().ForEach(s => s.Update(gametime));
		}

		private TileSet GetTileSet(int gid) {
			return Tilesets.Where(t => gid >= t.FirstGID && gid <= t.LastGID).First();
		}

		public int MapWidthPx { get { return Layers[0].Width * Tilesets[0].TileWidth; } }
		public int MapHeightPx { get { return Layers[0].Height * Tilesets[0].TileHeight; } }
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpDX.Toolkit.Graphics;
using SharpDX;

namespace TiledMap {
	public struct TileAnimation {
		public int ID { get; set; }
		public int TileID { get; set; }
		public int Duration { get; set; }
	}

	public class TileSet {
		public Texture2D FullTileSet { get; private set; }
		public int FirstGID { get; private set; }
		public int LastGID { get; private set; }
		private string Name { get; set; }
		public int TileWidth { get; private set; }
		public int TileHeight { get; private set; }
		private string GraphicAsset { get; set; }
		public int Margin { get; set; } //Seitenrand
		public int Spacing { get; set; } //Abstand
		private int Addon { get; set; }
		public Dictionary<int, TileAnimation> TileAnimation { get; set; } //<TileID, <TileID der Animation, Duration in ms>>

		public TileSet(Texture2D tileset, int firstGID, int lastGID, string name, int tileWidth, int tileHeight, string graphicAsset) {
			FullTileSet = tileset;
			FirstGID = firstGID;
			LastGID = lastGID;
			Name = name;
			TileWidth = tileWidth;
			TileHeight = tileHeight;
			GraphicAsset = graphicAsset;

			Margin = 0;
			Spacing = 0;

			TileAnimation = new Dictionary<int, TileAnimation>();

			double width = Convert.ToDouble(FullTileSet.Width) / Convert.ToDouble(TileWidth);
			int width2 = (int)Math.Floor(width);
			double rest = width - width2;

			if (rest > 0.5d) {
				Addon = -1;
			}
		}

		internal Rectangle GetRectangle(int gid, TileSet set) {
			int elementCountWidth = (int)Math.Floor(Convert.ToDouble(FullTileSet.Width) / Convert.ToDouble(TileWidth));

			elementCountWidth += Addon;

			int id = gid - set.FirstGID;
			int offsetY = (int)Math.Floor(Convert.ToDouble(id) / Convert.ToDouble(elementCountWidth));
			int offsetX = id - offsetY * (elementCountWidth);
			return new Rectangle(TileWidth * offsetX + Margin + offsetX * Spacing, TileHeight * offsetY + Margin + offsetY * Spacing, TileWidth, TileHeight);
		}
	}
}

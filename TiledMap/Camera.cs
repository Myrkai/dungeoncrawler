﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpDX;

namespace TiledMap {
	public class Camera {
		public Vector2 Position { get; set; }
		public int Width { get; private set; }
		public int Height { get; private set; }
		public Rectangle Rectangle { get { return new Rectangle((int)Position.X, (int)Position.Y, Width, Height); } }

		public Camera(Vector2 position, int width, int height) {
			Position = position;
			Width = width;
			Height = height;
		}
	}
}

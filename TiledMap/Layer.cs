﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TiledMap {
	public class Layer<T> {
		public List<T> layerInformation { get; private set; }
		public int Height { get; private set; }
		public int Width { get; private set; }
		private string Name { get; set; }
		public float Opacity { get; private set; }
		public bool Visible { get; private set; }

		public Layer(int width, int height, string name, float opacity, bool visible) {
			layerInformation = new List<T>();
			Height = height;
			Width = width;
			Name = name;
			Opacity = opacity;
			Visible = visible;
		}

		public void AddCellInformation(T information) {
			layerInformation.Add(information);
		}

		public T GetCell(int x, int y) {
			return layerInformation[SharpDX.MathUtil.Clamp(x + y * Width, 0, layerInformation.Count - 1)];
		}
	}
}

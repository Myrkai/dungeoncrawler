﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace PathFinding {
	static class Program {
		[STAThread]
		static void Main() {
//            string mazeStr = @"XXXXXXX
//							   X.X...X
//							   ..*.X.X
//							   XXXXX.X
//							   X.....X
//							   X.XXX.X";

//            string mazeStr2 = @"XXXXXXX
//							    X.X...X
//							    X.*.X.X
//							    XX.XX.X
//							    X....XX
//							    X.X.XXX";

//            string mazeStr3 = @"XXXXXXXXX
//							    X*X...X..
//							    X...X.X.X
//							    XXXXX.X.X
//							    X.....X.X
//							    X.XXXXX.X
//								X.......X
//								XXXXXXXXX";

			string mazeStr4 = @"XXXXXXX
							    X.X...X
							    X.*.X.X
							    XX.XX..
							    X....XX
							    X.X.XXX";

			Cell startCell;
			bool[,] walkable;
			char[,] maze = ReadMaze(mazeStr4, out walkable, out startCell);

			if (startCell == null) {
				Console.WriteLine("Kein Startpunkt angegeben.");
				return;
			}

			List<List<Cell>> possiblePaths = AllPaths(startCell, walkable);

			Console.WriteLine("---Source Maze---");
			WriteMaze(maze);

			foreach (List<Cell> path in possiblePaths) {
				Console.WriteLine("---Maze: " + path.Count.ToString() + "---");
				WriteMaze(GetPathMaze(maze, path));
			}

			Console.ReadKey();
		}

		private static List<List<Cell>> FlattenAllPaths(List<List<Cell>> possiblePaths) {
			List<List<Cell>> tmp = new List<List<Cell>>();
			List<Cell> current = new List<Cell>();

			foreach (List<Cell> path in possiblePaths) {
				foreach (Cell cell in path.OrderByDescending(s => s.Distance).ThenByDescending(s => s.Row).ThenByDescending(s => s.Column)) {
					if (current.Count == 0 && tmp.SelectMany(s => s).Where(s => (s.Distance == cell.Distance && s.Column == cell.Column && s.Row == cell.Row)).All(s => false)) {
						current.Add(cell);
					} else {
						if (current.Count > 0
							&& current.LastOrDefault().Distance - 1 == cell.Distance 
							&& (current.LastOrDefault().Row == cell.Row || current.LastOrDefault().Column == cell.Column)
							&& ((Math.Abs(current.LastOrDefault().Row - cell.Row) == 0 || Math.Abs(current.LastOrDefault().Row - cell.Row) == 1) && (Math.Abs(current.LastOrDefault().Column - cell.Column) == 0 || Math.Abs(current.LastOrDefault().Column - cell.Column) == 1))) {
							current.Add(cell);
						}
					}
				}
				tmp.Add(current);
				current = new List<Cell>();
			}

			return tmp;
		}

		private static char[,] GetPathMaze(char[,] maze, List<Cell> currentPath) {
			char[,] mazeCopy = (char[,])maze.Clone();
			foreach (Cell cell in currentPath) {
				mazeCopy[cell.Row, cell.Column] = '#';
			}
			return mazeCopy;
		}

		private static void WriteMaze(char[,] maze) {
			string line = "";
			for (int x = 0; x < maze.GetLength(0); x++) {
				for (int y = 0; y < maze.GetLength(1); y++) {
					if (y == maze.GetLength(1) - 1) {
						Console.WriteLine(line + maze[x, y]);
						line = "";
					} else {
						line += maze[x, y];
					}
				}
			}
		}

		public static char[,] ReadMaze(string m, out bool[,] walkable, out Cell startCell) {
			char[,] output;
			string[] maze = m.Split('\n');
			maze = maze.Select(s => s.Trim()).ToArray();
			startCell = null;

			walkable = new bool[maze.Length, maze[0].ToCharArray().Length];
			output = new char[maze.Length, maze[0].ToCharArray().Length];

			for (int x = 0; x < maze.Length; x++) {
				for (int y = 0; y < maze[0].ToCharArray().Length; y++) {
					walkable[x, y] = maze[x].ToCharArray()[y] == '.';
					output[x, y] = maze[x].ToCharArray()[y];

					if (maze[x].ToCharArray()[y] == '*') {
						walkable[x, y] = true;
						startCell = new Cell(x, y, 0);
					}
				}
			}

			return output;
		}

		public static List<List<Cell>> AllPaths(Cell startCell, bool[,] walkable) {
			if (startCell == null) {
				return null;
			}

			Queue<Cell> visitedCells = new Queue<Cell>();
			List<List<Cell>> possiblePaths = new List<List<Cell>>();
			List<Cell> currentPath = new List<Cell>();
			VisitCell(visitedCells, walkable, startCell.Row, startCell.Column, 0, currentPath);

			while (visitedCells.Count > 0) {
				Cell currentCell = visitedCells.Dequeue();
				int row = currentCell.Row;
				int column = currentCell.Column;
				int distance = currentCell.Distance;

				if (row == 0 || row == walkable.GetLength(0) - 1 || column == 0 || column == walkable.GetLength(1) - 1) {
					currentPath.Add(currentCell);
					possiblePaths.Add(currentPath.Where(s => s.Distance <= distance).ToList());
				}

				VisitCell(visitedCells, walkable, row, column + 1, distance + 1, currentPath);
				VisitCell(visitedCells, walkable, row, column - 1, distance + 1, currentPath);
				VisitCell(visitedCells, walkable, row + 1, column, distance + 1, currentPath);
				VisitCell(visitedCells, walkable, row - 1, column, distance + 1, currentPath);
			}

			return FlattenAllPaths(possiblePaths);
		}

		public static void VisitCell(Queue<Cell> visitedCells, bool[,] walkable, int row, int column, int distance, List<Cell> currentPath) {
			if (row >= 0 && column >= 0 && row <= walkable.GetLength(0) - 1 && column <= walkable.GetLength(1) - 1 && walkable[row, column]) {
				walkable[row, column] = false;
				visitedCells.Enqueue(new Cell(row, column, distance));
				currentPath.Add(new Cell(row, column, distance));
			}
		}

		public class Cell {
			public int Row { get; set; }
			public int Column { get; set; }
			public int Distance { get; set; }

			public Cell(int row, int column, int distance) {
				Row = row;
				Column = column;
				Distance = distance;
			}
		}
	}
}

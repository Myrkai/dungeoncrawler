﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpDX.Toolkit.Input;
using SharpDX.Toolkit;
using SharpDX.Toolkit.Graphics;
using SharpDX;

namespace GUI {
	public class GameKeyStateEmulator {
		public GameKeys Key;
		public GameKeysState KeyState;
	}

	public enum GameKeys {
		Menue
	}

	public enum GameKeysState {
		//Button is in a none state.
		None = 0,
		// The button is being pressed.
		Down = 1,
		// The button was pressed since last frame.
		Pressed = 2,
		//The button was released since last frame.
		Released = 4
	}

	public static class VirtualGamePad {
		static MouseManager mouseManager;
		static MouseState mouseState;
		static int screenWidth;
		static int screenHeight;
		static KeyboardManager keyBoardManager;
		static KeyboardState keyBoardState;
		public static bool ignoreOneFrame = false;

		const Keys MenueKey = Keys.Escape;

		static GameKeyStateEmulator keyEmulator;

		public static void Initialize(Game game, GraphicsDevice device) {
			mouseManager = new MouseManager(game);
			keyBoardManager = new KeyboardManager(game);

			var backbuffer = device.BackBuffer;
			screenWidth = backbuffer.Width;
			screenHeight = backbuffer.Height;
		}

		public static void Update() {
			mouseState = mouseManager.GetState();
			keyBoardState = keyBoardManager.GetState();
			if (ignoreOneFrame) {
				ignoreOneFrame = false;
			}
			keyEmulator = null;
		}

		public static bool ClickReleased() {
			return mouseState.LeftButton.Released;
		}

		public static bool ClickBegin() {
			return mouseState.LeftButton.Pressed;
		}

		public static bool ClickHolded() {
			return mouseState.LeftButton.Down;
		}

		public static Vector2 GetClickPoint() {
			return new Vector2(mouseState.X * screenWidth, mouseState.Y * screenHeight);
		}

		public static GameKeysState GetGameKeyState(GameKeys gameKey) {
			if (!ignoreOneFrame) {
				if (gameKey == GameKeys.Menue || keyEmulator != null && keyEmulator.Key == GameKeys.Menue) {
					if (keyBoardState.IsKeyDown(MenueKey) || keyEmulator != null && keyEmulator.KeyState == GameKeysState.Down) {
						return GameKeysState.Down;
					}

					if (keyBoardState.IsKeyPressed(MenueKey) || keyEmulator != null && keyEmulator.KeyState == GameKeysState.Pressed) {
						return GameKeysState.Pressed;
					}

					if (keyBoardState.IsKeyReleased(MenueKey) || keyEmulator != null && keyEmulator.KeyState == GameKeysState.Released) {
						ignoreOneFrame = true;
						return GameKeysState.Released;
					}
				}
			}

			return GameKeysState.None;
		}

		public static void SetGameKeyState(GameKeys key, GameKeysState state) {
			keyEmulator = new GameKeyStateEmulator();
			keyEmulator.Key = key;
			keyEmulator.KeyState = state;
		}
	}
}

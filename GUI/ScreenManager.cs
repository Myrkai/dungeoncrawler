﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GUI.Interface;
using SharpDX.Toolkit;
using SharpDX.Toolkit.Graphics;

namespace GUI {
	public class ScreenManager {

		private static ScreenManager _manager;
		public static ScreenManager Instance { get { if(_manager == null) { _manager = new ScreenManager();}  return _manager;} } 

		Dictionary<string, IScreen> screens; //alle zur Verfügung stehenden Screens
		Stack<IScreen> currentScreens; //alle Screens die Verarbeitet werden sollen
		public event EventHandler<ScreenManagerEventArgs> ScreenAdded;
		public event EventHandler<ScreenManagerEventArgs> ScreenRemoved;
		public event EventHandler<ScreenManagerEventArgs> ScreenRegistered;

		private ScreenManager() {
			screens = new Dictionary<string, IScreen>();
			currentScreens = new Stack<IScreen>();
		}

		/// <summary>
		/// Registiert einen Screen beim Manager, sodass dieser auch verwendet werden kann.
		/// </summary>
		public void RegisterScreen(string name, IScreen screen) {
			if (screens.ContainsKey(name)) {
				screens[name] = screen;
			} else {
				screens.Add(name, screen);
			}

			if (ScreenRegistered != null) {
				ScreenRegistered(this, new ScreenManagerEventArgs(screen, name));
			}
		}

		public IScreen GetScreen(string name) {
			if (screens.ContainsKey(name)) {
				return screens[name];
			} else {
				throw new Exception("Key " + name + " ist nicht vorhanden");
			}
		}

		public void Update(GameTime gameTime) {
			List<IScreen> iList = currentScreens.ToList();
			foreach (IScreen i in iList) {
				i.Update(gameTime);
			}
		}

		public void Draw(SpriteBatch spriteBatch) {
			List<IScreen> iList = currentScreens.ToList();
			iList.Reverse();
			foreach (IScreen i in iList) {
				i.Draw(spriteBatch);
			}
		}

		/// <summary>
		/// fügt einen Screen auf den Stack hinzu
		/// </summary>
		public void AddScreen(string name, bool stopAllOther) {
			if (stopAllOther) {
				foreach (IScreen i in currentScreens) {
					i.SetIsActiv(false);
				}
			}
			currentScreens.Push(GetScreen(name));
			if (ScreenAdded != null) {
				ScreenAdded(this, new ScreenManagerEventArgs(GetScreen(name), name));
			}
		}

		/// <summary>
		/// entfernt den obersten/letzten Screen
		/// </summary>
		public void RemoveScreen() {
			IScreen s = currentScreens.Pop();
			if (ScreenRemoved != null) {
				ScreenRemoved(this, new ScreenManagerEventArgs(s, ""));
			}
		}

		/// <summary>
		/// entfernt alle Screens
		/// </summary>
		public void RemoveAllScreens() {
			foreach (IScreen s in currentScreens) {
				if (ScreenRemoved != null) {
					ScreenRemoved(this, new ScreenManagerEventArgs(s, ""));
				}
			}
			currentScreens.Clear();
		}

		/// <summary>
		/// wechsel zum Aktuellen screen. Alle anderen werden entfernt
		/// </summary>
		/// <param name="screen"></param>
		public void Transfer(string name) {
			RemoveAllScreens();
			AddScreen(name, false);
		}
	}

	public class ScreenManagerEventArgs : EventArgs {
		public IScreen Screen { get; private set; }
		public string Title { get; private set; }

		public ScreenManagerEventArgs(IScreen screen, string title) {
			this.Screen = screen;
			this.Title = title;
		}
	}
}

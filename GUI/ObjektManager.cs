﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GUI.Interface;
using SharpDX.Toolkit;
using SharpDX;

namespace GUI {
	public class ObjektManager {
		private Dictionary<string, IClickable> Controls { get; set; }

		public ObjektManager() {
			Controls = new Dictionary<string, IClickable>();
		}

		public void Update(GameTime gameTime) {
			if (VirtualGamePad.ClickReleased()) {
				PerformClick(VirtualGamePad.GetClickPoint());
			}
		}

		public void AddClickableObjekt(string key, IClickable o) {
			Controls.Add(key, o);
		}

		public void RemoveClickableObjekt(string key) {
			Controls.Remove(key);
		}

		public void PerformClick(Vector2 position) {
			IClickable a = Controls.Select(e => e.Value).Where(e => e.IsClickable() && e.IsEnable() && e.GetDimension().Intersects(new Rectangle((int)position.X, (int)position.Y, 1, 1))).FirstOrDefault();

			if (a != null) {
				a.PerformClick(position);
			}
		}

		public void Reset() {
			Controls.Clear();
		}

		public int Count() {
			return Controls.Count();
		}
	}
}

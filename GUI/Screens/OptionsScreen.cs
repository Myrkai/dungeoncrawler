﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GUI.Controls;
using SharpDX.Toolkit.Graphics;
using SharpDX.Toolkit;
using GUI.Interface;
using SharpDX;

namespace GUI.Screens {
	public class OptionsScreen<T> : BaseScreen<T> {
		LinkControl title;
		ImageControl background;
		Dictionary<string, IControl> controls;

		public OptionsScreen(T gameRef, ScreenManager screenManagerRef)
			: base(gameRef, screenManagerRef) {
			controls = new Dictionary<string, IControl>();
		}

		public override void Draw(SpriteBatch spriteBatch) {
			if (IsActiv && PerformDraw) {
				foreach (IControl control in CustomControls.Where(c => c.Item3).Select(c => c.Item2)) {
					control.Draw(spriteBatch);
				}

				if (background != null) {
					background.Draw(spriteBatch);
				}

				if (title != null) {
					title.Draw(spriteBatch);
				}

				foreach (IControl control in controls.Values) {
					control.Draw(spriteBatch);
				}

				foreach (IControl control in CustomControls.Where(c => !c.Item3).Select(c => c.Item2)) {
					control.Draw(spriteBatch);
				}
			}
		}

		public override void Update(GameTime gameTime) {
			if (IsActiv && PerformUpdate) {
				ObjektManager.Update(gameTime);
				foreach (IControl control in CustomControls.Select(c => c.Item2)) {
					control.Update(gameTime);
				}

				if (background != null) {
					background.Update(gameTime);
				}

				if (title != null) {
					title.Update(gameTime);
				}

				foreach (IControl control in controls.Values) {
					control.Update(gameTime);
				}
			}
		}

		public override void Initialize() {

		}

		public override void LoadContent() {

		}

		/// <summary>
		/// Fügt einen Titel hinzu. Reine Text anzeige.
		/// </summary>
		/// <param name="text">Text der angezeigt werden soll</param>
		/// <param name="position">Wo soll der Text anscheinen</param>
		public void SetTitle(string text, Vector2 position, SpriteFont font, Color fontColor) {
			title = new LinkControl(text, position, font, fontColor, null);
		}

		/// <summary>
		/// Fügt ein Hintergrundbild hinzu. Dieses ist Bildschirmfüllend
		/// </summary>
		/// <param name="texture">Das jeweilige Bild</param>
		public void SetBackground(Texture2D texture, float scale) {
			background = new ImageControl(texture, Vector2.Zero);
			background.Scale = scale;
		}

		/// <summary>
		/// Hier werden die Kontrols dem Optionsmenü hinzugefügt
		/// </summary>
		public void AddControl(string key, IControl control) {
			ObjektManager.RemoveClickableObjekt(key);
			controls.Remove(key);
			ObjektManager.AddClickableObjekt(key, control);
			controls.Add(key, control);
		}

		public void SaveOptions(Action saveAction) {
			if (saveAction != null) {
				saveAction();
			}
		}

		public void LoadOptions(Action loadAction) {
			if (loadAction != null) {
				loadAction();
			}
		}
	}
}

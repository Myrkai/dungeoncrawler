﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpDX.Toolkit;
using SharpDX.Toolkit.Graphics;
using GUI.Controls;
using GUI.Interface;
using SharpDX;

namespace GUI.Screens {
	public class MainMenuScreen<T> : BaseScreen<T> {

		LinkControl title;
		ImageControl background;
		Dictionary<string, ButtonControl> buttons;

		public MainMenuScreen(T gameRef, ScreenManager screenManagerRef)
			: base(gameRef, screenManagerRef) {
			buttons = new Dictionary<string, ButtonControl>();
		}

		public override void Draw(SpriteBatch spriteBatch) {
			if (IsActiv && PerformDraw) {
				foreach (IControl control in CustomControls.Where(c => c.Item3).Select(c => c.Item2)) {
					control.Draw(spriteBatch);
				}

				if (background != null) {
					background.Draw(spriteBatch);
				}

				if (title != null) {
					title.Draw(spriteBatch);
				}

				foreach (ButtonControl button in buttons.Values) {
					button.Draw(spriteBatch);
				}

				foreach (IControl control in CustomControls.Where(c => !c.Item3).Select(c => c.Item2)) {
					control.Draw(spriteBatch);
				}
			}
		}

		public override void Update(GameTime gameTime) {
			if (IsActiv && PerformUpdate) {
				ObjektManager.Update(gameTime);
				foreach (IControl control in CustomControls.Select(c => c.Item2)) {
					control.Update(gameTime);
				}

				if (background != null) {
					background.Update(gameTime);
				}

				if (title != null) {
					title.Update(gameTime);
				}

				foreach (ButtonControl button in buttons.Values) {
					button.Update(gameTime);
				}

			}
		}

		public override void Initialize() {

		}

		public override void LoadContent() {

		}

		/// <summary>
		/// Fügt einen Titel hinzu. Reine Text anzeige.
		/// </summary>
		/// <param name="text">Text der angezeigt werden soll</param>
		/// <param name="position">Wo soll der Text anscheinen</param>
		public void SetTitle(string text, Vector2 position, SpriteFont font, Color fontColor) {
			title = new LinkControl(text, position, font, fontColor, null);
		}

		/// <summary>
		/// Fügt einen Titel hinzu. Reine Text anzeige.
		/// </summary>
		public void SetTitle(string text) {
			title.Text = text;
		}

		/// <summary>
		/// Fügt ein Hintergrundbild hinzu. Dieses ist Bildschirmfüllend
		/// </summary>
		/// <param name="texture">Das jeweilige Bild</param>
		public void SetBackground(Texture2D texture) {
			background = new ImageControl(texture, Vector2.Zero);
		}

		/// <summary>
		/// Fügt ein Button hinzu.
		/// </summary>
		/// <param name="buttonText">Text der auf den Button erscheinen soll</param>
		/// <param name="position">Wo soll der Button erscheinen</param>
		/// <param name="buttonTexture">Textur des Buttons</param>
		/// <param name="onClickEvent">Funktion die beim Betätigen des Buttons aufgerufen werden soll</param>
		public void AddButton(string buttonText, Vector2 position, SpriteFont font, Color fontColor, Texture2D buttonTexture, Action onClickEvent, Color backgroundRenderColor) {
			ObjektManager.RemoveClickableObjekt(buttonText);
			buttons.Remove(buttonText);
			ButtonControl button = new ButtonControl(buttonText, position, font, buttonTexture, fontColor, onClickEvent, backgroundRenderColor);
			ObjektManager.AddClickableObjekt(buttonText, button);
			buttons.Add(buttonText, button);
		}
	}
}

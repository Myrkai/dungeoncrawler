﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GUI.Interface;
using SharpDX.Toolkit;
using SharpDX.Toolkit.Graphics;

namespace GUI.Screens {
	public abstract class BaseScreen<T> : IScreen {
		public abstract void Draw(SpriteBatch spriteBatch);
		public abstract void Update(GameTime gameTime);
		public abstract void Initialize();
		public abstract void LoadContent();

		public bool IsActiv { get; set; }
		public bool PerformDraw { get; set; }
		public bool PerformUpdate { get; set; }

		public T GameRef { get; private set; }
		public ScreenManager ScreenManagerRef { get; private set; }
		public ObjektManager ObjektManager { get; private set; }

		public List<Tuple<string, IControl, bool>> CustomControls { get; set; } //key, Control selbst, Rendering @ first

		public BaseScreen(T gameref, ScreenManager screenManagerRef) {
			ObjektManager = new ObjektManager();
			IsActiv = true;
			PerformDraw = true;
			PerformUpdate = true;

			GameRef = gameref;
			ScreenManagerRef = screenManagerRef;

			CustomControls = new List<Tuple<string, IControl, bool>>();
		}

		public void SetIsActiv(bool activ) {
			IsActiv = activ;
		}

		public void SetDoDraw(bool draw) {
			PerformDraw = draw;
		}

		public void SetDoUpdate(bool update) {
			PerformUpdate = update;
		}

		/// <summary>
		/// Fügt ein individuelles Kontrol hinzu. Fürhandene Schlüssel werden dabei überschrieben.
		/// </summary>
		/// <param name="renderingFirst">true = wird vor allen anderen Gerendert; false= wird als letztes gerendert</param>
		public virtual void AddCustomControl(string key, IControl control, bool renderingFirst) {
			if (CustomControls.Where(e => e.Item1 == key).Count() >= 1) {
				RemoveCustomControl(key);
			}
			CustomControls.Add(new Tuple<string, IControl, bool>(key, control, renderingFirst));
			ObjektManager.AddClickableObjekt(key, control);
		}

		public void RemoveCustomControl(string key) {
			var a = CustomControls.Where(e => e.Item1 == key).FirstOrDefault();

			if (a != null) {
				CustomControls.Remove(a);
			}
			ObjektManager.RemoveClickableObjekt(key);
		}

		public void RemoveCustomControl(IControl control) {
			var a = CustomControls.Where(e => e.Item2 == control).FirstOrDefault();

			if (a != null) {
				CustomControls.Remove(a);
			}
			ObjektManager.RemoveClickableObjekt(a.Item1);
		}

		public void ClearCustomControls() {
			CustomControls.Clear();
		}

		public IControl GetCustomControl(string key) {
			return CustomControls.Where(e => e.Item1 == key).First().Item2;
		}
	}
}

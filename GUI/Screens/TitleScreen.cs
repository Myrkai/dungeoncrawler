﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpDX.Toolkit.Graphics;
using SharpDX.Toolkit;
using GUI.Controls;
using SharpDX;
using GUI.Interface;

namespace GUI.Screens {
	public class TitleScreen<T> : BaseScreen<T> {

		LinkControl title;
		ImageControl background;
		ButtonControl next;

		float timer;
		float MaxTimer;
		bool useTimeout;
		Action timeoutEvent;

		public TitleScreen(T gameRef, ScreenManager screenManagerRef)
			: base(gameRef, screenManagerRef) {
			Initialize();
			LoadContent();
		}

		public override void Draw(SpriteBatch spriteBatch) {
			if (IsActiv && PerformDraw) {
				foreach (IControl control in CustomControls.Where(c => c.Item3).Select(c => c.Item2)) {
					control.Draw(spriteBatch);
				}
				
				if (background != null) {
					background.Draw(spriteBatch);
				}

				if (title != null) {
					title.Draw(spriteBatch);
				}

				if (next != null) {
					next.Draw(spriteBatch);
				}

				foreach (IControl control in CustomControls.Where(c => !c.Item3).Select(c => c.Item2)) {
					control.Draw(spriteBatch);
				}
			}
		}

		public override void Update(GameTime gameTime) {
			if (IsActiv && PerformUpdate) {
				ObjektManager.Update(gameTime);
				foreach (IControl control in CustomControls.Select(c => c.Item2)) {
					control.Update(gameTime);
				}

				if (background != null) {
					background.Update(gameTime);
				}

				if (title != null) {
					title.Update(gameTime);
				}

				if (next != null) {
					next.Update(gameTime);
				}

				if (useTimeout) {
					timer += (float)gameTime.ElapsedGameTime.TotalSeconds;
					if (timer > MaxTimer) {
						useTimeout = false;
						if (timeoutEvent != null) {
							timeoutEvent();
						}
					}
				}
			}
		}

		public override void Initialize() {

		}

		public override void LoadContent() {

		}

		/// <summary>
		/// Fügt einen Titel hinzu. Reine Text anzeige.
		/// </summary>
		/// <param name="text">Text der angezeigt werden soll</param>
		/// <param name="position">Wo soll der Text anscheinen</param>
		public void SetTitle(string text, Vector2 position, SpriteFont font, Color fontColor) {
			title = new LinkControl(text, position, font, fontColor, null);
		}

		/// <summary>
		/// Fügt ein Hintergrundbild hinzu. Dieses ist Bildschirmfüllend
		/// </summary>
		/// <param name="texture">Das jeweilige Bild</param>
		public void SetBackground(Texture2D texture) {
			background = new ImageControl(texture, Vector2.Zero);
		}

		/// <summary>
		/// Fügt ein Button hinzu.
		/// </summary>
		/// <param name="buttonText">Text der auf den Button erscheinen soll</param>
		/// <param name="position">Wo soll der Button erscheinen</param>
		/// <param name="buttonTexture">Textur des Buttons</param>
		/// <param name="onClickEvent">Funktion die beim Betätigen des Buttons aufgerufen werden soll</param>
		public void SetButton(string buttonText, Vector2 position, SpriteFont font, Color fontColor, Texture2D buttonTexture, Action onClickEvent, Color backGroundRenderColor) {
			ObjektManager.RemoveClickableObjekt("next");
			next = new ButtonControl(buttonText, position, font, buttonTexture, fontColor, onClickEvent, backGroundRenderColor);
			ObjektManager.AddClickableObjekt("next", next);
		}

		/// <summary>
		/// Es kann ein Event/Aktion hinterlegt werden, die nach einer bestimmten Zeit aufgeführt wird (nur einmal ausgeführt)
		/// </summary>
		/// <param name="time">Zeit in Sekunden</param>
		/// <param name="timeoutEvent">Funktion die ausgeführt werden soll</param>
		public void SetTimeoutEvent(float time, Action timeoutEvent) {
			timer = 0f;
			MaxTimer = time;
			useTimeout = true;
			this.timeoutEvent = timeoutEvent;
		}
	}
}

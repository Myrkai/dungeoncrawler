﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpDX.Toolkit.Graphics;
using SharpDX.Toolkit;

namespace GUI.Interface {
	public interface IScreen {
		void Draw(SpriteBatch spriteBatch);
		void Update(GameTime gameTime);

		void SetIsActiv(bool activ); //ist das Fenstergerade aktiv
		void SetDoDraw(bool draw); // soll das aktive Fenster seine DrawMethoden aufführen.
		void SetDoUpdate(bool update); //soll das aktive Fenster seine UpdateMethoden ausführen
	}
}

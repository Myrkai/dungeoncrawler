﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpDX;

namespace GUI.Interface {
	public interface IClickable {
		void PerformClick(Vector2 position);
		bool IsEnable();
		bool IsClickable();
		void SetIsEnable(bool enable);
		Rectangle GetDimension();
		void SetOnClickEvent(Action onClickEvent);
	}
}

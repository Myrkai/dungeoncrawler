﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpDX.Toolkit.Graphics;
using SharpDX.Toolkit;
using SharpDX;

namespace GUI.Interface {
	public interface IControl : IClickable {
		void Draw(SpriteBatch spriteBatch);
		void Update(GameTime gameTime);
		object GetObjectFromElement();
	}
}

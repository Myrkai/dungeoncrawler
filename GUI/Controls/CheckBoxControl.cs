﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GUI.Interface;
using SharpDX;
using SharpDX.Toolkit.Graphics;
using SharpDX.Toolkit;

namespace GUI.Controls {

	public class CheckBoxControl : IControl {
		private bool enable;
		private Vector2 location;
		private object tempObject;

		private SpriteFont font;
		private Color fontColor;
		private LinkControl checkBox;
		private bool value;

		private Action OnClickEvent;
		public event EventHandler ValueChanged;

		public CheckBoxControl(SpriteFont font, Color fontColor, Vector2 position) {
			enable = true;
			value = false;

			this.font = font;
			this.fontColor = fontColor;

			checkBox = new LinkControl(value ? "An" : "Aus", position, font, fontColor, null);
			location = position;
			tempObject = null;
		}

		public void SetValue(bool value) {
			this.value = value;
			checkBox.Text = value ? "An" : "Aus";

			if (ValueChanged != null) {
				ValueChanged(this, null);
			}
		}

		public bool GetValue() {
			return this.value;
		}

		public object GetObjectFromElement() {
			return tempObject;
		}

		public void Draw(SpriteBatch spriteBatch) {
			if (IsEnable()) {
				checkBox.Draw(spriteBatch);
			}
		}

		public void Update(GameTime gameTime) {
			if (IsEnable()) {
				checkBox.Update(gameTime);
			}
		}

		public Rectangle GetDimension() {
			return checkBox.GetDimension();
		}

		public void PerformClick(Vector2 position) {
			if (OnClickEvent != null) {
				OnClickEvent();
			}

			SetValue(!GetValue());
		}

		public bool IsEnable() {
			return enable;
		}

		public bool IsClickable() {
			return true;
		}

		public void SetIsEnable(bool enable) {
			this.enable = enable;
		}

		public void SetOnClickEvent(Action onClickEvent) {
			OnClickEvent = onClickEvent;
		}

		public void SetPosition(Vector2 position) {
			checkBox.Position = position;
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GUI.Interface;
using SharpDX.Toolkit.Graphics;
using SharpDX;
using SharpDX.Toolkit;

namespace GUI.Controls {
	public class SelectBoxControl : IControl {
		#region Variablen
		private ButtonControl previewElement;
		private ButtonControl nextElement;
		private LinkControl currentElement;

		private List<string> elements;
		public string Text {
			set {
				index = GetIndex(elements, value);
				currentElement.Text = elements[index];

				if (ValueChanged != null) {
					ValueChanged(this, null);
				}
			}
			get {
				return elements[index];
			}
		}
		private SpriteFont font;
		private Color fontColor;
		private Vector2 location;

		private int widthBetweenElements;
		private int index;
		private bool enable;
		private Action OnClickEvent;
		public object Object;

		public event EventHandler ValueChanged;
		#endregion

		#region Kontruktor
		//nächstes und vorheriges Element ist optionale Elemente, fehlen diese, wird mit dem Druck auf den text, immer zum nächsten Element durchgeschaltet
		public SelectBoxControl(List<string> elements, SpriteFont font, Color fontColor, int widthBetweenElements, ButtonControl previewButton, ButtonControl nextButton) {
			this.enable = true;

			location = Vector2.Zero;
			this.elements = elements;
			this.font = font;
			this.fontColor = fontColor;

			this.widthBetweenElements = widthBetweenElements;
			this.index = 0;

			this.previewElement = previewButton;
			this.currentElement = new LinkControl(elements[0], new Vector2(previewElement.GetDimension().TopRight.X + widthBetweenElements, 0), font, fontColor, null);
			this.nextElement = nextButton;

			previewButton.SetOnClickEvent(() => { index = index - 1 < 0 ? elements.Count - 1 : index - 1; Text = Text; });
			nextButton.SetOnClickEvent(() => { index = index + 1 > elements.Count - 1 ? 0 : index + 1; Text = Text; });
			previewButton.SetPosition(Vector2.Zero);
			nextButton.SetPosition(new Vector2(currentElement.GetDimension().TopRight.X + widthBetweenElements, 0));
		}

		public SelectBoxControl(List<string> elements, SpriteFont font, Color fontColor, int widthBetweenElements) {
			this.enable = true;

			location = Vector2.Zero;
			this.elements = elements;
			this.font = font;
			this.fontColor = fontColor;

			this.widthBetweenElements = widthBetweenElements;
			this.index = 0;

			this.currentElement = new LinkControl(elements[0], Vector2.Zero, font, fontColor, () => { index = index + 1 > elements.Count - 1 ? 0 : index + 1; Text = Text; });
		}
		#endregion

		#region sonstigeFunktionen
		private int GetIndex(List<string> list, string o) {
			for (int i = 0; i < list.Count; i++) {
				if (list[i].Equals(o)) {
					return i;
				}
			}

			return 0;
		}
		#endregion

		public void Draw(SpriteBatch spriteBatch) {
			if (IsEnable()) {
				if (previewElement != null) {
					previewElement.Draw(spriteBatch);
				}
				currentElement.Draw(spriteBatch);
				if (nextElement != null) {
					nextElement.Draw(spriteBatch);
				}
			}
		}

		public void Update(GameTime gameTime) {
			if (IsEnable()) {
				if (previewElement != null) {
					previewElement.Update(gameTime);
				}
				currentElement.Update(gameTime);
				if (nextElement != null) {
					nextElement.Update(gameTime);
				}
			}
		}

		public Rectangle GetDimension() {
			if (previewElement == null && nextElement == null) {
				return currentElement.GetDimension();
			} else {
				return new Rectangle(previewElement.GetDimension().X, previewElement.GetDimension().Y, previewElement.GetDimension().Width + currentElement.GetDimension().Width + nextElement.GetDimension().Width + widthBetweenElements + widthBetweenElements, previewElement.GetDimension().Height);
			}
		}

		public object GetObjectFromElement() {
			return Object;
		}

		public void PerformClick(Vector2 position) {
			if (IsClickable()) {
				if (previewElement != null && previewElement.GetDimension().Intersects(new Rectangle((int)position.X, (int)position.Y, 1, 1))) {
					previewElement.PerformClick(position);
				}

				if (nextElement != null && nextElement.GetDimension().Intersects(new Rectangle((int)position.X, (int)position.Y, 1, 1))) {
					nextElement.PerformClick(position);
				}

				if (currentElement.GetDimension().Intersects(new Rectangle((int)position.X, (int)position.Y, 1, 1))) {
					currentElement.PerformClick(position);
					if (OnClickEvent != null) {
						OnClickEvent();
					}
				}
			}
		}

		public bool IsEnable() {
			return enable;
		}

		public bool IsClickable() {
			return true;
		}

		public void SetIsEnable(bool enable) {
			this.enable = enable;
		}

		public void SetOnClickEvent(Action onClickEvent) {
			OnClickEvent = onClickEvent;
		}

		public void SetPosition(Vector2 position) {
			if (previewElement != null && nextElement != null) {
				previewElement.SetPosition(position);
				currentElement.Position = new Vector2(previewElement.GetDimension().TopRight.X + widthBetweenElements, 0);
				nextElement.SetPosition(new Vector2(currentElement.GetDimension().TopRight.X + widthBetweenElements, 0));
			} else {
				currentElement.Position = position;
			}
		}

		public void SetNextElement() {
			index = index + 1 > elements.Count - 1 ? 0 : index + 1;
			Text = Text;
		}

		public void SetPreviewElement() {
			index = index - 1 < 0 ? elements.Count - 1 : index - 1;
			Text = Text;
		}
	}
}

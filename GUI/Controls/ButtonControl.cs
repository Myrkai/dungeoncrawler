﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpDX;
using GUI.Interface;
using SharpDX.Toolkit.Graphics;
using SharpDX.Toolkit;

namespace GUI.Controls {
	public class ButtonControl : IControl {
		public LinkControl Text { get; private set; }
		private ImageControl ButtonTexture { get; set; }
		public Vector2 Position { get; private set; }

		public event EventHandler ButtonClicked;
		public Vector2 OffSetPositionText { get; private set; }
		public object ObjectForButton { get; set; }
		public Action OnClickEvent { get; private set; }
		bool Enable;
		bool Clickable;

		public SpriteFont Font { get; private set; }
		public Color FontColor { get; private set; }
		public string Value { get; private set; }
		public Texture2D Texture { get; private set; }

		public ButtonControl(string text, Vector2 position, SpriteFont font, Texture2D buttonTexture, Color fontColor, Action onClickEvent) {
			ButtonTexture = new ImageControl(buttonTexture, position);

			int textWidth = (int)font.MeasureString(text).X;
			int textHeight = (int)font.MeasureString(text).Y;
			OffSetPositionText = new Vector2(ButtonTexture.GetDimension().Width / 2 - textWidth / 2, ButtonTexture.GetDimension().Height / 2 - textHeight / 2);

			Text = new LinkControl(text, position + OffSetPositionText, font, fontColor, null);

			OnClickEvent = onClickEvent;

			Enable = true;
			Clickable = true;

			Font = font;
			FontColor = fontColor;
			Value = text;
			Texture = buttonTexture;

			Position = position;
		}

		public ButtonControl(string text, Vector2 position, SpriteFont font, Texture2D buttonTexture, Color fontColor, Action onClickEvent, Color backGroundRenderColor) {
			ButtonTexture = new ImageControl(buttonTexture, position, backGroundRenderColor);

			int textWidth = (int)font.MeasureString(text).X;
			int textHeight = (int)font.MeasureString(text).Y;
			OffSetPositionText = new Vector2(ButtonTexture.GetDimension().Width / 2 - textWidth / 2, ButtonTexture.GetDimension().Height / 2 - textHeight / 2);

			Text = new LinkControl(text, position + OffSetPositionText, font, fontColor, null);

			OnClickEvent = onClickEvent;

			Enable = true;
			Clickable = true;

			Font = font;
			FontColor = fontColor;
			Value = text;
			Texture = buttonTexture;

			Position = position;
		}

		public void Draw(SpriteBatch spriteBatch) {
			if (Enable) {
				ButtonTexture.Draw(spriteBatch);
				Text.Draw(spriteBatch);
			}
		}

		public void Update(GameTime gameTime) {
			if (Enable) {
				ButtonTexture.Update(gameTime);
				Text.Update(gameTime);
			}
		}

		public Rectangle GetDimension() {
			return ButtonTexture.GetDimension();
		}

		public object GetObjectFromElement() {
			return ObjectForButton;
		}

		public void PerformClick(Vector2 position) {
			if (Enable && Clickable && OnClickEvent != null) {
				OnClickEvent();
				if (ButtonClicked != null) {
					ButtonClicked(this, null);
				}
			}
		}

		public bool IsEnable() {
			return Enable;
		}

		public bool IsClickable() {
			return Clickable;
		}

		public void SetIsEnable(bool enable) {
			this.Enable = enable;
			this.Clickable = enable;
		}

		public void SetOnClickEvent(Action onClickEvent) {
			OnClickEvent = onClickEvent;
		}

		public void SetPosition(Vector2 position) {
			this.Position = position;
			ButtonTexture.Position = position;

			int textWidth = (int)Font.MeasureString(Value).X;
			int textHeight = (int)Font.MeasureString(Value).Y;
			OffSetPositionText = new Vector2(ButtonTexture.GetDimension().Width / 2 - textWidth / 2, ButtonTexture.GetDimension().Height / 2 - textHeight / 2);

			Text = new LinkControl(Value, position + OffSetPositionText, Font, FontColor, null);
		}

		public void SetText(string value) {
			Text.Text = value;

			int textWidth = (int)Font.MeasureString(value).X;
			int textHeight = (int)Font.MeasureString(value).Y;
			OffSetPositionText = new Vector2(ButtonTexture.GetDimension().Width / 2 - textWidth / 2, ButtonTexture.GetDimension().Height / 2 - textHeight / 2);

			Text.Position = Position + OffSetPositionText;
		}

		public void SetOffSetPositionText(Vector2 pos) {
			OffSetPositionText = pos;
			Text.Position = Position + OffSetPositionText;
		}

		public void SetBackgroundRenderColor(Color color) {
			ButtonTexture.RenderColor = color;
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpDX.Toolkit.Graphics;
using SharpDX;
using GUI.Interface;
using SharpDX.Toolkit;

namespace GUI.Controls {
	public class ImageControl : IControl {
		public Texture2D Texture { get; set; }
		public Vector2 Position { get; set; }
		private float Opacity { get; set; }
		public Color RenderColor { get; set; }

		private Action OnClickEvent { get; set; }
		bool Enable;
		bool Clickable;
		object ControlObject;

		public float Scale { get; set; }

		public ImageControl(Texture2D texture, Vector2 position) {
			this.Texture = texture;
			this.Position = position;
			Enable = true;
			Clickable = false;
			Opacity = 1f;
			RenderColor = Color.White;

			Scale = 1f;

			ControlObject = null;
		}

		public ImageControl(Texture2D texture, Vector2 position, float opacity) {
			this.Texture = texture;
			this.Position = position;
			Enable = true;
			Clickable = false;
			Opacity = opacity;
			RenderColor = Color.White;

			Scale = 1f;

			ControlObject = null;
		}

		public ImageControl(Texture2D texture, Vector2 position, float opacity, Color color) {
			this.Texture = texture;
			this.Position = position;
			Enable = true;
			Clickable = false;
			Opacity = opacity;
			RenderColor = color;

			Scale = 1f;

			ControlObject = null;
		}

		public ImageControl(Texture2D texture, Vector2 position, Color color) {
			this.Texture = texture;
			this.Position = position;
			Enable = true;
			Clickable = false;
			Opacity = 1f;
			RenderColor = color;

			Scale = 1f;

			ControlObject = null;
		}

		public void Draw(SpriteBatch spriteBatch) {
			if (Enable) {
				//spriteBatch.Draw(Texture, Position, RenderColor * Opacity);
				spriteBatch.Draw(Texture, new Rectangle((int)Position.X, (int)Position.Y, (int)(Texture.Width * Scale), (int)(Texture.Height * Scale)), RenderColor * Opacity);
			}
		}

		public void Update(GameTime gameTime) {

		}

		public Rectangle GetDimension() {
			return new Rectangle((int)Position.X, (int)Position.Y, (int)(Texture.Width * Scale), (int)(Texture.Height * Scale));
		}

		public object GetObjectFromElement() {
			return ControlObject;
		}

		public void PerformClick(Vector2 position) {
			if (OnClickEvent != null && Clickable) {
				OnClickEvent();
			}
		}

		public bool IsEnable() {
			return Enable;
		}

		public bool IsClickable() {
			return Clickable;
		}

		public void SetIsEnable(bool enable) {
			this.Enable = enable;
			this.Clickable = enable;
		}

		public void SetOnClickEvent(Action onClickEvent) {
			this.OnClickEvent = OnClickEvent;
		}
	}
}

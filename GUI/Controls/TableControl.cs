﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GUI.Interface;
using SharpDX.Toolkit.Graphics;
using SharpDX.Toolkit;
using SharpDX;

namespace GUI.Controls {
	public class TableData {
		public int Index;
		public TableEntityControl Control;
	}

	public class TableControl : IControl {

		public Vector2 Position { get; set; }
		public Texture2D Background { get; set; }
		private Dictionary<string, TableData> Entitys { get; set; }
		public event EventHandler<TableControlEventArgs> EntitySelected;

		public TableControl(Vector2 position, Texture2D background) {
			Position = position;
			Background = background;
			Entitys = new Dictionary<string, TableData>();
			Index = 0;
		}

		public TableEntityControl GetEntity(string key) {
			if (Entitys.ContainsKey(key)) {
				return Entitys[key].Control;
			} else {
				return null;
			}
		}

		public int Index { get; set; }
		int currentIndex = 0;
		public void AddEntity(string key, TableEntityControl control) {
			if (Entitys.ContainsKey(key)) {
				control.Position = Position + new Vector2(10f, 5f + 20f * Entitys.Where(s => s.Value.Index == currentIndex).Count());
				Entitys[key] = new TableData() { Index = currentIndex, Control = control };
			} else {
				control.Position = Position + new Vector2(10f, 5f + 20f * Entitys.Where(s => s.Value.Index == currentIndex).Count());
				Entitys.Add(key, new TableData() { Index = currentIndex, Control = control });
			}

			if (control.GetDimension().BottomLeft.Y + 25f > Position.Y + Background.Height) {
				currentIndex += 1;
			}
		}

		public void ClearEntity() {
			Entitys.Clear();
			Index = 0;
			currentIndex = 0;
		}

		public void Draw(SpriteBatch spriteBatch) {
			spriteBatch.Draw(Background, Position, Color.White * 0.80f);

			foreach (TableEntityControl control in Entitys.Values.Where(s => s.Index == Index).Select(s => s.Control)) {
				control.Draw(spriteBatch);
			}
		}

		public void Update(GameTime gameTime) {
			if (VirtualGamePad.ClickReleased()) {
				//TableData tmp = Entitys.Where(e => Index == e.Value.Index && e.Value.Control.GetDimension().Intersects(new Rectangle((int)VirtualGamePad.GetClickPoint().X, (int)VirtualGamePad.GetClickPoint().Y, 1, 1))).Select(e => e.Value).FirstOrDefault();
				TableData tmp = Entitys.Where(e => Index == e.Value.Index && e.Value.Control.InteractWithText(VirtualGamePad.GetClickPoint())).Select(e => e.Value).FirstOrDefault();
				if (tmp != null && EntitySelected != null) {
					EntitySelected(this, new TableControlEventArgs(tmp));
				}
			}
		}

		public Rectangle GetDimension() {
			throw new NotImplementedException();
		}

		public object GetObjectFromElement() {
			throw new NotImplementedException();
		}

		public void PerformClick(Vector2 position) {
			throw new NotImplementedException();
		}

		public bool IsEnable() {
			return false;
		}

		public bool IsClickable() {
			return false;
		}

		public void SetIsEnable(bool enable) {
			throw new NotImplementedException();
		}

		public void SetOnClickEvent(Action onClickEvent) {
			throw new NotImplementedException();
		}

		public int MaxIndex { get { return Entitys.Select(e => e.Value.Index).Max(); } }
	}

	public class TableControlEventArgs : EventArgs {
		public TableData Data { get; private set; }

		public TableControlEventArgs(TableData data) {
			Data = data;
		}
	}
}

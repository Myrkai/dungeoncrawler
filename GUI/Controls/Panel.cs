﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GUI.Interface;
using SharpDX;
using SharpDX.Toolkit;
using SharpDX.Toolkit.Graphics;

namespace GUI.Controls {
	public class Panel : IControl {
		private Dictionary<string, IControl> Objekts { get; set; }
		private int Width { get; set; }
		private int Height { get; set; }
		public Vector2 Position { get; set; }

		public Panel(int width, int height, Vector2 position) {
			Width = width;
			Height = height;
			Position = position;
			throw new NotImplementedException();
		}

		public void Draw(SpriteBatch spriteBatch) {
			if (IsEnable()) {
				foreach (IControl control in Objekts.Values) {
					control.Draw(spriteBatch);
				}
			}
		}

		public void Update(GameTime gameTime) {
			if (IsEnable()) {
				foreach (IControl control in Objekts.Values) {
					control.Update(gameTime);
				}
			}
		}

		public void PerformClick(Vector2 position) {

		}

		public void AddControl(string key, IControl control) {
			if (control.GetDimension().TopRight.X > Width || control.GetDimension().BottomRight.Y > Height) {
				throw new ArgumentOutOfRangeException();
			}
			Objekts.Add(key, control);
		}

		public void RemoveControl(string key) {
			Objekts.Remove(key);
		}

		public IControl GetControl(string key) {
			return Objekts[key];
		}

		public Rectangle GetDimension() { return new Rectangle((int)Position.X, (int)Position.Y, Width, Height); }
		public bool IsEnable() { return true; }
		public object GetObjectFromElement() { return null; }
		public bool IsClickable() { return true; }
		public void SetIsEnable(bool enable) { }
		public void SetOnClickEvent(Action onClickEvent) { }
	}
}

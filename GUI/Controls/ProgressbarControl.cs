﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GUI.Interface;
using SharpDX;
using SharpDX.Toolkit;
using SharpDX.Toolkit.Graphics;

namespace GUI.Controls {
	public class ProgressbarControl : IControl {
		private int Width { get; set; }
		private int Height { get; set; }
		public Vector2 Position { get; private set; }
		private Texture2D FullProgressbar { get; set; }
		private Texture2D EmptyProgressbar { get; set; }
		public float ProgressFilled { get; set; }
		private float BorderEmpty { get; set; }

		public ProgressbarControl(int width, int height, Vector2 postion, Texture2D fullProgressbar, Texture2D emptyProgressbar, float progessFilled, float borderEmpty) {
			this.Width = width;
			this.Height = height;
			this.Position = postion;
			this.FullProgressbar = fullProgressbar;
			this.EmptyProgressbar = emptyProgressbar;
			this.ProgressFilled = progessFilled;
			this.BorderEmpty = borderEmpty;
		}

		public void Draw(SpriteBatch spriteBatch) {
			//spriteBatch.Draw(FullProgressbar, Position, Color.White);
			spriteBatch.Draw(FullProgressbar, new Rectangle(Convert.ToInt32(Position.X), Convert.ToInt32(Position.Y), Convert.ToInt32(FullProgressbar.Width * ((decimal)Width / (decimal)FullProgressbar.Width)), FullProgressbar.Height), Color.White);
			spriteBatch.Draw(EmptyProgressbar, new RectangleF(Position.X + (float)Width * ProgressFilled + BorderEmpty, Position.Y + BorderEmpty, Width * (1f - ProgressFilled) - BorderEmpty * 2f, Height - BorderEmpty * 2f), new Rectangle(0, 0, Width, Height), Color.White);
		}

		public void Update(GameTime gameTime) {

		}

		public Rectangle GetDimension() {
			return new Rectangle((int)Position.X, (int)Position.Y, Width, Height);
		}

		public object GetObjectFromElement() { return null; }
		public void PerformClick(Vector2 position) { }
		public bool IsEnable() { return true; }
		public bool IsClickable() { return false; }
		public void SetIsEnable(bool enable) { }
		public void SetOnClickEvent(Action onClickEvent) { }
	}
}

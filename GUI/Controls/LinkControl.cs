﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GUI.Interface;
using SharpDX.Toolkit.Graphics;
using SharpDX.Toolkit;
using SharpDX;

namespace GUI.Controls {
	public class LinkControl : IControl {

		public string Text { get; set; }
		public Vector2 Position { get; set; }
		public SpriteFont Font { get; private set; }
		public Color FontColor { get; set; }
		private Action OnClickEvent { get; set; }
		bool Enable;
		bool Clickable;
		object ControlObject;

		public LinkControl(string text, Vector2 position, SpriteFont font, Color fontColor, Action onClickEvent) {
			this.Text = text;
			this.Position = position;
			this.Font = font;
			this.FontColor = fontColor;
			this.OnClickEvent = onClickEvent;
			Enable = true;
			Clickable = true;

			ControlObject = null;
		}

		public void Draw(SpriteBatch spriteBatch) {
			if (Enable) {
				spriteBatch.DrawString(Font, Text, Position, FontColor);
			}
		}

		public void Update(GameTime gameTime) {

		}

		public Rectangle GetDimension() {
			return new Rectangle((int)Position.X, (int)Position.Y, (int)Font.MeasureString(Text).X, (int)Font.MeasureString(Text).Y);
		}

		public object GetObjectFromElement() {
			return ControlObject;
		}

		public void PerformClick(Vector2 position) {
			if (OnClickEvent != null && Clickable) {
				OnClickEvent();
			}
		}

		public bool IsEnable() {
			return Enable;
		}

		public bool IsClickable() {
			return Clickable;
		}

		public void SetIsEnable(bool enable) {
			this.Enable = enable;
			this.Clickable = enable;
		}

		public void SetOnClickEvent(Action onClickEvent) {
			this.OnClickEvent = OnClickEvent;
		}
	}
}

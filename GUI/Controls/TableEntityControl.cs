﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GUI.Interface;
using SharpDX.Toolkit.Graphics;
using SharpDX.Toolkit;
using SharpDX;

namespace GUI.Controls {
	public class TableEntityControl : IControl {

		public Vector2 Position { get; set; }
		private Vector2 ValueOffset { get; set; }
		public string Title { get; private set; }
		public string Value { get; private set; }

		private SpriteFont Font { get; set; }
		private Color FontColor { get; set; }

		public TableEntityControl(string title, string value, SpriteFont font, Color fontColor, float maxWidth) {
			Position = Vector2.Zero;
			Title = title;
			Value = value;

			Font = font;
			FontColor = fontColor;

			ValueOffset = new Vector2(maxWidth - Font.MeasureString(Value).X, 0);
		}

		public void Draw(SpriteBatch spriteBatch) {
			spriteBatch.DrawString(Font, Title, Position, FontColor);
			spriteBatch.DrawString(Font, Value, Position + ValueOffset, FontColor);
		}

		public void Draw(SpriteBatch spriteBatch, Vector2 offset) {
			spriteBatch.DrawString(Font, Title, Position + offset, FontColor);
			spriteBatch.DrawString(Font, Value, Position + ValueOffset + offset, FontColor);
		}

		public void Update(GameTime gameTime) {

		}

		public Rectangle GetDimension() {
			return new Rectangle((int)Position.X, (int)Position.Y, (int)(Font.MeasureString(Value).X + ValueOffset.X), (int)(Font.MeasureString(Value).Y + ValueOffset.Y));
		}

		public bool InteractWithText(Vector2 position) {
			Rectangle titleRec = new Rectangle((int)Position.X, (int)Position.Y, (int)Font.MeasureString(Title).X, (int)Font.MeasureString(Title).Y);
			Rectangle valueRec = new Rectangle((int)(Position + ValueOffset).X, (int)(Position + ValueOffset).Y, (int)Font.MeasureString(Value).X, (int)Font.MeasureString(Value).Y);
			Rectangle posRec = new Rectangle((int)position.X, (int)position.Y, 1, 1);

			return titleRec.Intersects(posRec) || valueRec.Intersects(posRec);
		}

		public object GetObjectFromElement() {
			throw new NotImplementedException();
		}

		public void PerformClick(Vector2 position) {
			throw new NotImplementedException();
		}

		public bool IsEnable() {
			return false;
		}

		public bool IsClickable() {
			return false;
		}

		public void SetIsEnable(bool enable) {
			throw new NotImplementedException();
		}

		public void SetOnClickEvent(Action onClickEvent) {
			throw new NotImplementedException();
		}
	}
}

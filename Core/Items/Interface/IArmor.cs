﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.Items.Interface {
	public interface IArmor : IItem {
		int Defence();
		void SetDefence(int defence);
		int MinDefence();
		int MaxDefence();
	}
}

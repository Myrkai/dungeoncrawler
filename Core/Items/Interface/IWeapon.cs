﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.Items.Interface {
	public interface IWeapon : IItem {
		int MinDamage();
		int MaxDamage();
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.Items.Enum;

namespace Core.Items.Interface {
	public interface ISkill {
		int MinItemLevel();
		void SetValue(float value);

		int GetAffectedType();
		int GetSkillType();
		List<ItemType> GetAvailableItemTyps();
		int GetMinQualityType();

		string GetPrefix();
		string GetSurfix();

		int GetLevel();

		ISkill Clone();
	}
}

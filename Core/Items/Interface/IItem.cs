﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.Items.Enum;

namespace Core.Items.Interface {
	public interface IItem {
		ItemType Type();

		Quality Quality();
		void SetQuality(Quality quality);

		int Itemlevel();
		string Title();
		void SetSpecialTitle(string title);

		List<ISkill> AvaiableSkills();
		void AddSkill(ISkill skill);

		IItem Clone();
	}
}

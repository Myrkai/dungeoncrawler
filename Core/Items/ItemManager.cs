﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.Items.Interface;
using System.Collections.ObjectModel;
using Core.Items.Enum;
using Core.Interface;

namespace Core.Items {
	public class ItemManager {
		public ReadOnlyCollection<IItem> BaseItems { get { return new ReadOnlyCollection<IItem>(_BaseItems); } }
		private List<IItem> _BaseItems { get; set; }

		public ReadOnlyCollection<Tuple<float, float, ISkill>> BaseSkills { get { return new ReadOnlyCollection<Tuple<float, float, ISkill>>(_BaseSkills); } }
		private List<Tuple<float, float, ISkill>> _BaseSkills { get; set; } //Min, Max, Skill
		private Random rnd;

		public ItemManager(Random rnd) {
			_BaseItems = new List<IItem>();
			_BaseSkills = new List<Tuple<float, float, ISkill>>();

			this.rnd = rnd;
		}

		public void AddBaseItem(IItem item) {
			_BaseItems.Add(item);
		}

		public void AddBaseItem(List<IItem> items) {
			_BaseItems.AddRange(items);
		}

		public void AddBaseSkill(float min, float max, ISkill skill) {
			_BaseSkills.Add(new Tuple<float, float, ISkill>(min, max, skill));
		}

		public IItem GenerateItem(int iLvl, ItemType type, Quality quality) {
			//Gegenstand erzeugen der zum level passt
			List<IItem> tmp = BaseItems.Where(i => i.Itemlevel() <= iLvl && i.Type() == type).ToList();
			IItem item = tmp[rnd.Next(0, tmp.Count)].Clone();

			//Zufälligen Rüstungswert setzen
			if (item.Type() != ItemType.Weapon) {
				((IArmor)item).SetDefence(rnd.Next(((IArmor)item).MinDefence(), ((IArmor)item).MaxDefence() + 1));
			}

			//zufällig Qualität erzeugen
			Quality itemQuality = quality;
			item.SetQuality(itemQuality);

			//passende Skills filtern (minimum Item Level und minimum Qualität berücksichtigen)
			List<Tuple<float, float, ISkill>> skills = BaseSkills.Where(s => s.Item3.MinItemLevel() <= iLvl && (int)item.Quality() >= s.Item3.GetMinQualityType()).ToList();
			//filter auf passenden ItemType (Schwert zu SchwertSkills)
			skills = skills.Where(s => s.Item3.GetAvailableItemTyps().Contains(item.Type())).ToList();

			int skillCount = GetSkillCount(item);
			for (int i = 0; i < skillCount; i++) {
				Tuple<float, float, ISkill> tmpSkill = skills[rnd.Next(0, skills.Count)];
				ISkill skill = tmpSkill.Item3.Clone();
				skill.SetValue(rnd.Next((int)tmpSkill.Item1, (int)tmpSkill.Item2));
				item.AddSkill(skill);

				skills = skills.Where(s => s.Item3 != skill && s.Item3.GetSkillType() != skill.GetSkillType()).ToList();
			}

			SetSpecialTitle(ref item);

			return item;
		}

		public IItem GenerateItem(int iLvl, float magicfind) {
			float magic = 0f;
			if (magicfind > 1f) {
				magic = magicfind / 100f;
			} else {
				magic = magicfind;
			}

			//Gegenstand erzeugen der zum level passt
			List<IItem> tmp = BaseItems.Where(i => i.Itemlevel() <= iLvl).ToList();
			IItem item = tmp[rnd.Next(0, tmp.Count)].Clone();

			//Zufälligen Rüstungswert setzen
			if (item.Type() != ItemType.Weapon) {
				((IArmor)item).SetDefence(rnd.Next(((IArmor)item).MinDefence(), ((IArmor)item).MaxDefence() + 1));
			}

			//zufällig Qualität erzeugen
			List<KeyValuePair<float, Quality>> qualityDrops = new List<KeyValuePair<float, Quality>>();
			qualityDrops.Add(new KeyValuePair<float, Quality>(0.10f, Quality.Normal));
			qualityDrops.Add(new KeyValuePair<float, Quality>(0.65f * (1f + magic), Quality.Magic));
			qualityDrops.Add(new KeyValuePair<float, Quality>(0.20f * (1f + magic * 2.0f), Quality.Rare));
			qualityDrops.Add(new KeyValuePair<float, Quality>(0.05f * (1f + magic * 4.0f), Quality.Legendary));

			Quality itemQuality = Drop<Quality>(qualityDrops, rnd);
			item.SetQuality(itemQuality);

			//passende Skills filtern (minimum Item Level und minimum Qualität berücksichtigen)
			List<Tuple<float, float, ISkill>> skills = BaseSkills.Where(s => s.Item3.MinItemLevel() <= iLvl && (int)item.Quality() >= s.Item3.GetMinQualityType()).ToList();
			//filter auf passenden ItemType (Schwert zu SchwertSkills)
			skills = skills.Where(s => s.Item3.GetAvailableItemTyps().Contains(item.Type())).ToList();

			int skillCount = GetSkillCount(item);
			for (int i = 0; i < skillCount; i++) {
				Tuple<float, float, ISkill> tmpSkill = skills[rnd.Next(0, skills.Count)];
				ISkill skill = tmpSkill.Item3.Clone();
				skill.SetValue(rnd.Next((int)tmpSkill.Item1, (int)tmpSkill.Item2));
				item.AddSkill(skill);

				skills = skills.Where(s => s.Item3 != skill && s.Item3.GetSkillType() != skill.GetSkillType()).ToList();
			}

			SetSpecialTitle(ref item);

			return item;
		}

		public IItem GenerateItem(int iLvl, IPlayer player, float magicFind) {
			float magic = 0f;
			if (magicFind > 1f) {
				magic = magicFind / 100f;
			} else {
				magic = magicFind;
			}
			
			//Absicherung das alle BaseItems identisch mit den ShopSkills heißen
			List<string> a = BaseItems.Select(i => i.Title().ToLower().Replace(" ", "")).ToList();
			List<string> b = player.GetShopSkills().Select(s => s.Key.ToLower()).ToList();
			List<string> c = b.Where(i => a.Contains(i)).ToList();
			if (a.Count != c.Count + 5) { //+5 da Level 1 Items Items kein Shopskill sind und davon gibt es nur 5 Stück
				throw new Exception();
			}

			//Gegenstand erzeugen der zum level passt und dessen Fähigkeit auch gekauft wurde, Achtung Level 1 Items sind kein ShopSkills und damit immer verfügbar
			List<IItem> tmp = BaseItems.Where(i => i.Itemlevel() <= iLvl && (!player.GetShopSkills().ContainsKey(i.Title().ToLower().Replace(" ", "")) || player.GetShopSkills()[i.Title().ToLower().Replace(" ", "")])).ToList();
			IItem item = tmp[rnd.Next(0, tmp.Count)].Clone();

			//Zufälligen Rüstungswert setzen
			if (item.Type() != ItemType.Weapon) {
				((IArmor)item).SetDefence(rnd.Next(((IArmor)item).MinDefence(), ((IArmor)item).MaxDefence() + 1));
			}

			//zufällige Qualität erzeugen, wenn qualität nicht gekauft wurde verringert sich dessen stufe um eins
			List<KeyValuePair<float, Quality>> qualityDrops = new List<KeyValuePair<float, Quality>>();
			qualityDrops.Add(new KeyValuePair<float, Quality>(0.10f, Quality.Normal));
			if (player.GetShopSkills()["magic" + item.Type().ToString().ToLower()]) {
				qualityDrops.Add(new KeyValuePair<float, Quality>(0.65f * (1f + magic), Quality.Magic));
			} else {
				qualityDrops.Add(new KeyValuePair<float, Quality>(0.65f, Quality.Normal));
			}

			if (player.GetShopSkills()["rare" + item.Type().ToString().ToLower()]) {
				qualityDrops.Add(new KeyValuePair<float, Quality>(0.20f * (1f + magic * 1.5f), Quality.Rare));
			} else if (player.GetShopSkills()["magic" + item.Type().ToString().ToLower()]) {
				qualityDrops.Add(new KeyValuePair<float, Quality>(0.20f * (1f + magic), Quality.Magic));
			} else {
				qualityDrops.Add(new KeyValuePair<float, Quality>(0.20f, Quality.Normal));
			}

			if (player.GetShopSkills()["legendary" + item.Type().ToString().ToLower()]) {
				qualityDrops.Add(new KeyValuePair<float, Quality>(0.05f * (1f + magic * 4.0f), Quality.Legendary));
			} else if (player.GetShopSkills()["rare" + item.Type().ToString().ToLower()]) {
				qualityDrops.Add(new KeyValuePair<float, Quality>(0.05f * (1f + magic * 2.0f), Quality.Rare));
			} else if (player.GetShopSkills()["magic" + item.Type().ToString().ToLower()]) {
				qualityDrops.Add(new KeyValuePair<float, Quality>(0.05f * (1f + magic), Quality.Magic));
			} else {
				qualityDrops.Add(new KeyValuePair<float, Quality>(0.05f, Quality.Normal));
			}

			Quality itemQuality = Drop<Quality>(qualityDrops, rnd);
			item.SetQuality(itemQuality);

			//passende Skills filtern (minimum Item Level und minimum Qualität berücksichtigen)
			List<Tuple<float, float, ISkill>> skills = BaseSkills.Where(s => s.Item3.MinItemLevel() <= iLvl && (int)item.Quality() >= s.Item3.GetMinQualityType()).ToList();
			//filter auf passenden ItemType (Schwert zu SchwertSkills)
			skills = skills.Where(s => s.Item3.GetAvailableItemTyps().Contains(item.Type())).ToList();

			int skillCount = GetSkillCount(item);
			for (int i = 0; i < skillCount; i++) {
				Tuple<float, float, ISkill> tmpSkill = skills[rnd.Next(0, skills.Count)];
				ISkill skill = tmpSkill.Item3.Clone();
				skill.SetValue(rnd.Next((int)tmpSkill.Item1, (int)tmpSkill.Item2));
				item.AddSkill(skill);

				skills = skills.Where(s => s.Item3 != skill && s.Item3.GetSkillType() != skill.GetSkillType()).ToList();
			}

			SetSpecialTitle(ref item);

			return item;
		}

		private void SetSpecialTitle(ref IItem item) {
			//Anpassung des Titels
			switch (item.Quality()) {
				case Quality.Normal:
					item.SetSpecialTitle(item.Title());
					break;
				case Quality.Magic:
					if (item.AvaiableSkills().Count == 1) {
						item.SetSpecialTitle(item.AvaiableSkills()[0].GetPrefix() + " " + item.Title());
					} else {
						item.SetSpecialTitle(item.AvaiableSkills()[0].GetPrefix() + " " + item.Title() + " " + item.AvaiableSkills()[1].GetSurfix());
					}
					break;
				case Quality.Rare:
				case Quality.Legendary:
					string prefix = item.AvaiableSkills()[rnd.Next(0, item.AvaiableSkills().Count)].GetPrefix();
					string surfix = item.AvaiableSkills()[rnd.Next(0, item.AvaiableSkills().Count)].GetSurfix();
					item.SetSpecialTitle(prefix + " " + item.Title() + " " + surfix);
					break;
			}
		}

		private int GetSkillCount(IItem item) {
			int skillCount;
			switch (item.Quality()) {
				case Quality.Normal: skillCount = 0; break;
				case Quality.Magic: skillCount = rnd.Next(1, 3); break; //1-2 Skills
				case Quality.Rare: skillCount = rnd.Next(3, 5); break; //3-4 Skills
				case Quality.Legendary: skillCount = rnd.Next(3, 7); break; //3-6 Skills
				default: skillCount = 0; break;
			}
			return skillCount;
		}

		private static T Drop<T>(List<KeyValuePair<float, T>> dropableItems, Random rnd) {
			float dice = (float)rnd.NextDouble();

			//korrigiere die Chancen auf einen Gesamtwert von 100%
			float sumChance = dropableItems.Sum(d => d.Key);
			List<KeyValuePair<float, T>> items;
			if (sumChance != 1f) {
				items = dropableItems.Select(e => new KeyValuePair<float, T>(e.Key / sumChance, e.Value)).ToList();
			} else {
				items = dropableItems;
			}

			//bestimmte das Zufällige Item anhand der Chancen. Die Chancen werden aufsummiert. 
			//z.B. A=10, B=30, C=60; Dice=90; Erg=A+B+C; 90 < 100 = C;
			//z.B. A=10, B=30, C=60; Dice=39; Erg=A+B; 39 < 40 = B
			float cumulative = 0.0f;
			foreach (KeyValuePair<float, T> item in items.OrderBy(di => di.Key)) {
				cumulative += item.Key;

				if (dice < cumulative) {
					return item.Value;
				}
			}

			return default(T);
		}
	}
}

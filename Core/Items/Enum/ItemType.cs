﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.Items.Enum {
	public enum ItemType {
		Weapon,
		Head,
		Body,
		Shoe,
		Schild
	}
}

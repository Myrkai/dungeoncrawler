﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.BattleSystem {
	public class NextBattleChance {
		public TimeSpan MaxTimeForNextBattle { get; private set; }
		public TimeSpan MinTimeForNextBattle { get; private set; }
		public TimeSpan TimeToNextBattle { get; private set; }
		public TimeSpan CurrentTimeNextBattle { get; private set; }

		public NextBattleChance(TimeSpan maxTimeForNextBattle, TimeSpan minTimeForNextBattle, TimeSpan timeToNextBattle, TimeSpan currentTimeNextBattle) {
			MaxTimeForNextBattle = maxTimeForNextBattle;
			MinTimeForNextBattle = minTimeForNextBattle;
			TimeToNextBattle = timeToNextBattle;
			CurrentTimeNextBattle = currentTimeNextBattle;
		}

		public NextBattleChance(TimeSpan maxTimeForNextBattle, TimeSpan minTimeForNextBattle, Random rnd) {
			MaxTimeForNextBattle = maxTimeForNextBattle;
			MinTimeForNextBattle = minTimeForNextBattle;
			TimeToNextBattle = new TimeSpan(0, 0, 0, 0, rnd.Next((int)MinTimeForNextBattle.TotalMilliseconds, (int)MaxTimeForNextBattle.TotalMilliseconds));
			CurrentTimeNextBattle = new TimeSpan(TimeToNextBattle.Days, TimeToNextBattle.Hours, TimeToNextBattle.Minutes, TimeToNextBattle.Seconds, TimeToNextBattle.Milliseconds);
		}

		public void ReduceTimeToNextBattle(TimeSpan time) {
			CurrentTimeNextBattle = CurrentTimeNextBattle.Subtract(time);
		}

		public static TimeSpan ConvertToTimespan(int seconds) {
			return new TimeSpan(0, 0, 0, seconds, 0);
		}
	}
}

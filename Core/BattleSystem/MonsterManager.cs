﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.BattleSystem.Interface;
using Core.Items.Interface;
using Core.Items;
using Core.Items.Enum;

namespace Core.BattleSystem {
	internal class MonsterStruct {
		public string Name { get; set; }
		public string GraphicAsset { get; set; }
	}

	public class MonsterManager {

		private ItemManager ItemManager { get; set; }
		private List<MonsterStruct> BaseMonsterSources { get; set; }
		private List<Tuple<float, float, ISkill>> BaseMonsterSkills { get; set; }
		private EXPTable EXPTable { get; set; }
		private IMonster Monster { get; set; }

		private static MonsterManager manager;
		private MonsterManager() { }
		public static MonsterManager Instance { get { if (manager == null) { manager = new MonsterManager(); } return manager; } }

		public void Init(EXPTable expTable, IMonster templateMonster, ItemManager itemManager) {
			EXPTable = expTable;
			BaseMonsterSources = new List<MonsterStruct>();
			BaseMonsterSkills = new List<Tuple<float, float, ISkill>>();
			Monster = templateMonster;
			ItemManager = itemManager;
		}

		public void AddMonster(string name, string graphicAsset) {
			BaseMonsterSources.Add(new MonsterStruct() { Name = name, GraphicAsset = graphicAsset });
		}

		public void AddSkill(Tuple<float, float, ISkill> skill) {
			BaseMonsterSkills.Add(skill);
		}

		public void AddSkill(List<Tuple<float, float, ISkill>> skills) {
			BaseMonsterSkills.AddRange(skills);
		}

		public IMonster GenerateRandomMonster(int level, Random rnd) {
			IMonster monster = Monster.Clone();
			MonsterStruct tmp = BaseMonsterSources[rnd.Next(0, BaseMonsterSources.Count)];

			monster.SetLevel(level);
			monster.LoadNewTexture(tmp.GraphicAsset);
			monster.SetName(tmp.Name);
			monster.SetExpTable(EXPTable);

			int skillCount = Convert.ToInt32(Math.Ceiling(level / 10d));
			for (int i = 0; i < skillCount; i++) {
				List<Tuple<float, float, ISkill>> a = BaseMonsterSkills.Where(s => !monster.GetUsedSkills().Select(ms => ms.GetSkillType()).Contains(s.Item3.GetSkillType()) && s.Item3.MinItemLevel() <= level).ToList();
				if (a == null || a.Count == 0) {//fallback falls alle verfügbaren skills ausgeschöpft sind
					a = BaseMonsterSkills.Where(s => s.Item3.MinItemLevel() <= level).ToList();
				}
				Tuple<float, float, ISkill> skill = a[rnd.Next(0, a.Count)];
				var monsterSkill = skill.Item3.Clone();
				monsterSkill.SetValue(rnd.Next((int)skill.Item1, (int)skill.Item2));
				monster.AddSkill(monsterSkill);
			}

			//give Monster a Weapon & Armor
			List<IItem> items = ItemManager.BaseItems.Where(i => i.Itemlevel() >= 40 && i.Itemlevel() <= level && i.Type() == ItemType.Weapon).ToList();
			IItem item;
			if (items == null || items.Count == 0) {
				item = ItemManager.BaseItems.Where(i => i.Itemlevel() >= Math.Min(level, 40) && i.Type() == ItemType.Weapon).OrderBy(i => i.Itemlevel()).First().Clone();
			} else {
				item = items[rnd.Next(0, items.Count)].Clone();
			}
			monster.EquipSword(item);

			IArmor armor = (IArmor)ItemManager.BaseItems.Where(i => i.Type() == ItemType.Body && i.Itemlevel() <= level).OrderBy(i => i.Itemlevel()).First().Clone();
			armor.SetDefence(rnd.Next(armor.MinDefence(), armor.MaxDefence() + 1));
			monster.EquipArmor(armor, ItemType.Body);

			armor = (IArmor)ItemManager.BaseItems.Where(i => i.Type() == ItemType.Schild && i.Itemlevel() <= level).OrderBy(i => i.Itemlevel()).First().Clone();
			armor.SetDefence(rnd.Next(armor.MinDefence(), armor.MaxDefence() + 1));
			monster.EquipArmor(armor, ItemType.Schild);

			armor = (IArmor)ItemManager.BaseItems.Where(i => i.Type() == ItemType.Head && i.Itemlevel() <= level).OrderBy(i => i.Itemlevel()).First().Clone();
			armor.SetDefence(rnd.Next(armor.MinDefence(), armor.MaxDefence() + 1));
			monster.EquipArmor(armor, ItemType.Head);

			armor = (IArmor)ItemManager.BaseItems.Where(i => i.Type() == ItemType.Shoe && i.Itemlevel() <= level).OrderBy(i => i.Itemlevel()).First().Clone();
			armor.SetDefence(rnd.Next(armor.MinDefence(), armor.MaxDefence() + 1));
			monster.EquipArmor(armor, ItemType.Shoe);

			monster.CalcAllStatValues();
			monster.SetMaxHP(monster.GetMaxHP() * Math.Min(1f, level / 12f));
			monster.SetCurrentHP(monster.GetMaxHP());
			return monster;
		}

		//Boss erhalten zusätzlich +3 Level und +2 Fähigkeiten
		public IMonster GenerateRandomBossMonster(int level, Random rnd) {
			IMonster monster = Monster.Clone();
			MonsterStruct tmp = BaseMonsterSources[rnd.Next(0, BaseMonsterSources.Count)];

			monster.SetLevel(level + 3);
			monster.LoadNewTexture(tmp.GraphicAsset);
			monster.SetName(tmp.Name);
			monster.SetExpTable(EXPTable);
			monster.SetIsBoss(true);

			int skillCount = Convert.ToInt32(Math.Ceiling(level / 10d)) + 2;
			for (int i = 0; i < skillCount; i++) {
				List<Tuple<float, float, ISkill>> a = BaseMonsterSkills.Where(s => !monster.GetUsedSkills().Select(ms => ms.GetSkillType()).Contains(s.Item3.GetSkillType()) && s.Item3.MinItemLevel() <= level).ToList();
				if (a == null || a.Count == 0) {//fallback falls alle verfügbaren skills ausgeschöpft sind
					a = BaseMonsterSkills.Where(s => s.Item3.MinItemLevel() <= level).ToList();
				}
				Tuple<float, float, ISkill> skill = a[rnd.Next(0, a.Count)];
				var Tskill = skill.Item3.Clone();
				Tskill.SetValue(rnd.Next((int)skill.Item1, (int)skill.Item2));
				monster.AddSkill(Tskill);
			}

			//give Monster a Weapon & Armor
			List<IItem> items = ItemManager.BaseItems.Where(i => i.Itemlevel() >= 40 && i.Itemlevel() <= level && i.Type() == ItemType.Weapon).ToList();
			IItem item;
			if (items == null || items.Count == 0) {
				item = ItemManager.BaseItems.Where(i => i.Itemlevel() >= Math.Min(level, 40) && i.Type() == ItemType.Weapon).OrderBy(i => i.Itemlevel()).First().Clone();
			} else {
				item = items[rnd.Next(0, items.Count)].Clone();
			}
			monster.EquipSword(item);

			IArmor armor = (IArmor)ItemManager.BaseItems.Where(i => i.Type() == ItemType.Body && i.Itemlevel() <= level).OrderBy(i => i.Itemlevel()).First().Clone();
			armor.SetDefence(rnd.Next(armor.MinDefence(), armor.MaxDefence() + 1));
			monster.EquipArmor(armor, ItemType.Body);

			armor = (IArmor)ItemManager.BaseItems.Where(i => i.Type() == ItemType.Schild && i.Itemlevel() <= level).OrderBy(i => i.Itemlevel()).First().Clone();
			armor.SetDefence(rnd.Next(armor.MinDefence(), armor.MaxDefence() + 1));
			monster.EquipArmor(armor, ItemType.Schild);

			armor = (IArmor)ItemManager.BaseItems.Where(i => i.Type() == ItemType.Head && i.Itemlevel() <= level).OrderBy(i => i.Itemlevel()).First().Clone();
			armor.SetDefence(rnd.Next(armor.MinDefence(), armor.MaxDefence() + 1));
			monster.EquipArmor(armor, ItemType.Head);

			armor = (IArmor)ItemManager.BaseItems.Where(i => i.Type() == ItemType.Shoe && i.Itemlevel() <= level).OrderBy(i => i.Itemlevel()).First().Clone();
			armor.SetDefence(rnd.Next(armor.MinDefence(), armor.MaxDefence() + 1));
			monster.EquipArmor(armor, ItemType.Shoe);

			monster.CalcAllStatValues();
			monster.SetMaxHP(monster.GetMaxHP() * Math.Min(1f, level / 12f));
			monster.SetCurrentHP(monster.GetMaxHP());
			return monster;
		}
	}
}

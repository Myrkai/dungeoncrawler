﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;

namespace Core.BattleSystem {
	public struct LevelEntity {
		/// <summary>
		/// Für welchen Level (z.B. 12)
		/// </summary>
		public int Level;
		/// <summary>
		/// Wieviel EXP werden fürs nächste Level benötigt (z.B. 50000)
		/// </summary>
		public int NextLevelEXP;
		/// <summary>
		/// EXPSteigerung zum nächsten Level (z.B. Steigerung 1050 => Level 13 mit 51050 NextLevelEXP)
		/// </summary>
		public int EXPStep;

		public LevelEntity(int level, int levelUpExp, int expStep) {
			Level = level;
			NextLevelEXP = levelUpExp;
			EXPStep = expStep;
		}
	}

	public class EXPTable {
		public ReadOnlyCollection<LevelEntity> LevelUpStats { get { return new ReadOnlyCollection<LevelEntity>(_LevelUpStats); } }
		private List<LevelEntity> _LevelUpStats { get; set; }

		public EXPTable() {
			_LevelUpStats = new List<LevelEntity>();
		}

		public void AddLevelUpStat(int level, int nextLevelUpEXP, int expStep) {
			_LevelUpStats.Add(new LevelEntity(level, nextLevelUpEXP, expStep));
		}

		public LevelEntity GetLevelUpStats(int level) {
			return _LevelUpStats.Single(l => l.Level == level);
		}
	}
}

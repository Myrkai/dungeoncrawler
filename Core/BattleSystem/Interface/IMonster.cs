﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpDX.Toolkit.Graphics;
using SharpDX;
using Core.Items.Interface;
using Core.Items.Enum;
using Core.Interface;

namespace Core.BattleSystem.Interface {
	public interface IMonster {
		IMonster Clone();
		
		bool GetIsBoss();
		int GetGold();
		int GetGold(int addonBaseGoldMultiplier);
		int GetEXP();
		int GetExp(int addonBaseExpMultiplier);
		float GetStrength();
		float GetDexterity();
		float GetStamina();
		float GetLuck();
		List<ISkill> GetUsedSkills();
		List<IItem> GetDropableItems();
		string GetName();
		int GetLevel();
		float GetCriticalHitChance();
		float GetCriticalDamage();
		float GetEvade();
		float GetDamage(Random rnd, out bool criticalHit);
		float GetCurrentHP();
		float GetMaxHP();

		void SetIsBoss(bool value);
		void LoadNewTexture(string textureAsset);
		void SetExpTable(EXPTable expTable);
		void SetName(string name);
		void SetLevel(int level);
		void SetPosition(Vector2 position);
		void SetCurrentHP(float HP);
		
		void AddSkill(ISkill skill);
		void AddBaseDropableItem(IItem item);

		void CalcAllStatValues();
		void EquipSword(IItem item);
		void EquipArmor(IItem item, ItemType type);

		bool TakeDamage(float damage, int playerLevel, Random rnd);
		void SetMaxHP(float hp);

		List<string> PerformAllBattleActions(IMonster owner, IPlayer target, Random rnd);
	}
}

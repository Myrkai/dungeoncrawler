﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.BattleSystem {
	public enum FightDirection { 
		PlayerToEnermy = 0,
		EnermyToPlayer = 1
	}

	public enum TargetType { 
		Owner = 0,
		Target = 1
	}

	public enum EffectType { 
		Heal = 0,
		Damage = 1,
		BattleRoundUp = 2,
		BattleTimeUp = 3
	}

	public class SkillEffect {
		public TargetType TargetType { get; private set; }
		public EffectType EffectType { get; private set; }
		public int Value { get; private set; }

		public SkillEffect(TargetType target, EffectType effect, int value) {
			TargetType = target;
			EffectType = effect;
			Value = value;
		}
	}

	public class BattleStep {
		public FightDirection Direction { get; private set; } //Wer greift wen an
		public float Damage { get; private set; } //wieviel Schaden wurde verursacht
		public float HealingPlayer { get; private set; } //heilungsmerge des Spielers
		public float HealingEnermy { get; private set; } //heilungsmenge des Monsters
		public bool Missing { get; private set; } //hat der schlag das monster verfehlt
		public bool Critical { get; private set; } //war es ein kritischer Treffer
		public List<SkillEffect> ItemEffects { get; private set; }

		public BattleStep(FightDirection direction, float damage, float healPlayer, float healEnermy, bool missingHit, bool criticalHit, List<SkillEffect> itemEffects) {
			Direction = direction;
			Damage = damage;
			HealingPlayer = healPlayer;
			HealingEnermy = healEnermy;
			Missing = missingHit;
			Critical = criticalHit;

			if (itemEffects == null) {
				ItemEffects = new List<SkillEffect>();
			} else {
				ItemEffects = itemEffects;
			}		
		}
	}
}

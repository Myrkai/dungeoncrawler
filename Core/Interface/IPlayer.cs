﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpDX;
using Core.BattleSystem;
using Core.Enums;
using SharpDX.Toolkit.Graphics;
using SharpDX.Toolkit;
using Core.Items.Interface;
using Core.BattleSystem.Interface;

namespace Core.Interface {
	public interface IPlayer {
		Vector2 GetPosition();
		int GetLevel();
		int GetLayer();
		int GetCurrentEXP();
		int GetNextLevelEXP();
		int GetBeforeLevelEXP();
		int GetBattleRounds();
		TimeSpan GetNextBattleTime();
		TimeSpan GetTimeToNextBattle();

		GameModes GetGameMode();
		List<GameModeAddons> GetGameModeAddons();
		int GetLayersToBeat();
		void SetLayersToBeat(int value);

		float GetCurrentHP();
		float GetDamage(Random rnd, out bool wasCriticalHit);

		void Draw(SpriteBatch spriteBatch);
		void Draw(SpriteBatch spriteBatch, Vector2 position);
		void Update(GameTime gameTime);

		Dictionary<string, bool> GetShopSkills();

		void SetLayer(int layer);
		void SetPosition(Vector2 position);
		float GetMovementSpeed();
		float GetMagicFind();

		RectangleF GetDimension();

		void AddEXP(int exp);
		void AddGold(uint gold);
		bool TakeDamage(float damage, int monsterLevel, Random rnd);
		void SetCurrentHealth(float value);
		List<string> PerformAllBattleActions(IPlayer owner, IMonster target, Random rnd);
		List<string> PerformAllAfterBattleActions(Random Randomizer);

		void Heal();
		void SetBattleRounds(int value);
		TimeSpan GetBattleTime();
	}
}

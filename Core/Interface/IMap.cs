﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpDX.Toolkit.Graphics;
using SharpDX.Toolkit;
using SharpDX;

namespace Core.Interface {
	public interface IMap {
		List<bool> GetCollisionLayer();
		bool IsWalkable(int indexX, int indexY);
		bool IsWalkable(RectangleF rec);
		int GetLayerWidth();
		int GetLayerHeight();
		void Draw(SpriteBatch spriteBatch);
		void Update(GameTime gameTime);
		ICamera GetCamera();

		int MapWidthPx();
		int MapHeightPx();
	}
}

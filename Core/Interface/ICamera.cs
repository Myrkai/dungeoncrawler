﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpDX;

namespace Core.Interface {
	public interface ICamera {
		Vector2 GetPosition();
		Rectangle GetDimension();
		void SetPosition(Vector2 position);
		int GetWidth();
		int GetHeight();
	}
}

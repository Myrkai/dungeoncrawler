﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.Map {
	public class MapManager {
		private List<Map> AvaiableMaps { get; set; }

		public MapManager() {
			AvaiableMaps = new List<Map>();
		}

		public void RegisterMap(Map map) {
			AvaiableMaps.Add(map);
		}

		public void RegisterMap(List<Map> maps) {
			AvaiableMaps.AddRange(maps);
		}

		public Map GetRandomMap(Random rnd) {
			return AvaiableMaps[rnd.Next(0, AvaiableMaps.Count)];
		}
	}
}

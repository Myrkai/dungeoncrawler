﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpDX;
using Core.BattleSystem.Interface;
using Core.BattleSystem;

namespace Core.Map {
	public class Monsterzone {
		public Rectangle Dimension { get; private set; }
		public int MinMonsterLevel { get; private set; }
		public int MaxMonsterLevel { get; private set; }
		public int CurrentLayer { get; set; }
		public Vector2 Size { get { return new Vector2(Dimension.Width, Dimension.Height); } }

		public Monsterzone(Rectangle dimension, int minlevel, int maxLevel) {
			Dimension = dimension;
			MinMonsterLevel = minlevel;
			MaxMonsterLevel = maxLevel;
			CurrentLayer = 0;
		}

		public int GetRandomMonsterLevel(Random rnd) {
			return rnd.Next((CurrentLayer - 1) * 5 + 1 + MinMonsterLevel, (CurrentLayer - 1) * 5 + 1 + MaxMonsterLevel + 1);
		}

		public string GetDescreiption() {
			int maxLength = Math.Max(((CurrentLayer - 1) * 5 + 1 + MinMonsterLevel).ToString().Length, ((CurrentLayer - 1) * 5 + 1 + MaxMonsterLevel).ToString().Length);

			string t = "";
			for (int i = 0; i < maxLength; i++) {
				t += "0";
			}

			return ((CurrentLayer - 1) * 5 + 1 + MinMonsterLevel).ToString(t) + " - " + ((CurrentLayer - 1) * 5 + 1 + MaxMonsterLevel).ToString(t);
		}

		public IMonster GetRandomMonster(Random rnd, bool isBoss = false) {
			if (isBoss) {
				return MonsterManager.Instance.GenerateRandomBossMonster(GetRandomMonsterLevel(rnd), rnd);
			} else {
				return MonsterManager.Instance.GenerateRandomMonster(GetRandomMonsterLevel(rnd), rnd);
			}
		}
	}
}

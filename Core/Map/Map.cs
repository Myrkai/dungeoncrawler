﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.Interface;
using SharpDX;

namespace Core.Map {
	public class Map {
		public IMap TiledMap { get; private set; }
		public List<Monsterzone> Zones { get; private set; }
		public ICamera Camera { get { return TiledMap.GetCamera(); } }
		public Monsterzone BossZone { get; private set; }

		public Map(IMap tiledMap) {
			TiledMap = tiledMap;
			Zones = new List<Monsterzone>();
		}

		public void AddMonsterzone(Monsterzone zone) {
			Zones.Add(zone);
		}

		public void AddMonsterzone(Rectangle dimension, int minLevel, int maxLevel) {
			Zones.Add(new Monsterzone(dimension, minLevel, maxLevel));
		}

		public List<Monsterzone> CurrentVisibleZones {
			get {
				return Zones.Where(z => z.Dimension.Intersects(Engine.Instance.CurrentMap.Camera.GetDimension())).ToList();
			}
		}

		public void SetBossZone(int x, int y, int width, int height) { 
			Rectangle rec = new Rectangle(x, y, width, height);
			Monsterzone zone = Zones.Where(z => z.Dimension.Intersects(rec)).FirstOrDefault();
			if (zone == null) {
				throw new Exception("Monsterzonen müssen vor der BossZone angelegt werden");
			}

			BossZone = new Monsterzone(rec, zone.MaxMonsterLevel, zone.MaxMonsterLevel);
		}

		public bool IsBossZoneVisible() {
			return BossZone.Dimension.Intersects(Engine.Instance.CurrentMap.Camera.GetDimension());
		}
	}
}

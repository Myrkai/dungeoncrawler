﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.Interface;
using Core.Map;
using SharpDX.Toolkit;
using SharpDX;
using Core.Items;
using Core.BattleSystem;
using Core.BattleSystem.Interface;
using Core.Items.Interface;

namespace Core {
	public class Engine {

		private static Engine engine;
		public static Engine Instance { get { if (engine == null) { engine = new Engine(); } return engine; } }

		public Map.Map CurrentMap { get; private set; }
		public IPlayer Player { get; private set; }
		private MapManager MapManager { get; set; }
		public ItemManager ItemManager { get; private set; }
		private MonsterManager MonsterManager { get; set; }
		public Random Randomizer { get; private set; }
		private float timeStepDuration { get; set; }

		private Engine() {

		}

		public void Init(IPlayer player, MapManager mapManager, ItemManager itemManager, MonsterManager monsterManager, Random rnd) {
			Player = player;
			MapManager = mapManager;
			ItemManager = itemManager;
			MonsterManager = monsterManager;
			Randomizer = rnd;
			timeStepDuration = 0f;

			CurrentMap = MapManager.GetRandomMap(Randomizer);
		}

		public void ResetEngine(IPlayer player) {
			Player = player;
			CreateNewMap(new Vector2(100f, 100f));
			ResetCamera();
		}

		public void ResetCamera() {
			CurrentMap.Camera.SetPosition(new Vector2(
					MathUtil.Clamp(Player.GetPosition().X - CurrentMap.Camera.GetWidth() / 2 + Player.GetDimension().Width / 2, 0, CurrentMap.TiledMap.MapWidthPx() - CurrentMap.Camera.GetWidth()),
					MathUtil.Clamp(Player.GetPosition().Y - CurrentMap.Camera.GetHeight() / 2 + Player.GetDimension().Height / 2, 0, CurrentMap.TiledMap.MapHeightPx() - CurrentMap.Camera.GetHeight())
				));
		}

		public void CreateNewMap(Vector2 playerStartPosition) {
			CurrentMap = MapManager.GetRandomMap(Randomizer);
			Player.SetPosition(playerStartPosition);
		}

		public void Update(GameTime gameTime) {
			timeStepDuration = (float)gameTime.ElapsedGameTime.TotalSeconds; // es wird immer der letzte "Herzschlag" des Programms gemerkt und danach den Spieler verschoben
			Player.Update(gameTime);
		}

		public void MovePlayer(Vector2 movedirection) {
			RectangleF playerRec = Player.GetDimension();

			if (!CurrentMap.TiledMap.IsWalkable(new RectangleF(playerRec.X + movedirection.X * Player.GetMovementSpeed() * timeStepDuration, playerRec.Y, playerRec.Width, playerRec.Height))) {
				movedirection = new Vector2(0f, movedirection.Y);
			}

			if (!CurrentMap.TiledMap.IsWalkable(new RectangleF(playerRec.X, playerRec.Y + movedirection.Y * Player.GetMovementSpeed() * timeStepDuration, playerRec.Width, playerRec.Height))) {
				movedirection = new Vector2(movedirection.X, 0f);
			}

			//Keep Player in Map
			Player.SetPosition(new Vector2(
					MathUtil.Clamp(Player.GetPosition().X + movedirection.X * Player.GetMovementSpeed() * timeStepDuration, 0, CurrentMap.TiledMap.MapWidthPx() - Player.GetDimension().Width),
					MathUtil.Clamp(Player.GetPosition().Y + movedirection.Y * Player.GetMovementSpeed() * timeStepDuration, 0, CurrentMap.TiledMap.MapHeightPx() - Player.GetDimension().Height)
				));

			//Bind Camera to Player
			ResetCamera();
		}

		public Vector2 GetPlayerMoveDirection(Vector2 position) {
			Vector2 tmp = Player.GetPosition() + new Vector2(Player.GetDimension().Width / 2, Player.GetDimension().Height / 2) - CurrentMap.Camera.GetPosition();

			tmp = position - tmp; // Berechnung der Positionsrichtung in abhängigkeit der Spielerpositon (d.h. z.B. ist die Position Links oder Rechts vom Spieler)
			tmp.Normalize();
			return tmp;
		}

		public IMonster StartBattle() {
			Player.Heal();

			if (Player.GetDimension().Intersects(CurrentMap.BossZone.Dimension)) {
				IMonster monster = CurrentMap.BossZone.GetRandomMonster(Randomizer, true);
				return monster;
			} else {
				IMonster monster = CurrentMap.CurrentVisibleZones.Where(z => z.Dimension.Intersects((Rectangle)Player.GetDimension())).First().GetRandomMonster(Randomizer);
				return monster;
			}
		}

		public List<BattleStep> CalculateBattle(IMonster monster) {
			float playerHealth = Player.GetCurrentHP();
			float monsterHealth = monster.GetCurrentHP();

			List<BattleStep> fullBattle = new List<BattleStep>();

			List<KeyValuePair<float, bool>> coinFlipp = new List<KeyValuePair<float, bool>>();
			coinFlipp.Add(new KeyValuePair<float, bool>(0.7f, true));
			coinFlipp.Add(new KeyValuePair<float, bool>(0.3f, false));
			bool playerStarts = Chance<bool>(coinFlipp, Randomizer);

			do {
				if (playerStarts) {
					bool criticalHit = false;
					float dmg = Player.GetDamage(Randomizer, out criticalHit);

					bool dmgTaken = monster.TakeDamage(dmg, Player.GetLevel(), Randomizer);

					List<string> result = Player.PerformAllBattleActions(Player, monster, Randomizer);
					if (result != null && result.Count > 0) {

						List<SkillEffect> effects = new List<SkillEffect>();
						foreach (string type in result) {
							string[] types = type.Split(';');

							TargetType target;
							switch (types[0]) {
								case "o": target = TargetType.Owner; break;
								case "t": target = TargetType.Target; break;
								default: throw new Exception();
							}


							EffectType effect;
							switch (types[1]) {
								case "heal": effect = EffectType.Heal; break;
								case "dmg": effect = EffectType.Damage; break;
								case "battleRound": effect = EffectType.BattleRoundUp; break;
								case "battleTime": effect = EffectType.BattleTimeUp; break;
								default: throw new Exception();
							}

							effects.Add(new SkillEffect(target, effect, Int32.Parse(types[2])));
						}

						fullBattle.Add(new BattleStep(FightDirection.PlayerToEnermy, dmg, 0f, 0f, !dmgTaken, criticalHit, effects));
					} else {
						fullBattle.Add(new BattleStep(FightDirection.PlayerToEnermy, dmg, 0f, 0f, !dmgTaken, criticalHit, null));
					}
				} else {
					bool criticalHit = false;
					float dmg = monster.GetDamage(Randomizer, out criticalHit);

					bool dmgTaken = Player.TakeDamage(dmg, monster.GetLevel(), Randomizer);

					List<string> result = monster.PerformAllBattleActions(monster, Player, Randomizer);
					if (result != null && result.Count > 0) {

						List<SkillEffect> effects = new List<SkillEffect>();
						foreach (string type in result) {
							string[] types = type.Split(';');

							TargetType target;
							switch (types[0]) {
								case "o": target = TargetType.Owner; break;
								case "t": target = TargetType.Target; break;
								default: throw new Exception();
							}


							EffectType effect;
							switch (types[1]) {
								case "heal": effect = EffectType.Heal; break;
								case "dmg": effect = EffectType.Damage; break;
								case "battleRound": effect = EffectType.BattleRoundUp; break;
								case "battleTime": effect = EffectType.BattleTimeUp; break;
								default: throw new Exception();
							}

							effects.Add(new SkillEffect(target, effect, Int32.Parse(types[2])));
						}

						fullBattle.Add(new BattleStep(FightDirection.EnermyToPlayer, dmg, 0f, 0f, !dmgTaken, criticalHit, effects));
					} else {
						fullBattle.Add(new BattleStep(FightDirection.EnermyToPlayer, dmg, 0f, 0f, !dmgTaken, criticalHit, null));
					}
				}
				playerStarts = !playerStarts;
			} while (Player.GetCurrentHP() >= 0f && monster.GetCurrentHP() >= 0f);

			if (Player.GetCurrentHP() <= 0f) {
				//erstmal nichts zu tun
			} else if (monster.GetCurrentHP() <= 0f) {
				//erstmal nichts zu tun
			} else {
				throw new Exception();
			}

			Player.SetCurrentHealth(playerHealth);
			monster.SetCurrentHP(monsterHealth);
			return fullBattle;
		}


		/// <summary>
		/// Exp und Gold hat er Spieler danach erhalten.
		/// Der Gegenstand wird gerade gedropt und fällt damit quasi auf den "boden".
		/// </summary>
		public IItem EndBattle(IMonster monster, out int expGained, out uint goldGained) {
			int exp = monster.GetEXP();
			uint gold = (uint)monster.GetGold();
			float expFactor = 1f;
			float goldFactor = 1f;

			float gameModeGoldAddon = 1f;
			float gameModeExpAddon = 1f;

			int monsterLevel = monster.GetLevel();
			int playerLevel = Player.GetLevel();

			if (Player.GetGameMode() == Enums.GameModes.RoundAttack) {
				Player.SetBattleRounds(Player.GetBattleRounds() - 1);
				gameModeGoldAddon += 0.5f;
				gameModeExpAddon += 0.25f;
			}

			if (Player.GetGameMode() == Enums.GameModes.TimeAttack) {
				gameModeGoldAddon += 0.5f;
				gameModeExpAddon += 0.25f;
			}

			if (Player.GetGameModeAddons().Contains(Enums.GameModeAddons.Hardcore)) {
				gameModeGoldAddon += 2.0f;
				gameModeExpAddon += 1.5f;
			}

			if (Player.GetGameModeAddons().Contains(Enums.GameModeAddons.Ironman)) {
				gameModeGoldAddon += 2.0f;
				gameModeExpAddon += 1.5f;
			}

			expFactor = GetExpPercentage(expFactor, monsterLevel, playerLevel);
			goldFactor = GetGoldPercentage(monster, goldFactor, monsterLevel, playerLevel);

			float addonExpFactor = 1f;
			float addonGoldFactor = 1f;
			List<string> result = Player.PerformAllAfterBattleActions(Randomizer);
			if (result != null && result.Count > 0) {

				List<SkillEffect> effects = new List<SkillEffect>();
				foreach (string type in result) {
					string[] types = type.Split(';');

					switch (types[0]) {
						case "o": break;
						default: throw new Exception();
					}

					switch (types[1]) {
						case "nodeath":
							if (Player.GetCurrentHP() <= 0 && types[2] == "1") {
								Player.SetCurrentHealth(Player.GetCurrentHP());
							}
							break;
						case "baseExp":
							exp = monster.GetExp(Convert.ToInt32(types[2]));
							break;
						case "percentageExp":
							addonExpFactor = 1f + Convert.ToSingle(types[2]) / 100f;
							break;
						case "baseGold":
							gold = (uint)monster.GetGold(Convert.ToInt32(types[2]));
							break;
						case "percentageGold":
							addonGoldFactor = 1f + Convert.ToSingle(types[2]) / 100f;
							break;
						default: throw new Exception();
					}
				}
			}

			Player.AddEXP(Convert.ToInt32(exp * expFactor * addonExpFactor * gameModeExpAddon));
			Player.AddGold(Convert.ToUInt32(gold * goldFactor * addonGoldFactor * gameModeGoldAddon));
			expGained = Convert.ToInt32(exp * expFactor * addonExpFactor * gameModeExpAddon);
			goldGained = Convert.ToUInt32(gold * goldFactor * addonGoldFactor * gameModeGoldAddon);

			List<KeyValuePair<float, bool>> itemDropable = new List<KeyValuePair<float, bool>>();
			if (monster.GetIsBoss()) {
				itemDropable.Add(new KeyValuePair<float, bool>(0.8f, true));
				itemDropable.Add(new KeyValuePair<float, bool>(0.2f, false));
			} else { // Item wird nur in 30% aller Fälle gedropt
				itemDropable.Add(new KeyValuePair<float, bool>(0.3f, true));
				itemDropable.Add(new KeyValuePair<float, bool>(0.7f, false));
			}

			if (Player.GetCurrentHP() <= 0) {
				Player.SetLayer(1);
				if (Player.GetGameModeAddons().Contains(Enums.GameModeAddons.Hardcore)) {
					throw new ApplicationException("Delete save game (Hardmode).");
				}

				throw new ApplicationException("Game has ended");
			}

			if (Player.GetGameMode() == Enums.GameModes.RoundAttack && Player.GetBattleRounds() <= 0) {
				Player.SetLayer(1);
				throw new ApplicationException("Game has ended (RoundAttack)");
			}

			if (Player.GetGameMode() == Enums.GameModes.TimeAttack && Player.GetBattleTime().TotalSeconds <= 0d) {
				Player.SetLayer(1);
				throw new ApplicationException("Game has ended (TimeAttack)");
			}

			bool performDrop = Chance<bool>(itemDropable, Randomizer);
			if (performDrop) {
				return ItemManager.GenerateItem(Math.Max(monsterLevel, playerLevel), Player, Player.GetMagicFind());
			} else {
				return null;
			}
		}

		private static float GetGoldPercentage(IMonster monster, float goldFactor, int monsterLevel, int playerLevel) {
			//berechnen wieviel Gold der Spieler erhält
			switch (monsterLevel - playerLevel) {
				case 4:
				case 3:
				case 2:
				case 1:
					goldFactor = 1.5f;
					break;
				case 0:
				case -1:
				case -2:
					goldFactor = 1f;
					break;
				case -3:
					goldFactor = 0.25f;
					break;
				case -4:
					goldFactor = 0.1f;
					break;
				default:
					goldFactor = 0.01f;
					break;
			}

			if (monster.GetIsBoss()) {
				goldFactor += 0.5f;
			}
			return goldFactor;
		}

		private float GetExpPercentage(float expFactor, int monsterLevel, int playerLevel) {
			//berechnen wieviel EXP der Spieler erhält
			if (Player.GetLevel() < 25) {
				int lvlDiff = Math.Abs(playerLevel - monsterLevel);

				switch (lvlDiff) {
					case 0:
					case 1:
					case 2:
					case 3:
					case 4:
					case 5:
						expFactor = 1f;
						break;
					case 6:
						expFactor = 0.8f;
						break;
					case 7:
						expFactor = 0.6f;
						break;
					case 8:
						expFactor = 0.4f;
						break;
					case 9:
						expFactor = 0.2f;
						break;
					case 10:
						expFactor = 0.05f;
						break;
					default:
						expFactor = 0.05f;
						break;

				}
			} else {
				switch (monsterLevel - playerLevel) {
					case 0:
					case -1:
					case -2:
					case -3:
					case -4:
					case -5:
						expFactor = 1f;
						break;
					case -6:
						expFactor = 0.8f;
						break;
					case -7:
						expFactor = 0.6f;
						break;
					case -8:
						expFactor = 0.4f;
						break;
					case -9:
						expFactor = 0.2f;
						break;
					case -10:
						expFactor = 0.05f;
						break;
					default:
						if (playerLevel == 99) {
							expFactor = 0f;
						} else {
							if (monsterLevel - playerLevel < -10) {
								expFactor = 0.05f;
							}

							if (monsterLevel - playerLevel > 0) {
								expFactor = playerLevel * 1f / monsterLevel * 1.5f;

								if (expFactor <= 0f) {
									throw new Exception();
								}
							}
						}
						break;
				}
			}
			return expFactor;
		}

		private static T Chance<T>(List<KeyValuePair<float, T>> list, Random rnd) {
			float dice = (float)rnd.NextDouble();

			//korrigiere die Chancen auf einen Gesamtwert von 100%
			float sumChance = list.Sum(d => d.Key);
			List<KeyValuePair<float, T>> items;
			if (sumChance != 1f) {
				items = list.Select(e => new KeyValuePair<float, T>(e.Key / sumChance, e.Value)).ToList();
			} else {
				items = list;
			}

			//bestimmte das Zufällige Item anhand der Chancen. Die Chancen werden aufsummiert. 
			//z.B. A=10, B=30, C=60; Dice=90; Erg=A+B+C; 90 < 100 = C;
			//z.B. A=10, B=30, C=60; Dice=39; Erg=A+B; 39 < 40 = B
			float cumulative = 0.0f;
			foreach (KeyValuePair<float, T> item in items.OrderBy(di => di.Key)) {
				cumulative += item.Key;

				if (dice < cumulative) {
					return item.Value;
				}
			}

			return default(T);
		}
	}
}

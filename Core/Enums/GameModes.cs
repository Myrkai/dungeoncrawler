﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.Enums {
	public enum GameModes {
		Normal,
		TimeAttack,
		RoundAttack
	}

	public enum GameModeAddons { 
		Ironman,
		Hardcore
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RandomTester {
	class Program {
		static void Main(string[] args) {
			List<Item> dropableItems = new List<Item>();

			float mf = 0.5f; //1f =100%

			dropableItems.Add(new Item(0.5f, "nothing"));
			dropableItems.Add(new Item(0.35f * (1f + mf), "normal"));
			dropableItems.Add(new Item(0.10f * (1f + mf), "magic"));
			dropableItems.Add(new Item(0.05f * (1f + mf), "rare"));

			List<string> dropedItems = new List<string>();
			int seed = 65;//Guid.NewGuid().GetHashCode();
			Random rnd = new Random(seed);

			int dropTrys = 0;
			//simuliere 1.000.000 gefallene Gegenstände
			for (int i = 0; i < 1000000; i++) {
				rnd = new Random(seed);
				
				dropTrys += 1;

				Item item = DropItem(dropableItems.Select(e => new KeyValuePair<float, Item>(e.Chance, e)).ToList(), rnd);
				dropedItems.Add(item.Name);
			}

			foreach (var group in dropedItems.GroupBy(d => d).Select(t => new { count = t.Count(), key = t.Key }).OrderByDescending(d => d.count)) {
				Console.WriteLine(group.key + "	" + group.count + "	" + (group.count * 100 / dropTrys) + "%");
			}

			for (int i = 0; i < 10; i++) {
				rnd = new Random(seed);
				rnd.NextDouble();
				rnd.NextDouble();
				rnd.NextDouble();
				rnd.NextDouble();
				Console.WriteLine(rnd.NextDouble());
			}

				Console.ReadKey();
		}

		//http://www.vcskicks.com/random-element.php
		public static T DropItem<T>(List<KeyValuePair<float, T>> dropableItems, Random rnd) where T : class {
			float dice = (float)rnd.NextDouble();

			//korrigiere die Chancen auf einen Gesamtwert von 100%
			float sumChance = dropableItems.Sum(d => d.Key);
			List<KeyValuePair<float, T>> items;
			if (sumChance != 1f) {
				items = dropableItems.Select(e => new KeyValuePair<float, T>(e.Key / sumChance, e.Value)).ToList();
			} else {
				items = dropableItems;
			}

			//bestimmte das Zufällige Item anhand der Chancen. Die Chancen werden aufsummiert. 
			//z.B. A=10, B=30, C=60; Dice=90; Erg=A+B+C; 90 < 100 = C;
			//z.B. A=10, B=30, C=60; Dice=39; Erg=A+B; 39 < 40 = B
			float cumulative = 0.0f;
			foreach (KeyValuePair<float, T> item in items.OrderBy(di => di.Key)) {
				cumulative += item.Key;

				if (dice < cumulative) {
					return item.Value;
				}
			}

			return null;
		}
	}

	public class Item {
		public float Chance { get; private set; }
		public string Name { get; private set; }

		public Item(float chance, string title) {
			this.Chance = chance;
			this.Name = title;
		}
	}
}

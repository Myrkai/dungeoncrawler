﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpDX.Toolkit;
using SharpDX.Toolkit.Graphics;
using SharpDX;

namespace RPG.Sprites {
	public enum AnimationDirection {
		UP = 3,
		DOWN = 0,
		LEFT = 1,
		RIGHT = 2
	}

	public class Animation {

		public Texture2D Texture { get; private set; }
		public string TextureAsset { get; private set; }

		public int AnimationStepsPerAnimation { get; private set; }
		public int AnimationCount { get; private set; }
		public float AnimationSpeed { get; private set; }

		public AnimationDirection CurrentAnimationDirection { get; private set; }
		public int CurrentAnimationStepIndex { get; private set; }

		public float Timer { get; private set; }
		public bool DoAnimationLooping { get; set; }

		public int Step { get; private set; }
		public int SpriteSingleWidth { get; private set; }
		public int SpriteSingleHeight { get; private set; }
		public int AnimationStopIndex { get; set; }

		public ReadOnlyDictionary<AnimationDirection, int> AnimationIndex { get { return new ReadOnlyDictionary<AnimationDirection, int>(AnimationIndexes); } }
		private Dictionary<AnimationDirection, int> AnimationIndexes;

		/// <summary>
		/// Eine Animierte Figur. Default der Animationen sind: DOWN, LEFT, RIGHT, UP
		/// </summary>
		/// <param name="texture">Das jeweilioge Bild mit allen Animationsphasen</param>
		/// <param name="imagesPerAni">Wieviele Bilder besitzt die Animation (Pro Animation)</param>
		/// <param name="animationCount">Wieviele verschiedene Animationen gibt es</param>
		/// <param name="animationspeed">Wie schnell wird die Animation abgespielt</param>
		public Animation(Texture2D texture, string textureAsset, int imagesPerAni, int animationCount, float animationspeed) {
			Texture = texture;
			TextureAsset = textureAsset;

			AnimationStepsPerAnimation = imagesPerAni;
			AnimationCount = animationCount;
			AnimationSpeed = animationspeed;

			CurrentAnimationDirection = AnimationDirection.DOWN;
			CurrentAnimationStepIndex = 0;

			Step = 1;
			Timer = 0f;
			DoAnimationLooping = true;
			AnimationStopIndex = 1;

			SpriteSingleWidth = Texture.Width / imagesPerAni;
			SpriteSingleHeight = Texture.Height / animationCount;

			AnimationIndexes = new Dictionary<AnimationDirection, int>();
			AnimationIndexes.Add(AnimationDirection.DOWN, (int)AnimationDirection.DOWN);
			AnimationIndexes.Add(AnimationDirection.RIGHT, (int)AnimationDirection.RIGHT);
			AnimationIndexes.Add(AnimationDirection.LEFT, (int)AnimationDirection.LEFT);
			AnimationIndexes.Add(AnimationDirection.UP, (int)AnimationDirection.UP);
		}

		public void Draw(SpriteBatch spriteBatch, Vector2 postion, float scale = 1f, float opacity = 1f) {
			//spriteBatch.Draw(Texture, postion, CurrentTextureRectangle(), Color.White);
			spriteBatch.Draw(Texture, new RectangleF(postion.X, postion.Y, CurrentTextureRectangle().Width * scale, CurrentTextureRectangle().Height * scale), CurrentTextureRectangle(), Color.White * opacity);
		}

		public Rectangle CurrentTextureRectangle() {
			return new Rectangle(SpriteSingleWidth * CurrentAnimationStepIndex, SpriteSingleHeight * AnimationIndexes[CurrentAnimationDirection], SpriteSingleWidth, SpriteSingleHeight);
		}

		public void Update(GameTime gameTime) {
			Timer += (float)gameTime.ElapsedGameTime.TotalSeconds;

			if (Timer > AnimationSpeed) {
				Timer -= AnimationSpeed;
				CurrentAnimationStepIndex += Step;

				if (DoAnimationLooping) {
					if (CurrentAnimationStepIndex >= AnimationStepsPerAnimation) {
						Step = -1;
						CurrentAnimationStepIndex -= 2;
					}

					if (CurrentAnimationStepIndex < 0) {
						Step = 1;
						CurrentAnimationStepIndex += 2;
					}
				} else if (!DoAnimationLooping && CurrentAnimationStepIndex >= AnimationStepsPerAnimation) {
					CurrentAnimationStepIndex = AnimationStopIndex;
					Step = 0;
				}
			}
		}

		public void SetDirection(AnimationDirection d) {
			if (CurrentAnimationDirection != d) {
				CurrentAnimationDirection = d;
				ResetAnimation();
			}
		}

		public void ResetAnimation() {
			CurrentAnimationStepIndex = 0;
			Step = 1;
		}

		/// <summary>
		/// Hier wird angegeben welche Animationen sich im Bild befinden. 
		/// Betrachtet wird dabei das Bild von oben(first) nach unten(four).
		/// RPG Maker-Sprites sind immer (Down, Right, Left, Up).
		/// </summary>
		public void SetAnimationConfiguration(AnimationDirection first, AnimationDirection second, AnimationDirection third, AnimationDirection four) {
			AnimationIndexes[first] = 0;
			AnimationIndexes[second] = 1;
			AnimationIndexes[third] = 2;
			AnimationIndexes[four] = 3;
		}

		public void OverrideAnimationIndexes(Dictionary<AnimationDirection, int> animationIndexes) {
			AnimationIndexes = animationIndexes;
		}
	}
}

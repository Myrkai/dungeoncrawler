using System;
using System.Text;
using System.Collections.Generic;
using System.Globalization;
using System.Xml.Linq;
using System.Xml;

namespace MathEval {

	internal class Scanner {
		
		private static Dictionary<string, TokenType> lookupTable;
		
		private string input;
		private int curPos;

		static Scanner() {
			lookupTable = new Dictionary<string, TokenType>(50);
			lookupTable.Add("RAD", TokenType.Rad);
			lookupTable.Add("E", TokenType.E);
			lookupTable.Add("PI", TokenType.PI);
			lookupTable.Add("R2", TokenType.R2);
			lookupTable.Add("R3", TokenType.R3);
			lookupTable.Add("ABS", TokenType.Abs);
			lookupTable.Add("ACOS", TokenType.Acos);
			lookupTable.Add("ASIN", TokenType.Asin);
			lookupTable.Add("ATAN", TokenType.Atan);
			lookupTable.Add("CEIL", TokenType.Ceil);
			lookupTable.Add("COS", TokenType.Cos);
			lookupTable.Add("COSH", TokenType.Cosh);
			lookupTable.Add("EXP", TokenType.Exp);
			lookupTable.Add("FLOOR", TokenType.Floor);
			lookupTable.Add("LOG", TokenType.Log);
			lookupTable.Add("LOG2", TokenType.Log2);
			lookupTable.Add("LOG10", TokenType.Log10);
			lookupTable.Add("MAX", TokenType.Max);
			lookupTable.Add("MIN", TokenType.Min);
			lookupTable.Add("POW", TokenType.Pow);
			lookupTable.Add("SIGN", TokenType.Sign);
			lookupTable.Add("SIN", TokenType.Sin);
			lookupTable.Add("SINH", TokenType.Sinh);
			lookupTable.Add("SQRT", TokenType.Sqrt);
			lookupTable.Add("SUM", TokenType.Sum);
			lookupTable.Add("PROD", TokenType.Prod);
			lookupTable.Add("TAN", TokenType.Tan);
			lookupTable.Add("TANH", TokenType.Tanh);
			lookupTable.Add("RAND", TokenType.Rand);
			lookupTable.Add("ROOT", TokenType.Root);
			lookupTable.Add("INV", TokenType.Inv);
			lookupTable.Add("RANDDOUBLE", TokenType.RandDouble);
			lookupTable.Add("DEG2RAD", TokenType.DegToRad);
			lookupTable.Add("RAD2DEG", TokenType.RadToDeg);
			lookupTable.Add("TOINT", TokenType.ToInt);
			lookupTable.Add("XOR", TokenType.Xor);
			lookupTable.Add("TO", TokenType.To);
			lookupTable.Add("HEX", TokenType.Hex);
			lookupTable.Add("OCT", TokenType.Oct);
			lookupTable.Add("BIN", TokenType.Bin);
			lookupTable.Add("DEC", TokenType.Dec);
			lookupTable.Add("CHR", TokenType.Chr);
			lookupTable.Add("GEN", TokenType.Gen);
			lookupTable.Add("FIX", TokenType.Fix);
			lookupTable.Add("SCI", TokenType.Sci);
		}
		
		public Scanner() {
			input = null;
			curPos = -1;
		}
		
		public void Reset(string s) {
			input = s;
			curPos = 0;
		}
		
		public Token ScanNext() {
			char c;
			Token ret;
			
			while (curPos < input.Length && Char.IsWhiteSpace(input[curPos])) {
				curPos++;
			}
			
			if (curPos >= input.Length) {
				return new Token(input.Length, TokenType.EOF);
			}
			
			ret = new Token(curPos);
			c = input[curPos];
			if (c == '+' || c == '-' || c == '*' || c == '/' || c == '\\' || c == '%' || c == '!' ||
				c == '^' || c == '|' || c == '&' || c == '~' || c == '(' || c == ')' || c == '�' || c == ',') {
			
				curPos++;
				ret.Kind = (TokenType)c;
		
			} else if (c == '<') {
				if (++curPos >= input.Length || input[curPos] != '<') {
					Errors.Error(curPos - 1, ErrorCode.LeftShiftExpected);
				}
				curPos++;
				ret.Kind = TokenType.LeftShift;
			
			} else if (c == '>') {
				if (++curPos >= input.Length || input[curPos] != '>') {
					Errors.Error(curPos - 1, ErrorCode.RightShiftExpected);
				}
				curPos++;
				ret.Kind = TokenType.RightShift;
			
			} else if (c == '\'') {
				/* char const */
				if (curPos + 2 >= input.Length || input[curPos + 2] != '\'') {
					Errors.Error(curPos, ErrorCode.CharConstExpected);
				}
				ret.Number = new Number((long)input[curPos + 1]);
				curPos += 3;
				
			} else if (c == '.' || Char.IsDigit(c)) {
				ret.Number = ParseNumber();
			
			} else if (Char.IsLetter(c)) {
				ret.Kind = ParseIdentifier();
				
			} else {
				Errors.Error(curPos, ErrorCode.UnknownSymbolEncountered);
			}
			
			return ret;
		}
		
		/* [0x|0b]{number} | {number}.{number}[e[-|+]{number}] */
		private Number ParseNumber() {
			Number ret = new Number();
			int startIndex = curPos, errorPos = curPos;
			int radix = 10;
			bool isFloatingPoint = false;
			
			if (input[curPos] == '0') {
				if (curPos + 1 < input.Length) {
					char c = input[curPos + 1];
					if (c == 'x' || c == 'X') {
						radix = 16;
						startIndex += 2;//skip base specifier
						curPos += 2;
					} else if (c == 'b' || c == 'B') {
						radix = 2;
						startIndex += 2;//skip base specifier
						curPos += 2;
					} else if (c >= '0' && c <= '9') {
						radix = 8;
					}
					
				}
			}
			
			while (curPos < input.Length && IsNumber(input[curPos])) {
				curPos++;
			}
			
			if (curPos < input.Length && (input[curPos] == '.')) {
				curPos++;
				isFloatingPoint = true;
				while (curPos < input.Length && Char.IsDigit(input[curPos])) {
					curPos++;
				}
				if (curPos < input.Length && Char.ToUpper(input[curPos]) == 'E') {
					curPos++;
					if (curPos < input.Length && (input[curPos] == '+' || input[curPos] == '-')) {
						curPos++;
					}
					while (curPos < input.Length && Char.IsDigit(input[curPos])) {
						curPos++;
					}
				}
			}
			
			string s = input.Substring(startIndex, curPos - startIndex);
			if (String.IsNullOrEmpty(s)) {
				Errors.Error(curPos, ErrorCode.NumberParseError);
			}
			
			try {
				if (isFloatingPoint) {
					ret = Convert.ToDouble(s, NumberFormatInfo.InvariantInfo);
				} else {
					ret = Convert.ToInt64(s, radix);
				}
			} catch (FormatException) {
				Errors.Error(errorPos, ErrorCode.NumberParseError);
			} catch (OverflowException) {
				Errors.Error(errorPos, ErrorCode.ConstNumberOverflow);
			}
			return ret;
		}
		
		private bool IsNumber(char c) {
			return ((c >= '0' && c <= '9') || (c >= 'a' && c <= 'f') || (c >= 'A' && c <= 'F'));
		}
		
		private TokenType ParseIdentifier() {
			TokenType kind;
			StringBuilder sBuild = new StringBuilder();
			
			while (curPos < input.Length && Char.IsLetterOrDigit(input[curPos])) {
				sBuild.Append(input[curPos++]);
			}
			
			string identifier = sBuild.ToString();
			if (!lookupTable.TryGetValue(identifier.ToUpper(), out kind)) {
				Errors.Error(curPos - identifier.Length, ErrorCode.UnknownIdentifier);
			}
			return kind;
		}
	}
}
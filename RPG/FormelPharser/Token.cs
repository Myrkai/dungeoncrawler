using System;

namespace MathEval {

	internal class Token {
		private TokenType kind;
		private Number number;
		private int col;
		
		public Token(int col) {
			this.col = col;
		}
		
		public Token(int col, TokenType kind) {
			this.col = col;
			this.kind = kind;
		}
		
		public TokenType Kind {
			get {
				return kind;
			}
			
			set {
				kind = value;
			}
		}
		
		public Number Number {
			get {
				return number;
			}
			
			set {
				kind = TokenType.Number;
				number = value;
			}
		}
		
		public int Col {
			get {
				return col;
			}
			
			set {
				col = value;
			}
		}
		
		public override string ToString() {
			if (kind == TokenType.Number) {
				return String.Format("Col {0}: Number {1}", col, number.ToString());
			} else {
				return String.Format("Col {0}: Symbol {1}", col, kind.ToString());
			}
		}
	}
}
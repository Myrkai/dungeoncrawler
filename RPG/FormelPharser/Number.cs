using System;
using System.Runtime.InteropServices;

namespace MathEval {
	
	internal enum NumberType : int { 
		Integer,
		FloatingPoint
	}
	
	[StructLayout(LayoutKind.Explicit)]
	internal struct Number {
		[FieldOffset(0)]private NumberType kind;
		[FieldOffset(4)]private long integerNumber;
		[FieldOffset(4)]private double floatingPointNumber;
		
		public Number(long number) {
			kind = NumberType.Integer;
			floatingPointNumber = 0.0;
			integerNumber = number;
		}
		
		public Number(double number) {
			kind = NumberType.FloatingPoint;
			integerNumber = 0L;
			floatingPointNumber = number;
		}
		
		public NumberType Kind {
			get {
				return kind;
			}
		}
		
		public long IntegerNumber {
			get {
				return integerNumber;
			}
			
			set {
				integerNumber = value;
				kind = NumberType.Integer;
			}
		}
		
		public double FloatingPointNumber {
			get {
				return floatingPointNumber;
			}
			
			set {
				floatingPointNumber = value;
				kind = NumberType.FloatingPoint;
			}
		}
		
		public static bool operator<(Number x, Number y) {
			if (x.kind == NumberType.Integer && y.kind == NumberType.Integer) {
				return x.integerNumber < y.integerNumber;
			} else {
				return (double)x < (double)y;
			}
		}
		
		public static bool operator<=(Number x, Number y) {
			if (x.kind == NumberType.Integer && y.kind == NumberType.Integer) {
				return x.integerNumber <= y.integerNumber;
			} else {
				return (double)x <= (double)y;
			}
		}
		
		public static bool operator>(Number x, Number y) {
			if (x.kind == NumberType.Integer && y.kind == NumberType.Integer) {
				return x.integerNumber > y.integerNumber;
			} else {
				return (double)x > (double)y;
			}
		}
		
		public static bool operator>=(Number x, Number y) {
			if (x.kind == NumberType.Integer && y.kind == NumberType.Integer) {
				return x.integerNumber >= y.integerNumber;
			} else {
				return (double)x >= (double)y;
			}
		}
		
		public static bool operator==(Number x, Number y) {
			if (x.kind == NumberType.Integer && y.kind == NumberType.Integer) {
				return x.integerNumber == y.integerNumber;
			} else {
				return (double)x == (double)y;
			}
		}
		
		public static bool operator!=(Number x, Number y) {
			if (x.kind == NumberType.Integer && y.kind == NumberType.Integer) {
				return x.integerNumber != y.integerNumber;
			} else {
				return (double)x != (double)y;
			}
		}
		
		public static implicit operator double(Number x) {
			if (x.kind == NumberType.Integer) {
				return x.integerNumber;
			} else {
				return x.floatingPointNumber;
			}
		}
		
		public static implicit operator Number(double x) {
			return new Number(x);
		}
		
		public static explicit operator long(Number x) {
			if (x.kind == NumberType.Integer) {
				return x.integerNumber;
			} else {
				return (long)x.floatingPointNumber;
			}
		}
		
		public static implicit operator Number(long x) {
			return new Number(x);
		}
		
		public static Number operator-(Number x) {
			Number ret = new Number();
			
			if (x.kind == NumberType.Integer && x.integerNumber != Int64.MinValue) {
				ret.IntegerNumber = -x.integerNumber;
			} else {
				ret.FloatingPointNumber = -(double)x;
			}
			
			return ret;
		}
		
		public static Number operator+(Number x, Number y) {
			Number ret = new Number();
			
			if (x.kind == NumberType.Integer && y.kind == NumberType.Integer) {
				try {
					ret.IntegerNumber = checked(x.integerNumber + y.integerNumber);
				} catch (OverflowException) {
					ret.FloatingPointNumber = (double)x.integerNumber + (double)y.integerNumber;
				}
			} else {
				ret.FloatingPointNumber = (double)x + (double)y;
			}
			
			return ret;
		}
		
		public static Number operator-(Number x, Number y) {
			Number ret = new Number();
			
			if (x.kind == NumberType.Integer && y.kind == NumberType.Integer) {
				try {
					ret.IntegerNumber = checked(x.integerNumber - y.integerNumber);
				} catch (OverflowException) {
					ret.FloatingPointNumber = (double)x.integerNumber - (double)y.integerNumber;
				}
			} else {
				ret.FloatingPointNumber = (double)x - (double)y;
			}
			
			return ret;
		}
		
		public static Number operator*(Number x, Number y) {
			Number ret = new Number();
			
			if (x.kind == NumberType.Integer && y.kind == NumberType.Integer) {
				try {
					ret.IntegerNumber = checked(x.integerNumber * y.integerNumber);
				} catch (OverflowException) {
					ret.FloatingPointNumber = (double)x.integerNumber * (double)y.integerNumber;
				}
			} else {
				ret.FloatingPointNumber = (double)x * (double)y;
			}
			
			return ret;
		}
		
		public static Number operator/(Number x, Number y) {
			return new Number((double)x / (double)y);
		}
		
		public static Number Pow(Number x, Number y) {
			double val = Math.Pow(x, y);
			
			if (x.kind == NumberType.Integer && y.kind == NumberType.Integer && y.integerNumber >= 1 && val < Int64.MaxValue) {
				return new Number((long)val);
			} else {
				return new Number(val);
			}
		}
		
		public static Number Factorial(Number x) {
			double fact = 1.0;
			int n = (int)x.integerNumber;
			
			if (n < 0 || n > 170) {
				return Double.PositiveInfinity;
			}
			while (n > 1) {
				fact *= n--;
			}
			
			if (fact < Int64.MaxValue) {
				return (long)fact;
			} else {
				return fact;
			}
		}
		
		public override bool Equals(object o) {
			Number n = (Number)o;
			
			return this == n;
		}
		
		public override int GetHashCode() {
			if (kind == NumberType.Integer) {
				return integerNumber.GetHashCode();
			} else {
				return floatingPointNumber.GetHashCode();
			}
		}
		
		public override string ToString() {
			if (kind == NumberType.Integer) {
				return integerNumber.ToString();
			} else {
				return floatingPointNumber.ToString(System.Globalization.NumberFormatInfo.InvariantInfo);
			}
		}
	}
}
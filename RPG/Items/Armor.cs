﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RPG.Items.Enum;
using SharpDX.Toolkit.Graphics;

namespace RPG.Items {
	public class Armor : Item {
		public int Defence { get; set; }

		public Armor(int defence, string title, int ilvl, Quality quality, ItemType type, Texture2D texture, string textureAsset, uint cost)
			: base(title, ilvl, quality, type, texture, textureAsset, cost) {
			Defence = defence;
		}
	}
}

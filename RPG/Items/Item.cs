﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using RPG.Items.Enum;
using SharpDX.Toolkit.Graphics;
using SharpDX;

namespace RPG.Items {
	public class Item {
		public ReadOnlyCollection<Skill> AvaiableSkills { get { return new ReadOnlyCollection<Skill>(_AvaiableSkills); } }
		private List<Skill> _AvaiableSkills { get; set; }

		public ItemType Type { get; private set; }
		public Quality Quality { get; set; }

		public int Itemlevel { get; private set; }
		public string Title { get; private set; }
		public string SpecialTitle { get; set; }
		public uint Cost { get; private set; }

		public Texture2D Texture { get; private set; }
		public string TextureAsset { get; private set; }

		private static int m_Counter = 0;
        public int ID { get; private set; }

		public Item(string title, int iLvl, Quality quality, ItemType type, Texture2D texture, string textureAsset, uint cost) {
			_AvaiableSkills = new List<Skill>();
			this.ID = System.Threading.Interlocked.Increment(ref m_Counter);

			Title = title;
			SpecialTitle = "undefined";
			Itemlevel = iLvl;
			Quality = quality;
			Type = type;
			Cost = cost;

			Texture = texture;
			TextureAsset = textureAsset;
		}

		public void AddSkill(Skill skill) {
			_AvaiableSkills.Add(skill);
		}

		public void AddSkill(List<Skill> skills) {
			_AvaiableSkills.AddRange(skills);
		}

		public Item Clone() {
			Item tmp = new Item(this.Title, this.Itemlevel, this.Quality, this.Type, this.Texture, this.TextureAsset, this.Cost);
			tmp.SpecialTitle = this.SpecialTitle;

			foreach (Skill skill in this.AvaiableSkills) {
				tmp.AddSkill(skill);
			}

			return tmp;
		}

		public override string ToString() {
			String s = String.Format("Item ID: {2}, Level: {0}, STitle: {1}, Skills: [", Itemlevel, SpecialTitle, ID);
			foreach (var skill in AvaiableSkills) {
				s += "\"" + skill.ToString() + "\", ";
			}
			s += "]";
			return s;
		}
	}
}

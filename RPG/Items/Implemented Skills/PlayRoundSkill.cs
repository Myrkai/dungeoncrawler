﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RPG.Chars;
using RPG.Items.Enum;

namespace RPG.Items.Implemented_Skills {
	public class PlayRoundSkill : Skill {

		#region Konstruktor
		public PlayRoundSkill(float value, string title, string prefix, string surfix, SkillType type, int level, int minItemLevel, Quality minQuality, string look, string description, List<ItemType> availableItemTyps) 
			: base(value, title, prefix, surfix, type, level, minItemLevel, minQuality, AffectType.PlayRoundSkill, look, description, availableItemTyps) {

		}

		public PlayRoundSkill(float value, string title, string prefix, string surfix, SkillType type, int level, int minItemLevel, Quality minQuality, string look, string description, params ItemType[] availableItemTyps)
			: base(value, title, prefix, surfix, type, level, minItemLevel, minQuality, AffectType.PlayRoundSkill, look, description, availableItemTyps) {

		}

		public PlayRoundSkill(float value, float chance, string title, string prefix, string surfix, SkillType type, int level, int minItemLevel, Quality minQuality, string look, string description, List<ItemType> availableItemTyps)
			: base(value, chance, title, prefix, surfix, type, level, minItemLevel, minQuality, AffectType.PlayRoundSkill, look, description, availableItemTyps) {

		}

		public PlayRoundSkill(float value, float chance, string title, string prefix, string surfix, SkillType type, int level, int minItemLevel, Quality minQuality, string look, string description, params ItemType[] availableItemTyps)
			: base(value, chance, title, prefix, surfix, type, level, minItemLevel, minQuality, AffectType.PlayRoundSkill, look, description, availableItemTyps) {

		}

		public PlayRoundSkill(string title, string prefix, string surfix, SkillType type, int level, int minItemLevel, Quality minQuality, string look, string description, List<ItemType> availableItemTyps)
			: base(title, prefix, surfix, type, level, minItemLevel, minQuality, AffectType.PlayRoundSkill, look, description, availableItemTyps) {

		}

		public PlayRoundSkill(string title, string prefix, string surfix, SkillType type, int level, int minItemLevel, Quality minQuality, string look, string description, params ItemType[] availableItemTyps)
			: base(title, prefix, surfix, type, level, minItemLevel, minQuality, AffectType.PlayRoundSkill, look, description, availableItemTyps) {

		}
		#endregion

		public override void PerformAction(Entity player) {
			throw new NotImplementedException();
			//base.PerformAction(player);
			
			//switch (base.Type) {
			//    case Enum.SkillType.AbsoluteBattleRounds: 
					
			//        break;
			//    case Enum.SkillType.AbsoluteBattleTime: 

			//        break;
			//    case Enum.SkillType.PercentageFasterWalk: 
					
			//        break;
			//    case Enum.SkillType.PercentageSlowerWalk: 
					
			//        break;
			//    case Enum.SkillType.PercentageFastMonsterfights: 
					
			//        break;
			//    case Enum.SkillType.PercentageSlowMonsterfights: 
					
			//        break;
			//    default: throw new NotImplementedException(base.Type.ToString());
			//}
		}

		public virtual void RemoveAction(Entity player) {
			throw new NotImplementedException();
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RPG.Chars;
using RPG.Items.Enum;

namespace RPG.Items.Implemented_Skills {
	public class AfterBattleSkill : Skill {

		#region Konstruktor
		public AfterBattleSkill(float value, string title, string prefix, string surfix, SkillType type, int level, int minItemLevel, Quality minQuality, string look, string description, List<ItemType> availableItemTyps)
			: base(value, title, prefix, surfix, type, level, minItemLevel, minQuality, AffectType.AfterBattleSkill, look, description, availableItemTyps) {

		}

		public AfterBattleSkill(float value, string title, string prefix, string surfix, SkillType type, int level, int minItemLevel, Quality minQuality, string look, string description, params ItemType[] availableItemTyps)
			: base(value, title, prefix, surfix, type, level, minItemLevel, minQuality, AffectType.AfterBattleSkill, look, description, availableItemTyps) {

		}

		public AfterBattleSkill(float value, float chance, string title, string prefix, string surfix, SkillType type, int level, int minItemLevel, Quality minQuality, string look, string description, List<ItemType> availableItemTyps)
			: base(value, chance, title, prefix, surfix, type, level, minItemLevel, minQuality, AffectType.AfterBattleSkill, look, description, availableItemTyps) {

		}

		public AfterBattleSkill(float value, float chance, string title, string prefix, string surfix, SkillType type, int level, int minItemLevel, Quality minQuality, string look, string description, params ItemType[] availableItemTyps)
			: base(value, chance, title, prefix, surfix, type, level, minItemLevel, minQuality, AffectType.AfterBattleSkill, look, description, availableItemTyps) {

		}

		public AfterBattleSkill(string title, string prefix, string surfix, SkillType type, int level, int minItemLevel, Quality minQuality, string look, string description, List<ItemType> availableItemTyps)
			: base(title, prefix, surfix, type, level, minItemLevel, minQuality, AffectType.AfterBattleSkill, look, description, availableItemTyps) {

		}

		public AfterBattleSkill(string title, string prefix, string surfix, SkillType type, int level, int minItemLevel, Quality minQuality, string look, string description, params ItemType[] availableItemTyps)
			: base(title, prefix, surfix, type, level, minItemLevel, minQuality, AffectType.AfterBattleSkill, look, description, availableItemTyps) {

		}
		#endregion

		public override void PerformAction(Entity player) {
			throw new Exception("Use PerformAction (Player, rnd).");
		}

		public virtual string PerformAction(Entity player, Random rnd) {
			switch (base.Type) {
				case Enum.SkillType.AbsoluteNoDeath:
					float chance = 0f;
					switch (base.Level) {
						case 1:
							chance = 0.03f;
							break;
						case 2:
							chance = 0.06f;
							break;
						case 3:
							chance = 0.09f;
							break;
						case 4:
							chance = 0.12f;
							break;
						case 5:
							chance = 0.15f;
							break;
						default:
							throw new Exception(Level.ToString());
					}

					if (CoinFlip(chance, rnd)) {
						return "o;nodeath;1";
					} else {
						return "o;nodeath;0";
					}
				case Enum.SkillType.AbsoluteMoreEXP:
					return "o;baseExp;" + base.Value.ToString();
				case Enum.SkillType.PercentageMoreEXP:
					return "o;percentageExp;" + base.Value.ToString();
				case Enum.SkillType.AbsoluteMoreGold:
					return "o;baseGold;" + base.Value.ToString();
				case Enum.SkillType.PercentageMoreGold:
					return "o;percentageGold;" + base.Value.ToString();
				default: throw new NotImplementedException(base.Type.ToString());
			}
		}

		private bool CoinFlip(float chanceToTrue, Random rnd) {
			List<KeyValuePair<float, bool>> tmp = new List<KeyValuePair<float, bool>>();
			tmp.Add(new KeyValuePair<float, bool>(chanceToTrue, true));
			tmp.Add(new KeyValuePair<float, bool>(1f - chanceToTrue, false));

			return Drop<bool>(tmp, rnd);
		}

		private T Drop<T>(List<KeyValuePair<float, T>> dropableItems, Random rnd) {
			float dice = (float)rnd.NextDouble();

			//korrigiere die Chancen auf einen Gesamtwert von 100%
			float sumChance = dropableItems.Sum(d => d.Key);
			List<KeyValuePair<float, T>> items;
			if (sumChance != 1f) {
				items = dropableItems.Select(e => new KeyValuePair<float, T>(e.Key / sumChance, e.Value)).ToList();
			} else {
				items = dropableItems;
			}

			//bestimmte das Zufällige Item anhand der Chancen. Die Chancen werden aufsummiert. 
			//z.B. A=10, B=30, C=60; Dice=90; Erg=A+B+C; 90 < 100 = C;
			//z.B. A=10, B=30, C=60; Dice=39; Erg=A+B; 39 < 40 = B
			float cumulative = 0.0f;
			foreach (KeyValuePair<float, T> item in items.OrderBy(di => di.Key)) {
				cumulative += item.Key;

				if (dice < cumulative) {
					return item.Value;
				}
			}

			return default(T);
		}
	}
}

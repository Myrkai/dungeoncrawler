﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RPG.Chars;
using RPG.Items.Enum;

namespace RPG.Items.Implemented_Skills {
	public class BattleSkill : Skill {

		#region Konstruktor
		public BattleSkill(float value, string title, string prefix, string surfix, SkillType type, int level, int minItemLevel, Quality minQuality, string look, string description, List<ItemType> availableItemTyps)
			: base(value, title, prefix, surfix, type, level, minItemLevel, minQuality, AffectType.BattleSkill, look, description, availableItemTyps) {

		}

		public BattleSkill(float value, string title, string prefix, string surfix, SkillType type, int level, int minItemLevel, Quality minQuality, string look, string description, params ItemType[] availableItemTyps)
			: base(value, title, prefix, surfix, type, level, minItemLevel, minQuality, AffectType.BattleSkill, look, description, availableItemTyps) {

		}

		public BattleSkill(float value, float chance, string title, string prefix, string surfix, SkillType type, int level, int minItemLevel, Quality minQuality, string look, string description, List<ItemType> availableItemTyps)
			: base(value, chance, title, prefix, surfix, type, level, minItemLevel, minQuality, AffectType.BattleSkill, look, description, availableItemTyps) {

		}

		public BattleSkill(float value, float chance, string title, string prefix, string surfix, SkillType type, int level, int minItemLevel, Quality minQuality, string look, string description, params ItemType[] availableItemTyps)
			: base(value, chance, title, prefix, surfix, type, level, minItemLevel, minQuality, AffectType.BattleSkill, look, description, availableItemTyps) {

		}

		public BattleSkill(string title, string prefix, string surfix, SkillType type, int level, int minItemLevel, Quality minQuality, string look, string description, List<ItemType> availableItemTyps)
			: base(title, prefix, surfix, type, level, minItemLevel, minQuality, AffectType.BattleSkill, look, description, availableItemTyps) {

		}

		public BattleSkill(string title, string prefix, string surfix, SkillType type, int level, int minItemLevel, Quality minQuality, string look, string description, params ItemType[] availableItemTyps)
			: base(title, prefix, surfix, type, level, minItemLevel, minQuality, AffectType.BattleSkill, look, description, availableItemTyps) {

		}
		#endregion

		public override void PerformAction(Entity player) {
			throw new Exception("Use PerformAction (Player, rnd).");
		}

		/// <summary>
		/// Heilung und Zusatzschaden werden sofort umgesetzt.
		/// Zusätzliche Kampfrunden oder Kampfzeit muss erst noch hinzugefügt werden.
		/// 
		/// Return: [o=owner || t=target];[heal || dmg || battleRound || battleTime];[Anzahl] 
		/// Returnbeispiel: o;heal;230
		/// </summary>
		public virtual string PerformAction(Entity owner, Entity target, Random rnd) {
			//base.PerformAction(owner);
			float chance = 0f;

			switch (base.Type) {
				case Enum.SkillType.AbsoluteHealInRound:
					switch (Level) {
						case 1:
						case 2:
							chance = 0.05f;
							break;
						case 3:
						case 4:
							chance = 0.1f;
							break;
						case 5:
							chance = 0.15f;
							break;
						default:
							throw new Exception(Level.ToString());
					}

					if (CoinFlip(chance, rnd)) {
						owner.CurrentHP = Math.Min(owner.MaxHP, owner.CurrentHP + Value);
						return "o;heal;" + Convert.ToInt32(Value).ToString();
					}
					break;
				case Enum.SkillType.PercentageHealInRound:
					switch (Level) {
						case 1:
						case 2:
							chance = 0.05f;
							break;
						case 3:
						case 4:
							chance = 0.10f;
							break;
						case 5:
							chance = 0.15f;
							break;
						default:
							throw new Exception(Level.ToString());
					}

					if (CoinFlip(chance, rnd)) {
						owner.CurrentHP = Math.Min(owner.MaxHP, owner.CurrentHP + owner.MaxHP * (Value / 100f));
						return "o;heal;" + (owner.CurrentHP + owner.MaxHP * (Value / 100f)).ToString();
					}
					break;
				case Enum.SkillType.AbsoluteDamageInRound:
					switch (Level) {
						case 1:
						case 2:
						case 3:
							chance = 0.05f;
							break;
						case 4:
						case 5:
							chance = 0.10f;
							break;
						default:
							throw new Exception(Level.ToString());
					}

					if (CoinFlip(chance, rnd)) {
						target.CurrentHP = target.CurrentHP - Value;
						return "t;dmg;" + Value.ToString();
					}
					break;
				case Enum.SkillType.AbsoluteBattleRoundsInRound:
					switch (Level) {
						case 1:
							chance = 0.05f;
							break;
						case 2:
							chance = 0.1f;
							break;
						case 3:
						case 4:
							chance = 0.05f;
							break;
						case 5:
							chance = 0.1f;
							break;
						default:
							throw new Exception(Level.ToString());
					}

					if (CoinFlip(chance, rnd)) {
						return "o;battleRound;" + Value.ToString();
					}
					break;
				case Enum.SkillType.AbsoluteBattleTimeInRound:
					switch (Level) {
						case 1:
							chance = 0.05f;
							break;
						case 2:
							chance = 0.1f;
							break;
						case 3:
						case 4:
							chance = 0.05f;
							break;
						case 5:
							chance = 0.1f;
							break;
						default:
							throw new Exception(Level.ToString());
					}

					if (CoinFlip(chance, rnd)) {
						return "o;battleTime;" + Value.ToString();
					}
					break;
				case Enum.SkillType.AbsoluteMonsterSpawn:
					throw new NotImplementedException(""); // will ich nicht im Spiel besitzen
				case Enum.SkillType.PercentageMultiAttack:
					throw new NotImplementedException(""); // will ich nicht im Spiel besitzen
				default:
					throw new NotImplementedException(base.Type.ToString());
			}

			return null;
		}

		private bool CoinFlip(float chanceToTrue, Random rnd) {
			List<KeyValuePair<float, bool>> tmp = new List<KeyValuePair<float, bool>>();
			tmp.Add(new KeyValuePair<float, bool>(chanceToTrue, true));
			tmp.Add(new KeyValuePair<float, bool>(1f - chanceToTrue, false));

			return Drop<bool>(tmp, rnd);
		}

		private T Drop<T>(List<KeyValuePair<float, T>> dropableItems, Random rnd) {
			float dice = (float)rnd.NextDouble();

			//korrigiere die Chancen auf einen Gesamtwert von 100%
			float sumChance = dropableItems.Sum(d => d.Key);
			List<KeyValuePair<float, T>> items;
			if (sumChance != 1f) {
				items = dropableItems.Select(e => new KeyValuePair<float, T>(e.Key / sumChance, e.Value)).ToList();
			} else {
				items = dropableItems;
			}

			//bestimmte das Zufällige Item anhand der Chancen. Die Chancen werden aufsummiert. 
			//z.B. A=10, B=30, C=60; Dice=90; Erg=A+B+C; 90 < 100 = C;
			//z.B. A=10, B=30, C=60; Dice=39; Erg=A+B; 39 < 40 = B
			float cumulative = 0.0f;
			foreach (KeyValuePair<float, T> item in items.OrderBy(di => di.Key)) {
				cumulative += item.Key;

				if (dice < cumulative) {
					return item.Value;
				}
			}

			return default(T);
		}
	}
}

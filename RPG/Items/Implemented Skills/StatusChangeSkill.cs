﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RPG.Chars;
using RPG.Items.Enum;
using RPG.Chars.Enum;

namespace RPG.Items.Implemented_Skills {
	public class StatusChangeSkill : Skill {

		#region Konstruktor
		public StatusChangeSkill(float value, string title, string prefix, string surfix, SkillType type, int level, int minItemLevel, Quality minQuality, string look, string description, List<ItemType> availableItemTyps)
			: base(value, title, prefix, surfix, type, level, minItemLevel, minQuality, AffectType.StatusChangeSkill, look, description, availableItemTyps) {

		}

		public StatusChangeSkill(float value, string title, string prefix, string surfix, SkillType type, int level, int minItemLevel, Quality minQuality, string look, string description, params ItemType[] availableItemTyps)
			: base(value, title, prefix, surfix, type, level, minItemLevel, minQuality, AffectType.StatusChangeSkill, look, description, availableItemTyps) {

		}

		public StatusChangeSkill(float value, float chance, string title, string prefix, string surfix, SkillType type, int level, int minItemLevel, Quality minQuality, string look, string description, List<ItemType> availableItemTyps)
			: base(value, chance, title, prefix, surfix, type, level, minItemLevel, minQuality, AffectType.StatusChangeSkill, look, description, availableItemTyps) {

		}

		public StatusChangeSkill(float value, float chance, string title, string prefix, string surfix, SkillType type, int level, int minItemLevel, Quality minQuality, string look, string description, params ItemType[] availableItemTyps)
			: base(value, chance, title, prefix, surfix, type, level, minItemLevel, minQuality, AffectType.StatusChangeSkill, look, description, availableItemTyps) {

		}

		public StatusChangeSkill(string title, string prefix, string surfix, SkillType type, int level, int minItemLevel, Quality minQuality, string look, string description, List<ItemType> availableItemTyps)
			: base(title, prefix, surfix, type, level, minItemLevel, minQuality, AffectType.StatusChangeSkill, look, description, availableItemTyps) {

		}

		public StatusChangeSkill(string title, string prefix, string surfix, SkillType type, int level, int minItemLevel, Quality minQuality, string look, string description, params ItemType[] availableItemTyps)
			: base(title, prefix, surfix, type, level, minItemLevel, minQuality, AffectType.StatusChangeSkill, look, description, availableItemTyps) {

		}
		#endregion

		public override void PerformAction(Entity player) {
			base.PerformAction(player);

			switch (base.Type) {
				case SkillType.AbsoluteStrength: player.AddStat(StatType.BonusStrength, Value); break;
				case SkillType.PercentageStrength: player.AddStat(StatType.BonusStrength, player.Stats[StatType.BaseStrength] * (Value / 100f)); break;
				case SkillType.AbsoluteDexterity: player.AddStat(StatType.BonusDexterity, Value); break;
				case SkillType.PercentageDexterity: player.AddStat(StatType.BonusDexterity, player.Stats[StatType.BaseDexterity] * (Value / 100f)); break;
				case SkillType.AbsoluteStamina: player.AddStat(StatType.BonusStamina, Value); break;
				case SkillType.PercentageStamina: player.AddStat(StatType.BonusStamina, player.Stats[StatType.BaseStamina] * (Value / 100f)); break;
				case SkillType.AbsoluteLuck: player.AddStat(StatType.BonusLuck, Value); break;
				case SkillType.PercentageLuck: player.AddStat(StatType.BonusLuck, player.Stats[StatType.BaseLuck] * (Value / 100f)); break;
				case SkillType.AbsoluteDefence: player.AddStat(StatType.BonusDefence, Value); break;
				case SkillType.PercentageDefence: player.AddStat(StatType.BonusDefence, player.Stats[StatType.BaseDefence] * (Value / 100f)); break;
				case SkillType.AbsoluteDefenceValue: player.AddStat(StatType.BonusDefenceValue, Value); break;
				case SkillType.PercentageDefenceValue: player.AddStat(StatType.BonusDefenceValue, player.Stats[StatType.BaseDefenceValue] * (Value / 100f)); break;
				case SkillType.AbsoluteCriticalHitChance: player.AddStat(StatType.BonusCriticalHitCHance, Value); break;
				case SkillType.PercentageMagicFind: player.AddStat(StatType.BonusMagicFind, Value); break;
				case SkillType.AbsoluteHP: player.AddStat(StatType.BonusHP, Value); break;
				case SkillType.PercentageHP: player.AddStat(StatType.BonusHP, player.Stats[StatType.BaseHP] * (Value / 100f)); break;
				case SkillType.AbsoluteEvade: player.AddStat(StatType.BonusEvade, Value); break;
				case SkillType.AbsoluteMinWeaponDmg: player.AddStat(StatType.BonusMinWeaponDmg, Value); break;
				case SkillType.PercentageMinWeaponDmg: player.AddStat(StatType.BonusMinWeaponDmg, player.Stats[StatType.BaseMinWeaponDmg] * (Value / 100f)); break;
				case SkillType.AbsoluteMaxWeaponDmg: player.AddStat(StatType.BonusMaxWeaponDmg, Value); break;
				case SkillType.PercentageMaxWeaponDmg: player.AddStat(StatType.BonusMaxWeaponDmg, player.Stats[StatType.BaseMaxWeaponDmg] * (Value / 100f)); break;
				case SkillType.AbsoluteMinDamage: player.AddStat(StatType.BonusMinDamage, Value); break;
				case SkillType.PercentageMinDamage: player.AddStat(StatType.BonusMinDamage, player.Stats[StatType.BaseMinDamage] * (Value / 100f)); break;
				case SkillType.AbsoluteMaxDamage: player.AddStat(StatType.BonusMaxDamage, Value); break;
				case SkillType.PercentageMaxDamage: player.AddStat(StatType.BonusMaxDamage, player.Stats[StatType.BaseMaxDamage] * (Value / 100f)); break;
				case SkillType.AbsoluteWeaponDamage: player.AddStat(StatType.BonusMinWeaponDmg, Value); player.AddStat(StatType.BonusMaxWeaponDmg, Value); break;
				case SkillType.PercentageWeaponDamage: player.AddStat(StatType.BonusMinWeaponDmg, player.Stats[StatType.BaseMinWeaponDmg] * (Value / 100f)); player.AddStat(StatType.BonusMaxWeaponDmg, player.Stats[StatType.BaseMaxWeaponDmg] * (Value / 100f)); break;
				case SkillType.AbsoluteDamage: player.AddStat(StatType.BonusMinDamage, Value); player.AddStat(StatType.BonusMaxDamage, Value); break;
				case SkillType.PercentageDamage: player.AddStat(StatType.BonusMinDamage, player.Stats[StatType.BaseMinDamage] * (Value / 100f)); player.AddStat(StatType.BonusMaxDamage, player.Stats[StatType.BaseMaxDamage] * (Value / 100f)); break;
				case SkillType.AbsoluteCriticalDamage: player.AddStat(StatType.BonusCriticalDmg, Value); break;
				case SkillType.AbsoluteAllStats: player.AddStat(StatType.BonusStrength, Value); player.AddStat(StatType.BonusDexterity, Value); player.AddStat(StatType.BonusStamina, Value); player.AddStat(StatType.BonusLuck, Value); break;
				default: throw new NotImplementedException(base.Type.ToString());
			}
		}
	}
}

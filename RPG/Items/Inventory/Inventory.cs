﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpDX.Toolkit.Graphics;
using SharpDX.Toolkit;
using SharpDX;
using System.Collections.ObjectModel;

namespace RPG.Items.Inventory {

	public abstract class Inventory {
		//Wie größe ist das Inventar wenn es komplett freigeschaltet ist (alles was nicht freigeschaltet ist, wird grau
		public int MaxWidth { get; private set; }
		public int MaxHeight { get; private set; }

		//Wie groß ist das Inventar Momentan
		public int Width { get; private set; }
		public int Height { get; private set; }

		// wird automatisch erstellt
		private Texture2D AktivCell { get; set; }
		// wird automatisch erstellt
		private Texture2D InaktiveCell { get; set; }

		public Vector2 Position { get; set; }
		private GraphicsDevice GraphicsDevice { get; set; }

		const float stoneScale = 0.865f;
		const float transparent = 0.85f;

		//deren Position ist das Offset NICHT die Zielposition
		public ReadOnlyCollection<InventoryPosition> Items { get { return new ReadOnlyCollection<InventoryPosition>(_Items); } }
		private List<InventoryPosition> _Items { get; set; }

		public ReadOnlyCollection<Vector2> CellPositionsOffsets { get { return new ReadOnlyCollection<Vector2>(_CellPositionsOffsets); } }
		private List<Vector2> _CellPositionsOffsets { get; set; }
		public void SetCellPositionsOffsets(List<Vector2> offsets) { _CellPositionsOffsets = offsets; }

		#region events
		public abstract event EventHandler<InventoryEvent> ItemDoubleClicked;
		public abstract event EventHandler<InventoryEvent> ItemBeginDrag;
		public abstract event EventHandler<InventoryEvent> ItemEndDrag;
		public abstract event EventHandler<InventoryEvent> ItemMouseHoverBegin;
		public abstract event EventHandler<InventoryEvent> ItemMouseHoverEnd;
		#endregion

		public Inventory(int maxWidth = 24, int maxHeight = 6, int width = 4, int height = 6) {
			Position = Vector2.Zero;

			MaxWidth = maxWidth;
			MaxHeight = maxHeight;

			Width = width;
			Height = height;

			_Items = new List<InventoryPosition>();
			_CellPositionsOffsets = new List<Vector2>();
			for (int x = 0; x < MaxWidth; x++) {
				for (int y = 0; y < MaxHeight; y++) {
					if (x < Width && y < Height) {
						_CellPositionsOffsets.Add(new Vector2(x * (32f * stoneScale) - 2f * stoneScale * x, y * (32f * stoneScale) - 2f * stoneScale * y));
					}
				}
			}
		}

		public virtual void Draw(SpriteBatch spriteBatch) {
			if (AktivCell == null) {//one time creation
				AktivCell = TextureHelper.CreateTextureWidthBoarder(32, 32, Color.Black, 2, Color.White, spriteBatch.GraphicsDevice);
				InaktiveCell = TextureHelper.CreateTextureWidthBoarder(32, 32, Color.Gray, 2, Color.White, spriteBatch.GraphicsDevice);
				GraphicsDevice = spriteBatch.GraphicsDevice;
			}

			for (int x = 0; x < MaxWidth; x++) {
				for (int y = 0; y < MaxHeight; y++) {
					if (x < Width && y < Height) {
						spriteBatch.Draw(AktivCell, new RectangleF(Position.X + x * (AktivCell.Width * stoneScale) - 2f * stoneScale * x, Position.Y + y * (AktivCell.Height * stoneScale) - 2f * stoneScale * y, AktivCell.Width * stoneScale, AktivCell.Height * stoneScale), Color.White * transparent);
					} else {
						spriteBatch.Draw(InaktiveCell, new RectangleF(Position.X + x * (InaktiveCell.Width * stoneScale) - 2f * stoneScale * x, Position.Y + y * (InaktiveCell.Height * stoneScale) - 2f * stoneScale * y, InaktiveCell.Width * stoneScale, InaktiveCell.Height * stoneScale), Color.White * (transparent - 0.2f));
					}
				}
			}

			foreach (InventoryPosition item in Items) {
				if (item.back == null) {
					item.back = TextureHelper.CreateTexture(item.Item.Texture.Width, item.Item.Texture.Height, Color.Blue, spriteBatch.GraphicsDevice);
				}
				spriteBatch.Draw(item.back, item.Position + Position, Color.White * 0.45f);
				spriteBatch.Draw(item.Item.Texture, item.Position + Position, Color.White);
			}
		}

		public abstract void Update(GameTime gameTime);

		public virtual Rectangle GetDimension() {
			return new Rectangle((int)Position.X, (int)Position.Y, (int)((32f - 2f) * stoneScale * (MaxWidth - 1) + 32f * stoneScale), (int)((32f - 2f) * stoneScale * (MaxHeight - 1) + 32f * stoneScale));
		}

		public virtual Rectangle GetActiveDimension() {
			return new Rectangle((int)Position.X, (int)Position.Y, (int)((32f - 2f) * stoneScale * (Width - 1) + 32f * stoneScale), (int)((32f - 2f) * stoneScale * (Height - 1) + 32f * stoneScale));
		}

		#region Get- & SetItem
		//Gibt aus ob das hinzufügen erfolgreich war
		public bool AddItem(Item item) {
			if (item != null) {
				List<Vector2> free = GetFreeCellOffets();
				//filtern aller Positionen in dem das Schwert beim Aktiven Inventar hineinpasst 
				List<Rectangle> a = free.Select(f => new Rectangle((int)(f.X + (3f * stoneScale) + Position.X), (int)(f.Y + (3f * stoneScale) + Position.Y), item.Texture.Width, item.Texture.Height)).Where(f => Inside(GetActiveDimension(), f)).ToList();
				//welche positionen überschneiden sich nicht mit anderen Items
				List<Vector2> useAbleCells = new List<Vector2>();
				foreach (Rectangle rec in a) {
					var b = _Items.Select(i => new Rectangle((int)(i.Position.X + Position.X), (int)(i.Position.Y + Position.Y), i.Item.Texture.Width, i.Item.Texture.Height)).Where(i => i.Intersects(rec));
					if (b != null && b.Count() == 0) {
						useAbleCells.Add(ToVector2(rec));
					}
				}

				if (useAbleCells.Count > 0) {
					_Items.Add(new InventoryPosition(useAbleCells[0] - Position, item));

					return true;
				} else {
					return false;
				}
			}

			return false;
		}

		//Gibt aus ob das hinzufügen erfolgreich war
		public bool AddItem(Item item, Vector2 absolutScreenPosition) {
			if (item != null) {

				List<Vector2> free = GetFreeCellOffets();
				free = free.Where(f => absolutScreenPosition.X - Position.X >= f.X && absolutScreenPosition.Y - Position.Y >= f.Y).OrderByDescending(e => (e.X + e.Y)).ToList();

				//filtern aller Positionen in dem das Schwert beim Aktiven Inventar hineinpasst 
				List<Rectangle> a = free.Select(f => new Rectangle((int)(f.X + (3f * stoneScale) + Position.X), (int)(f.Y + (3f * stoneScale) + Position.Y), item.Texture.Width, item.Texture.Height)).Where(f => Inside(GetActiveDimension(), f)).ToList();
				//welche positionen überschneiden sich nicht mit anderen Items
				List<Rectangle> useAbleCells = new List<Rectangle>();
				foreach (Rectangle rec in a) {
					var b = _Items.Select(i => new Rectangle((int)(i.Position.X + Position.X), (int)(i.Position.Y + Position.Y), i.Item.Texture.Width, i.Item.Texture.Height)).Where(i => i.Intersects(rec));
					if (b != null && b.Count() == 0) {
						useAbleCells.Add(rec);
					}
				}

				//Untersuchen ob die Position passt
				Rectangle target = useAbleCells.Where(b => b.Intersects(VectorToRectangle(absolutScreenPosition))).FirstOrDefault();

				if (!target.IsEmpty) {
					_Items.Add(new InventoryPosition(ToVector2(target) - Position, item));

					return true;
				} else {
					return false;
				}
			}
			return false;
		}

		public Item GetItem(Vector2 position) {
			Vector2 offsetPositon = position - Position;

			InventoryPosition tmp = _Items.Where(i => VectorToRectangle(offsetPositon).Intersects(new Rectangle((int)i.Position.X, (int)i.Position.Y, i.Item.Texture.Width, i.Item.Texture.Height))).FirstOrDefault();

			if (tmp == null) {
				return null;
			} else {
				return tmp.Item;
			}
		}

		public InventoryPosition GetItemInventoryPosition(Vector2 position) {
			Vector2 offsetPositon = position - Position;

			InventoryPosition tmp = _Items.Where(i => VectorToRectangle(offsetPositon).Intersects(new Rectangle((int)i.Position.X, (int)i.Position.Y, i.Item.Texture.Width, i.Item.Texture.Height))).FirstOrDefault();

			if (tmp == null) {
				return null;
			} else {
				return tmp;
			}
		}

		public void RemoveItem(Item item) {
			InventoryPosition tmp = _Items.Where(i => i.Item == item).FirstOrDefault();
			if (tmp != null) {
				_Items.Remove(tmp);
			}
		}

		#endregion

		private List<Vector2> GetFreeCellOffets() {
			return _CellPositionsOffsets.Where(c => !_Items.Where(i => new Rectangle((int)(i.Position.X), (int)(i.Position.Y), i.Item.Texture.Width, i.Item.Texture.Height).Intersects(VectorToRectangle(c + 16f))).Any()).ToList();
		}

		#region Hilfen
		private Rectangle VectorToRectangle(Vector2 vector) {
			return new Rectangle((int)vector.X, (int)vector.Y, 1, 1);
		}

		private bool Inside(Rectangle full, Rectangle target) {
			return target.X >= full.X && target.Y >= full.Y && target.BottomRight.X <= full.BottomRight.X && target.BottomRight.Y <= full.BottomRight.Y;
		}

		private static Vector2 ToVector2(Rectangle p) {
			return new Vector2(p.X, p.Y);
		}
		#endregion

		public void SetInventoryDimension(int width, int height) {
			Width = width;
			Height = height;

			_CellPositionsOffsets.Clear();
			for (int x = 0; x < MaxWidth; x++) {
				for (int y = 0; y < MaxHeight; y++) {
					if (x < Width && y < Height) {
						_CellPositionsOffsets.Add(new Vector2(x * (32f * stoneScale) - 2f * stoneScale * x, y * (32f * stoneScale) - 2f * stoneScale * y));
					}
				}
			}
		}
	}

	public class InventoryPosition {
		public Vector2 Position { get; set; }
		public Item Item { get; set; }
		// wird automatisch erstellt
		public Texture2D back { get; set; }

		public InventoryPosition(Vector2 position, Item item) {
			this.Position = position;
			this.Item = item;
		}
	}

	public class InventoryEvent : EventArgs {
		public Item Item { get; private set; }
		public Vector2 Position { get; private set; }
		public InventoryPosition InventoryPosition { get; private set; }
		public Vector2 OldPosition { get; private set; }

		public InventoryEvent(Item item, Vector2 position, InventoryPosition inventoryPosition, Vector2 oldPosition) {
			Item = item;
			Position = position;
			InventoryPosition = inventoryPosition;
			OldPosition = oldPosition;
		}
	}
}

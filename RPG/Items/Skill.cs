﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RPG.Items.Enum;
using RPG.Chars;
using System.Collections.ObjectModel;
using SharpDX;
using SharpDX.Toolkit.Graphics;

namespace RPG.Items {
	public class Skill {
		//SkillType - Bildung von Skillgruppen, sodass z.B. Prozent-Geschick-Steigerung nur einmal aufgerüstet werden kann
		//skill level - ungenutzt nur für die Wertigkeit der Fähigkeit pro SkillGruppe(SkillType) da
		//Beschreibung
		//Titel
		//Aussehen
		//Minimum Item Qualität
		//Verfügbar für welche AusrüstungsTypen
		//Minium Item Level
		//AffectType - Wie wird der Skill eingesetzt (verbessern der Statuswerte, automatischer Einsatz während des Kampfes, verbessern der Fähigkeiten nach einem Kampf und Verbessern der Spielspielrundeneigenschaften)

		public SkillType Type { get; private set; }
		public int Level { get; private set; }
		public string Description { get; set; }
		public string Title { get; private set; }

		public string Look { get; private set; } //hier stelle ich mir eine Liste vor die angibt welche Felder verwendet werden beim 4x4 grid 0, 1, 5, 6 ergibt den liegen Z-Stein beim Tetris (also ZellenIndex 0, 1, 5 und 6 belegt)
		public string OriginLook { get; private set; }
		public void SetNewLook(string look, string originLook) { Look = look; OriginLook = originLook; }

		public int MinimalItemLevel { get; private set; }

		public string Prefix { get; private set; }
		public string Surfix { get; private set; }

		public ReadOnlyCollection<ItemType> AvailableItemTyps { get { return new ReadOnlyCollection<ItemType>(_AvailableItemTyps); } }
		private List<ItemType> _AvailableItemTyps { get; set; }

		public Quality MinimalQualityType { get; private set; }
		public AffectType AffectType { get; private set; }

		private float? _value;
		public float Value {
			get {
				return _value.HasValue ? (float)_value.Value : 0f;
			}

			private set {
				if (!_value.HasValue || _value.Value == 0f) {
					_value = value;
				} else {
					throw new Exception("Skill Value");
				}
			}
		}
		public float Chance { get; private set; }

		public int GridWidth { get; private set; }
		public int GridHeight { get; private set; }
		public Color StoneColor = Color.White;
		public Color BoarderColor = Color.Black;
		private GraphicsDevice graphicDevice;

		private const int StoneDimension = 32;

		// wird automatisch erstellt
		public Texture2D StoneTexture { get; private set; }

		public int MinItemLevel() { return MinimalItemLevel; }

		public override bool Equals(object obj) {
			return obj.GetType() == GetType() ? this.Equals((Skill)obj) : false; ;
		}

		public bool Equals(Skill obj) {
			return obj.Type == Type && obj.Level == Level && obj.AffectType == AffectType && obj.Value == Value && obj.Title == Title && obj.Description == Description && obj.Chance == Chance;
		}

		#region Konstruktor
		public Skill(float value, float chance, string title, string prefix, string surfix, SkillType type, int level, int minItemLevel, Quality minQuality, AffectType affectType, string look, string description, List<ItemType> availableItemTyps) {
			Title = title;
			Type = type;
			Level = level;
			MinimalItemLevel = minItemLevel;
			MinimalQualityType = minQuality;
			AffectType = affectType;
			OriginLook = Look = look;
			Description = description;
			_AvailableItemTyps = availableItemTyps;

			Value = value;
			Chance = chance;

			Prefix = prefix;
			Surfix = surfix;
		}

		public Skill(float value, float chance, string title, string prefix, string surfix, SkillType type, int level, int minItemLevel, Quality minQuality, AffectType affectType, string look, string description, params ItemType[] availableItemTyps) {
			Title = title;
			Type = type;
			Level = level;
			MinimalItemLevel = minItemLevel;
			MinimalQualityType = minQuality;
			AffectType = affectType;
			OriginLook = Look = look;
			Description = description;
			_AvailableItemTyps = availableItemTyps.ToList();

			Value = value;
			Chance = chance;

			Prefix = prefix;
			Surfix = surfix;
		}

		public Skill(float value, string title, string prefix, string surfix, SkillType type, int level, int minItemLevel, Quality minQuality, AffectType affectType, string look, string description, List<ItemType> availableItemTyps) {
			Title = title;
			Type = type;
			Level = level;
			MinimalItemLevel = minItemLevel;
			MinimalQualityType = minQuality;
			AffectType = affectType;
			OriginLook = Look = look;
			Description = description;
			_AvailableItemTyps = availableItemTyps;

			Value = value;
			Chance = 100f;

			Prefix = prefix;
			Surfix = surfix;
		}

		public Skill(float value, string title, string prefix, string surfix, SkillType type, int level, int minItemLevel, Quality minQuality, AffectType affectType, string look, string description, params ItemType[] availableItemTyps) {
			Title = title;
			Type = type;
			Level = level;
			MinimalItemLevel = minItemLevel;
			MinimalQualityType = minQuality;
			AffectType = affectType;
			OriginLook = Look = look;
			Description = description;
			_AvailableItemTyps = availableItemTyps.ToList();

			Value = value;
			Chance = 100f;

			Prefix = prefix;
			Surfix = surfix;
		}

		public Skill(string title, string prefix, string surfix, SkillType type, int level, int minItemLevel, Quality minQuality, AffectType affectType, string look, string description, List<ItemType> availableItemTyps) {
			Title = title;
			Type = type;
			Level = level;
			MinimalItemLevel = minItemLevel;
			MinimalQualityType = minQuality;
			AffectType = affectType;
			OriginLook = Look = look;
			Description = description;
			_AvailableItemTyps = availableItemTyps;

			Value = 0f;
			Chance = 100f;

			Prefix = prefix;
			Surfix = surfix;
		}

		public Skill(string title, string prefix, string surfix, SkillType type, int level, int minItemLevel, Quality minQuality, AffectType affectType, string look, string description, params ItemType[] availableItemTyps) {
			Title = title;
			Type = type;
			Level = level;
			MinimalItemLevel = minItemLevel;
			MinimalQualityType = minQuality;
			AffectType = affectType;
			OriginLook = Look = look;
			Description = description;
			_AvailableItemTyps = availableItemTyps.ToList();

			Value = 0f;
			Chance = 100f;

			Prefix = prefix;
			Surfix = surfix;
		}
		#endregion

		/// <summary>
		/// Als Basis werden nur StatusChangeSkills unterstützt.
		/// </summary>
		public virtual void PerformAction(Entity player) {
			if (AffectType == Enum.AffectType.StatusChangeSkill) {

			} else {
				throw new NotSupportedException();
			}
		}

		public void ChangeGridSize(int width, int height, string look) {
			GridWidth = width;
			GridHeight = height;
			Look = look;
		}

		public void ChangeStoneTexture(Texture2D texture) {
			StoneTexture = texture;
		}

		public void ChangeStoneRenderingColor(Color color) {
			StoneColor = color;
			if (graphicDevice != null) {
				StoneTexture = TextureHelper.CreateTextureWidthBoarder(StoneDimension, StoneDimension, StoneColor, 2, BoarderColor, graphicDevice);
			} else {
				StoneTexture = null;
			}
		}

		public void ChangeBorderColor(Color color) {
			BoarderColor = color;
			if (graphicDevice != null) {
				StoneTexture = TextureHelper.CreateTextureWidthBoarder(StoneDimension, StoneDimension, StoneColor, 2, BoarderColor, graphicDevice);
			} else {
				StoneTexture = null;
			}
		}

		public virtual void Draw(SpriteBatch spriteBatch) {
			if (StoneTexture == null) { //nicht schön aber selten!
				StoneTexture = TextureHelper.CreateTextureWidthBoarder(StoneDimension, StoneDimension, StoneColor, 2, BoarderColor, spriteBatch.GraphicsDevice);
				GridWidth = 4;
				GridHeight = 4;

				graphicDevice = spriteBatch.GraphicsDevice;
			}
		}

		public void SetValue(float value) {
			this.Value = value;
		}

		public void SetChance(float chance) {
			this.Chance = chance;
		}

		public override string ToString() {
			return String.Format("Skill Lvl: {0}, Value: {1}, Chance: {2}, Title: {3}", Level, Math.Round(Value, 2), Math.Round(Chance, 2), Title);
		}

		public Skill Clone() {
			Skill tmp = new Skill(this.Title, this.Prefix, this.Surfix, this.Type, this.Level, this.MinimalItemLevel, this.MinimalQualityType, this.AffectType, this.OriginLook, this.Description, AvailableItemTyps.ToArray());
			tmp = (Skill)this.MemberwiseClone();

			return tmp;
		}
	}
}

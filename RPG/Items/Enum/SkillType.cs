﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RPG.Items.Enum {
	public enum SkillType {
		AbsoluteStrength = 0,
		AbsoluteDexterity = 1,
		AbsoluteStamina = 2,
		AbsoluteLuck = 3,
		AbsoluteDefence = 4,
		AbsoluteDefenceValue = 5,
		AbsoluteCriticalHitChance = 6,
		AbsoluteHP = 7,
		AbsoluteEvade = 8,

		PercentageStrength = 9,
		PercentageDexterity = 10,
		PercentageStamina = 11,
		PercentageLuck = 12,
		PercentageDefence = 13,
		PercentageDefenceValue = 14,
		PercentageHP = 15,
		PercentageMagicFind = 16,

		AbsoluteMinWeaponDmg = 17,
		AbsoluteMaxWeaponDmg = 18,
		AbsoluteMinDamage = 19,
		AbsoluteMaxDamage = 20,
		AbsoluteWeaponDamage = 21,
		AbsoluteDamage = 22,
		AbsoluteCriticalDamage = 23,

		PercentageMinWeaponDmg = 24,
		PercentageMaxWeaponDmg = 25,
		PercentageMinDamage = 26,
		PercentageMaxDamage = 27,
		PercentageWeaponDamage = 28,
		PercentageDamage = 29,

		AbsoluteBattleRounds = 30,
		AbsoluteBattleTime = 31,
		AbsoluteHealInRound = 32,
		AbsoluteDamageInRound = 33,
		AbsoluteBattleRoundsInRound = 34,
		AbsoluteBattleTimeInRound = 35,
		AbsoluteNoDeath = 36,
		AbsoluteMoreGold = 37,
		AbsoluteMoreEXP = 38,
		AbsoluteMonsterSpawn = 39,
		PercentageHealInRound = 48,

		PercentageMoreGold = 40,
		PercentageMoreEXP = 41,
		PercentageMultiAttack = 42,
		PercentageFasterWalk = 43,
		PercentageSlowerWalk = 44,
		PercentageFastMonsterfights = 45,
		PercentageSlowMonsterfights = 46,

		AbsoluteAllStats = 47
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RPG.Items.Enum {
	public enum ItemType {
		Weapon = 0,
		Head = 1,
		Body = 2,
		Shoe = 3,
		Schild = 4
	}
}

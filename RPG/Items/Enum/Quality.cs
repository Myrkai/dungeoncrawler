﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RPG.Items.Enum {
	public enum Quality {
		Normal = 0,
		Magic = 1,
		Rare = 2,
		Legendary = 3
	}
}

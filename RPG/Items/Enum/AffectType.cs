﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RPG.Items.Enum {
	public enum AffectType {
		StatusChangeSkill = 0,
		BattleSkill = 1,
		AfterBattleSkill = 2,
		PlayRoundSkill = 3
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RPG.Items.Enum;
using SharpDX.Toolkit.Graphics;

namespace RPG.Items {
	public class Weapon : Item {
		public int MinDamage { get; private set; }
		public int MaxDamage { get; private set; }

		public Weapon(int minDamage, int maxDamage, string title, int iLvl, Quality quality, Texture2D texture, string textureAsset, uint cost)
			: base(title, iLvl, quality, ItemType.Weapon, texture, textureAsset, cost) {
			MinDamage = minDamage;
			MaxDamage = maxDamage;
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RPG.Chars.Enum {
	public enum StatisticType {
		MonstersKilled,
		Deaths,
		RoundsPlayed,
		GoldCollected,
		EXPCollected
	}
}

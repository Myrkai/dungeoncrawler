﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RPG.Chars.Enum {
	public enum StatType {
		BaseStrength = 1, //Grundstärke
		BaseDexterity = 2,
		BaseStamina = 3,
		BaseLuck = 4,
		BaseHP = 5,
		BaseDefence = 6,
		BaseDefenceValue = 7,
		BaseCriticalHitChance = 8,
		BaseEvade = 9,
		BaseMagicFinde = 10,
		BaseMinWeaponDmg = 11,
		BaseMaxWeaponDmg = 12,
		BaseMinDamage = 13,
		BaseMaxDamage = 14,
		BaseCriticalDmg = 15,

		BonusStrength = 100,//Stärkeverbesserung durch Skills/Ausrüstung
		BonusDexterity = 200,
		BonusStamina = 300,
		BonusLuck = 400,
		BonusHP = 500,
		BonusDefence = 600,
		BonusDefenceValue = 700,
		BonusCriticalHitCHance = 800,
		BonusEvade = 900,
		BonusMagicFind = 1000,
		BonusMinWeaponDmg = 1100,
		BonusMaxWeaponDmg = 1200,
		BonusMinDamage = 1300,
		BonusMaxDamage = 1400,
		BonusCriticalDmg = 1500,

		Strength = 10000,//Gesamtstärke (Base + Bonus)
		Dexterity = 20000,
		Stamina = 30000,
		Luck = 40000,
		HP = 50000,
		Defence = 60000,
		DefenceValue = 70000,
		CriticalHitChance = 80000,
		Evade = 90000,
		MagicFind = 100000,
		MinWeaponDmg = 110000,
		MaxWeaponDmg = 120000,
		MinDamage = 130000,
		MaxDamage = 140000,
		CriticalDmg = 150000
	}
}

// TODO es gibt noch ein Bug mit den Fähigkeiten auf den Gegenstände/Ausgerüsteten. Diese verändern sich nach einem Level up.
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RPG.Chars.Enum;
using RPG.Utility;
using SharpDX.Toolkit.Diagnostics;

namespace RPG.Chars {
	public class ClassType {
		public ReadOnlyDictionary<StatType, float> StatBonus { get { return new ReadOnlyDictionary<StatType, float>(_StatBonus); } }
		private Dictionary<StatType, float> _StatBonus { get; set; }

		public string Title { get; private set; }

		public ClassType(string title) {
			Title = title;

			_StatBonus = new Dictionary<StatType, float>();

			foreach (string type in Extensions.EnumToList<StatType>()) {
				_StatBonus[type.ToEnum<StatType>()] = 0f;
			}
		}

		/// <summary>
		/// Hier kann der Bonus für die Klasse hinzugefügt werden
		/// </summary>
		/// <param name="type">Welche STat-Type istz gemeint</param>
		/// <param name="bonus">Angabe des Prozentualen Bonus (0.05 == 5%)</param>
		public void SetBonus(StatType type, float bonus) {
			if ((int)type > 100) {
				return;
			}
			_StatBonus[type] = bonus;
		}
	}
}

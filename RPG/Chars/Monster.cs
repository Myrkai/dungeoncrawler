﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MathEval;
using RPG.Items;
using System.Collections.ObjectModel;
using SharpDX.Toolkit.Graphics;
using SharpDX;
using RPG.Chars.Enum;

namespace RPG.Chars {
	public class Monster : Entity {
		#region fields
		public bool IsBoss { get; set; }
		public virtual int Gold() { return (int)Convert.ToDouble(Evaluator.EvaluateExpression("Pow(lvl, 1 + Log(lvl, 10)) * 7", "lvl", Level.ToString()).Replace(".", ",")); }
		public virtual int Gold(int baseAddon) { return (int)Convert.ToDouble(Evaluator.EvaluateExpression("Pow(lvl, 1 + Log(lvl, 10)) * (7 + base)", "lvl", Level.ToString(), "base", baseAddon.ToString()).Replace(".", ",")); }
		public virtual int EXP() { return (int)Convert.ToDouble(Evaluator.EvaluateExpression("((Pow(Max(Log(ExpStep, 10), 1), 6)) / 2) * (1 + Log(lvl, 10))", "lvl", Level.ToString(), "ExpStep", EXPTable.GetLevelUpStats(Level).EXPStep.ToString()).Replace(".", ",")); }
		public virtual int EXP(int baseAddon) { return (int)Convert.ToDouble(Evaluator.EvaluateExpression("((Pow(Max(Log(ExpStep, 10), 1), 6)) / (2 - base / 100)) * (1 + Log(lvl, 10))", "lvl", Level.ToString(), "ExpStep", EXPTable.GetLevelUpStats(Level).EXPStep.ToString(), "base", baseAddon.ToString()).Replace(".", ",")); }
		public virtual float Strength { get { return Convert.ToSingle(Evaluator.EvaluateExpression("(lvl * lvl % 4 / 2) + (lvl / 2)", "lvl", Level.ToString()).Replace(".", ",")); } }
		public virtual float Dexterity { get { return Convert.ToSingle(Evaluator.EvaluateExpression("(lvl * (lvl+1) % 4 / 2) + (lvl / 2)", "lvl", Level.ToString()).Replace(".", ",")); } }
		public virtual float Stamina { get { return Convert.ToSingle(Evaluator.EvaluateExpression("(lvl * (lvl+2) % 4 / 2) + (lvl / 2)", "lvl", Level.ToString()).Replace(".", ",")); } }
		public virtual float Luck { get { return Convert.ToSingle(Evaluator.EvaluateExpression("(lvl * (lvl+3) % 4 / 2) + (lvl / 2)", "lvl", Level.ToString()).Replace(".", ",")); } }

		public ReadOnlyCollection<Skill> UsedSkills { get { return new ReadOnlyCollection<Skill>(_usedSkills); } }
		private List<Skill> _usedSkills { get; set; }

		public Texture2D Texture { get; set; }
		public float DropChanceMagic { get; private set; }
		public float DropChanceRare { get; private set; }
		public float DropChanceLegendary { get; private set; }

		public ReadOnlyCollection<Item> DropAbleBaseItems { get { return new ReadOnlyCollection<Item>(_dropAbleBaseItems); } }
		private List<Item> _dropAbleBaseItems { get; set; }

		private ExpressionEvaluator Evaluator { get; set; }
		public EXPTable EXPTable { get; set; }
		#endregion

		#region constructor
		public Monster(bool isBoss, Texture2D texture, EXPTable expTable, float dropChanceMagic, float dropChanceRare, float dropChanceLegendary, string name, int level, Vector2 position)
			: base(name, level, null, position, 100) {
			Evaluator = new ExpressionEvaluator();

			IsBoss = isBoss;
			Texture = texture;
			EXPTable = expTable;
			DropChanceMagic = dropChanceMagic;
			DropChanceRare = dropChanceRare;
			DropChanceLegendary = dropChanceLegendary;

			_dropAbleBaseItems = new List<Item>();
			_usedSkills = new List<Skill>();

			base.CurrentHP = base.Stats[StatType.HP];
		}
		#endregion

		#region functions
		public void AddBaseDropableItem(Item item) {
			_dropAbleBaseItems.Add(item);
		}

		public void AddBaseDropableItem(List<Item> items) {
			_dropAbleBaseItems.AddRange(items);
		}

		public void AddSkill(Skill skill) {
			_usedSkills.Add(skill);
			base.AddUseSkill(skill);
		}

		public void AddSkill(List<Skill> skills) {
			_usedSkills.AddRange(skills);
			skills.ForEach(s => base.AddUseSkill(s));
		}
		#endregion
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RPG.Chars;
using RPG.Chars.Enum;
using System.Collections.ObjectModel;
using RPG.Items;
using MathEval;
using SharpDX;
using RPG.Items.Enum;
using RPG.Utility;
using SharpDX.Toolkit;
using SharpDX.Toolkit.Graphics;

namespace RPG {
	public class Entity {
		#region fields
		public ReadOnlyDictionary<StatType, float> Stats { get { return new ReadOnlyDictionary<StatType, float>(_Stats); } }
		private Dictionary<StatType, float> _Stats { get; set; }

		public ReadOnlyCollection<Skill> AvaiableSkills { get { return new ReadOnlyCollection<Skill>(_AvaiableSkills); } }
		private List<Skill> _AvaiableSkills { get; set; }

		public ReadOnlyCollection<Skill> CurrentSkills { get { return new ReadOnlyCollection<Skill>(_CurrentSkills); } }
		private List<Skill> _CurrentSkills { get; set; }

		public ReadOnlyDictionary<ItemType, Item> UsedItems { get { return new ReadOnlyDictionary<ItemType, Item>(_UsedItems); } }
		private Dictionary<ItemType, Item> _UsedItems { get; set; }

		public int Level { get; set; }
		public float CriticalHitChance { get { return Stats[StatType.CriticalHitChance]; } }
		public float CriticalDamage { get { return Stats[StatType.CriticalDmg]; } }
		public float Evade { get { return Stats[StatType.Evade]; } }
		public float MagicFind { get { return Stats[StatType.MagicFind]; } }

		public virtual float GetDamageReduce(int monsterLevel) { return Convert.ToSingle(Evaluator.EvaluateExpression("Max(100*((RWg-(2*Mlvl+6))/(RWg-(2*Mlvl+6)+Max(Mlvl*Mlvl*2, 150))), 0)", "RWg", Stats[StatType.DefenceValue].ToString().Replace(",", "."), "Mlvl", monsterLevel.ToString().Replace(",", ".")).Replace(".", ",")); }
		public float GetDamage(Random rnd) { return rnd.NextFloat(Stats[StatType.MinDamage], Stats[StatType.MaxDamage]); }

		public ClassType ClassType { get; private set; }
		public Vector2 Position { get; set; }
		public string Name { get; set; }
		public float CurrentHP { get; set; }
		public float MaxHP { get { return Stats[StatType.HP]; } }

		private ExpressionEvaluator Evaluator { get; set; }
		#endregion

		#region events
		public event EventHandler IsDead;
		#endregion

		#region contructor
		public Entity(string name, int level, ClassType classType, Vector2 position, float currentHP) {
			Evaluator = new ExpressionEvaluator();

			if (classType == null) {
				ClassType = new ClassType("Monster");
			} else {
				ClassType = classType;
			}

			Name = name;
			Level = level;
			Position = position;
			CurrentHP = currentHP;

			_Stats = new Dictionary<StatType, float>();
			_AvaiableSkills = new List<Skill>();
			_CurrentSkills = new List<Skill>();
			_UsedItems = new Dictionary<ItemType, Item>();

			foreach (string type in Extensions.EnumToList<StatType>()) {
				_Stats[type.ToEnum<StatType>()] = 0f;
			}

			foreach (string type in Extensions.EnumToList<ItemType>()) {
				_UsedItems[type.ToEnum<ItemType>()] = null;
			}
		}
		#endregion

		#region StatRefresh
		/// <summary>
		/// Es werden die Finalen Stauswerte errechnet (Base + Bonus).
		/// Der Bonusvalue wird hierbei nicht berechnet.
		/// Ein paar BasisStatus werden hierbei auch errechnet (z.B. HP)
		/// </summary>
		public virtual void CalcFinalStatValue(StatType baseStatType) {
			if ((int)baseStatType > 99) {
				throw new IndexOutOfRangeException("Bitte nur die basis Stats angeben");
			}

			switch (baseStatType) {
				case StatType.BaseDefenceValue: _Stats[baseStatType] = (float)Math.Ceiling(Convert.ToDouble(Evaluator.EvaluateExpression("def * (1 + Dex / 10)", "def", _Stats[StatType.Defence].ToString().Replace(",", "."), "Dex", _Stats[StatType.Dexterity].ToString().Replace(",", ".")).Replace(".", ","))); break;
				case StatType.BaseCriticalDmg: _Stats[baseStatType] = 50f; break;
				case StatType.BaseCriticalHitChance: _Stats[baseStatType] = Convert.ToSingle(Evaluator.EvaluateExpression("0.75 + 4 * Luck / 20 + Lvl * 0.05", "Luck", _Stats[StatType.Luck].ToString().Replace(",", "."), "Lvl", Level.ToString().Replace(",", ".")).Replace(".", ",")); break;
				case StatType.BaseEvade: _Stats[baseStatType] = Convert.ToSingle(Evaluator.EvaluateExpression("5 + Luck * 0.2", "Luck", _Stats[StatType.Luck].ToString().Replace(",", ".")).Replace(".", ",")); break;
				case StatType.BaseHP: _Stats[baseStatType] = Convert.ToSingle(Evaluator.EvaluateExpression("Aus * 80 + Lvl * 20 + 50", "Aus", _Stats[StatType.Stamina].ToString().Replace(",", "."), "Lvl", Level.ToString().Replace(",", ".")).Replace(".", ",")); break;
				case StatType.BaseMagicFinde: _Stats[baseStatType] = Convert.ToSingle(Evaluator.EvaluateExpression("(Luck - 1) * 0.75", "Luck", _Stats[StatType.Luck].ToString().Replace(",", ".")).Replace(".", ",")); break;
				case StatType.BaseMaxDamage: _Stats[baseStatType] = (float)Math.Ceiling(Convert.ToDouble(Evaluator.EvaluateExpression("weapMax * (1 + Str / 10)", "weapMax", _Stats[StatType.MaxWeaponDmg].ToString().Replace(",", "."), "Str", _Stats[StatType.Strength].ToString().Replace(",", ".")).Replace(".", ","))); break;
				case StatType.BaseMinDamage: _Stats[baseStatType] = (float)Math.Ceiling(Convert.ToDouble(Evaluator.EvaluateExpression("weapMin * (1 + Str / 10)", "weapMin", _Stats[StatType.MinWeaponDmg].ToString().Replace(",", "."), "Str", _Stats[StatType.Strength].ToString().Replace(",", ".")).Replace(".", ","))); break;
			}

			//berechnet den finalen Statuswert
			_Stats[((int)baseStatType * 10000).ToString().ToEnum<StatType>()] = _Stats[baseStatType] + _Stats[((int)baseStatType * 100).ToString().ToEnum<StatType>()] + (_Stats[baseStatType] * ClassType.StatBonus[baseStatType]);//Finalen Wert berechnen

			// todo 
			if (baseStatType == StatType.BaseStrength) { 
			
			}
		}

		/// <summary>
		/// Es werden die Finalen Stauswerte errechnet (Base + Bonus).
		/// Der Bonusvalue wird hierbei nicht berechnet.
		/// Ein paar BasisStatus werden hierbei auch errechnet (z.B. HP)
		/// </summary>
		public virtual void CalcFinalStatsValues() {
			foreach (string type in Extensions.EnumToList<StatType>().Where(e => (int)e.ToEnum<StatType>() < 100)) {
				CalcFinalStatValue(type.ToEnum<StatType>());
			}
		}

		/// <summary>
		/// Führt die Funktion CalcFinalStatsValues() aus. Zusätzlich werden vorher alle BonusStats neu berechnet.
		/// </summary>
		public virtual void RefreshAllStatValue() {
			foreach (string type in Extensions.EnumToList<StatType>().Where(e => (int)e.ToEnum<StatType>() >= 100 && (int)e.ToEnum<StatType>() < 10000)) {//Set BonusStats zu 0
				SetStat(type.ToEnum<StatType>(), 0f);
			}

			foreach (Skill skill in CurrentSkills) {//Neuberechnet der BonusStats
				skill.PerformAction(this);
			}

			CalcFinalStatsValues();

			_Stats[StatType.MinDamage] = Math.Min(_Stats[StatType.MinDamage], _Stats[StatType.MaxDamage]);
			_Stats[StatType.MaxDamage] = Math.Max(_Stats[StatType.MinDamage], _Stats[StatType.MaxDamage]);
		}
		#endregion

		#region ItemHandle
		/// <summary>
		/// Es wird ein Gegenstand angezogen.
		/// Dabei wird der alte Gegenstand komplett überschrieben.
		/// </summary>
		/// <param name="slotType"></param>
		/// <param name="item"></param>
		public virtual void WearItem(ItemType slotType, Item item) {
			Item oldItem = _UsedItems[slotType];
			if (oldItem != null) {
				if (oldItem.Type != ItemType.Weapon) {
					_Stats[StatType.BaseDefence] -= ((Armor)oldItem).Defence;
				}
				foreach (Skill skill in oldItem.AvaiableSkills) {
					_AvaiableSkills.Remove(skill);
					_CurrentSkills.Remove(skill);
				}
			}
			_UsedItems[slotType] = item;

			if (slotType == ItemType.Weapon) {
				_Stats[StatType.BaseMinWeaponDmg] = item == null ? 0 : ((Weapon)item).MinDamage;
				_Stats[StatType.BaseMaxWeaponDmg] = item == null ? 0 : ((Weapon)item).MaxDamage;
			} else {
				_Stats[StatType.BaseDefence] += item == null ? 0 : ((Armor)item).Defence;
			}

			if (item != null) {
				_AvaiableSkills.AddRange(item.AvaiableSkills);
			}

			RefreshAllStatValue();
		}
		#endregion

		/// <summary>
		/// Aktualierungsmethode
		/// </summary>
		public virtual void Update(GameTime gameTime) {
			if (CurrentHP < 0 && IsDead != null) {
				IsDead(this, null);
			}
		}

		/// <summary>
		/// Zeichnenoperation
		/// </summary>
		public virtual void Draw(SpriteBatch spriteBatch) {

		}

		/// <summary>
		/// Setzte die Eigenschaft des Status
		/// </summary>
		public void SetStat(StatType type, float value) {
			int valueInt = (int)value;
			_Stats[type] = valueInt;
		}

		/// <summary>
		/// Setzte die Eigenschaft des Status
		/// </summary>
		public void SetBaseStat(StatType type, float value) {
			if ((int)type > 100) {
				return;
			}
			
			int valueInt = (int)value;
			_Stats[type] = valueInt;
		}

		/// <summary>
		/// Addiert einen Wert zum Status hinzu
		/// </summary>
		public void AddStat(StatType type, float value) {
			int valueInt = (int)Math.Ceiling(value);
			_Stats[type] += valueInt;
		}

		/// <summary>
		/// Ein Skill wird ausgerüstet.
		/// </summary>
		public void AddUseSkill(Skill skill) {
			_CurrentSkills.Add(skill);
		}

		/// <summary>
		/// Ausgerüsteter Skill wird wieder entfernt
		/// </summary>
		public void RemoveUseSkill(Skill skill) {
			_CurrentSkills.Remove(skill);
		}
	}
}

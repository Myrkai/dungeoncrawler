﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RPG.Sprites;
using SharpDX;
using SharpDX.Toolkit;
using SharpDX.Toolkit.Graphics;
using RPG.Items.Enum;
using RPG.Items;
using RPG.Chars.Enum;
using RPG.Utility;

namespace RPG.Chars {
	public class Player : Entity {
		#region fields
		public int CurrentEXP { get; private set; }
		public int NextLevelEXP { get { return EXPTable.GetLevelUpStats(base.Level).NextLevelEXP; } }
		public Animation PlayerAnimation { get; set; }
		public int StatPoints { get; set; }
		public int StatPointsPerLevelUp { get; private set; }
		public uint Gold { get; set; }

		public ReadOnlyDictionary<StatisticType, UInt64> Statistics { get { return new ReadOnlyDictionary<StatisticType, UInt64>(_Statistics); } }
		private Dictionary<StatisticType, UInt64> _Statistics { get; set; }

		public EXPTable EXPTable { get; private set; }
		#endregion

		#region constructor
		public Player(Animation animation, int statPointsPerLevelUp, EXPTable expTable, string name, int level, ClassType classType, Vector2 position, int currentHP) : base(name, level, classType, position, currentHP) {
			PlayerAnimation = animation;
			CurrentEXP = 0;
			StatPoints = 0;
			Gold = 0;
			StatPointsPerLevelUp = statPointsPerLevelUp;
			EXPTable = expTable;

			SetStat(StatType.BaseStrength, 1f);
			SetStat(StatType.BaseDexterity, 1f);
			SetStat(StatType.BaseStamina, 1f);
			SetStat(StatType.BaseLuck, 1f);
			CalcFinalStatsValues();

			_Statistics = new Dictionary<StatisticType, UInt64>();
			foreach (string type in Extensions.EnumToList<StatisticType>()) {
				_Statistics[type.ToEnum<StatisticType>()] = 0;
			}
		}
		#endregion

		#region itemHandle
		public override void WearItem(ItemType slotType, Item item) {
			base.WearItem(slotType, item);
		}
		#endregion

		public override void Draw(SpriteBatch spriteBatch) {
			base.Draw(spriteBatch);
			PlayerAnimation.Draw(spriteBatch, Position);
		}

		public override void Update(GameTime gameTime) {
			base.Update(gameTime);
			PlayerAnimation.Update(gameTime);
		}

		public void AddExp(int exp) {
			CurrentEXP += exp;
			if (CurrentEXP >= NextLevelEXP) {
				int nextLevel = Level;
				while (CurrentEXP >= EXPTable.GetLevelUpStats(nextLevel).NextLevelEXP) {
					nextLevel += 1;
				}

				int lvlDiff = nextLevel - Level;

				Level = nextLevel;
				StatPoints += lvlDiff * StatPointsPerLevelUp;
			}
		}

		public void SetExpAndLevel(int level, int currentEXP) {
			Level = level;
			CurrentEXP = currentEXP;
		}

		public void SetStatPointsPerLevelup(int points) {
			StatPointsPerLevelUp = points;
		}

		public void SetStatistic(StatisticType type, UInt64 value) {
			_Statistics[type] = value;
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RPG.Utility {
	public static class Extensions {
		public static List<string> EnumToList<T>() {
			return Enum.GetValues(typeof(T)).Cast<T>().Select(x => x.ToString()).ToList();
		}

		public static T ToEnum<T>(this string s) {
			return (T)Enum.Parse(typeof(T), s);
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpDX.Toolkit;
using SharpDX;
using SharpDX.Toolkit.Graphics;
using ShapeBlaster;

namespace SampleShooter {
	public class BlackHole : Entity {
		public override void Update(GameTime gametime) {
			scale = 1 + 0.1f * (float)Math.Sin(10 * gametime.TotalGameTime.TotalSeconds);
			Game1.Grid.ApplyImplosiveForce((float)Math.Sin(sprayAngle / 2) * 10 + 20, Position, 50);

			var entities = EntityManager.GetNearbyEntities(Position, 250f);

			foreach (Entity entity in entities) {
				if (entity is Enemy && !(entity as Enemy).IsActive)
					continue;

				// bullets are repelled by black holes and everything else is attracted
				if (entity is Bullet)
					entity.Velocity += (entity.Position - Position).ScaleTo(0.55f); //kraft des schwarzen loches
				else {
						var dPos = Position - entity.Position;
						var length = dPos.Length();

						entity.Velocity += dPos.ScaleTo(SharpDX.MathUtil.Lerp(2, 0, length / 500f));
				}
			}

			// The black holes spray some orbiting particles. The spray toggles on and off every quarter second.
			if ((Game1.GameTime.TotalGameTime.Milliseconds / 250) % 2 == 0) {
				Vector2 sprayVel = MathUtil.FromPolar(sprayAngle, rand.NextFloat(12, 15));
				Color color = ColorUtil.HSVToColor(5, 0.5f, 0.8f);  // light purple
				Vector2 pos = Position + 2f * new Vector2(sprayVel.Y, -sprayVel.X) + rand.NextVector2(4, 8);
				var state = new ParticleState() {
					Velocity = sprayVel,
					LengthMultiplier = 1,
					Type = ParticleType.Enemy
				};

				Game1.ParticleManager.CreateParticle(Art.LineParticle, pos, color, 190, 1.5f, state);
			}

			// rotate the spray direction
			sprayAngle -= MathHelper.TwoPi / 50f;
		}

		private static Random rand = new Random();

		private int hitpoints = 10;
		float scale = 1f;
		private float sprayAngle = 0;

		public BlackHole(Vector2 position) {
			Image = Art.BlackHole;
			Position = position;
			Radius = Image.Width / 2f;
		}

		public void WasShot() {
			hitpoints--;
			if (hitpoints <= 0)
				IsExpired = true;

			float hue = (float)((3 * Game1.GameTime.TotalGameTime.TotalSeconds) % 6);
			Color color = ColorUtil.HSVToColor(hue, 0.25f, 1);
			const int numParticles = 150;
			float startOffset = rand.NextFloat(0, MathHelper.TwoPi / numParticles);

			for (int i = 0; i < numParticles; i++) {
				Vector2 sprayVel = MathUtil.FromPolar(MathHelper.TwoPi * i / numParticles + startOffset, rand.NextFloat(8, 16));
				Vector2 pos = Position + 2f * sprayVel;

				float speed = 18f * (1f - 1 / rand.NextFloat(1f, 10f));
				var state = new ParticleState() {
					Velocity = rand.NextVector2(speed, speed),
					Type = ParticleType.IgnoreGravity,
					LengthMultiplier = 1f
				};

				Game1.ParticleManager.CreateParticle(Art.LineParticle, pos, color, 90, 1.5f, state);
			}
		}

		public void Kill() {
			hitpoints = 0;
			WasShot();
		}

		public override void Draw(SpriteBatch spriteBatch) {
			// make the size of the black hole pulsate
			spriteBatch.Draw(Image, Position, null, Color, Orientation, Size / 2f, scale, 0, 0);
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpDX.Toolkit;
using SharpDX.Toolkit.Graphics;
using SharpDX;
using ShapeBlaster;
using System.Threading;

namespace SampleShooter {
	public class Game1 : Game {

		int fps = 0;
		int lastFps = 0;
		public int FPS { get { return lastFps; } }
		float timer = 0f;
		SpriteFont font;

		bool renderFPS = true;
		bool fixFPS = true;
		bool fullscreen = false;

		public GraphicsDeviceManager graphics { get; set; }
		public SpriteBatch spriteBatch { get; set; }

		public static Game1 Instance { get; private set; }
		public static ParticleManager<ParticleState> ParticleManager { get; private set; }
		public static ViewportF Viewport { get { return Instance.GraphicsDevice.Viewport; } }
		public static Vector2 ScreenSize { get { return new Vector2(Viewport.Width, Viewport.Height); } }
		public static GameTime GameTime { get; private set; }
		public static Grid Grid { get; private set; }

		/*
		 Cycle:
		 Game1()
		 Init();
		 LoadContent();
		 Update();
		 Draw();
		 */
		public Game1()
			: base() {
			graphics = new GraphicsDeviceManager(this);
			Instance = this;

			Content.RootDirectory = "Content";

			if (fullscreen) {
				graphics.PreferredBackBufferWidth = 1920;
				graphics.PreferredBackBufferHeight = 1080;
			} else {
				graphics.PreferredBackBufferWidth = 1280;
				graphics.PreferredBackBufferHeight = 768;
			}

			IsFixedTimeStep = fixFPS;
			graphics.IsFullScreen = fullscreen;
		}

		protected override void Initialize() {
			base.Initialize();
			VirtualGamePad2.Initialize(this, GraphicsDevice);

			Window.Title = "Shooter";
			Window.IsMouseVisible = false;
			Window.AllowUserResizing = false;

			if (!fixFPS) {
				base.GraphicsDevice.Presenter.PresentInterval = PresentInterval.Immediate;
			}

			GraphicsDevice.SetViewport(new ViewportF(0, 0, graphics.PreferredBackBufferWidth, graphics.PreferredBackBufferHeight));
			MediaManager.Instance.RegisterSound("music", new MediaPlayer("Content/Sound/Music.mp3", 0.5f));
			MediaManager.Instance.Play("music", true);

			ParticleManager = new ParticleManager<ParticleState>(1024 * 1, ParticleState.UpdateParticle);

			const int maxGridPoints = 1600;
			Vector2 gridSpacing = new Vector2((float)Math.Sqrt(Viewport.Width * Viewport.Height / maxGridPoints));
			Grid = new Grid(new Rectangle((int)(-gridSpacing.X), (int)-gridSpacing.Y, (int)(ScreenSize.X + gridSpacing.X * 2), (int)(ScreenSize.Y + gridSpacing.Y * 2)),  gridSpacing);
		}

		protected override void Update(GameTime gameTime) {
			GameTime = gameTime;

			if (renderFPS) {
				fps += 1;
				timer += (float)gameTime.ElapsedGameTime.TotalSeconds;
				if (timer > 1f) {
					lastFps = fps;
					fps = 0;
					timer = 0f;
				}
			}

			Grid.Update();
			VirtualGamePad2.Update(gameTime);
			EntityManager.Update(gameTime);
			EnemySpawner.Update(gameTime);
			ParticleManager.Update();

			if (VirtualGamePad2.GetGameKeyState(GameKeys.Menue) == GameKeysState.Released) {
				Environment.Exit(0);
			}
		}

		protected override void Draw(GameTime gameTime) {
			base.Draw(gameTime);

			GraphicsDevice.Clear(GraphicsDevice.BackBuffer, Color.Black);

			spriteBatch.Begin(SpriteSortMode.Deferred, GraphicsDevice.BlendStates.Additive);
			Grid.Draw(spriteBatch);
			ParticleManager.Draw(spriteBatch);
			spriteBatch.End();

			spriteBatch.Begin(SpriteSortMode.Texture, GraphicsDevice.BlendStates.Additive);

			EntityManager.Draw(spriteBatch);

			if (renderFPS) {
				spriteBatch.DrawString(font, lastFps.ToString(), new Vector2(10, 60), Color.White);
			}

			if (PlayerStatus.IsGameOver) {
				string text = "Game Over\n" +
					"Your Score: " + String.Format("{0:n0}", PlayerStatus.Score) + "\n" +
					"High Score: " + String.Format("{0:n0}", PlayerStatus.HighScore);

				Vector2 textSize = font.MeasureString(text);
				spriteBatch.DrawString(font, text, ScreenSize / 2 - textSize / 2, Color.White);
			}

			spriteBatch.DrawString(font, "Lives: " + PlayerStatus.Lives.ToString(), new Vector2(5, 5), Color.White);
			DrawRightAlignedString("Score: " + String.Format("{0:n0}", PlayerStatus.Score), 5);
			DrawRightAlignedString("Multiplier: " + PlayerStatus.Multiplier.ToString(), 35);


			spriteBatch.Draw(Art.Pointer, VirtualGamePad2.MousePosition, Color.White);
			spriteBatch.End();
		}

		private void DrawRightAlignedString(string text, float y) {
			var textWidth = font.MeasureString(text).X;
			spriteBatch.DrawString(font, text, new Vector2(ScreenSize.X - textWidth - 5, y), Color.White);
		}

		protected override void LoadContent() {
			base.LoadContent();
			spriteBatch = new SpriteBatch(GraphicsDevice);
			Art.Load(Content);
			font = Content.Load<SpriteFont>(@"Fonts\Areal16.tk");

			EntityManager.Add(PlayerShip.Instance);
		}
	}
}

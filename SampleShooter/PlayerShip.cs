﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpDX;
using SharpDX.Toolkit;
using SharpDX.Toolkit.Graphics;
using ShapeBlaster;

namespace SampleShooter {
	public class PlayerShip : Entity {
		private static PlayerShip instance;
		private const float cooldown = 0.1f;
		private float cooldowntimer = 0f;
		private static Random rand = new Random();

		float timeUntilRespawn = 0f;
		public bool IsDead { get { return timeUntilRespawn > 0f; } }

		public static PlayerShip Instance {
			get {
				if (instance == null)
					instance = new PlayerShip();

				return instance;
			}
		}

		private PlayerShip() {
			Image = Art.Player;
			Position = Game1.ScreenSize / 2;
			Radius = 10;
		}

		public override void Update(GameTime gametime) {
			if (IsDead) {
				timeUntilRespawn -= (float)gametime.ElapsedGameTime.TotalSeconds;

				if (PlayerStatus.Lives == 0 && timeUntilRespawn <= 0f) {
					Game1.Grid.ApplyDirectedForce(new Vector3(0, 0, 5000), new Vector3(Position, 0), 50);
				}

				if (PlayerStatus.Lives == 0 && timeUntilRespawn <= 0f) {
					PlayerStatus.Reset();
				}

				return;
			}
			
			const float speed = 480f;
			Position += Velocity;
			Position = Vector2.Clamp(Position, Size / 2, Game1.ScreenSize - Size / 2);
			Velocity = speed * VirtualGamePad2.GetMovementDirection() * (float)gametime.ElapsedGameTime.TotalSeconds;

			if (Velocity.LengthSquared() > 0)
				Orientation = Velocity.ToAngle();

			var aim = VirtualGamePad2.GetMouseAimDirection();
			if (aim.LengthSquared() > 0 && cooldowntimer <= 0f) {
				cooldowntimer = cooldown;
				float aimAngle = aim.ToAngle();
				Quaternion aimQuat = MathUtil.CreateFromYawPitchRoll(0f, 0f, aimAngle);

				float randomSpread = rand.NextFloat(-0.04f, 0.04f) + rand.NextFloat(-0.04f, 0.04f);
				Vector2 vel = MathUtil.FromPolar(aimAngle + randomSpread, 11f);

				Vector2 offset = Vector2.Transform(new Vector2(25, -8), aimQuat);
				EntityManager.Add(new Bullet(Position + offset, vel));

				offset = Vector2.Transform(new Vector2(25, 8), aimQuat);
				EntityManager.Add(new Bullet(Position + offset, vel));

				MediaManager.Instance.PlayRandomShot();
			}

			if (cooldowntimer > 0f)
				cooldowntimer -= (float)gametime.ElapsedGameTime.TotalSeconds;

			MakeExhaustFire();
			PlayerStatus.Update(gametime);
		}

		private void MakeExhaustFire() {
			if (Velocity.LengthSquared() > 0.1f) {
				// set up some variables
				Orientation = Velocity.ToAngle();
				Quaternion rot = MathUtil.CreateFromYawPitchRoll(0f, 0f, Orientation);

				double t = Game1.GameTime.TotalGameTime.TotalSeconds;
				// The primary velocity of the particles is 3 pixels/frame in the direction opposite to which the ship is travelling.
				Vector2 baseVel = Velocity.ScaleTo(-3);
				// Calculate the sideways velocity for the two side streams. The direction is perpendicular to the ship's velocity and the
				// magnitude varies sinusoidally.
				Vector2 perpVel = new Vector2(baseVel.Y, -baseVel.X) * (0.6f * (float)Math.Sin(t * 10));
				Color sideColor = new Color(200, 38, 9);    // deep red
				Color midColor = new Color(255, 187, 30);   // orange-yellow
				Vector2 pos = Position + Vector2.Transform(new Vector2(-25, 0), rot);   // position of the ship's exhaust pipe.
				const float alpha = 0.7f;

				// middle particle stream
				Vector2 velMid = baseVel + rand.NextVector2(0, 1);
				Game1.ParticleManager.CreateParticle(Art.LineParticle, pos, Color.White * alpha, 60f, new Vector2(0.5f, 1), new ParticleState(velMid, ParticleType.Enemy));
				Game1.ParticleManager.CreateParticle(Art.Glow, pos, midColor * alpha, 60f, new Vector2(0.5f, 1), new ParticleState(velMid, ParticleType.Enemy));

				// side particle streams
				Vector2 vel1 = baseVel + perpVel + rand.NextVector2(0, 0.3f);
				Vector2 vel2 = baseVel - perpVel + rand.NextVector2(0, 0.3f);
				Game1.ParticleManager.CreateParticle(Art.LineParticle, pos, Color.White * alpha, 60f, new Vector2(0.5f, 1), new ParticleState(vel1, ParticleType.Enemy));
				Game1.ParticleManager.CreateParticle(Art.LineParticle, pos, Color.White * alpha, 60f, new Vector2(0.5f, 1), new ParticleState(vel2, ParticleType.Enemy));

				Game1.ParticleManager.CreateParticle(Art.Glow, pos, sideColor * alpha, 60f, new Vector2(0.5f, 1), new ParticleState(vel1, ParticleType.Enemy));
				Game1.ParticleManager.CreateParticle(Art.Glow, pos, sideColor * alpha, 60f, new Vector2(0.5f, 1), new ParticleState(vel2, ParticleType.Enemy));
			}
		}

		public override void Draw(SpriteBatch spriteBatch) {
			if (!IsDead)
				base.Draw(spriteBatch);
		}

		public void Kill() {
			PlayerStatus.RemoveLife();
			PlayerStatus.ResetMultiplier();

			timeUntilRespawn = PlayerStatus.IsGameOver ? 8f : 1f;

			Color yellow = new Color(0.8f, 0.8f, 0.4f);

			for (int i = 0; i < 1200; i++) {
				float speed = 18f * (1f - 1 / rand.NextFloat(1f, 10f));
				Color color = Color.Lerp(Color.White, yellow, rand.NextFloat(0, 1));
				var state = new ParticleState() {
					Velocity = rand.NextVector2(speed, speed),
					Type = ParticleType.None,
					LengthMultiplier = 1
				};

				Game1.ParticleManager.CreateParticle(Art.LineParticle, Position, color, 190, 1.5f, state);
			}
		}
	}
}

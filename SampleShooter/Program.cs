﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace SampleShooter {
	static class Program {
		/// <summary>
		/// Der Haupteinstiegspunkt für die Anwendung.
		/// </summary>
		[STAThread]
		static void Main() {
			using (Game1 game = new Game1()) {
				game.Run();
			};
		}
	}
}

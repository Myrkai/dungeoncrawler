﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpDX.Toolkit;
using System.IO;

namespace SampleShooter {
	static class PlayerStatus {
		// amount of time it takes, in seconds, for a multiplier to expire.
		private const float multiplierExpiryTime = 0.35f;
		private const int maxMultiplier = 150;

		public static int Lives { get; private set; }
		public static int Score { get; private set; }
		public static int Multiplier { get; private set; }

		private static float multiplierTimeLeft;    // time until the current multiplier expires
		private static int scoreForExtraLife;       // score required to gain an extra life

		public static bool IsGameOver { get { return Lives == 0; } }
		public static int HighScore { get; set; }

		// Static constructor
		static PlayerStatus() {
			HighScore = LoadHighScore();
			Reset();
		}

		public static void Reset() {
			if (Score > HighScore)
				SaveHighScore(HighScore = Score);

			Score = 0;
			Multiplier = 1;
			Lives = 4;
			scoreForExtraLife = 500000;
			multiplierTimeLeft = 0;
		}

		public static void Update(GameTime gametime) {
			multiplierTimeLeft -= (float)gametime.ElapsedGameTime.TotalSeconds;
			if (Multiplier > 1) {
				// update the multiplier timer
				if (multiplierTimeLeft <= 0f) {
					multiplierTimeLeft = multiplierExpiryTime;
					//ResetMultiplier();
					Multiplier = Math.Max(1, Multiplier - 1);
				}
			}
		}

		public static void AddPoints(int basePoints) {
			if (PlayerShip.Instance.IsDead)
				return;

			Score += basePoints * Multiplier;
			while (Score >= scoreForExtraLife) {
				scoreForExtraLife += 500000;
				Lives++;
			}
		}

		public static void IncreaseMultiplier() {
			if (PlayerShip.Instance.IsDead)
				return;

			multiplierTimeLeft = multiplierExpiryTime;
			if (Multiplier < maxMultiplier)
				Multiplier++;
		}

		public static void ResetMultiplier() {
			Multiplier = 1;
		}

		public static void RemoveLife() {
			Lives--;
		}

		private const string highScoreFilename = "highscore.txt";

		private static int LoadHighScore() {
			// return the saved high score if possible and return 0 otherwise
			int score;
			return File.Exists(highScoreFilename) && int.TryParse(File.ReadAllText(highScoreFilename), out score) ? score : 0;
		}

		private static void SaveHighScore(int score) {
			File.WriteAllText(highScoreFilename, score.ToString());
		}
	}
}

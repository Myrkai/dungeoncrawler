﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpDX.Toolkit.Graphics;
using SharpDX.Toolkit.Content;
using SharpDX;

namespace SampleShooter {
	public static class Art {
		public static Texture2D Player { get; private set; }
		public static Texture2D Seeker { get; private set; }
		public static Texture2D Wanderer { get; private set; }
		public static Texture2D Bullet { get; private set; }
		public static Texture2D Pointer { get; private set; }
		public static Texture2D BlackHole { get; private set; }
		public static Texture2D LineParticle { get; private set; }
		public static Texture2D Glow { get; private set; }
		public static Texture2D Pixel { get; private set; }

		public static void Load(ContentManager content) {
			Player = content.Load<Texture2D>(@"Art\Player.png");
			Seeker = content.Load<Texture2D>(@"Art\Seeker.png");
			Wanderer = content.Load<Texture2D>(@"Art\Wanderer.png");
			Bullet = content.Load<Texture2D>(@"Art\Bullet.png");
			Pointer = content.Load<Texture2D>(@"Art\Pointer.png");
			BlackHole = content.Load<Texture2D>(@"Art\Black Hole.png");
			LineParticle = content.Load<Texture2D>(@"Art\Laser.png");
			Glow = content.Load<Texture2D>(@"Art\Glow.png");

			Pixel = Texture2D.New(Player.GraphicsDevice, 1, 1, PixelFormat.R8G8B8A8.UNorm, TextureFlags.ShaderResource, 1, SharpDX.Direct3D11.ResourceUsage.Dynamic);
			Pixel.SetData(new[] { Color.White });
		}
	}
}

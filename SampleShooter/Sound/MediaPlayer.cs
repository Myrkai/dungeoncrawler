﻿ ﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.InteropServices;
using System.Media;
using System.Net;
using System.Threading;
using CSCore.Codecs;
using CSCore.XAudio2;
using CSCore;
using SharpDX.MediaFoundation;
using SharpDX.Multimedia;
using NVorbis;
using CSCore.SoundOut;
using DungeonCrawler.Sound;
using CSCore.Streams;
using CSCore.Streams.Effects;

public class MediaPlayer {

	private IWaveSource _source;
	private XAudio2 _xaudio2;
	private XAudio2MasteringVoice _masteringVoice;
	private StreamingSourceVoice _streamingSourceVoice;
	public event EventHandler Stopped;
	private Uri uri;
	private string filename;

	private bool _playing;
	public bool IsPlaying { get { return _playing; } }

	public MediaPlayer(Uri uri, float volume) {
		if (!CodecFactory.Instance.GetSupportedFileExtensions().Contains(".ogg")) {
			CodecFactory.Instance.Register("ogg-vorbis", new CodecFactoryEntry(s => new NVorbisSource(s).ToWaveSource(), ".ogg"));
		}

		this.uri = uri;

		_source = CodecFactory.Instance.GetCodec(uri);

		_xaudio2 = XAudio2.CreateXAudio2();
		_masteringVoice = _xaudio2.CreateMasteringVoice();
		_streamingSourceVoice = StreamingSourceVoice.Create(_xaudio2, _source);
		_streamingSourceVoice.Stopped += new EventHandler(_streamingSourceVoice_Stopped);

		StreamingSourceVoiceListener.Default.Add(_streamingSourceVoice);

		_streamingSourceVoice.SetVolume(volume, XAudio2.CommitNow);
		_playing = false;
	}

	public MediaPlayer(string filename, float volume) {
		if (!CodecFactory.Instance.GetSupportedFileExtensions().Contains(".ogg")) {
			CodecFactory.Instance.Register("ogg-vorbis", new CodecFactoryEntry(s => new NVorbisSource(s).ToWaveSource(), ".ogg"));
		}

		this.filename = filename;

		_source = CodecFactory.Instance.GetCodec(filename);
		//_source = BuildASourceChainEcho();
		//_source = BuildASourceChain();

		_xaudio2 = XAudio2.CreateXAudio2();
		_masteringVoice = _xaudio2.CreateMasteringVoice();
		_streamingSourceVoice = StreamingSourceVoice.Create(_xaudio2, _source);
		_streamingSourceVoice.Stopped += new EventHandler(_streamingSourceVoice_Stopped);

		StreamingSourceVoiceListener.Default.Add(_streamingSourceVoice);

		_streamingSourceVoice.SetVolume(volume, XAudio2.CommitNow);
		_playing = false;
	}

	public void Play() {
		if (_source.Position != 0) {
			_streamingSourceVoice.Stop();
			_source.Position = 0;
			_streamingSourceVoice.Refill();
		}

		_streamingSourceVoice.Start();
		_playing = true;
	}

	private IWaveSource BuildASourceChainEcho() {
		IWaveSource fileSource = CodecFactory.Instance.GetCodec(filename);

		VolumeSource volumeSource = new VolumeSource(fileSource);
		PanSource panSource = new PanSource(volumeSource);

		IWaveSource panWaveSource = panSource.ToWaveSource();

		DmoEchoEffect echoEffect = new DmoEchoEffect(panWaveSource);

		volumeSource.Volume = 0.5f; //50% volume
		echoEffect.LeftDelay = 500; //500 ms
		echoEffect.RightDelay = 250; //250 ms

		return echoEffect;
	}

	private IWaveSource BuildASourceChain() {
		IWaveSource fileSource = CodecFactory.Instance.GetCodec(filename);

		VolumeSource volumeSource = new VolumeSource(fileSource);
		PanSource panSource = new PanSource(volumeSource);

		IWaveSource panWaveSource = panSource.ToWaveSource();

		DmoChorusEffect chrorusEffect = new DmoChorusEffect(panWaveSource);

		volumeSource.Volume = 0.5f; //50% volume
		chrorusEffect.Feedback = 0.5f;

		return chrorusEffect;
	}

	void _streamingSourceVoice_Stopped(object sender, EventArgs e) {
		if (Stopped != null) {
			Stopped(this, e);
		}
		_playing = false;
	}

	public void Dispos() {
		_streamingSourceVoice.Stop();

		StreamingSourceVoiceListener.Default.Remove(_streamingSourceVoice);
		if (_streamingSourceVoice != null) {
			_streamingSourceVoice.Dispose();
		}

		if (_masteringVoice != null)
			_masteringVoice.Dispose();
		if (_xaudio2 != null) {
			_xaudio2.StopEngine();
			_xaudio2.Dispose();
		}
		if (_source != null)
			_source.Dispose();
	}
}

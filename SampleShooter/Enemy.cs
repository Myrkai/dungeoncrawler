﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpDX.Toolkit.Graphics;
using SharpDX;
using SharpDX.Toolkit;
using ShapeBlaster;

namespace SampleShooter {
	class Enemy : Entity {

		public static Random rand = new Random();
		private float timeUntilStart = 1f;
		public bool IsActive { get { return timeUntilStart <= 0f; } }
		private List<IEnumerator<int>> behaviours = new List<IEnumerator<int>>();
		public int PointValue { get; private set; }

		public Enemy(Texture2D image, Vector2 position) {
			this.Image = image;
			Position = position;
			Radius = image.Width / 2f;
			Color = Color.Transparent;
			PointValue = 1;
		}

		public override void Update(GameTime gameTime) {
			const float speed = 60f;
			if (timeUntilStart <= 0f) {
				ApplyBehaviours();
			} else {
				timeUntilStart -= (float)gameTime.ElapsedGameTime.TotalSeconds;
				Color = Color.White * (1 - Math.Max(timeUntilStart, 0f) / 1f);
			}

			Position += Velocity * speed * (float)gameTime.ElapsedGameTime.TotalSeconds;
			Position = Vector2.Clamp(Position, Size / 2, Game1.ScreenSize - Size / 2);

			Velocity *= 0.8f;
		}

		public void WasShot() {
			PlayerStatus.AddPoints(PointValue);
			PlayerStatus.IncreaseMultiplier();
			IsExpired = true;

			float hue1 = rand.NextFloat(0, 6);
			float hue2 = (hue1 + rand.NextFloat(0, 2)) % 6f;
			Color color1 = ColorUtil.HSVToColor(hue1, 0.5f, 1);
			Color color2 = ColorUtil.HSVToColor(hue2, 0.5f, 1);

			for (int i = 0; i < 120; i++) {
				float speed = 18f * (1f - 1 / rand.NextFloat(1f, 10f));
				var state = new ParticleState() {
					Velocity = rand.NextVector2(speed, speed),
					Type = ParticleType.Enemy,
					LengthMultiplier = 1f
				};

				Color color = Color.Lerp(color1, color2, rand.NextFloat(0, 1));
				Game1.ParticleManager.CreateParticle(Art.LineParticle, Position, color, 190, 1.5f, state);
			}

			MediaManager.Instance.PlayRandomExplosion();
		}

		private void AddBehaviour(IEnumerable<int> behaviour) {
			behaviours.Add(behaviour.GetEnumerator());
		}

		private void ApplyBehaviours() {
			for (int i = 0; i < behaviours.Count; i++) {
				if (!behaviours[i].MoveNext())
					behaviours.RemoveAt(i--);
			}
		}

		public void HandleCollision(Enemy other) {
			var d = Position - other.Position;
			Velocity += 10 * d / (d.LengthSquared() + 1);
		}

		#region EnermyTypes
		public static Enemy CreateSeeker(Vector2 position) {
			var enemy = new Enemy(Art.Seeker, position);
			enemy.AddBehaviour(enemy.FollowPlayer());
			enemy.PointValue = 2;

			return enemy;
		}

		public static Enemy CreateSquare(Vector2 position) {
			var enemy = new Enemy(Art.Wanderer, position);
			enemy.AddBehaviour(enemy.MoveInASquare());

			return enemy;
		}

		public static Enemy CreateWanderer(Vector2 position) {
			var enemy = new Enemy(Art.Wanderer, position);
			enemy.AddBehaviour(enemy.MoveRandomly());
			return enemy;
		}
		#endregion

		#region Behaviours
		IEnumerable<int> FollowPlayer(float acceleration = 1f) {
			while (true) {
				if (!PlayerShip.Instance.IsDead)
					Velocity += (PlayerShip.Instance.Position - Position).ScaleTo(acceleration);

				if (Velocity != Vector2.Zero)
					Orientation = Velocity.ToAngle();

				yield return 0;
			}
		}

		IEnumerable<int> MoveInASquare() {
			const int framesPerSide = 120;
			while (true) {
				// move right for 30 frames
				for (int i = 0; i < framesPerSide; i++) {
					Velocity = Vector2.UnitX;
					yield return 0;
				}

				// move down
				for (int i = 0; i < framesPerSide; i++) {
					Velocity = Vector2.UnitY;
					yield return 0;
				}

				// move left
				for (int i = 0; i < framesPerSide; i++) {
					Velocity = -Vector2.UnitX;
					yield return 0;
				}

				// move up
				for (int i = 0; i < framesPerSide; i++) {
					Velocity = -Vector2.UnitY;
					yield return 0;
				}
			}
		}

		IEnumerable<int> MoveRandomly() {
			float direction = rand.NextFloat(0f, (float)Math.PI * 2f);

			while (true) {
				direction += rand.NextFloat(-0.1f, 0.1f);
				direction = MathHelper.WrapAngle(direction);

				for (int i = 0; i < 6; i++) {
					Velocity += MathUtil.FromPolar(direction, 0.4f);
					Orientation -= 0.05f;

					var bounds = Game1.Viewport.Bounds;
					bounds.Inflate(-Image.Width, -Image.Height);

					// if the enemy is outside the bounds, make it move away from the edge
					if (!bounds.Contains(Position.ToPoint()))
						direction = (Game1.ScreenSize / 2 - Position).ToAngle() + rand.NextFloat(-MathHelper.PiOver2, MathHelper.PiOver2);

					yield return 0;
				}
			}
		}
		#endregion
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpDX.Toolkit.Graphics;
using SharpDX;
using SharpDX.Toolkit;

namespace SampleShooter {
	public abstract class Entity {
		protected Texture2D Image { get; set; }
		// The tint of the image. This will also allow us to change the transparency.
		protected Color Color = Color.White;

		public Vector2 Position { get; set; }
		public Vector2 Velocity { get; set; }
		public float Orientation { get; set; }
		public float Radius = 20;   // used for circular collision detection
		public bool IsExpired { get; set; }      // true if the entity was destroyed and should be deleted.

		public Vector2 Size {
			get {
				return Image == null ? Vector2.Zero : new Vector2(Image.Width, Image.Height);
			}
		}

		public abstract void Update(GameTime gametime);

		public virtual void Draw(SpriteBatch spriteBatch) {
			spriteBatch.Draw(Image, Position, null, Color, Orientation, Size / 2f, 1f, 0, 0);
		}
	}
}

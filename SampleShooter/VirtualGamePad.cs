﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpDX.Toolkit.Input;
using SharpDX.Toolkit;
using SharpDX.Toolkit.Graphics;
using SharpDX;
using SampleShooter;

namespace SampleShooter {
	public class GameKeyStateEmulator {
		public GameKeys Key;
		public GameKeysState KeyState;
	}

	public enum GameKeys {
		Menue,
		Mouse,
		Bomb
	}

	public enum GameKeysState {
		//Button is in a none state.
		None = 0,
		// The button is being pressed.
		Down = 1,
		// The button was pressed since last frame.
		Pressed = 2,
		//The button was released since last frame.
		Released = 4
	}

	public static class VirtualGamePad2 {
		static MouseManager mouseManager;
		static MouseState mouseState;
		static int screenWidth;
		static int screenHeight;
		static KeyboardManager keyBoardManager;
		static KeyboardState keyBoardState;
		static bool ignoreOneFrame = false;

		const Keys MenueKey = Keys.Escape;
		const Keys BombKey = Keys.Space;

		static GameKeyStateEmulator keyEmulator;

		public static Vector2 MousePosition { get { return new Vector2(mouseState.X * screenWidth, mouseState.Y * screenHeight); } }

		public static void Initialize(Game game, GraphicsDevice device) {
			mouseManager = new MouseManager(game);
			keyBoardManager = new KeyboardManager(game);

			var backbuffer = device.BackBuffer;
			screenWidth = backbuffer.Width;
			screenHeight = backbuffer.Height;
		}

		public static void Update(GameTime gameTime) {
			keyBoardManager.Update(gameTime);

			mouseState = mouseManager.GetState();
			keyBoardState = keyBoardManager.GetState();
			if (ignoreOneFrame) {
				ignoreOneFrame = false;
			}
			keyEmulator = null;
		}

		public static Vector2 GetMovementDirection() {
			Vector2 direction = Vector2.Zero;

			if (keyBoardState.IsKeyDown(Keys.A))
				direction.X -= 1;
			if (keyBoardState.IsKeyDown(Keys.D))
				direction.X += 1;
			if (keyBoardState.IsKeyDown(Keys.W))
				direction.Y -= 1;
			if (keyBoardState.IsKeyDown(Keys.S))
				direction.Y += 1;

			// Clamp the length of the vector to a maximum of 1.
			if (direction.LengthSquared() > 1)
				direction.Normalize();

			return direction;
		}

		public static Vector2 GetMouseAimDirection() {
			Vector2 direction = MousePosition - PlayerShip.Instance.Position;

			if (direction == Vector2.Zero)
				return Vector2.Zero;
			else
				return Vector2.Normalize(direction);
		}

		public static bool WasBombButtonPressed() {
			return GetGameKeyState(GameKeys.Bomb) == GameKeysState.Pressed || GetGameKeyState(GameKeys.Mouse) == GameKeysState.Pressed;
		}

		public static GameKeysState GetGameKeyState(GameKeys gameKey) {
			if (!ignoreOneFrame) {
				if (gameKey == GameKeys.Menue || keyEmulator != null && keyEmulator.Key == GameKeys.Menue) {
					if (keyBoardState.IsKeyDown(MenueKey) || keyEmulator != null && keyEmulator.KeyState == GameKeysState.Down) {
						return GameKeysState.Down;
					}

					if (keyBoardState.IsKeyPressed(MenueKey) || keyEmulator != null && keyEmulator.KeyState == GameKeysState.Pressed) {
						return GameKeysState.Pressed;
					}

					if (keyBoardState.IsKeyReleased(MenueKey) || keyEmulator != null && keyEmulator.KeyState == GameKeysState.Released) {
						ignoreOneFrame = true;
						return GameKeysState.Released;
					}
				}

				if (gameKey == GameKeys.Mouse || keyEmulator != null && keyEmulator.Key == GameKeys.Mouse) {
					if (mouseState.RightButton.Down || keyEmulator != null && keyEmulator.KeyState == GameKeysState.Down) {
						return GameKeysState.Down;
					}

					if (mouseState.RightButton.Pressed || keyEmulator != null && keyEmulator.KeyState == GameKeysState.Pressed) {
						return GameKeysState.Pressed;
					}

					if (mouseState.RightButton.Released || keyEmulator != null && keyEmulator.KeyState == GameKeysState.Released) {
						ignoreOneFrame = true;
						return GameKeysState.Released;
					}
				}

				if (gameKey == GameKeys.Bomb || keyEmulator != null && keyEmulator.Key == GameKeys.Bomb) {
					if (keyBoardState.IsKeyDown(BombKey) || keyEmulator != null && keyEmulator.KeyState == GameKeysState.Down) {
						return GameKeysState.Down;
					}

					if (keyBoardState.IsKeyPressed(BombKey) || keyEmulator != null && keyEmulator.KeyState == GameKeysState.Pressed) {
						return GameKeysState.Pressed;
					}

					if (keyBoardState.IsKeyReleased(BombKey) || keyEmulator != null && keyEmulator.KeyState == GameKeysState.Released) {
						ignoreOneFrame = true;
						return GameKeysState.Released;
					}
				}
			}

			return GameKeysState.None;
		}

		public static void SetGameKeyState(GameKeys key, GameKeysState state) {
			keyEmulator = new GameKeyStateEmulator();
			keyEmulator.Key = key;
			keyEmulator.KeyState = state;
		}
	}
}

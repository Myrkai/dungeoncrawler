﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SampleShooter {
	public class MathHelper {
		public static float WrapAngle(double angle) {
			angle = (double)Math.IEEERemainder((double)angle, 6.2831854820251465);
			if (angle <= -3.14159274f) {
				angle += 6.28318548f;
			} else {
				if (angle > 3.14159274f) {
					angle -= 6.28318548f;
				}
			}
			return (float)angle;
		}

		public const float PiOver2 = (float)(Math.PI / 2.0f);

		public static int TwoPi { get { return (int)(Math.PI * 2d); } }
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpDX;
using SharpDX.Toolkit;

namespace SampleShooter {
	static class EnemySpawner {
		static Random rand = new Random();
		static float inverseSpawnChance = 60f;
		static float inverseBlackHoleChance = 400f;

		public static void Update(GameTime gameTime) {
			if (!PlayerShip.Instance.IsDead && EntityManager.Count < 50) {
				if (rand.Next((int)inverseSpawnChance) == 0)
					EntityManager.Add(Enemy.CreateSeeker(GetSpawnPosition()));

				if (rand.Next((int)inverseSpawnChance) == 0)
					EntityManager.Add(Enemy.CreateWanderer(GetSpawnPosition()));

				if (EntityManager.BlackHoleCount < 2 && rand.Next((int)inverseBlackHoleChance) == 0)
					EntityManager.Add(new BlackHole(GetSpawnPosition()));
			}

			// slowly increase the spawn rate as time progresses
			if (inverseSpawnChance > 10f)
				inverseSpawnChance -= (2f * (float)gameTime.ElapsedGameTime.TotalSeconds);
		}

		private static Vector2 GetSpawnPosition() {
			Vector2 pos;
			do {
				pos = new Vector2(rand.Next((int)Game1.ScreenSize.X), rand.Next((int)Game1.ScreenSize.Y));
			}
			while (Vector2.DistanceSquared(pos, PlayerShip.Instance.Position) < 250 * 250);

			return pos;
		}

		public static void Reset() {
			inverseSpawnChance = 60f;
		}
	}
}

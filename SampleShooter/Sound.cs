﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using DungeonCrawler;

namespace SampleShooter {
	public class MediaManager {
		private static Random rnd = new Random();
		private static MediaManager _mediaManager;
		public static MediaManager Instance { get { if (_mediaManager == null) { _mediaManager = new MediaManager(); } return _mediaManager; } }

		private Dictionary<string, MediaPlayer> AvaiableSounds { get; set; }
		private Dictionary<string, bool> SoundsPlayable { get; set; }

		private MediaManager() {
			AvaiableSounds = new Dictionary<string, MediaPlayer>();
			SoundsPlayable = new Dictionary<string, bool>();

			Enumerable.Range(1, 8).ToList().ForEach(x => RegisterSound("explosion-0" + x.ToString(), new MediaPlayer("Content/Sound/explosion-0" + x.ToString() + ".wav", 0.1f)));
			Enumerable.Range(1, 4).ToList().ForEach(x => RegisterSound("shoot-0" + x.ToString(), new MediaPlayer("Content/Sound/shoot-0" + x.ToString() + ".wav", 0.15f)));
			Enumerable.Range(1, 8).ToList().ForEach(x => RegisterSound("spawn-0" + x.ToString(), new MediaPlayer("Content/Sound/spawn-0" + x.ToString() + ".wav", 0.1f)));

			Enumerable.Range(1, 4).ToList().ForEach(x => RegisterSound("shoot-00" + x.ToString(), new MediaPlayer("Content/Sound/shoot-0" + x.ToString() + ".wav", 0.15f)));
		}

		public bool RegisterSound(string key, MediaPlayer mediaPlayer) {
			if (AvaiableSounds.ContainsKey(key)) {
				return false;
			} else {
				AvaiableSounds.Add(key, mediaPlayer);
				SoundsPlayable.Add(key, true);
				AvaiableSounds[key].Stopped += new EventHandler(MediaManager_Stopped);
				return true;
			}
		}

		public bool Play(string key, bool looping = false) {
			return false;
			if (!AvaiableSounds.ContainsKey(key) || SoundsPlayable[key] == false) {
				return false;
			} else {
				AvaiableSounds[key].Play();
				SoundsPlayable[key] = false;
				if (looping) {
					AvaiableSounds[key].Stopped -= MediaManager_Stopped2;
					AvaiableSounds[key].Stopped += new EventHandler(MediaManager_Stopped2);
				}
				return true;
			}
		}

		void MediaManager_Stopped(object sender, EventArgs e) {
			SoundsPlayable[AvaiableSounds.Single(s => s.Value == sender).Key] = true;
		}

		void MediaManager_Stopped2(object sender, EventArgs e) {
			Play(AvaiableSounds.Single(s => s.Value == sender).Key, true);
		}

		public void Dispose() {
			foreach (MediaPlayer player in AvaiableSounds.Values) {
				player.Dispos();
			}
		}

		public void PlayRandomExplosion() {
			List<string> tmp = AvaiableSounds.Where(x => x.Value.IsPlaying == false && x.Key.StartsWith("explosion")).Select(x => x.Key).ToList();
			if (tmp.Count > 0) {
				Play(tmp[rnd.Next(tmp.Count)]);
			}
		}

		public void PlayRandomShot() {
			List<string> tmp = AvaiableSounds.Where(x => x.Value.IsPlaying == false && x.Key.StartsWith("shoot")).Select(x => x.Key).ToList();
			if (tmp.Count > 0) {
				Play(tmp[rnd.Next(tmp.Count)]);
			}
		}

		public void PlayRandomSpawn() {
			List<string> tmp = AvaiableSounds.Where(x => x.Value.IsPlaying == false && x.Key.StartsWith("spawn")).Select(x => x.Key).ToList();
			if (tmp.Count > 0) {
				Play(tmp[rnd.Next(tmp.Count)]);
			}
		}
	}
}
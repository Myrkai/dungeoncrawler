﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HeightmapGenerator;
using System.Drawing;

namespace MapGenerator {
	public class SimpleMap {
		private Heightmap heightMap;
		public Bitmap Bitmap;

		private Bitmap water = new Bitmap("resource/water.png");
		private Bitmap tree = new Bitmap("resource/tree.png");
		private Bitmap sand = new Bitmap("resource/sand.png");
		private Bitmap gras = new Bitmap("resource/gras.png");
		private Bitmap berg = new Bitmap("resource/berg.png");

		public SimpleMap(Int32 height, Int32 width) {
			heightMap = Heightmap.Instance;
			heightMap.SetDimensions(width, height);

			Bitmap = new Bitmap(width * 16, height * 16);
		}

		public void Generate() {
			heightMap.PerlinNoise(0, 0.2f, 2, 300, true);

			using (Graphics g = Graphics.FromImage(Bitmap)) {
				foreach (Int32 x in Enumerable.Range(1, heightMap.Width)) {
					foreach (Int32 y in Enumerable.Range(1, heightMap.Height)) {
						if (Edge_of_Map(x, y)) {
							g.DrawImage(water, new Point(x * 16, y * 16));
						} else if (heightMap.MapValues[x + y * heightMap.Width] <= 10) {
							g.DrawImage(water, new Point(x * 16, y * 16));
						//} else if (heightMap.MapValues[x + y * heightMap.Width] >= 170) {
						//    g.DrawImage(berg, new Point(x * 16, y * 16));
						//} else if (heightMap.MapValues[x + y * heightMap.Width] >= 1 && heightMap.MapValues[x + y * heightMap.Width] <= 2) {
						//    g.DrawImage(sand, new Point(x * 16, y * 16));
						//} else if (heightMap.MapValues[x + y * heightMap.Width] >= 30 && heightMap.MapValues[x + y * heightMap.Width] <= 60) {
						//    g.DrawImage(tree, new Point(x * 16, y * 16));
						} else {
							g.DrawImage(gras, new Point(x * 16, y * 16));
						}
					}
				}
			}

			Bitmap.Save("texture.png");
		}

		private bool Edge_of_Map(Int32 x, Int32 y) {
			return false;
		}
	}
}

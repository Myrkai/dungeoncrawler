﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using HeightmapGenerator;

namespace MapGenerator {
	public partial class Form1 : Form {
		public Form1() {
			InitializeComponent();
		}

		private void button1_Click(object sender, EventArgs e) {
			//Heightmap heightmap = Heightmap.Instance;
			//heightmap.SetDimensions(100, 100);

			//heightmap.ImprovedPerlinNoise(4, Guid.NewGuid().GetHashCode());
			//heightmap.Dilate();

			////heightmap.DiffusionLimitedAgregation(Guid.NewGuid().GetHashCode(), 0.70f, 6);
			////heightmap.Dilate();
			////heightmap.GaussianBlur();

			//pictureBox1.Image = heightmap.Texture;

			SimpleMap map = new SimpleMap(100, 100);
			map.Generate();
			pictureBox1.Image = map.Bitmap;
		}
	}
}

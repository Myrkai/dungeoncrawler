﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpDX;
using SharpDX.Toolkit.Graphics;
using System.IO;
using System.IO.Compression;
using System.Xml.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace DungeonCrawler {
	public static class Extensions {
		public static T ToEnum<T>(this string s) {
			return (T)Enum.Parse(typeof(T), s);
		}
		
		public static Vector2 ToVector2(this Point p) {
			return new Vector2(p.X, p.Y);
		}

		public static Rectangle ToRectangle(this Vector2 p) {
			return new Rectangle((int)p.X, (int)p.Y, 1, 1);
		}

		public static Vector2 ToVector2(this Rectangle p) {
			return new Vector2(p.X, p.Y);
		}

		/// <summary>
		/// Prüft ob Target sich innerhalb (vollständig) des Rechtecks befindet.
		/// </summary>
		public static bool Inside(this Rectangle rec, Rectangle target) {
			return target.X >= rec.X && target.Y >= rec.Y && target.BottomRight.X <= rec.BottomRight.X && target.BottomRight.Y <= rec.BottomRight.Y;
		}

		//Überprüft ob 2 Listen mit Vectoren sich überschneiden
		public static bool Intersect(this Rectangle[] recs, Rectangle[] targets) {
			foreach (Rectangle source in recs) {
				foreach (Rectangle target in targets) {
					if (source.Intersects(target)) {
						return true;
					}
				}
			}

			return false;
		}

		//Überprüft ob 2 Listen mit Vectoren sich überschneiden
		public static bool Intersect(this Vector2[] recs, Vector2[] targets) {
			foreach (Vector2 source in recs) {
				foreach (Vector2 target in targets) {
					if (source == target) {
						return true;
					}
				}
			}

			return false;
		}

		/// <summary>
		/// wandelt eine Liste von Vectors in Rectangles um
		/// </summary>
		public static Rectangle[] ToRectangle(this Vector2[] recs, int width, int height) {
			Rectangle[] value = new Rectangle[recs.Length];

			for (int i = 0; i < value.Length; i++) {
				value[i] = new Rectangle((int)recs[i].X, (int)recs[i].Y, width, height);
			}

			return value;
		}

		/// <summary>
		/// Wandelt das komplette Enum in eine Liste um
		/// </summary>
		public static List<string> EnumToList<T>(this Enum type) {
			return Enum.GetValues(typeof(T)).Cast<T>().Select(x => x.ToString()).ToList();
		}

		/// <summary>
		/// Wandelt das komplette Enum in eine Liste um
		/// </summary>
		public static List<C> EnumToList<T, C>(this Enum type) {
			return Enum.GetValues(typeof(T)).Cast<T>().Select(x => (C)((object)x)).ToList();
		}

		public static int Width(this string s, SpriteFont f) {
			return (int)f.MeasureString(s).X;
		}

		public static int Height(this string s, SpriteFont f) {
			return (int)f.MeasureString(s).Y;
		}

		public static T Random<T>(this List<T> list, Random rnd) where T : class {
			return (T)list[rnd.Next(0, list.Count)];
		}

		public static Vector2 Size(this Texture2D t) {
			return new Vector2(t.Width, t.Height);
		}

		public static Vector2 Size(this RectangleF t) {
			return new Vector2(t.Width, t.Height);
		}

		public static Vector2 Size(this Rectangle t) {
			return new Vector2(t.Width, t.Height);
		}

		/// <summary>
		/// Achtung binary ist buggy.
		/// </summary>
		public static T Load<T>(this string path, bool binary) {
			try {
				if (path != null && File.Exists(path)) {
					using (FileStream fs = new FileStream(path, FileMode.Open)) {
						using (GZipStream zip = new GZipStream(fs, CompressionMode.Decompress)) {
							if (!binary) {
								XmlSerializer xf = new XmlSerializer(typeof(T));
								return (T)xf.Deserialize(zip);
							} else {
								BinaryFormatter bf = new BinaryFormatter();
								return (T)bf.Deserialize(zip);
							}
						}
					}
				}
				return default(T);
			} catch {
				return default(T);
			}
		}

		/// <summary>
		/// Achtung binary ist buggy.
		/// </summary>
		public static bool Save<T>(this T obj, string path, bool binary) {
			try {
				DirectoryInfo di = new FileInfo(path).Directory;
				if (obj != null && di != null && di.Exists) {
					using (FileStream fs = new FileStream(path, FileMode.Create)) {
						using (GZipStream zip = new GZipStream(fs, CompressionMode.Compress)) {
							if (!binary) {
								XmlSerializer xf = new XmlSerializer(typeof(T));
								xf.Serialize(zip, obj);
							} else {
								BinaryFormatter bf = new BinaryFormatter();
								bf.Serialize(zip, obj);
							}
						}
					}
					return true;
				}
				return false;
			} catch (Exception ex) {
				return false;
			}
		}
	}
}

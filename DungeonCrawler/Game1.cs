﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpDX.Toolkit;
using SideProjekt.Mechanic.Log;
using SharpDX.Toolkit.Graphics;
using SharpDX;
using GUI.Screens;
using GUI;
using GUI.Controls;
using DungeonCrawler.Controls;
using System.Windows.Forms;
using System.Threading;
using DungeonCrawler.Sound;
using Core;
using DungeonCrawler.CoreElements;

namespace DungeonCrawler {
	public partial class Game1 : Game {

		int fps = 0;
		int lastFps = 0;
		public int FPS { get { return lastFps; } }
		float timer = 0f;

		public GraphicsDeviceManager graphics { get; set; }
		public SpriteBatch spriteBatch { get; set; }
		ScreenManager screenManager { get; set; }

		private ButtonControl VirtualESCButton { get; set; }

		public Game1()
			: base() {
			GameOptions.Load2("options");
			Resolution.Init();

			graphics = new GraphicsDeviceManager(this);
			screenManager = ScreenManager.Instance;
			screenManager.ScreenRemoved += new EventHandler<ScreenManagerEventArgs>(screenManager_ScreenRemoved);
			screenManager.ScreenRegistered += new EventHandler<ScreenManagerEventArgs>(screenManager_ScreenRegistered);
			screenManager.ScreenAdded += new EventHandler<ScreenManagerEventArgs>(screenManager_ScreenAdded);

			Content.RootDirectory = "Content";

			if (Resolution.SourceRatio == Resolution.CurrentScreenRation) { //Auflösung des Spiels (16:9)
				graphics.PreferredBackBufferWidth = Resolution.SourceWidth;
				graphics.PreferredBackBufferHeight = Resolution.SourceHeight;
				Resolution.SetTargetResolution(Resolution.SourceWidth, Resolution.SourceHeight);
			} else if (Resolution.TargetRatio == Resolution.CurrentScreenRation) { //Auslösung 4:3
				graphics.PreferredBackBufferWidth = Resolution.TargetWidth;
				graphics.PreferredBackBufferHeight = Resolution.TargetHeight;
			} else {
			    MessageBox.Show("Ausflösung wird nicht unterstützt.", "Seitenverhältniss nicht verfügbar", MessageBoxButtons.OK, MessageBoxIcon.Error);
			    Environment.Exit(0);
			}

			graphics.IsFullScreen = GameOptions.Fullscreen;
			this.IsFixedTimeStep = GameOptions.ForceFPSLimiter;

			if (GameOptions.ShowLiveDebuger) {
				Log.AddOutput(new ConsoleLogger());
				Log.AddOutput(new DebugLogger());
				Log.Add("Application start");
			}

			this.Exiting += new EventHandler<EventArgs>(Game1_Exiting);
		}

		void Window_Disposing(object sender, EventArgs e) {
			MediaManager.Instance.Dispose();
			Log.Dispose();
		}

		void Game1_Exiting(object sender, EventArgs e) {
			MediaManager.Instance.Dispose();
			Log.Dispose();
		}

		void screenManager_ScreenAdded(object sender, ScreenManagerEventArgs e) {
			Log.Add("ScreenManager: " + e.Screen.GetType().ToString().Replace("GUI.Screens.", "").Substring(0, Math.Max(e.Screen.GetType().ToString().Replace("GUI.Screens.", "").IndexOf("`"), 10)) + "(" + e.Title + ")" + " added");
		}

		void screenManager_ScreenRegistered(object sender, ScreenManagerEventArgs e) {
			Log.Add("ScreenManager: " + e.Screen.GetType().ToString().Replace("GUI.Screens.", "").Substring(0, Math.Max(e.Screen.GetType().ToString().Replace("GUI.Screens.", "").IndexOf("`"), 10)) + "(" + e.Title + ")" + " registered");
		}

		void screenManager_ScreenRemoved(object sender, ScreenManagerEventArgs e) {
			Log.Add("ScreenManager: " + e.Screen.GetType().ToString().Replace("GUI.Screens.", "").Substring(0, Math.Max(e.Screen.GetType().ToString().Replace("GUI.Screens.", "").IndexOf("`"), 10)) + "(" + e.Title + ")" + " removed");
		}

		partial void RegisterAllScreens(ScreenManager screenManager);
		protected override void Initialize() {
			base.Initialize();

			VirtualGamePad.Initialize(this, GraphicsDevice);

			if (!GameOptions.ForceFPSLimiter) {
				base.GraphicsDevice.Presenter.PresentInterval = PresentInterval.Immediate;
			}

			Window.Title = "Dungeon Crawler";
			Window.IsMouseVisible = true;
			Window.AllowUserResizing = false;

			using (new WaitingCoursor()) {
				RegisterAllScreens(screenManager);
			}			

			VirtualESCButton = new ButtonControl("ESC", Vector2.Zero, Content.Load<SpriteFont>(@"Fonts\Areal16.tk"), TextureHelper.CreateTextureWidthBoarder(50, 50, Color.DarkGray, 2, Color.Black, GraphicsDevice), Color.Black, () => VirtualGamePad.SetGameKeyState(GameKeys.Menue, GameKeysState.Released));
			this.Window.Disposing += new EventHandler<EventArgs>(Window_Disposing);
		}

		protected override void Update(GameTime gameTime) {
			if (GameOptions.ShowFPS) {
				fps += 1;
				timer += (float)gameTime.ElapsedGameTime.TotalSeconds;
				if (timer > 1f) {
					lastFps = fps;
					fps = 0;
					timer = 0f;
				}
			}

			VirtualGamePad.Update();
			if (GameOptions.ShowVirtualESCButton && VirtualGamePad.ClickReleased() && VirtualESCButton.GetDimension().Intersects(new Rectangle((int)VirtualGamePad.GetClickPoint().X, (int)VirtualGamePad.GetClickPoint().Y, 1, 1))) {
				VirtualESCButton.PerformClick(Vector2.Zero);
			}
			base.Update(gameTime);
			screenManager.Update(gameTime);
		}

		protected override void Draw(GameTime gameTime) {

			base.Draw(gameTime);

			GraphicsDevice.Clear(GraphicsDevice.BackBuffer, Color.Black);

			spriteBatch.Begin(SpriteSortMode.Immediate, GraphicsDevice.BlendStates.NonPremultiplied, GraphicsDevice.SamplerStates.PointClamp);

			screenManager.Draw(spriteBatch);
			if (GameOptions.ShowFPS) {
				spriteBatch.DrawString(Content.Load<SpriteFont>(@"Fonts\Areal16.tk"), lastFps.ToString(), new Vector2(10, 10), Color.White);
			}

			if (GameOptions.ShowVirtualESCButton) {
				VirtualESCButton.Draw(spriteBatch);
			}
			spriteBatch.End();
		}

		protected override void LoadContent() {
			base.LoadContent();
			spriteBatch = new SpriteBatch(GraphicsDevice);
		}

		internal void ResetScreens(CorePlayer player) {
			/*
			 * OK -> gameScreen
			 * OK -> inGameMenu
			 * OK -> statusScreen
			 * OK -> detailsScreen
			 * OK -> skillScreen
			 * OK -> statistikScreen
			 * OK? -> inventoryScreen
			 * OK? -> shopScreen
			 * OK -> upgradesScreen
			 * OK? -> bankScreen
			 * OK? -> battleScreen
			 */

			((EngineControl)((GameScreen<Game1>)screenManager.GetScreen(Game1.GAMESCREEN)).GetCustomControl("engine")).ResetEngine(player);
			ResetIngameMenuShopButton(player);
			((StatusScreen)screenManager.GetScreen(Game1.STATUSSCREEN)).CreateTextboxes(player);
			((DetailsScreen)screenManager.GetScreen(Game1.DETAILSSCREEN)).CreateInformations(player);
			((SkillScreen)screenManager.GetScreen(Game1.SKILLSCREEN)).Reset(player);
			((StatistikScreen)screenManager.GetScreen(Game1.STATISTIKSCREEN)).CreateInformations(player);
			((InventoryScreen)screenManager.GetScreen(Game1.INVENTORYSCREEN)).Reset(player);
			((ShopScreen)screenManager.GetScreen(Game1.SHOPSCREEN)).Reset(player);
			((UpgradesScreen)screenManager.GetScreen(Game1.UPGRADESSCREEN)).Reset(player);
			((BankScreen)screenManager.GetScreen(Game1.BANKSCREEN)).Reset(player);
			((BattleScreen)screenManager.GetScreen(Game1.BATTLESCREEN)).Reset(player);
		}
	}
}

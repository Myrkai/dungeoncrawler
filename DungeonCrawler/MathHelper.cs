﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing.Drawing2D;
using System.Drawing;
using SharpDX;
using Rectangle = SharpDX.Rectangle;
using Matrix = System.Drawing.Drawing2D.Matrix;
using Point = System.Drawing.Point;

namespace DungeonCrawler {
	public static class MathHelper {
		/// <summary>
		/// Lässt einen Punkt rotieren.
		/// </summary>
		/// <param name="v">welcher Punkt soll rotiert werden</param>
		/// <param name="rotation">um wieviel Grad soll er rotiert werden</param>
		/// <param name="oV">ausgangspunkt der Rotation (stell dir einen Zirkel vor und die eisenspitze ist dieser punkt)</param>
		/// <returns></returns>
		public static Vector2 RotateAPoint(Vector2 v, double rotation, Vector2 oV) {
			Matrix matrix = new Matrix();
			matrix.RotateAt((float)rotation, new Point(Convert.ToInt32(oV.X), Convert.ToInt32(oV.Y)));
			Point[] a = new Point[1];
			a[0] = new Point(Convert.ToInt32(v.X), Convert.ToInt32(v.Y));
			matrix.TransformPoints(a);

			Vector2 L = new Vector2(a[0].X, a[0].Y);

			return L;
		}

		/// <summary>
		/// Berechnet den Winkel anhand des Bewegungsvectors.
		/// Ausgehend davon das oben 0° sind und die Richtung im Uhrzeigersinn verläuft.
		/// </summary>
		public static double GetRotation(Vector2 Position) {
			Position.Normalize();
			float value = (float)Math.Atan2(Position.Y, Position.X);
			if (value > 0f) {
				return Math.Abs(MathUtil.RadiansToDegrees(value)) + 90d;
			} else {
				float angel = MathUtil.RadiansToDegrees(value);
				return (Math.Abs(angel) >= 90f ? 360d - Math.Abs(angel) : angel) + 90d;
			}
		}

		/// <summary>
		/// Rechnet den Winkel (Bogenwinkel). 
		/// Ausgehend davon das der Tower immer direkt nach obeb schießt und das dir Drehrichtung im Uhrzeigersinn verläuft (nach rechts)
		/// </summary>
		/// <param name="Position">Position des Zieles</param>
		/// <param name="CenterFromTower">Position des Tower. Mittelpunkt des Kreises</param>
		/// <returns></returns>
		public static double GetRotation(Vector2 Position, Vector2 CenterFromTower) {
			if (Position == Vector2.Zero && CenterFromTower == Vector2.Zero || Position.X == CenterFromTower.X && Position.Y < CenterFromTower.Y) {
				return 0;
			} //befindet sich direkt über dem tower

			double winkel = 0;
			//ziel ist rechts oben
			if (Position.X > CenterFromTower.X && Position.Y < CenterFromTower.Y) {
				float länge = (CenterFromTower.Y - Position.Y);
				if (CenterFromTower.Y - länge == Position.Y && CenterFromTower.X + länge == Position.X) {
					winkel += 45;
				} else if (CenterFromTower.Y - länge == Position.Y && CenterFromTower.X + länge > Position.X) { //winkel kleiner 45 grad
					winkel += 90 - Math.Atan((CenterFromTower.Y - Position.Y) / (Position.X - CenterFromTower.X)) * 180 / Math.PI;
				} else { //winkel größer 45 grad
					winkel += Math.Atan((Position.X - CenterFromTower.X) / (CenterFromTower.Y - Position.Y)) * 180 / Math.PI;
				}

				return winkel;
			} else {
				winkel += 90;
			}

			//ziel befindet sich direkt rechts vom tower
			if (CenterFromTower.Y == Position.Y && CenterFromTower.X < Position.X) {
				return 90;
			}

			//ziel befindet sich unten rechts
			if (Position.X > CenterFromTower.X && Position.Y > CenterFromTower.Y) {
				float länge = (Position.Y - CenterFromTower.Y);
				if (CenterFromTower.Y + länge == Position.Y && CenterFromTower.X + länge == Position.X) {
					winkel += 45;
				} else if (CenterFromTower.Y + länge == Position.Y && CenterFromTower.X + länge < Position.X) { //winkel kleiner 45 grad
					winkel += Math.Atan((Position.Y - CenterFromTower.Y) / (Position.X - CenterFromTower.X)) * 180 / Math.PI;
				} else { //winkel größer 45 grad
					winkel += 90 - Math.Atan((Position.X - CenterFromTower.X) / (Position.Y - CenterFromTower.Y)) * 180 / Math.PI;
				}

				return winkel;
			} else {
				winkel += 90;
			}

			//ziel befindet sich unten links
			if (Position.X < CenterFromTower.X && Position.Y > CenterFromTower.Y) {
				float länge = (Position.Y - CenterFromTower.Y);

				if (CenterFromTower.Y + länge == Position.Y && CenterFromTower.X - länge == Position.X) {
					winkel += 45;
				} else if (CenterFromTower.Y + länge == Position.Y && CenterFromTower.X - länge < Position.X) { //winkel kleiner 45 grad
					winkel += 90 - Math.Atan((Position.Y - CenterFromTower.Y) / (CenterFromTower.X - Position.X)) * 180 / Math.PI;
				} else { //winkel größer 45 grad
					winkel += Math.Atan((CenterFromTower.X - Position.X) / (Position.Y - CenterFromTower.Y)) * 180 / Math.PI;
				}

				return winkel;
			} else {
				winkel += 90;
			}

			//ziel befindet sich oben links
			if (Position.X < CenterFromTower.X && Position.Y < CenterFromTower.Y) {
				float länge = (CenterFromTower.Y - Position.Y);

				if (CenterFromTower.Y - länge == Position.Y && CenterFromTower.X - länge == Position.X) {
					winkel += 45;
				} else if (CenterFromTower.Y - länge == Position.Y && CenterFromTower.X - länge < Position.X) { //winkel kleiner 45 grad
					winkel += Math.Atan((CenterFromTower.Y - Position.Y) / (CenterFromTower.X - Position.X)) * 180 / Math.PI;
				} else { //winkel größer 45 grad
					winkel += 90 - Math.Atan((CenterFromTower.X - Position.X) / (CenterFromTower.Y - Position.Y)) * 180 / Math.PI;
				}

				return winkel;
			} else {
				winkel += 90;
			}

			return winkel;
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RPG.Items.Inventory;
using DungeonCrawler.CoreElements.Shop;
using DungeonCrawler.CoreElements;
using SharpDX.Toolkit.Content;
using RPG.Sprites;
using SharpDX.Toolkit.Graphics;
using RPG.Chars;
using SharpDX;
using DungeonCrawler.Controls;
using RPG.Chars.Enum;
using Core.Items;
using RPG.Items.Enum;
using RPG.Items;
using DungeonCrawler.SaveLoad;
using DungeonCrawler.CoreElements.Items.Skills;
using Core.BattleSystem;
using System.IO;

namespace DungeonCrawler {
	public class PlayerAccount : ISave {
		public Dictionary<string, Inventory> BankInventories { get; private set; }
		public List<ShopSkill> ShopSkills { get; private set; }
		private List<CorePlayer> AvaiablePlayers { get; set; }

		public PlayerAccount() {
			ShopSkills = new List<ShopSkill>();
			BankInventories = new Dictionary<string, Inventory>();
			AvaiablePlayers = new List<CorePlayer>();
		}

		public void AddPlayer(CorePlayer player) {
			AvaiablePlayers.Add(player);
		}

		public CorePlayer GetPlayer(int index) {
			return AvaiablePlayers[index];
		}

		public int GetAvaiablePlayerCount() {
			return AvaiablePlayers.Count;
		}

		public static PlayerAccount CreateNew(List<ShopSkill> shopSkills, ContentManager content, Random rnd, ItemManager itemManager) {
			PlayerAccount acc = new PlayerAccount();
			acc.ShopSkills = shopSkills;

			RPG.Chars.EXPTable expTable = new RPG.Chars.EXPTable();
			expTable.AddLevelUpStat(0, 0, 0);
			expTable.AddLevelUpStat(1, 1000, 1000);
			expTable.AddLevelUpStat(2, 2000, 1100);
			expTable.AddLevelUpStat(3, 3100, 1105);
			expTable.AddLevelUpStat(4, 4205, 1105);
			expTable.AddLevelUpStat(5, 5310, 1105);
			expTable.AddLevelUpStat(6, 6415, 1105);
			expTable.AddLevelUpStat(7, 7520, 1105);
			expTable.AddLevelUpStat(8, 8625, 1105);
			expTable.AddLevelUpStat(9, 9730, 1105);
			expTable.AddLevelUpStat(10, 10835, 1105);
			expTable.AddLevelUpStat(11, 11940, 1394);
			expTable.AddLevelUpStat(12, 13334, 1727);
			expTable.AddLevelUpStat(13, 15061, 2092);
			expTable.AddLevelUpStat(14, 17153, 2484);
			expTable.AddLevelUpStat(15, 19637, 2902);
			expTable.AddLevelUpStat(16, 22539, 3345);
			expTable.AddLevelUpStat(17, 25884, 3810);
			expTable.AddLevelUpStat(18, 29694, 4297);
			expTable.AddLevelUpStat(19, 33991, 4805);
			expTable.AddLevelUpStat(20, 38796, 5332);
			expTable.AddLevelUpStat(21, 44128, 5876);
			expTable.AddLevelUpStat(22, 50004, 6438);
			expTable.AddLevelUpStat(23, 56442, 7015);
			expTable.AddLevelUpStat(24, 63457, 7608);
			expTable.AddLevelUpStat(25, 71065, 8216);
			expTable.AddLevelUpStat(26, 79281, 8837);
			expTable.AddLevelUpStat(27, 88118, 9471);
			expTable.AddLevelUpStat(28, 97589, 10117);
			expTable.AddLevelUpStat(29, 107706, 10775);
			expTable.AddLevelUpStat(30, 118481, 11444);
			expTable.AddLevelUpStat(31, 129925, 12124);
			expTable.AddLevelUpStat(32, 142049, 12814);
			expTable.AddLevelUpStat(33, 154863, 13513);
			expTable.AddLevelUpStat(34, 168376, 14222);
			expTable.AddLevelUpStat(35, 182598, 14940);
			expTable.AddLevelUpStat(36, 197538, 15666);
			expTable.AddLevelUpStat(37, 213204, 16400);
			expTable.AddLevelUpStat(38, 229604, 17142);
			expTable.AddLevelUpStat(39, 246746, 17890);
			expTable.AddLevelUpStat(40, 264636, 18646);
			expTable.AddLevelUpStat(41, 283282, 19409);
			expTable.AddLevelUpStat(42, 302691, 20178);
			expTable.AddLevelUpStat(43, 322869, 20953);
			expTable.AddLevelUpStat(44, 343822, 21735);
			expTable.AddLevelUpStat(45, 365557, 22522);
			expTable.AddLevelUpStat(46, 388079, 23316);
			expTable.AddLevelUpStat(47, 411395, 24113);
			expTable.AddLevelUpStat(48, 435508, 24917);
			expTable.AddLevelUpStat(49, 460425, 25725);
			expTable.AddLevelUpStat(50, 486150, 26538);
			expTable.AddLevelUpStat(51, 512688, 27355);
			expTable.AddLevelUpStat(52, 540043, 28177);
			expTable.AddLevelUpStat(53, 568220, 29002);
			expTable.AddLevelUpStat(54, 597222, 29833);
			expTable.AddLevelUpStat(55, 627055, 30667);
			expTable.AddLevelUpStat(56, 657722, 31505);
			expTable.AddLevelUpStat(57, 689227, 32346);
			expTable.AddLevelUpStat(58, 721573, 33191);
			expTable.AddLevelUpStat(59, 754764, 34039);
			expTable.AddLevelUpStat(60, 788803, 34890);
			expTable.AddLevelUpStat(61, 823693, 35745);
			expTable.AddLevelUpStat(62, 859438, 36603);
			expTable.AddLevelUpStat(63, 896041, 37464);
			expTable.AddLevelUpStat(64, 933505, 38327);
			expTable.AddLevelUpStat(65, 971832, 39193);
			expTable.AddLevelUpStat(66, 1011025, 40062);
			expTable.AddLevelUpStat(67, 1051087, 40934);
			expTable.AddLevelUpStat(68, 1092021, 41807);
			expTable.AddLevelUpStat(69, 1133828, 42684);
			expTable.AddLevelUpStat(70, 1176512, 43563);
			expTable.AddLevelUpStat(71, 1220075, 44444);
			expTable.AddLevelUpStat(72, 1264519, 45326);
			expTable.AddLevelUpStat(73, 1309845, 46211);
			expTable.AddLevelUpStat(74, 1356056, 47098);
			expTable.AddLevelUpStat(75, 1403154, 47987);
			expTable.AddLevelUpStat(76, 1451141, 48878);
			expTable.AddLevelUpStat(77, 1500019, 49770);
			expTable.AddLevelUpStat(78, 1549789, 50665);
			expTable.AddLevelUpStat(79, 1600454, 51562);
			expTable.AddLevelUpStat(80, 1652016, 52460);
			expTable.AddLevelUpStat(81, 1704476, 53359);
			expTable.AddLevelUpStat(82, 1757835, 54259);
			expTable.AddLevelUpStat(83, 1812094, 55161);
			expTable.AddLevelUpStat(84, 1867255, 56066);
			expTable.AddLevelUpStat(85, 1923321, 56971);
			expTable.AddLevelUpStat(86, 1980292, 57878);
			expTable.AddLevelUpStat(87, 2038170, 58785);
			expTable.AddLevelUpStat(88, 2096955, 59695);
			expTable.AddLevelUpStat(89, 2156650, 60605);
			expTable.AddLevelUpStat(90, 2217255, 61517);
			expTable.AddLevelUpStat(91, 2278772, 62429);
			expTable.AddLevelUpStat(92, 2341201, 63343);
			expTable.AddLevelUpStat(93, 2404544, 64258);
			expTable.AddLevelUpStat(94, 2468802, 65173);
			expTable.AddLevelUpStat(95, 2533975, 66090);
			expTable.AddLevelUpStat(96, 2600065, 67008);
			expTable.AddLevelUpStat(97, 2667073, 67926);
			expTable.AddLevelUpStat(98, 2734999, 68846);
			expTable.AddLevelUpStat(99, 2803845, 69766);
			expTable.AddLevelUpStat(100, 2873611, 70688);

			//Krieger
			Animation animationWarrior = new Animation(content.Load<Texture2D>(@"Sprites\Animation\guy1.png"), @"Sprites\Animation\guy1.png", 3, 4, 0.22f);
			ClassType classTypeWarrior = new ClassType("Krieger");
			classTypeWarrior.SetBonus(StatType.BaseMinDamage, 0.05f);
			classTypeWarrior.SetBonus(StatType.BaseMaxDamage, 0.05f);
			acc.AddPlayer(new CorePlayer(acc, animationWarrior, expTable, "Myrkai", 1, classTypeWarrior, new Vector2(75f, 75f), 100, new InventoryControl()));

			//Zauberin
			Animation animationSorc = new Animation(content.Load<Texture2D>(@"Sprites\Animation\Oracle.png"), @"Sprites\Animation\Oracle.png", 3, 4, 0.22f);
			ClassType classTypeSorc = new ClassType("Zauberin");
			classTypeSorc.SetBonus(StatType.BaseHP, 0.1f);
			acc.AddPlayer(new CorePlayer(acc, animationSorc, expTable, "Celina", 1, classTypeSorc, new Vector2(75f, 75f), 100, new InventoryControl()));

			//Barbar
			Animation animationBarbar = new Animation(content.Load<Texture2D>(@"Sprites\Animation\warror1.png"), @"Sprites\Animation\warror1.png", 3, 4, 0.22f);
			ClassType classTypeBarBar = new ClassType("Barbar");
			classTypeBarBar.SetBonus(StatType.BaseDefence, 0.1f);
			acc.AddPlayer(new CorePlayer(acc, animationBarbar, expTable, "Conan", 1, classTypeBarBar, new Vector2(75f, 75f), 100, new InventoryControl()));

			//Jäger
			Animation animationJaeger = new Animation(content.Load<Texture2D>(@"Sprites\Animation\makoto_naegi.png"), @"Sprites\Animation\makoto_naegi.png", 3, 4, 0.22f);
			ClassType classTypeJaeger = new ClassType("Jäger");
			classTypeJaeger.SetBonus(StatType.BaseDexterity, 0.1f);
			acc.AddPlayer(new CorePlayer(acc, animationJaeger, expTable, "Kilroy", 1, classTypeJaeger, new Vector2(75f, 75f), 100, new InventoryControl()));

			//Geist
			Animation animationGohst = new Animation(content.Load<Texture2D>(@"Sprites\Animation\dark_jak.png"), @"Sprites\Animation\dark_jak.png", 3, 4, 0.22f);
			ClassType classTypeGohst = new ClassType("Geistlich");
			classTypeGohst.SetBonus(StatType.BaseDefenceValue, 0.1f);
			acc.AddPlayer(new CorePlayer(acc, animationGohst, expTable, "Gohst", 1, classTypeGohst, new Vector2(75f, 75f), 100, new InventoryControl()));

			//Mönch
			Animation animationMoench = new Animation(content.Load<Texture2D>(@"Sprites\Animation\renji_arisu.png"), @"Sprites\Animation\renji_arisu.png", 3, 4, 0.22f);
			ClassType classTypeMoench = new ClassType("Mönch");
			classTypeMoench.SetBonus(StatType.BaseEvade, 0.1f);
			acc.AddPlayer(new CorePlayer(acc, animationMoench, expTable, "Karl", 1, classTypeMoench, new Vector2(75f, 75f), 100, new InventoryControl()));

			//Beschwörer
			Animation animationBeschwoerer = new Animation(content.Load<Texture2D>(@"Sprites\Animation\professor_negi.png"), @"Sprites\Animation\professor_negi.png", 3, 4, 0.22f);
			ClassType classTypeBeschwoerer = new ClassType("Beschwörer");
			classTypeBeschwoerer.SetBonus(StatType.BaseLuck, 0.1f);
			acc.AddPlayer(new CorePlayer(acc, animationBeschwoerer, expTable, "Zar", 1, classTypeBeschwoerer, new Vector2(75f, 75f), 100, new InventoryControl()));

			//Demon
			Animation animationDemon = new Animation(content.Load<Texture2D>(@"Sprites\Animation\jelan_mirana.png"), @"Sprites\Animation\jelan_mirana.png", 3, 4, 0.22f);
			ClassType classTypeDemon = new ClassType("Demon");
			classTypeDemon.SetBonus(StatType.BaseStrength, 0.1f);
			acc.AddPlayer(new CorePlayer(acc, animationDemon, expTable, "Demon", 1, classTypeDemon, new Vector2(75f, 75f), 100, new InventoryControl()));

			//Amazone
			Animation animationAmazone = new Animation(content.Load<Texture2D>(@"Sprites\Animation\scarlet.png"), @"Sprites\Animation\scarlet.png", 3, 4, 0.22f);
			ClassType classTypeAmazone = new ClassType("Amazone");
			classTypeAmazone.SetBonus(StatType.BaseStamina, 0.1f);
			acc.AddPlayer(new CorePlayer(acc, animationAmazone, expTable, "Julia", 1, classTypeAmazone, new Vector2(75f, 75f), 100, new InventoryControl()));

			foreach (CorePlayer player in acc.AvaiablePlayers) {
				player.SetNextBattleTime(rnd);

				player.WearItem(ItemType.Weapon, (Item)itemManager.GenerateItem(1, Core.Items.Enum.ItemType.Weapon, Core.Items.Enum.Quality.Magic));
				player.WearItem(ItemType.Body, (Item)itemManager.GenerateItem(1, Core.Items.Enum.ItemType.Body, Core.Items.Enum.Quality.Magic));
			}

			return acc;
		}

		private PlayerAccountDO GetDataObject() {
			PlayerAccountDO data = new PlayerAccountDO();

			// ShopSkills
			foreach (ShopSkill skill in ShopSkills) {
				data.ShopSkills.Add(new ShopSkillDO() { Buyed = skill.Buyed, Cost = skill.Cost, Typ = skill.Typ, Skill = skill.Skill });
			}

			// BankInventories
			foreach (var inventory in BankInventories.Select(i => new { Title = i.Key, Inventory = i.Value })) {
				InventoryDO newInventory = new InventoryDO();

				newInventory.CellPositionsOffsets = new List<FakeVector2>();
				foreach (Vector2 vector in inventory.Inventory.CellPositionsOffsets) {
					newInventory.CellPositionsOffsets.Add(new FakeVector2() { X = vector.X, Y = vector.Y });
				}

				newInventory.Items = new List<InventoryPositionDO>();
				foreach (InventoryPosition inventoryPos in inventory.Inventory.Items) {
					InventoryPositionDO newInvPos = new InventoryPositionDO() { PositionX = inventoryPos.Position.X, PositionY = inventoryPos.Position.Y };
					ItemDO item = new ItemDO() {
						Type = inventoryPos.Item.Type,
						Title = inventoryPos.Item.Title,
						SpecialTitle = inventoryPos.Item.SpecialTitle,
						Quality = inventoryPos.Item.Quality,
						Itemlevel = inventoryPos.Item.Itemlevel,
						Cost = inventoryPos.Item.Cost,
						TextureAsset = inventoryPos.Item.TextureAsset
					};

					if (item.Type == ItemType.Weapon) {
						item.MinDamage = ((CoreWeapon)inventoryPos.Item).MinDamage();
						item.MaxDamage = ((CoreWeapon)inventoryPos.Item).MaxDamage();
					} else {
						item.MinDefence = ((CoreArmor)inventoryPos.Item).MinDefence;
						item.MaxDefence = ((CoreArmor)inventoryPos.Item).MaxDefence;
						item.Defence = ((CoreArmor)inventoryPos.Item).Defence();
					}

					newInvPos.Item = item;

					item.AvaiableSkills = new List<SkillDO>();

					foreach (Skill skill in inventoryPos.Item.AvaiableSkills) {
						SkillDO newSkill = new SkillDO() {
							AffectType = skill.AffectType,
							BoarderColor = skill.BoarderColor.ToRgba(),
							Chance = skill.Chance,
							Description = skill.Description,
							GridHeight = skill.GridHeight,
							GridWidth = skill.GridWidth,
							Level = skill.Level,
							Look = skill.Look,
							MinimalItemLevel = skill.MinimalItemLevel,
							MinimalQualityType = skill.MinimalQualityType,
							OriginLook = skill.OriginLook,
							Prefix = skill.Prefix,
							StoneColor = skill.StoneColor.ToRgba(),
							Surfix = skill.Surfix,
							Title = skill.Title,
							Value = skill.Value,
							Type = skill.Type
						};

						newSkill.AvailableItemTyps = new List<ItemType>();
						foreach (ItemType type in skill.AvailableItemTyps) {
							newSkill.AvailableItemTyps.Add(type);
						}

						item.AvaiableSkills.Add(newSkill);
					}

					newInventory.Items.Add(newInvPos);
				}

				newInventory.Height = inventory.Inventory.Height;
				newInventory.MaxHeight = inventory.Inventory.MaxHeight;
				newInventory.MaxWidth = inventory.Inventory.MaxWidth;
				newInventory.PositionX = inventory.Inventory.Position.X;
				newInventory.PositionY = inventory.Inventory.Position.Y;
				newInventory.Width = inventory.Inventory.Width;

				data.BankInventories.Add(inventory.Title, newInventory);
			}

			// AvaiablePlayers
			foreach (CorePlayer player in AvaiablePlayers) {
				CorePlayerDO newPlayer = new CorePlayerDO() {
					Statistics = new SerializableDictionary<StatisticType, ulong>(),
					AvaiableSkills = new List<SkillDO>(),
					BattleRounds = player.GetBattleRounds(),
					BattleTime = player.GetBattleTime(),
					ClassType = new ClassTypeDO(),
					CurrentEXP = player.GetCurrentEXP(),
					CurrentHP = player.GetCurrentHP(),
					CurrentSkills = new List<SkillDO>(),
					EXPTable = new EXPTableDO(),
					GameMode = player.GetGameMode(),
					Gold = player.Gold,
					Inventory = new InventoryDO(),
					Layer = player.GetLayer(),
					LayersToBeat = player.GetLayersToBeat(),
					Level = player.Level,
					MovementSpeedModifier = player.MovementSpeedModifier,
					Name = player.Name,
					NextBattleFightModifier = player.NextBattleFightModifier,
					NextBattleTime = new NextBattleChanceDO(),
					PlayerMovementSpeed = player.GetMovementSpeed(),
					Position = new FakeVector2() { X = player.Position.X, Y = player.Position.Y },
					StatPoints = player.StatPoints,
					StatPointsPerLevelUp = player.StatPointsPerLevelUp,
					Stats = new SerializableDictionary<StatType, float>(),
					UsedItems = new SerializableDictionary<ItemType, ItemDO>()
				};

				foreach (var gridDO in player.ActiveSkillsFromGrid) {
					var newGridDO = new SkillGridEntityControlDO();
					newGridDO.Position = new FakeVector2() { X = gridDO.Position.X, Y = gridDO.Position.Y };
					newGridDO.Rotation = gridDO.Rotation;

					SkillDO newSkill = new SkillDO() {
						AffectType = gridDO.Skill.AffectType,
						BoarderColor = gridDO.Skill.BoarderColor.ToRgba(),
						Chance = gridDO.Skill.Chance,
						Description = gridDO.Skill.Description,
						GridHeight = gridDO.Skill.GridHeight,
						GridWidth = gridDO.Skill.GridWidth,
						Level = gridDO.Skill.Level,
						Look = gridDO.Skill.Look,
						MinimalItemLevel = gridDO.Skill.MinimalItemLevel,
						MinimalQualityType = gridDO.Skill.MinimalQualityType,
						OriginLook = gridDO.Skill.OriginLook,
						Prefix = gridDO.Skill.Prefix,
						StoneColor = gridDO.Skill.StoneColor.ToRgba(),
						Surfix = gridDO.Skill.Surfix,
						Title = gridDO.Skill.Title,
						Value = gridDO.Skill.Value,
						Type = gridDO.Skill.Type
					};

					newSkill.AvailableItemTyps = new List<ItemType>();
					foreach (ItemType type in gridDO.Skill.AvailableItemTyps) {
						newSkill.AvailableItemTyps.Add(type);
					}

					newGridDO.Skill = newSkill;

					newPlayer.ActiveSkillsFromGrid.Add(newGridDO);
				}

				// newPlayer.Animation
				newPlayer.PlayerAnimation = new AnimationDO() {
					AnimationCount = player.PlayerAnimation.AnimationCount,
					AnimationSpeed = player.PlayerAnimation.AnimationSpeed,
					AnimationStepsPerAnimation = player.PlayerAnimation.AnimationStepsPerAnimation,
					AnimationStopIndex = player.PlayerAnimation.AnimationStopIndex,
					CurrentAnimationDirection = player.PlayerAnimation.CurrentAnimationDirection,
					CurrentAnimationStepIndex = player.PlayerAnimation.CurrentAnimationStepIndex,
					DoAnimationLooping = player.PlayerAnimation.DoAnimationLooping,
					SpriteSingleHeight = player.PlayerAnimation.SpriteSingleHeight,
					SpriteSingleWidth = player.PlayerAnimation.SpriteSingleWidth,
					Step = player.PlayerAnimation.Step,
					TextureAsset = player.PlayerAnimation.TextureAsset,
					Timer = player.PlayerAnimation.Timer
				};

				foreach (var a in player.PlayerAnimation.AnimationIndex) {
					newPlayer.PlayerAnimation.AnimationIndexes.Add(a.Key, a.Value);
				}

				// newPlayer.Statistics
				foreach (var statistic in player.Statistics) {
					newPlayer.Statistics.Add(statistic.Key, statistic.Value);
				}

				// newPlayer.AvaiableSkills
				foreach (Skill skill in player.AvaiableSkills) {
					SkillDO newSkill = new SkillDO() {
						AffectType = skill.AffectType,
						BoarderColor = skill.BoarderColor.ToRgba(),
						Chance = skill.Chance,
						Description = skill.Description,
						GridHeight = skill.GridHeight,
						GridWidth = skill.GridWidth,
						Level = skill.Level,
						Look = skill.Look,
						MinimalItemLevel = skill.MinimalItemLevel,
						MinimalQualityType = skill.MinimalQualityType,
						OriginLook = skill.OriginLook,
						Prefix = skill.Prefix,
						StoneColor = skill.StoneColor.ToRgba(),
						Surfix = skill.Surfix,
						Title = skill.Title,
						Value = skill.Value,
						Type = skill.Type
					};

					newSkill.AvailableItemTyps = new List<ItemType>();
					foreach (ItemType type in skill.AvailableItemTyps) {
						newSkill.AvailableItemTyps.Add(type);
					}

					newPlayer.AvaiableSkills.Add(newSkill);
				}

				// newPlayer.ClassType
				newPlayer.ClassType = new ClassTypeDO() {
					Title = player.ClassType.Title,
					StatBonus = new SerializableDictionary<StatType, float>()
				};
				foreach (var type in player.ClassType.StatBonus) {
					newPlayer.ClassType.StatBonus.Add(type.Key, type.Value);
				}

				// newPlayer.CurrentSkills
				foreach (var skill in player.CurrentSkills) {
					SkillDO newSkill = new SkillDO() {
						AffectType = skill.AffectType,
						BoarderColor = skill.BoarderColor.ToRgba(),
						Chance = skill.Chance,
						Description = skill.Description,
						GridHeight = skill.GridHeight,
						GridWidth = skill.GridWidth,
						Level = skill.Level,
						Look = skill.Look,
						MinimalItemLevel = skill.MinimalItemLevel,
						MinimalQualityType = skill.MinimalQualityType,
						OriginLook = skill.OriginLook,
						Prefix = skill.Prefix,
						StoneColor = skill.StoneColor.ToRgba(),
						Surfix = skill.Surfix,
						Title = skill.Title,
						Value = skill.Value,
						Type = skill.Type
					};

					newSkill.AvailableItemTyps = new List<ItemType>();
					foreach (ItemType type in skill.AvailableItemTyps) {
						newSkill.AvailableItemTyps.Add(type);
					}

					newPlayer.CurrentSkills.Add(newSkill);
				}

				// newPlayer.EXPTable
				foreach (var exp in player.EXPTable.LevelUpStats) {
					newPlayer.EXPTable.LevelUpStats.Add(new LevelEntityDO() { EXPStep = exp.EXPStep, Level = exp.Level, NextLevelEXP = exp.NextLevelEXP });
				}

				// newPlayer.Inventory
				InventoryDO newInventory = new InventoryDO();

				newInventory.CellPositionsOffsets = new List<FakeVector2>();
				foreach (Vector2 vector in player.Inventory.CellPositionsOffsets) {
					newInventory.CellPositionsOffsets.Add(new FakeVector2() { X = vector.X, Y = vector.Y });
				}

				newInventory.Items = new List<InventoryPositionDO>();
				foreach (InventoryPosition inventoryPos in player.Inventory.Items) {
					InventoryPositionDO newInvPos = new InventoryPositionDO() { PositionX = inventoryPos.Position.X, PositionY = inventoryPos.Position.Y };
					ItemDO item = new ItemDO() {
						Type = inventoryPos.Item.Type,
						Title = inventoryPos.Item.Title,
						SpecialTitle = inventoryPos.Item.SpecialTitle,
						Quality = inventoryPos.Item.Quality,
						Itemlevel = inventoryPos.Item.Itemlevel,
						Cost = inventoryPos.Item.Cost,
						TextureAsset = inventoryPos.Item.TextureAsset
					};

					if (item.Type == ItemType.Weapon) {
						item.MinDamage = ((CoreWeapon)inventoryPos.Item).MinDamage();
						item.MaxDamage = ((CoreWeapon)inventoryPos.Item).MaxDamage();
					} else {
						item.MinDefence = ((CoreArmor)inventoryPos.Item).MinDefence;
						item.MaxDefence = ((CoreArmor)inventoryPos.Item).MaxDefence;
						item.Defence = ((CoreArmor)inventoryPos.Item).Defence();
					}

					newInvPos.Item = item;

					item.AvaiableSkills = new List<SkillDO>();

					foreach (Skill skill in inventoryPos.Item.AvaiableSkills) {
						SkillDO newSkill = new SkillDO() {
							AffectType = skill.AffectType,
							BoarderColor = skill.BoarderColor.ToRgba(),
							Chance = skill.Chance,
							Description = skill.Description,
							GridHeight = skill.GridHeight,
							GridWidth = skill.GridWidth,
							Level = skill.Level,
							Look = skill.Look,
							MinimalItemLevel = skill.MinimalItemLevel,
							MinimalQualityType = skill.MinimalQualityType,
							OriginLook = skill.OriginLook,
							Prefix = skill.Prefix,
							StoneColor = skill.StoneColor.ToRgba(),
							Surfix = skill.Surfix,
							Title = skill.Title,
							Value = skill.Value,
							Type = skill.Type
						};

						newSkill.AvailableItemTyps = new List<ItemType>();
						foreach (ItemType type in skill.AvailableItemTyps) {
							newSkill.AvailableItemTyps.Add(type);
						}

						item.AvaiableSkills.Add(newSkill);
					}

					newInventory.Items.Add(newInvPos);
				}

				newInventory.Height = player.Inventory.Height;
				newInventory.MaxHeight = player.Inventory.MaxHeight;
				newInventory.MaxWidth = player.Inventory.MaxWidth;
				newInventory.PositionX = player.Inventory.Position.X;
				newInventory.PositionY = player.Inventory.Position.Y;
				newInventory.Width = player.Inventory.Width;

				newPlayer.Inventory = newInventory;

				// newPlayer.NextBattleTime
				newPlayer.NextBattleTime = new NextBattleChanceDO() {
					CurrentTimeNextBattle = player.NextBattleTime.CurrentTimeNextBattle,
					MaxTimeForNextBattle = player.NextBattleTime.MaxTimeForNextBattle,
					MinTimeForNextBattle = player.NextBattleTime.MinTimeForNextBattle,
					TimeToNextBattle = player.NextBattleTime.TimeToNextBattle
				};

				// newPlayer.Stats
				foreach (var stat in player.Stats) {
					newPlayer.Stats.Add(stat.Key, stat.Value);
				}

				// newPlayer.UsedItems
				foreach (var item_ in player.UsedItems) {
					if (item_.Value == null) {
						newPlayer.UsedItems.Add(item_.Key, null);
						continue;
					}

					ItemDO item = new ItemDO() {
						Type = item_.Value.Type,
						Title = item_.Value.Title,
						SpecialTitle = item_.Value.SpecialTitle,
						Quality = item_.Value.Quality,
						Itemlevel = item_.Value.Itemlevel,
						Cost = item_.Value.Cost,
						TextureAsset = item_.Value.TextureAsset
					};

					if (item.Type == ItemType.Weapon) {
						item.MinDamage = ((CoreWeapon)item_.Value).MinDamage();
						item.MaxDamage = ((CoreWeapon)item_.Value).MaxDamage();
					} else {
						item.MinDefence = ((CoreArmor)item_.Value).MinDefence;
						item.MaxDefence = ((CoreArmor)item_.Value).MaxDefence;
						item.Defence = ((CoreArmor)item_.Value).Defence();
					}

					item.AvaiableSkills = new List<SkillDO>();

					foreach (Skill skill in item_.Value.AvaiableSkills) {
						SkillDO newSkill = new SkillDO() {
							AffectType = skill.AffectType,
							BoarderColor = skill.BoarderColor.ToRgba(),
							Chance = skill.Chance,
							Description = skill.Description,
							GridHeight = skill.GridHeight,
							GridWidth = skill.GridWidth,
							Level = skill.Level,
							Look = skill.Look,
							MinimalItemLevel = skill.MinimalItemLevel,
							MinimalQualityType = skill.MinimalQualityType,
							OriginLook = skill.OriginLook,
							Prefix = skill.Prefix,
							StoneColor = skill.StoneColor.ToRgba(),
							Surfix = skill.Surfix,
							Title = skill.Title,
							Value = skill.Value,
							Type = skill.Type
						};

						newSkill.AvailableItemTyps = new List<ItemType>();
						foreach (ItemType type in skill.AvailableItemTyps) {
							newSkill.AvailableItemTyps.Add(type);
						}

						item.AvaiableSkills.Add(newSkill);
					}

					newPlayer.UsedItems.Add(item_.Key, item);
				}

				data.AvaiablePlayers.Add(newPlayer);
			}

			return data;
		}

		public void Save(string filename = "acc.sav") {
			if (File.Exists(filename + ".bak")) {
				File.Delete(filename + ".bak");
			}

			if (File.Exists(filename)) {
				File.Move(filename, filename + ".bak");
			}
			
			var a = GetDataObject();
			a.Save<PlayerAccountDO>(filename, false);
		}

		public void Load(ContentManager content, string filename = "acc.sav") {
			PlayerAccountDO acc = filename.Load<PlayerAccountDO>(false);

			// ShopSkills
			ShopSkills.Clear();
			foreach (var skill in acc.ShopSkills) {
				ShopSkills.Add(new ShopSkill(skill.Typ, skill.Skill, skill.Buyed, skill.Cost));
			}

			// BankInventories
			BankInventories.Clear();
			foreach (var inv in acc.BankInventories) {
				Inventory bankInv = new InventoryControl(inv.Value.MaxWidth, inv.Value.MaxHeight, inv.Value.Width, inv.Value.Height);
				bankInv.Position = new Vector2(inv.Value.PositionX, inv.Value.PositionY);
				bankInv.SetCellPositionsOffsets(inv.Value.CellPositionsOffsets.Select(c => new Vector2(c.X, c.Y)).ToList());

				foreach (var itemDO in inv.Value.Items) {
					Item item;
					if (itemDO.Item.Type == ItemType.Weapon) {
						item = new CoreWeapon(itemDO.Item.MinDamage, itemDO.Item.MaxDamage, itemDO.Item.Title, itemDO.Item.Itemlevel, itemDO.Item.Quality, content.Load<Texture2D>(itemDO.Item.TextureAsset), itemDO.Item.TextureAsset, itemDO.Item.Cost);
					} else {
						item = new CoreArmor(itemDO.Item.Defence, itemDO.Item.Title, itemDO.Item.Itemlevel, itemDO.Item.Quality, itemDO.Item.Type, content.Load<Texture2D>(itemDO.Item.TextureAsset), itemDO.Item.TextureAsset, itemDO.Item.Cost);
						((CoreArmor)item).MinDefence = itemDO.Item.MinDefence;
						((CoreArmor)item).MaxDefence = itemDO.Item.MaxDefence;
					}

					item.SpecialTitle = itemDO.Item.SpecialTitle;
					foreach (var skillDO in itemDO.Item.AvaiableSkills) {
						Skill skill;

						if (skillDO.AffectType == AffectType.AfterBattleSkill) {
							skill = new CoreAfterBattleSkill(skillDO.Title, skillDO.Prefix, skillDO.Surfix, skillDO.Type, skillDO.MinimalItemLevel, skillDO.Level, skillDO.MinimalQualityType, skillDO.Look, skillDO.Description, skillDO.AvailableItemTyps.ToArray());
						} else if (skillDO.AffectType == AffectType.BattleSkill) {
							skill = new CoreBattleSkill(skillDO.Title, skillDO.Prefix, skillDO.Surfix, skillDO.Type, skillDO.MinimalItemLevel, skillDO.Level, skillDO.MinimalQualityType, skillDO.Look, skillDO.Description, skillDO.AvailableItemTyps.ToArray());
						} else if (skillDO.AffectType == AffectType.PlayRoundSkill) {
							skill = new CorePlayRoundSkill(skillDO.Title, skillDO.Prefix, skillDO.Surfix, skillDO.Type, skillDO.MinimalItemLevel, skillDO.Level, skillDO.MinimalQualityType, skillDO.Look, skillDO.Description, skillDO.AvailableItemTyps.ToArray());
						} else {
							skill = new CoreStatusChangeSkill(skillDO.Title, skillDO.Prefix, skillDO.Surfix, skillDO.Type, skillDO.MinimalItemLevel, skillDO.Level, skillDO.MinimalQualityType, skillDO.Look, skillDO.Description, skillDO.AvailableItemTyps.ToArray());
						}

						skill.SetNewLook(skillDO.Look, skillDO.OriginLook);
						skill.SetValue(skillDO.Value);
						skill.SetChance(skillDO.Chance);
						skill.ChangeGridSize(skillDO.GridWidth, skillDO.GridHeight, skillDO.Look);
						skill.ChangeBorderColor(Color.FromRgba(skillDO.BoarderColor));
						skill.ChangeStoneRenderingColor(Color.FromRgba(skillDO.StoneColor));

						item.AddSkill(skill);
					}

					if (!bankInv.AddItem(item, new Vector2(itemDO.PositionX + bankInv.Position.X, itemDO.PositionY + bankInv.Position.Y))) {
						if (!bankInv.AddItem(item)) {
							throw new Exception("Gegenstand konnte nicht hinzugefügt werden.");
						}
					}
				}

				BankInventories.Add(inv.Key, bankInv);
			}

			// AvaiablePlayers
			AvaiablePlayers.Clear();
			foreach (var playerDO in acc.AvaiablePlayers) {
				RPG.Chars.EXPTable exp = new RPG.Chars.EXPTable();
				playerDO.EXPTable.LevelUpStats.ForEach(e => exp.AddLevelUpStat(e.Level, e.NextLevelEXP, e.EXPStep));

				Animation ani = new Animation(content.Load<Texture2D>(playerDO.PlayerAnimation.TextureAsset), playerDO.PlayerAnimation.TextureAsset, playerDO.PlayerAnimation.AnimationStepsPerAnimation, playerDO.PlayerAnimation.AnimationCount, playerDO.PlayerAnimation.AnimationSpeed);
				ani.OverrideAnimationIndexes(playerDO.PlayerAnimation.AnimationIndexes);
				ani.SetDirection(playerDO.PlayerAnimation.CurrentAnimationDirection);
				ani.DoAnimationLooping = playerDO.PlayerAnimation.DoAnimationLooping;
				ani.AnimationStopIndex = playerDO.PlayerAnimation.AnimationStopIndex;
				ani.ResetAnimation();

				ClassType classType = new ClassType(playerDO.ClassType.Title);
				playerDO.ClassType.StatBonus.ToList().ForEach(s => classType.SetBonus(s.Key, s.Value));

				InventoryControl inv = new InventoryControl(playerDO.Inventory.MaxWidth, playerDO.Inventory.MaxHeight, playerDO.Inventory.Width, playerDO.Inventory.Height);
				inv.Position = new Vector2(playerDO.Inventory.PositionX, playerDO.Inventory.PositionY);
				inv.SetCellPositionsOffsets(playerDO.Inventory.CellPositionsOffsets.Select(c => new Vector2(c.X, c.Y)).ToList());


				foreach (var itemDO in playerDO.Inventory.Items) {
					Item item;
					if (itemDO.Item.Type == ItemType.Weapon) {
						item = new CoreWeapon(itemDO.Item.MinDamage, itemDO.Item.MaxDamage, itemDO.Item.Title, itemDO.Item.Itemlevel, itemDO.Item.Quality, content.Load<Texture2D>(itemDO.Item.TextureAsset), itemDO.Item.TextureAsset, itemDO.Item.Cost);
					} else {
						item = new CoreArmor(itemDO.Item.Defence, itemDO.Item.Title, itemDO.Item.Itemlevel, itemDO.Item.Quality, itemDO.Item.Type, content.Load<Texture2D>(itemDO.Item.TextureAsset), itemDO.Item.TextureAsset, itemDO.Item.Cost);
						((CoreArmor)item).MinDefence = itemDO.Item.MinDefence;
						((CoreArmor)item).MaxDefence = itemDO.Item.MaxDefence;
					}

					item.SpecialTitle = itemDO.Item.SpecialTitle;
					foreach (var skillDO in itemDO.Item.AvaiableSkills) {
						Skill skill;

						if (skillDO.AffectType == AffectType.AfterBattleSkill) {
							skill = new CoreAfterBattleSkill(skillDO.Title, skillDO.Prefix, skillDO.Surfix, skillDO.Type, skillDO.MinimalItemLevel, skillDO.Level, skillDO.MinimalQualityType, skillDO.Look, skillDO.Description, skillDO.AvailableItemTyps.ToArray());
						} else if (skillDO.AffectType == AffectType.BattleSkill) {
							skill = new CoreBattleSkill(skillDO.Title, skillDO.Prefix, skillDO.Surfix, skillDO.Type, skillDO.MinimalItemLevel, skillDO.Level, skillDO.MinimalQualityType, skillDO.Look, skillDO.Description, skillDO.AvailableItemTyps.ToArray());
						} else if (skillDO.AffectType == AffectType.PlayRoundSkill) {
							skill = new CorePlayRoundSkill(skillDO.Title, skillDO.Prefix, skillDO.Surfix, skillDO.Type, skillDO.MinimalItemLevel, skillDO.Level, skillDO.MinimalQualityType, skillDO.Look, skillDO.Description, skillDO.AvailableItemTyps.ToArray());
						} else {
							skill = new CoreStatusChangeSkill(skillDO.Title, skillDO.Prefix, skillDO.Surfix, skillDO.Type, skillDO.MinimalItemLevel, skillDO.Level, skillDO.MinimalQualityType, skillDO.Look, skillDO.Description, skillDO.AvailableItemTyps.ToArray());
						}

						skill.SetNewLook(skillDO.Look, skillDO.OriginLook);
						skill.SetValue(skillDO.Value);
						skill.SetChance(skillDO.Chance);
						skill.ChangeGridSize(skillDO.GridWidth, skillDO.GridHeight, skillDO.Look);
						skill.ChangeBorderColor(Color.FromRgba(skillDO.BoarderColor));
						skill.ChangeStoneRenderingColor(Color.FromRgba(skillDO.StoneColor));

						item.AddSkill(skill);
					}

					inv.AddItem(item, new Vector2(itemDO.PositionX + playerDO.Inventory.PositionX, itemDO.PositionY + playerDO.Inventory.PositionY));
				}

				CorePlayer player = new CorePlayer(this, ani, exp, playerDO.Name, playerDO.Level, classType, new Vector2(playerDO.Position.X, playerDO.Position.Y), (int)playerDO.CurrentHP, (Inventory)inv);
				player.SetLayer(playerDO.Layer);
				player.SetBattleRounds(playerDO.BattleRounds);
				player.SetBattleTime(playerDO.BattleTime);
				player.SetGameMode(playerDO.GameMode);
				player.SetLayersToBeat(playerDO.LayersToBeat);
				player.SetPlayerMovementSpeedAndModifier(playerDO.PlayerMovementSpeed, playerDO.MovementSpeedModifier);
				player.NextBattleFightModifier = playerDO.NextBattleFightModifier;
				player.SetCurrentEXPAndStatPoints(playerDO.CurrentEXP, playerDO.StatPoints, playerDO.StatPointsPerLevelUp);
				player.Gold = playerDO.Gold;
				player.SetCurrentHealth(playerDO.CurrentHP);
				playerDO.Stats.ToList().ForEach(s => player.SetBaseStat(s.Key, s.Value));
				playerDO.Statistics.ToList().ForEach(s => player.SetStatistic(s.Key, s.Value));

				// Die Rüstung wird später erst angezogen
				player.SetStat(StatType.BaseDefence, 0);
				player.SetStat(StatType.BaseDefenceValue, 0);
				player.SetStat(StatType.BonusDefence, 0);
				player.SetStat(StatType.BonusDefenceValue, 0);
				player.SetStat(StatType.Defence, 0);
				player.SetStat(StatType.DefenceValue, 0);

				foreach (var itemDO in playerDO.UsedItems) {
					Item item = null;
					if (itemDO.Value != null) {
						if (itemDO.Value.Type == ItemType.Weapon) {
							item = new CoreWeapon(itemDO.Value.MinDamage, itemDO.Value.MaxDamage, itemDO.Value.Title, itemDO.Value.Itemlevel, itemDO.Value.Quality, content.Load<Texture2D>(itemDO.Value.TextureAsset), itemDO.Value.TextureAsset, itemDO.Value.Cost);
						} else {
							item = new CoreArmor(itemDO.Value.Defence, itemDO.Value.Title, itemDO.Value.Itemlevel, itemDO.Value.Quality, itemDO.Value.Type, content.Load<Texture2D>(itemDO.Value.TextureAsset), itemDO.Value.TextureAsset, itemDO.Value.Cost);
							((CoreArmor)item).MinDefence = itemDO.Value.MinDefence;
							((CoreArmor)item).MaxDefence = itemDO.Value.MaxDefence;
						}

						item.SpecialTitle = itemDO.Value.SpecialTitle;
						foreach (var skillDO in itemDO.Value.AvaiableSkills) {
							Skill skill;

							if (skillDO.AffectType == AffectType.AfterBattleSkill) {
								skill = new CoreAfterBattleSkill(skillDO.Title, skillDO.Prefix, skillDO.Surfix, skillDO.Type, skillDO.MinimalItemLevel, skillDO.Level, skillDO.MinimalQualityType, skillDO.Look, skillDO.Description, skillDO.AvailableItemTyps.ToArray());
							} else if (skillDO.AffectType == AffectType.BattleSkill) {
								skill = new CoreBattleSkill(skillDO.Title, skillDO.Prefix, skillDO.Surfix, skillDO.Type, skillDO.MinimalItemLevel, skillDO.Level, skillDO.MinimalQualityType, skillDO.Look, skillDO.Description, skillDO.AvailableItemTyps.ToArray());
							} else if (skillDO.AffectType == AffectType.PlayRoundSkill) {
								skill = new CorePlayRoundSkill(skillDO.Title, skillDO.Prefix, skillDO.Surfix, skillDO.Type, skillDO.MinimalItemLevel, skillDO.Level, skillDO.MinimalQualityType, skillDO.Look, skillDO.Description, skillDO.AvailableItemTyps.ToArray());
							} else {
								skill = new CoreStatusChangeSkill(skillDO.Title, skillDO.Prefix, skillDO.Surfix, skillDO.Type, skillDO.MinimalItemLevel, skillDO.Level, skillDO.MinimalQualityType, skillDO.Look, skillDO.Description, skillDO.AvailableItemTyps.ToArray());
							}

							skill.SetNewLook(skillDO.Look, skillDO.OriginLook);
							skill.SetValue(skillDO.Value);
							skill.SetChance(skillDO.Chance);
							skill.ChangeGridSize(skillDO.GridWidth, skillDO.GridHeight, skillDO.Look);
							skill.ChangeBorderColor(Color.FromRgba(skillDO.BoarderColor));
							skill.ChangeStoneRenderingColor(Color.FromRgba(skillDO.StoneColor));

							item.AddSkill(skill);
						}
					}

					player.WearItem(itemDO.Key, item);
				}

				foreach (SkillDO skillDO in playerDO.CurrentSkills) {
					Skill skill;

					if (skillDO.AffectType == AffectType.AfterBattleSkill) {
						skill = new CoreAfterBattleSkill(skillDO.Title, skillDO.Prefix, skillDO.Surfix, skillDO.Type, skillDO.MinimalItemLevel, skillDO.Level, skillDO.MinimalQualityType, skillDO.Look, skillDO.Description, skillDO.AvailableItemTyps.ToArray());
					} else if (skillDO.AffectType == AffectType.BattleSkill) {
						skill = new CoreBattleSkill(skillDO.Title, skillDO.Prefix, skillDO.Surfix, skillDO.Type, skillDO.MinimalItemLevel, skillDO.Level, skillDO.MinimalQualityType, skillDO.Look, skillDO.Description, skillDO.AvailableItemTyps.ToArray());
					} else if (skillDO.AffectType == AffectType.PlayRoundSkill) {
						skill = new CorePlayRoundSkill(skillDO.Title, skillDO.Prefix, skillDO.Surfix, skillDO.Type, skillDO.MinimalItemLevel, skillDO.Level, skillDO.MinimalQualityType, skillDO.Look, skillDO.Description, skillDO.AvailableItemTyps.ToArray());
					} else {
						skill = new CoreStatusChangeSkill(skillDO.Title, skillDO.Prefix, skillDO.Surfix, skillDO.Type, skillDO.MinimalItemLevel, skillDO.Level, skillDO.MinimalQualityType, skillDO.Look, skillDO.Description, skillDO.AvailableItemTyps.ToArray());
					}

					skill.SetNewLook(skillDO.Look, skillDO.OriginLook);
					skill.SetValue(skillDO.Value);
					skill.SetChance(skillDO.Chance);
					skill.ChangeGridSize(skillDO.GridWidth, skillDO.GridHeight, skillDO.Look);
					skill.ChangeBorderColor(Color.FromRgba(skillDO.BoarderColor));
					skill.ChangeStoneRenderingColor(Color.FromRgba(skillDO.StoneColor));

					player.AddUseSkill(skill);
				}

				player.SetNextBattleTime(new NextBattleChance(playerDO.NextBattleTime.MaxTimeForNextBattle, playerDO.NextBattleTime.MinTimeForNextBattle, playerDO.NextBattleTime.TimeToNextBattle, playerDO.NextBattleTime.CurrentTimeNextBattle));
				
				// Load Skill Grid 
				foreach (var gridDO in playerDO.ActiveSkillsFromGrid) {
					SkillGridEntityControl con = new SkillGridEntityControl();
					con.Position = new Vector2(gridDO.Position.X, gridDO.Position.Y);
					con.Rotation = gridDO.Rotation;

					Skill skill;

					if (gridDO.Skill.AffectType == AffectType.AfterBattleSkill) {
						skill = new CoreAfterBattleSkill(gridDO.Skill.Title, gridDO.Skill.Prefix, gridDO.Skill.Surfix, gridDO.Skill.Type, gridDO.Skill.MinimalItemLevel, gridDO.Skill.Level, gridDO.Skill.MinimalQualityType, gridDO.Skill.Look, gridDO.Skill.Description, gridDO.Skill.AvailableItemTyps.ToArray());
					} else if (gridDO.Skill.AffectType == AffectType.BattleSkill) {
						skill = new CoreBattleSkill(gridDO.Skill.Title, gridDO.Skill.Prefix, gridDO.Skill.Surfix, gridDO.Skill.Type, gridDO.Skill.MinimalItemLevel, gridDO.Skill.Level, gridDO.Skill.MinimalQualityType, gridDO.Skill.Look, gridDO.Skill.Description, gridDO.Skill.AvailableItemTyps.ToArray());
					} else if (gridDO.Skill.AffectType == AffectType.PlayRoundSkill) {
						skill = new CorePlayRoundSkill(gridDO.Skill.Title, gridDO.Skill.Prefix, gridDO.Skill.Surfix, gridDO.Skill.Type, gridDO.Skill.MinimalItemLevel, gridDO.Skill.Level, gridDO.Skill.MinimalQualityType, gridDO.Skill.Look, gridDO.Skill.Description, gridDO.Skill.AvailableItemTyps.ToArray());
					} else {
						skill = new CoreStatusChangeSkill(gridDO.Skill.Title, gridDO.Skill.Prefix, gridDO.Skill.Surfix, gridDO.Skill.Type, gridDO.Skill.MinimalItemLevel, gridDO.Skill.Level, gridDO.Skill.MinimalQualityType, gridDO.Skill.Look, gridDO.Skill.Description, gridDO.Skill.AvailableItemTyps.ToArray());
					}

					skill.SetNewLook(gridDO.Skill.Look, gridDO.Skill.OriginLook);
					skill.SetValue(gridDO.Skill.Value);
					skill.SetChance(gridDO.Skill.Chance);
					skill.ChangeGridSize(gridDO.Skill.GridWidth, gridDO.Skill.GridHeight, gridDO.Skill.Look);
					skill.ChangeBorderColor(Color.FromRgba(gridDO.Skill.BoarderColor));
					skill.ChangeStoneRenderingColor(Color.FromRgba(gridDO.Skill.StoneColor));

					con.Skill = skill;

					player.ActiveSkillsFromGrid.Add(con);
				}
				
				player.RefreshAllStatValue();

				AvaiablePlayers.Add(player);
			}
		}
	}
}

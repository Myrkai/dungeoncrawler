﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpDX;
using SharpDX.Toolkit.Graphics;
using SharpDX.Toolkit;

namespace DungeonCrawler {
	public static class Resolution {
		//Source = Anwendungsauflösung
		public static int SourceWidth { get; private set; }
		public static int SourceHeight { get; private set; }

		//Bildschirmauflösung
		public static int TargetWidth { get; private set; }
		public static int TargetHeight { get; private set; }

		//Bestimmen der Seitenverhältnisse
		public static float SourceRatio { get { return (float)SourceWidth / (float)SourceHeight; } }
		public static float TargetRatio { get { return (float)TargetWidth / (float)TargetHeight; } }
		public static float CurrentScreenRation { get { return (float)System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width / (float)System.Windows.Forms.Screen.PrimaryScreen.Bounds.Height; } }

		//Bestimmen der Scalings
		public static float Scale { get { return Math.Max((float)(SourceWidth) / (float)TargetWidth, (float)SourceHeight / (float)TargetHeight); } }
		public static float ScaleWidth { get { return (float)(SourceWidth) / (float)TargetWidth; } }
		public static float ScaleHeight { get { return (float)SourceHeight / (float)TargetHeight; } }
		public static float ScaleAddon4_3 { get; private set; }

		public static Vector2 Size { get { return new Vector2(TargetWidth, TargetHeight); } }

		public static void Init() {
			//16:9
			SourceWidth = 1280;
			SourceHeight = 720;

			//4:3
			TargetWidth = 1024;
			TargetHeight = 768;
			ScaleAddon4_3 = 0.182f;
		}

		public static void SetSourceResolution(int width, int height) {
			SourceWidth = width;
			SourceHeight = height;
		}

		public static void SetTargetResolution(int width, int height) {
			TargetWidth = width;
			TargetHeight = height;

			if (width == SourceWidth && height == SourceHeight) {
				ScaleAddon4_3 = 0f;
			} else {
				ScaleAddon4_3 = 0.182f;
			}
		}
	}
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.IO;
using System.Xml.Serialization;
using System.Reflection;
//using System.Runtime.Serialization.Formatters.Soap;
using System.Runtime.Serialization.Formatters.Binary;

namespace DungeonCrawler {
	
	public static class GameOptions {
		public static bool Fullscreen = false; //Anwendung wird im Vollbildmodus gerendert
		public static float Volume = 0.5f; //Lautstärke der Musik
		public static bool DeleteTempSound = false; //Sollen die temporären Wave Files gelöscht werden nach der Erstellung
		public static bool ForceFPSLimiter = false; //FPS auf 60 beschränken
		public static bool ShowLiveDebuger = true; //Soll die DebugConsole (Logger) angezeigt werden
		public static bool ShowFPS = true; //Soll der Aktuelle FPS-Wert angezeitg werden
		public static bool ShowVirtualESCButton = false; //Soll der Aktuelle FPS-Wert angezeitg werden
		public static string RadioStream = "http://textville.net:80/all.mp3";
		public static Boolean autoContinue = false;

		public static bool Save2(string filename) {
			try {
				FieldInfo[] fields = (typeof(GameOptions)).GetFields(BindingFlags.Static | BindingFlags.Public);
				object[,] a = new object[fields.Length, 2];
				int i = 0;
				foreach (FieldInfo field in fields) {
					a[i, 0] = field.Name;
					a[i, 1] = field.GetValue(null);
					i++;
				};
				Stream f = File.Open(filename, FileMode.Create);
				BinaryFormatter formatter = new BinaryFormatter();
				//SoapFormatter formatter = new SoapFormatter();
				formatter.Serialize(f, a);
				f.Close();
				return true;
			} catch {
				return false;
			}
		}

		public static bool Load2(string filename) {
			try {
				FieldInfo[] fields = (typeof(GameOptions)).GetFields(BindingFlags.Static | BindingFlags.Public);
				object[,] a;
				Stream f = File.Open(filename, FileMode.Open);
				BinaryFormatter formatter = new BinaryFormatter();
				//SoapFormatter formatter = new SoapFormatter();
				a = formatter.Deserialize(f) as object[,];
				f.Close();
				if (a.GetLength(0) != fields.Length) return false;
				int i = 0;
				foreach (FieldInfo field in fields) {
					if (field.Name == (a[i, 0] as string)) {
						field.SetValue(null, a[i, 1]);
					}
					i++;
				};
				return true;
			} catch {
				return false;
			}
		}
		/*
		public static void Save(string name) {
			XmlWriterSettings settings = new XmlWriterSettings();
			settings.Encoding = Encoding.UTF8;
			settings.Indent = true;
			settings.IndentChars = "	";
			settings.NewLineOnAttributes = true;

			using (XmlWriter writer = XmlWriter.Create(name, settings)) {
				writer.WriteStartDocument(false);
				writer.WriteStartElement("options");

				writer.WriteStartElement("option");
				writer.WriteAttributeString("fullscreen", Fullscreen.ToString());
				writer.WriteAttributeString("volume", Volume.ToString());
				writer.WriteAttributeString("deleteTempSound", DeleteTempSound.ToString());
				writer.WriteAttributeString("forceFPSLimiter", ForceFPSLimiter.ToString());
				writer.WriteAttributeString("showLiveDebugger", ShowLiveDebuger.ToString());
				writer.WriteAttributeString("showFPS", ShowFPS.ToString());
				writer.WriteAttributeString("showVirtualESCButton", ShowVirtualESCButton.ToString());
				writer.WriteEndElement();

				writer.WriteEndElement();
			}
		}

		public static void Load(string name) {
			if (File.Exists(name)) {

				XmlDocument doc = new XmlDocument();
				doc.Load(name);

				XmlNode settingNode = doc.GetElementsByTagName("option").Item(0);

				Fullscreen = Convert.ToBoolean(settingNode.Attributes.GetNamedItem("fullscreen").Value);
				Volume = Convert.ToSingle(settingNode.Attributes.GetNamedItem("volume").Value);
				DeleteTempSound = Convert.ToBoolean(settingNode.Attributes.GetNamedItem("deleteTempSound").Value);
				ForceFPSLimiter = Convert.ToBoolean(settingNode.Attributes.GetNamedItem("forceFPSLimiter").Value);
				ShowLiveDebuger = Convert.ToBoolean(settingNode.Attributes.GetNamedItem("showLiveDebugger").Value);
				ShowFPS = Convert.ToBoolean(settingNode.Attributes.GetNamedItem("showFPS").Value);
				ShowVirtualESCButton = Convert.ToBoolean(settingNode.Attributes.GetNamedItem("showVirtualESCButton").Value);
			}
		}
		*/
	}
}

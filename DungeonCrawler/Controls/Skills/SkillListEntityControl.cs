﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GUI.Interface;
using SharpDX;
using SharpDX.Toolkit.Graphics;
using SharpDX.Toolkit;
using RPG.Items;
using RPG;

namespace DungeonCrawler.Controls {
	/// <summary>
	/// Ein einziger Listen eintrag im SkillListControl.
	/// </summary>
	public class SkillListEntityControl : IControl {

		public Skill Skill { get; private set; }
		private SpriteFont TitleFont { get; set; }
		private SpriteFont DescriptionFont { get; set; }
		public Vector2 Position { get; set; }

		private int width = 305;
		private int height = 55;
		private Texture2D background;

		public SkillListEntityControl(Skill skill, SpriteFont title, SpriteFont description, Vector2 position) {
			Skill = skill;
			TitleFont = title;
			DescriptionFont = description;
			Position = position;
		}

		public void Draw(SpriteBatch spriteBatch) {
			if (background == null) {
				Skill.Draw(spriteBatch);
				background = TextureHelper.CreateTextureWidthBoarder(width, height, Color.Black, 2, Color.DarkGray, spriteBatch.GraphicsDevice);
			}
			spriteBatch.Draw(background, Position, Color.White * 0.8f);
			spriteBatch.DrawString(TitleFont, Skill.Title.Replace("#value", Skill.Value.ToString()), Position + new Vector2(5f, 5f), Color.White);
			spriteBatch.DrawString(DescriptionFont, Skill.Description, Position + new Vector2(5f, 30f), Color.White);

			const float stoneScale = 0.5f;
			Vector2 startTopX = new Vector2(width - 5f - Skill.StoneTexture.Width * 4f * stoneScale, 5f) + Position;
			foreach (string point in Skill.Look.Trim().Split(',')) {
				int x = Convert.ToInt32(point) % Skill.GridWidth;
				int y = (int)(Convert.ToInt32(point) / Skill.GridHeight);
				
				//spriteBatch.Draw(Skill.StoneTexture, startTopX + new Vector2(x * Skill.StoneTexture.Width - 2f * x, y * Skill.StoneTexture.Height - 2f * y), Color.White);				
				spriteBatch.Draw(Skill.StoneTexture, new RectangleF(startTopX.X + x * (Skill.StoneTexture.Width * stoneScale) - 2f * x, startTopX.Y + y * (Skill.StoneTexture.Height * stoneScale) - 2f * y, Skill.StoneTexture.Width * stoneScale, Skill.StoneTexture.Height * stoneScale), Color.White);
			}
		}

		public void Update(GameTime gameTime) {

		}

		public Rectangle GetDimension() {
			return new Rectangle((int)Position.X, (int)Position.Y, width, height);
		}

		public object GetObjectFromElement() {
			throw new NotImplementedException();
		}

		public void PerformClick(Vector2 position) {
			throw new NotImplementedException();
		}

		public bool IsEnable() {
			throw new NotImplementedException();
		}

		public bool IsClickable() {
			return false;
		}

		public void SetIsEnable(bool enable) {
			throw new NotImplementedException();
		}

		public void SetOnClickEvent(Action onClickEvent) {
			throw new NotImplementedException();
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GUI.Interface;
using SharpDX;
using SharpDX.Toolkit;
using SharpDX.Toolkit.Graphics;
using RPG;
using RPG.Items;
using GUI;

namespace DungeonCrawler.Controls {
	/// <summary>
	/// Dazu da um Skills auszurüsten. In Form eines Grids. 
	/// Die Skills können sich auf dem Grid befinden oder daneben, aber es können keine Skills übereinander lagern.
	/// </summary>
	public class SkillGridControl : IControl {

		public int Width { get; set; }
		public int Height { get; set; }
		private int CellWidth { get; set; }
		private int CellHeight { get; set; }
		private Texture2D GridCell { get; set; }
		private Texture2D EmptyGridCell { get; set; }
		public Vector2 Position { get; set; }

		public List<SkillGridEntityControl> Skills { get; set; }

		#region Skills verschieben
		SkillGridEntityControl currentControl; //Welcher Skill wurde ausgewählt (derzeit ausgewählt mit Gedrücker MausTaste)
		Vector2 oldPosition = Vector2.Zero; //Position des Skills wenn angefangen wird ihn zu verschieben
		Vector2 offset = Vector2.Zero; //Unterschied zwischen Mausposition und Positon des Skills.
		List<Vector2> gridPositions; //beinhaltet alle GridCells, nämliche alle Linke oberen Positionen
		#endregion

		public Rectangle DeleteArea { get; set; }

		public event EventHandler<SkillGridEventArgs> SkillCompleteInGrid; //Skill befindet sich vollständig im grid
		public event EventHandler<SkillGridEventArgs> SkillNotInGrid; //Skill wurde aus dem grid entfernt, oder befindet sich nicht mehr vollständig auf dem Grid
		public event EventHandler<SkillGridEventArgs> SkillDeleted; //Skill wird entfernt und taucht damit wieder in der Skillliste auf.
		public event EventHandler<SkillGridEventArgs> SkillSelected; //Ein Skill wurde gerade ausgewählt

		public SkillGridControl(Vector2 position, int width, int height, Rectangle deleteArea) {
			Width = width;
			Height = height;
			CellWidth = 32;
			CellHeight = 32;

			Position = position;

			Skills = new List<SkillGridEntityControl>();

			DeleteArea = deleteArea;
		}

		public void AddSkill(Skill skill, Vector2 position) {
			SkillGridEntityControl control = new SkillGridEntityControl(skill, position);
			control.Rotated += new EventHandler<RotationEvent>(Skill_Rotated);
			Skills.Add(control);
		}

		void Skill_Rotated(object sender, RotationEvent e) {
			//prüfen ob der Skill nach dem rotieren kein anderen Skill überlagert
			var overlappingSkill = Skills.Where(s => SkillToVectors(s).Intersect(SkillToVectors(e.Skill)) && s != e.Skill).FirstOrDefault();
			if (overlappingSkill != null) {
				e.Skill.Skill.ChangeGridSize(e.Skill.Skill.GridWidth, e.Skill.Skill.GridHeight, e.OldLook);
				e.Skill.Rotation = e.OldRotation;
			}
		}

		public void Draw(SpriteBatch spriteBatch) {
			if (GridCell == null) { //oneTime Creation
				GridCell = RPG.TextureHelper.CreateTextureWidthBoarder(CellWidth, CellHeight, Color.White, 2, Color.White, spriteBatch.GraphicsDevice);
				EmptyGridCell = RPG.TextureHelper.CreateTextureWidthBoarder(CellWidth, CellHeight, Color.Transparent, 2, Color.Black, spriteBatch.GraphicsDevice);
			}

			bool addGridPositions = false;
			if (gridPositions == null) {
				addGridPositions = true;
				gridPositions = new List<Vector2>();
			}

			//rendern des Basis grids
			for (int x = 0; x < Width; x++) {
				for (int y = 0; y < Height; y++) {
					if (addGridPositions) {
						gridPositions.Add(new Vector2(x * CellWidth - x * 3, y * CellHeight - y * 3));
					}

					spriteBatch.Draw(GridCell, Position + new Vector2(x * CellWidth - x * 3, y * CellHeight - y * 3), Color.White);
				}
			}

			//rendern aller Skills die dem Grid hinzugefügt wurden
			Skills.ForEach(s => s.Draw(spriteBatch));

			//Rendern des Borders des Basis Grid um die individuellen Rahmen von den Skills zu überschreiben
			for (int x = 0; x < Width; x++) {
				for (int y = 0; y < Height; y++) {
					spriteBatch.Draw(EmptyGridCell, Position + new Vector2(x * CellWidth - x * 3, y * CellHeight - y * 3), Color.White);
				}
			}

			if (currentControl != null) {
				currentControl.Draw(spriteBatch);
			}
		}

		public void Update(GameTime gameTime) {
			foreach (SkillGridEntityControl skill in Skills) {
				skill.Update(gameTime);
			}

			//verschieben von Skills ermöglichen
			if (VirtualGamePad.ClickBegin()) {
				if (currentControl != null) {
					currentControl.ChangeBoarderColor(Color.Black);
				}

				Rectangle clickRec = new Rectangle((int)VirtualGamePad.GetClickPoint().X, (int)VirtualGamePad.GetClickPoint().Y, 1, 1);
				currentControl = Skills.Where(s => s.LookToVectors().Where(v => new Rectangle((int)v.X, (int)v.Y, 32, 32).Intersects(clickRec)).Any()).FirstOrDefault();

				if (currentControl != null) {
					offset = currentControl.Position - VirtualGamePad.GetClickPoint();
					oldPosition = currentControl.Position;

					currentControl.ChangeBoarderColor(Color.Red);

					if (SkillSelected != null) {
						SkillSelected(this, new SkillGridEventArgs(currentControl.Skill, currentControl));
					}
				} else {
					if (SkillSelected != null) {
						SkillSelected(this, null);
					}
				}
			}

			//SKill wird aktiv verschoben
			if (VirtualGamePad.ClickHolded()) {
				if (currentControl != null) {
					currentControl.Position = VirtualGamePad.GetClickPoint() + offset;
				}
			}

			//verschieben wird beendet
			if (VirtualGamePad.ClickReleased()) {
				if (currentControl != null) {
					//Skill befindet sich im Raster und wird automatisch ausgerichtet
					if (GetGridDimension().Contains(currentControl.Position)) { 
						var tmp = gridPositions.Where(y => y.Y <= currentControl.Position.Y - Position.Y && y.X <= currentControl.Position.X - Position.X);

						currentControl.Position = tmp.Select(t => new { sum = t.X + t.Y, vec = t }).OrderByDescending(o => o.sum).First().vec + Position;
						tmp = null;
					}

					var overlappingSkill = Skills.Where(s => SkillToVectors(s).Intersect(SkillToVectors(currentControl)) && s != currentControl).FirstOrDefault();
					//wenn sich 2 elemente auf dem Grid überscheiden werden diese zurückgesetzt
					if (overlappingSkill != null) { 
						currentControl.Position = oldPosition;
					//Skills die im gelöschten Bereich abgelegt werde, werden entfernt und tauchen via Event in der SkillListe auf
					} else if (DeleteArea.Inside(currentControl.GetDimension())) {
						Skills.Remove(currentControl);

						if (SkillSelected != null) {
							SkillSelected(this, null);
						}

						if (SkillNotInGrid != null) {
							SkillNotInGrid(this, new SkillGridEventArgs(currentControl.Skill, currentControl));
						}

						if (SkillDeleted != null) {
							SkillDeleted(this, new SkillGridEventArgs(currentControl.Skill, currentControl));
						}

						currentControl.ChangeBoarderColor(Color.Black);
						currentControl = null;
					//Skill überscheidet sich mit keinem anderen und wird auch nicht gelöscht-
					} else {
						if (GetGridDimension().Inside(currentControl.GetDimension())) {
							//rüste den Skill aus.
							if (SkillCompleteInGrid != null) {
								SkillCompleteInGrid(this, new SkillGridEventArgs(currentControl.Skill, currentControl));
							}
						} else {
							//entferne den Skill
							if (SkillNotInGrid != null) {
								SkillNotInGrid(this, new SkillGridEventArgs(currentControl.Skill, currentControl));
							}
						}
					}
				}
			}
		}

		private Vector2[] SkillToVectors(SkillGridEntityControl skill) {
			Vector2[] value = new Vector2[skill.Skill.Look.Split(',').Length];

			int i = 0;
			foreach (string point in skill.Skill.Look.Trim().Split(',')) {
				int x = Convert.ToInt32(point) % skill.Skill.GridWidth;
				int y = (int)(Convert.ToInt32(point) / skill.Skill.GridHeight);

				value[i] = new Vector2(skill.Position.X + x * (skill.Skill.StoneTexture.Width) - 3f * x, skill.Position.Y + y * (skill.Skill.StoneTexture.Height) - 3f * y);
				i += 1;
			}

			return value;
		}

		public void Remove(SkillGridEntityControl skill) {
			Skills.Remove(skill);
			currentControl = null;
		}

		public Rectangle GetDimension() {
			if (Skills.Count > 0) {
				float minX = Skills.Select(s => s.GetDimension().X).Min();
				float maxX = Skills.Select(s => s.GetDimension().BottomRight.X).Max();
				float minY = Skills.Select(s => s.GetDimension().Y).Min();
				float maxY = Skills.Select(s => s.GetDimension().BottomRight.Y).Max();

				return new Rectangle((int)minX, (int)minY, (int)(maxX - minX), (int)(maxY - minY));
			} else {
				return new Rectangle((int)Position.X, (int)Position.Y, 1, 1);
			}
		}

		public Rectangle GetGridDimension() {
			return new Rectangle((int)Position.X - 1, (int)Position.Y - 1, (CellWidth - 2) * (Width - 1) + CellWidth + 8, (CellHeight - 2) * (Height - 1) + CellHeight + 8);
		}

		public object GetObjectFromElement() {
			throw new NotImplementedException();
		}

		public void PerformClick(Vector2 position) {
			Rectangle clickRec = new Rectangle((int)position.X, (int)position.Y, 1, 1);
			SkillGridEntityControl skill = Skills.Where(s => s.LookToVectors().Where(v => new Rectangle((int)v.X, (int)v.Y, 32, 32).Intersects(clickRec)).Any()).FirstOrDefault();

			if (skill != null) {
				skill.PerformClick(position);
			}
		}

		public bool IsEnable() {
			return true;
		}

		public bool IsClickable() {
			return true;
		}

		public void SetIsEnable(bool enable) {
			throw new NotImplementedException();
		}

		public void SetOnClickEvent(Action onClickEvent) {
			throw new NotImplementedException();
		}

		public void SetGridDimension(int width, int height) {
			Width = width;
			Height = height;
			gridPositions = null;
		}
	}

	public class SkillGridEventArgs : EventArgs {
		public Skill Skill { get; private set; }
		public SkillGridEntityControl Control { get; private set; }

		public SkillGridEventArgs(Skill skill, SkillGridEntityControl control) {
			Skill = skill;
			Control = control;
		}
	}
}

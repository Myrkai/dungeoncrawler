﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GUI.Interface;
using SharpDX.Toolkit.Graphics;
using SharpDX.Toolkit;
using SharpDX;
using RPG.Items;
using RPG;
using GUI.Controls;
using SharpDX.Toolkit.Content;

namespace DungeonCrawler.Controls.Skills {
	public class SelectedSkillControl : IControl {

		private Skill _Skill { get; set; }
		public Skill Skill { get { return _Skill; } set { _Skill = value; Refresh(); } }
		public Vector2 Position { get; set; }
		private Texture2D Background { get; set; }
		
		private int width = 309;
		private int height = 55;

		private GraphicsDevice graphicsDevice;

		private LinkControl Title { get; set; }
		private LinkControl Description { get; set; }

		public SelectedSkillControl(Vector2 position, ContentManager content) {
			Position = position;
			Title = new LinkControl("", new Vector2(10f, 5f) + Position, content.Load<SpriteFont>(@"Fonts\Areal12.tk"), Color.White, null);
			Description = new LinkControl("", new Vector2(10f, 35f) + Position, content.Load<SpriteFont>(@"Fonts\Areal8Normal.tk"), Color.White, null);
		}

		private void Refresh() {
			if (Skill != null) {
				Title.Text = Skill.Title.Replace("#value", Skill.Value.ToString());
				Description.Text = Skill.Description;
			} else {
				Title.Text = "";
				Description.Text = "";
			}

			Title.Position = new Vector2(10f, 5f) + Position;
			Description.Position = new Vector2(10f, 35f) + Position;
		}

		public void Resize(int width) {
			this.width = width;
			if (graphicsDevice != null) {
				Background = TextureHelper.CreateTextureWidthBoarder(width, height, Color.Black, 2, Color.DarkGray, graphicsDevice);
				Refresh();
			} else {
				Background = null;
			}
		}

		public void Draw(SpriteBatch spriteBatch) {
			if (Background == null) {
				Background = TextureHelper.CreateTextureWidthBoarder(width, height, Color.Black, 2, Color.DarkGray, spriteBatch.GraphicsDevice);
				graphicsDevice = spriteBatch.GraphicsDevice;
				Refresh();
			}

			spriteBatch.Draw(Background, Position, Color.White * 0.9f);
			Title.Draw(spriteBatch);
			Description.Draw(spriteBatch);

			if (Skill != null) {
				const float stoneScale = 0.5f;
				Vector2 startTopX = new Vector2(width - 5f - Skill.StoneTexture.Width * 4f * stoneScale, 10f) + Position;
				foreach (string point in Skill.OriginLook.Trim().Split(',')) {
					int x = Convert.ToInt32(point) % Skill.GridWidth;
					int y = (int)(Convert.ToInt32(point) / Skill.GridHeight);
			
					spriteBatch.Draw(Skill.StoneTexture, new RectangleF(startTopX.X + x * (Skill.StoneTexture.Width * stoneScale) - 2f * x, startTopX.Y + y * (Skill.StoneTexture.Height * stoneScale) - 2f * y, Skill.StoneTexture.Width * stoneScale, Skill.StoneTexture.Height * stoneScale), Color.White);
				}
			}
		}

		public void Update(GameTime gameTime) {
			
		}

		public Rectangle GetDimension() {
			throw new NotImplementedException();
		}

		public object GetObjectFromElement() {
			throw new NotImplementedException();
		}

		public void PerformClick(Vector2 position) {
			throw new NotImplementedException();
		}

		public bool IsEnable() {
			throw new NotImplementedException();
		}

		public bool IsClickable() {
			return false;
		}

		public void SetIsEnable(bool enable) {
			throw new NotImplementedException();
		}

		public void SetOnClickEvent(Action onClickEvent) {
			throw new NotImplementedException();
		}
	}
}

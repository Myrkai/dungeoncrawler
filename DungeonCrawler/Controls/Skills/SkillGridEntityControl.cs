﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RPG.Items;
using SharpDX;
using GUI.Interface;
using SharpDX.Toolkit.Graphics;
using SharpDX.Toolkit;

namespace DungeonCrawler.Controls {
	/// <summary>
	/// Anzeigen und Rendern eines Skills im Grid.
	/// </summary>
	public class SkillGridEntityControl : IControl {
		public Skill Skill { get; set; }
		public Vector2 Position { get; set; }
		public float Rotation { get; set; }
		
		#region DoubleClick
		private float timer = 0f;
		private const float doubleClickTimer = 0.25f;
		private bool hasClicked = false;

		public event EventHandler<SkillListEventArgs> HasDoubleClicked;
		public event EventHandler<RotationEvent> Rotated;
		#endregion

		public SkillGridEntityControl() { 
		
		}

		public SkillGridEntityControl(Skill skill, Vector2 position) {
			Skill = skill;
			Position = position;
			Rotation = 0f;
		}

		private string RotateSkillLook(float rotation) {
			if (rotation == 0f || rotation == 360f) {
				return Skill.OriginLook;
			}

			//berechnung der Korrdinaten anhand des Looks
			Vector2[] positions = OriginLookToVectors();

			//rotation aller Punkte durchführen
			Vector2[] newCord = new Vector2[Skill.OriginLook.Split(',').Length];
			int i = 0;

			float avgX = OriginLookToVectors().Select(l => l.X).Average();
			float avgY = OriginLookToVectors().Select(l => l.Y).Average();

			foreach (Vector2 vector in positions) {
				newCord[i] = MathHelper.RotateAPoint(vector, Rotation, new Vector2(avgX, avgY));
				i += 1;
			}

			//Zurückrechnen der neuen Korrdinaten zum Look
			string look = "";
			foreach (Vector2 vec in newCord) {
				int x = (int)(((vec.X - Position.X) + 32 * 4) / 32);
				int y = (int)(((vec.Y - Position.Y) + 32 * 4) / 32);

				int a = (x + y * Skill.GridWidth) - ((Skill.OriginLook.Replace(" ", "") == "0,4,5,6" && rotation == 180f) ? 11 : 12);

				look += a.ToString() + ",";
			}

			look = look.Substring(0, look.LastIndexOf(","));

			//verschieben des Looks zum 0 Punkt
			string finalLook = "";
			int ausgleichLinks = look.Split(',').Select(l => Convert.ToInt32(l) % 4).Min();
			int ausgleichOben = look.Split(',').Select(l => Convert.ToInt32(l) / 4).Min() * 4;
			foreach (int num in look.Split(',').Select(l => Convert.ToInt32(l))) {
				finalLook += (num - ausgleichOben - ausgleichLinks).ToString() + ",";
			}
			finalLook = finalLook.Substring(0, finalLook.LastIndexOf(","));

			return finalLook;
		}

		private Vector2[] OriginLookToVectors() {
			Vector2[] positions = new Vector2[Skill.OriginLook.Split(',').Length];
			int i = 0;
			foreach (string point in Skill.OriginLook.Trim().Split(',')) {
				int x = Convert.ToInt32(point) % Skill.GridWidth;
				int y = (int)(Convert.ToInt32(point) / Skill.GridHeight);

				positions[i] = new Vector2(Position.X + x * Skill.StoneTexture.Width, Position.Y + y * Skill.StoneTexture.Height);
				i += 1;
			}
			return positions;
		}

		public Vector2[] LookToVectors() {
			Vector2[] positions = new Vector2[Skill.Look.Split(',').Length];
			int i = 0;
			foreach (string point in Skill.Look.Trim().Split(',')) {
				int x = Convert.ToInt32(point) % Skill.GridWidth;
				int y = (int)(Convert.ToInt32(point) / Skill.GridHeight);

				positions[i] = new Vector2(Position.X + x * Skill.StoneTexture.Width, Position.Y + y * Skill.StoneTexture.Height);
				i += 1;
			}
			return positions;
		}

		public void Draw(SpriteBatch spriteBatch) {
			if (Skill.StoneTexture == null) { //onetime Cretion
				Skill.Draw(spriteBatch);
			}
			
			const float stoneScale = 1f;
			foreach (string point in Skill.Look.Trim().Split(',')) {
				int x = Convert.ToInt32(point) % Skill.GridWidth;
				int y = (int)(Convert.ToInt32(point) / Skill.GridHeight);
			
				spriteBatch.Draw(Skill.StoneTexture, new RectangleF(Position.X + x * (Skill.StoneTexture.Width * stoneScale) - 3f * x, Position.Y + y * (Skill.StoneTexture.Height * stoneScale) - 3f * y, Skill.StoneTexture.Width * stoneScale, Skill.StoneTexture.Height * stoneScale), Color.White);
			}
		}

		public void Update(GameTime gameTime) {
			if (hasClicked) {
				timer += (float)gameTime.ElapsedGameTime.TotalSeconds;
			}
		}

		public Rectangle GetDimension() {
			float maxY = LookToVectors().Select(v => v.Y).Max();
			float maxX = LookToVectors().Select(v => v.X).Max();
			return new Rectangle((int)Position.X, (int)Position.Y, (int)(maxX + Skill.StoneTexture.Width) - (int)Position.X, (int)(maxY + Skill.StoneTexture.Height) - (int)Position.Y);
		}

		public object GetObjectFromElement() {
			throw new NotImplementedException();
		}

		public void PerformClick(Vector2 position) {
			Rectangle clickRec = new Rectangle((int)position.X, (int)position.Y, 1, 1);
			if (hasClicked) {
				if (timer < doubleClickTimer) {
					Rectangle vector = LookToVectors().Select(l => new Rectangle((int)l.X, (int)l.Y, Skill.StoneTexture.Width, Skill.StoneTexture.Height)).Where(l => l.Intersects(clickRec)).FirstOrDefault();

					if (HasDoubleClicked != null && vector != null) {
						HasDoubleClicked(this, null);
					}

					//element wurde doppelt geklickt
					if (vector != null) {
						string oldLook = Skill.Look;
						float oldRotation = Rotation;

						Rotation = (Rotation + 90f >= 360f) ? 0f : (Rotation + 90f);
						Skill.ChangeGridSize(Skill.GridWidth, Skill.GridHeight, RotateSkillLook(Rotation));
						hasClicked = false;

						if (Rotated != null) {
							Rotated(this, new RotationEvent(this, oldLook, oldRotation));
						}
					}

				}

				timer = 0f;
			} else {
				hasClicked = true;
			}
		}

		public bool IsEnable() {
			return true;
		}

		public bool IsClickable() {
			return true;
		}

		public void SetIsEnable(bool enable) {
			throw new NotImplementedException();
		}

		public void SetOnClickEvent(Action onClickEvent) {
			throw new NotImplementedException();
		}

		public void ChangeBoarderColor(Color color) {
			Skill.ChangeBorderColor(color);
		}
	}

	public class RotationEvent : EventArgs {
		public SkillGridEntityControl Skill { get; private set; }
		public string OldLook { get; private set; }
		public float OldRotation { get; private set; }

		public RotationEvent (SkillGridEntityControl skill, string oldlook, float oldRotation) {
			Skill = skill;
			OldLook = oldlook;
			OldRotation = oldRotation;
		}
	}
}

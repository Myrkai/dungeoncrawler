﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GUI.Interface;
using SharpDX.Toolkit.Graphics;
using SharpDX;
using SharpDX.Toolkit;
using GUI;
using SideProjekt.Mechanic.Log;
using GUI.Controls;
using RPG.Items;
using RPG.Items.Enum;

namespace DungeonCrawler.Controls {
	/// <summary>
	/// Dazu da um alle verfügbaren Skills auf der person in form einer scrollbaren Liste aufzulisten.
	/// </summary>
	public class SkillListControl : IControl {
		private List<SkillListEntityControl> Skills { get; set; }
		public Vector2 Position { get; set; }
		public Texture2D BackGround { get; private set; }
		private Texture2D BackBorder { get; set; }

		private ImageControl ButtonUp { get; set; }
		private ImageControl ButtonDown { get; set; }
		private ImageControl Separator { get; set; }

		private ImageControl HideButton { get; set; }
		private ImageControl VisibleButton { get; set; }
		private bool HideControl { get; set; }

		public event EventHandler VisibleChanged;

		public SkillListControl(Vector2 position) {
			Skills = new List<SkillListEntityControl>();
			Position = position;
			HideControl = false;
		}

		public void AddSkillControl(SkillListEntityControl control) {
			control.Position = new Vector2(3f, Skills.Count * control.GetDimension().Height - (Skills.Count - 1) * 2);
			Skills.Add(control);
			if (Separator != null) {
				RefreshSkillEntityPositions();
			}
		}

		public void RemoveSkillType(SkillType type) {
			List<SkillListEntityControl> controls = Skills.Where(s => s.Skill.Type == type).ToList();
			foreach (SkillListEntityControl control in controls) {
				Skills.Remove(control);
			}
			RefreshSkillEntityPositions();
		}

		public void RemoveAll() {
			Skills.Clear();
			RefreshSkillEntityPositions();
			//BackGround = null;
		}

		public void Draw(SpriteBatch spriteBatch) {
			if (BackGround == null) { //Damit die Zeichenoperation nur einmal durchgeführt werden muss
				BackGround = TextureHelper.CreateTextureWidthBoarder(327, Resolution.TargetHeight, Color.Black, 2, Color.DarkGray, spriteBatch.GraphicsDevice);
				BackBorder = TextureHelper.CreateTextureWidthBoarder(327, Resolution.TargetHeight, Color.Transparent, 2, Color.Black, spriteBatch.GraphicsDevice);

				ButtonUp = new ImageControl(TextureHelper.CreateTextureWidthBoarder(16, 16, Color.Black, 1, Color.DarkGray, spriteBatch.GraphicsDevice), new Vector2(308f, 21f));
				ButtonDown = new ImageControl(TextureHelper.CreateTextureWidthBoarder(16, 16, Color.Black, 1, Color.DarkGray, spriteBatch.GraphicsDevice), new Vector2(308f, Resolution.TargetHeight - 20f));
				Separator = new ImageControl(TextureHelper.CreateTextureWidthBoarder(16, 32, Color.Black, 1, Color.DarkGray, spriteBatch.GraphicsDevice), new Vector2(308f, 300f));
				Separator.Position = new Vector2(Separator.Position.X, (Resolution.TargetHeight - 88f) * (offset / (Skills.Count * 55f - (Resolution.TargetHeight - 52f) - (Skills.Count - 1f) * 2f + 5f) * -1f) + 39);

				HideButton = new ImageControl(TextureHelper.CreateTextureWidthBoarder(16, 16, Color.DarkGreen, 1, Color.DarkGray, spriteBatch.GraphicsDevice), new Vector2(308f, 3f));
				VisibleButton = new ImageControl(TextureHelper.CreateTextureWidthBoarder(16, 16, Color.DarkGreen, 1, Color.Black, spriteBatch.GraphicsDevice), new Vector2(4f, 4f));

				RefreshSkillEntityPositions();

				if (VisibleChanged != null) {
					VisibleChanged(this, null);
				}
			}

			if (HideControl) {
				VisibleButton.Draw(spriteBatch);
			} else {
				spriteBatch.Draw(BackGround, Vector2.Zero, Color.White * 0.8f);

				Skills.Where(e => e.Position.Y > -55 && e.Position.Y < Resolution.TargetHeight).ToList().ForEach(e => e.Draw(spriteBatch));
				ButtonUp.Draw(spriteBatch);
				Separator.Draw(spriteBatch);
				ButtonDown.Draw(spriteBatch);
				HideButton.Draw(spriteBatch);

				spriteBatch.Draw(BackBorder, Vector2.Zero, Color.White);
			}
		}

		Vector2 point = Vector2.Zero;
		Vector2 diff = Vector2.Zero;
		public int offset = 0;
		public void Update(GameTime gameTime) {
			if (hasClicked) {
				timer += (float)gameTime.ElapsedGameTime.TotalSeconds;
			}

			//Es wird hiermit ermöglicht die Skillliste zu verschieben, einfach gedrücke haben und die Maus dabei verschieben. Nach dem Loslassen werden von allen Elementen die Position entsprechend angepasst.
			if (GetDimension().Intersects(VirtualGamePad.GetClickPoint().ToRectangle())) {
				if (VirtualGamePad.ClickReleased()) {
					diff = VirtualGamePad.GetClickPoint() - point;

					offset = Math.Min(offset - (int)diff.Y, 0);
					RefreshSkillEntityPositions();
					point = diff = Vector2.Zero;
				}

				if (VirtualGamePad.ClickBegin()) {
					point = VirtualGamePad.GetClickPoint();
				}
			}
		}

		//Aktualisierung alle Positionen/Elemnte von der Fähigkeitenliste. Wird beim verschieben mit dem Finger/Maus oder für die Hoch und Runterbuttons benötigt.
		public void RefreshSkillEntityPositions() {
			if (offset < (Skills.Count * 55 - Resolution.TargetHeight - (Skills.Count - 1) * 2 + 5) * -1) {
				offset = (Skills.Count * 55 - Resolution.TargetHeight - (Skills.Count - 1) * 2 + 5) * -1;
			}

			for (int i = 0; i < Skills.Count; i++) {
				Skills[i].Position = new Vector2(Skills[i].Position.X, offset + i * Skills[i].GetDimension().Height - (i - 1) * 2);
			}

			if (Separator != null) {
				Separator.Position = new Vector2(Separator.Position.X, (Resolution.TargetHeight - 88f) * (offset / (Skills.Count * 55f - (Resolution.TargetHeight - 52f) - (Skills.Count - 1f) * 2f + 5f) * -1f) + 39);
			}
		}

		public void ForceRefreshSkillEntityPositions() {
			offset = 0;
			
			if (offset < (Skills.Count * 55 - Resolution.TargetHeight - (Skills.Count - 1) * 2 + 5) * -1) {
				offset = (Skills.Count * 55 - Resolution.TargetHeight - (Skills.Count - 1) * 2 + 5) * -1;
			}

			for (int i = 0; i < Skills.Count; i++) {
				Skills[i].Position = new Vector2(Skills[i].Position.X, offset + i * Skills[i].GetDimension().Height - (i - 1) * 2);
			}

			if (Separator != null) {
				Separator.Position = new Vector2(Separator.Position.X, (Resolution.TargetHeight - 88f) * (offset / (Skills.Count * 55f - (Resolution.TargetHeight - 52f) - (Skills.Count - 1f) * 2f + 5f) * -1f) + 39);
			}
		}

		public Rectangle GetDimension() {
			if (HideControl) {
				return VisibleButton.GetDimension();
			} else {
				return new Rectangle(0, 0, BackGround == null ? 1 : BackGround.Width, BackGround == null ? 1 : BackGround.Height);
			}
		}

		public object GetObjectFromElement() {
			throw new NotImplementedException();
		}

		#region DoubleClick
		private float timer = 0f;
		private const float doubleClickTimer = 0.25f;
		private bool hasClicked = false;
		public event EventHandler<SkillListEventArgs> HasDoubleClicked;
		#endregion
		public void PerformClick(Vector2 position) {
			Rectangle clickRec = new Rectangle((int)position.X, (int)position.Y, 1, 1);
			if (hasClicked) {
				if (timer < doubleClickTimer) {
					var tmp = Skills.Where(s => s.GetDimension().Intersects(clickRec)).FirstOrDefault();
					if (HasDoubleClicked != null && tmp != null) {
						HasDoubleClicked(this, new SkillListEventArgs(tmp.Skill));
					}
					hasClicked = false;
				}		
				timer = 0f;
			} else {
				hasClicked = true;
			}

			if (!HideControl) {
				if (ButtonUp.GetDimension().Intersects(clickRec)) {
					offset += 53;
					RefreshSkillEntityPositions();
				}

				if (ButtonDown.GetDimension().Intersects(clickRec)) {
					offset -= 53;
					RefreshSkillEntityPositions();
				}

				if (HideButton.GetDimension().Intersects(clickRec)) {
					HideControl = !HideControl;
					if (VisibleChanged != null) {
						VisibleChanged(this, null);
					}
				}
			} else if (HideControl && VisibleButton.GetDimension().Intersects(clickRec)) {
				HideControl = !HideControl;
				if (VisibleChanged != null) {
					VisibleChanged(this, null);
				}
			}
		}

		public bool IsEnable() {
			return true;
		}

		public bool IsClickable() {
			return true;
		}

		public void SetIsEnable(bool enable) {
			throw new NotImplementedException();
		}

		public void SetOnClickEvent(Action onClickEvent) {
			throw new NotImplementedException();
		}
	}

	public class SkillListEventArgs : EventArgs {
		public Skill SelectedSkill { get; private set; }

		public SkillListEventArgs(Skill skill) {
			SelectedSkill = skill;
		}
	}
}

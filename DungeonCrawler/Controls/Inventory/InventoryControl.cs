﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RPG.Items.Inventory;
using GUI.Interface;
using SharpDX.Toolkit;
using SharpDX;
using GUI;
using SharpDX.Toolkit.Graphics;

namespace DungeonCrawler.Controls {
	public class InventoryControl : Inventory, IControl {

		#region Events
		public override event EventHandler<InventoryEvent> ItemDoubleClicked;
		public override event EventHandler<InventoryEvent> ItemBeginDrag;
		public override event EventHandler<InventoryEvent> ItemEndDrag;
		public override event EventHandler<InventoryEvent> ItemMouseHoverBegin;
		public override event EventHandler<InventoryEvent> ItemMouseHoverEnd;
		#endregion

		#region DoubleClick
		private float timer = 0f;
		private const float doubleClickTimer = 0.25f;
		private bool hasClicked = false;
		#endregion

		#region Items verschieben
		private InventoryPosition currentItem; //Welches Item wurde ausgewählt (derzeit ausgewählt mit Gedrücker MausTaste)
		private Vector2 oldPosition = Vector2.Zero; //Position des Items wenn angefangen wird ihn zu verschieben
		private Vector2 offset = Vector2.Zero; //Unterschied zwischen Mausposition und Positon des Items.
		#endregion

		#region hoverItem
		private InventoryPosition hoverItem;
		#endregion

		public InventoryControl()
			: base() {
		}

		public InventoryControl(int maxWidth, int maxHeight, int width, int height)
			: base(maxWidth, maxHeight, width, height) {
		}

		public void PerformClick(Vector2 position) {
			//Check & perform Double Click			
			Rectangle clickRec = new Rectangle((int)(position.X - Position.X), (int)(position.Y - Position.Y), 1, 1);
			if (hasClicked) {
				if (timer < doubleClickTimer) {
					currentItem = null;
					InventoryPosition clickedItem = Items.Where(i => new Rectangle((int)i.Position.X, (int)i.Position.Y, i.Item.Texture.Width, i.Item.Texture.Height).Intersects(clickRec)).FirstOrDefault();

					if (clickedItem != null) {
						if (ItemDoubleClicked != null) {
							ItemDoubleClicked(this, new InventoryEvent(clickedItem.Item, clickedItem.Position + Position, clickedItem, oldPosition));
						}

						hasClicked = false;
					}
				}

				timer = 0f;
			} else {
				hasClicked = true;
			}
		}

		public override void Draw(SpriteBatch spriteBatch) {
			base.Draw(spriteBatch);

			if (currentItem != null) {
				spriteBatch.Draw(currentItem.Item.Texture, currentItem.Position + Position, Color.White);
			}
		}

		public override void Update(GameTime gameTime) {
			if (hasClicked) {
				timer += (float)gameTime.ElapsedGameTime.TotalSeconds;
			}

			//Conzept MouseHover: damit die Events nicht in 60FPS fliegen das der Coursor drüber ist, wird immer nur das Start- und End-Events des MouseHovers geworfen
			//Es werden nur neue Events geworfen wenn das Items sich zwischen 2 Zyklen verändert oder nicht mehr vorhanden ist
			Rectangle mouseRec = VirtualGamePad.GetClickPoint().ToRectangle();
			var tmp = Items.Where(i => new Rectangle((int)(i.Position.X + Position.X), (int)(i.Position.Y + Position.Y), i.Item.Texture.Width, i.Item.Texture.Height).Intersects(mouseRec)).FirstOrDefault();
			if (tmp != null) {//befindet sich das Coursor über ein Item
				if (hoverItem == null) {//Coursor war noch nie über ein item
					hoverItem = tmp;

					if (ItemMouseHoverBegin != null) {
						ItemMouseHoverBegin(this, new InventoryEvent(hoverItem.Item, hoverItem.Position, hoverItem, oldPosition));
					}
				} else if (hoverItem == tmp) {//Item ist über dem der Coursor ist, ist das gleiche
					//tu nichts
				} else {
					if (ItemMouseHoverEnd != null) {
						ItemMouseHoverEnd(this, new InventoryEvent(hoverItem.Item, hoverItem.Position, hoverItem,oldPosition));
						hoverItem = null;
					}

					hoverItem = tmp;

					if (ItemMouseHoverBegin != null) {
						ItemMouseHoverBegin(this, new InventoryEvent(hoverItem.Item, hoverItem.Position, hoverItem, oldPosition));
					}
				}
			} else if (hoverItem != null) {//coursor befindet sich nicht über ein Item, aber er war mal drüber
				if (ItemMouseHoverEnd != null) {
					ItemMouseHoverEnd(this, new InventoryEvent(hoverItem.Item, hoverItem.Position, hoverItem, oldPosition));
					hoverItem = null;
				}
			}

			//Begin Drag & Drop
			if (VirtualGamePad.ClickBegin()) {
				Rectangle clickRec = new Rectangle((int)VirtualGamePad.GetClickPoint().X, (int)VirtualGamePad.GetClickPoint().Y, 1, 1);
				currentItem = Items.Where(i => new Rectangle((int)(i.Position.X + Position.X), (int)(i.Position.Y + Position.Y), i.Item.Texture.Width, i.Item.Texture.Height).Intersects(clickRec)).FirstOrDefault();

				if (currentItem != null) {
					offset = currentItem.Position - VirtualGamePad.GetClickPoint();
					oldPosition = currentItem.Position;

					if (ItemBeginDrag != null) {
						ItemBeginDrag(this, new InventoryEvent(currentItem.Item, currentItem.Position, currentItem, oldPosition));
					}
				}
			}

			//perform Drag & Drop
			if (VirtualGamePad.ClickHolded()) {
				if (currentItem != null) {
					currentItem.Position = VirtualGamePad.GetClickPoint() + offset;
				}
			}

			//End Drag & Drop
			if (VirtualGamePad.ClickReleased()) {
				if (currentItem != null) {
					Rectangle currentItemRec = new Rectangle((int)currentItem.Position.X, (int)currentItem.Position.Y, 1, 1);
					InventoryPosition overlappingItem = Items.Where(i => new Rectangle((int)i.Position.X, (int)i.Position.Y, i.Item.Texture.Width, i.Item.Texture.Height).Intersects(currentItemRec) && i != currentItem).FirstOrDefault();

					//prüfen ob Item sich innerhalb des Inventars befindet
					if (!(currentItem.Position + Position).ToRectangle().Intersects(GetActiveDimension())) {
						RemoveItem(currentItem.Item);
						bool result = AddItem(currentItem.Item, oldPosition + Position);

						if (!result) {
							throw new Exception();
						}
					} else if (overlappingItem == null) {//prüfen ob position besetzt ist
						// wenn leer dann dort verschieben
						RemoveItem(currentItem.Item);
						if (!AddItem(currentItem.Item, currentItem.Position + Position)) {//nur wenn das Item wirklich zum Inventar hinzugefügt wurde
							bool result = AddItem(currentItem.Item, oldPosition + Position);

							if (!result) {
								throw new Exception();
							}
						}
					} else {// wenn nicht leer prüfen ob austausch der Items möglich ist
						RemoveItem(currentItem.Item);
						RemoveItem(overlappingItem.Item);
						Vector2 currentPos = oldPosition;
						Vector2 overPos = overlappingItem.Position;
						bool current = AddItem(currentItem.Item, overPos + Position);
						bool over = AddItem(overlappingItem.Item, currentPos + Position);

						if (current && over) {
							//  Wenn ja, dann tausche aus
						} else {
							//  wenn nein, dann tue nichts und items bekommen alte positionen wieder
							if (current) {
								RemoveItem(currentItem.Item);
							}

							if (over) {
								RemoveItem(overlappingItem.Item);
							}

							bool resultA = AddItem(currentItem.Item, currentPos+ Position);
							bool resultB = AddItem(overlappingItem.Item, overPos + Position);

							if (!resultA || !resultB) {
								throw new Exception();
							}
						}
					}
					
					if (ItemEndDrag != null) {
						ItemEndDrag(this, new InventoryEvent(currentItem.Item, currentItem.Position, currentItem, oldPosition));
					}
					currentItem = null;
					oldPosition = Vector2.Zero;
					offset = Vector2.Zero;
				}
			}
		}

		public bool IsEnable() {
			return true;
		}

		public bool IsClickable() {
			return true;
		}

		#region NotImplemented
		public void SetIsEnable(bool enable) {
			throw new NotImplementedException();
		}

		public void SetOnClickEvent(Action onClickEvent) {
			throw new NotImplementedException();
		}

		public object GetObjectFromElement() {
			throw new NotImplementedException();
		}
		#endregion

		internal void Clear() {
			foreach (var item in Items.ToList()) {
				RemoveItem(item.Item);
			}
		}
	}
}

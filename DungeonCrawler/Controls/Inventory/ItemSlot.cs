﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GUI.Interface;
using SharpDX.Toolkit.Graphics;
using SharpDX.Toolkit;
using SharpDX;
using RPG.Items;
using RPG.Items.Enum;
using DungeonCrawler;
using RPG;
using GUI;

namespace DungeonCrawler.Controls {
	public class ItemSlot : IControl {

		public Vector2 Position { get; private set; }
		public Rectangle PositionRec { get; private set; }
		private Texture2D BackGround { get; set; }
		public Item Item { get; private set; }
		public ItemType SlotType { get; private set; }

		public event EventHandler<ItemSlotEvents> DoubleClicked;
		public event EventHandler<ItemSlotEvents> BeginDrag;
		public event EventHandler<ItemSlotEvents> EndDrag;
		public event EventHandler<ItemSlotEvents> MouseHoverBegin;
		public event EventHandler<ItemSlotEvents> MouseHoverEnd;

		#region DoubleClick
		private float timer = 0f;
		private const float doubleClickTimer = 0.25f;
		private bool hasClicked = false;
		#endregion

		#region Items verschieben
		private Item currentItem;
		private Vector2 oldPosition = Vector2.Zero; //Position des Items wenn angefangen wird ihn zu verschieben
		private Vector2 offset = Vector2.Zero; //Unterschied zwischen Mausposition und Positon des Items.
		#endregion

		#region hoverItem
		private Item hoverItem;
		#endregion

		public ItemSlot(Rectangle positionRec, ItemType slotType) {
			Position = positionRec.ToVector2();
			PositionRec = positionRec;
			BackGround = null;
			Item = null;
			SlotType = slotType;
		}

		public void Draw(SpriteBatch spriteBatch) {
			if (BackGround == null) {
				BackGround = RPG.TextureHelper.CreateTexture(PositionRec.Width, PositionRec.Height, Color.White, spriteBatch.GraphicsDevice);
			}

			if (Item != null) {
				spriteBatch.Draw(BackGround, PositionRec.ToVector2(), Color.Black);
				spriteBatch.Draw(Item.Texture, new RectangleF(PositionRec.X, PositionRec.Y, PositionRec.Width, PositionRec.Height), Color.White);
			}

			if (currentItem != null && Position + offset != oldPosition) {
				spriteBatch.Draw(BackGround, new RectangleF(Position.X, Position.Y, Item.Texture.Width, Item.Texture.Height), Color.Blue * 0.45f);
				spriteBatch.Draw(Item.Texture, Position, Color.White);
			}
		}

		public void Update(GameTime gameTime) {
			if (hasClicked) {
				timer += (float)gameTime.ElapsedGameTime.TotalSeconds;
			}

			Rectangle clickRec = VirtualGamePad.GetClickPoint().ToRectangle();
			//double click
			if (Item != null && clickRec.Intersects(GetDimension()) && VirtualGamePad.ClickReleased()) {
				if (hasClicked) {
					if (timer < doubleClickTimer) {
						if (DoubleClicked != null) {
							DoubleClicked(this, new ItemSlotEvents(Item, Position));
						}

						hasClicked = false;
					}

					timer = 0f;
				} else {
					hasClicked = true;
				}
			}

			//hover Item
			if (Item != null && clickRec.Intersects(GetDimension())) {
				if (hoverItem == null) {
					hoverItem = Item;
					if (MouseHoverBegin != null) {
						MouseHoverBegin(this, new ItemSlotEvents(Item, clickRec.ToVector2()));
					}
				}
			} else if (hoverItem != null) {
				hoverItem = null;
				if (MouseHoverEnd != null) {
					MouseHoverEnd(this, new ItemSlotEvents(Item, clickRec.ToVector2()));
				}
			}

			//Begin Drag & Drop
			if (VirtualGamePad.ClickBegin()) {
				if (clickRec.Intersects(GetDimension())) {
					if (BeginDrag != null) {
						BeginDrag(this, new ItemSlotEvents(Item, Position));
					}

					offset = Position - VirtualGamePad.GetClickPoint();
					currentItem = Item;
					oldPosition = Position;
				}
			}

			//perform Drag & Drop
			if (VirtualGamePad.ClickHolded()) {
				if (currentItem != null) {
					Position = VirtualGamePad.GetClickPoint();
				}
			}

			//End Drag & Drop
			if (VirtualGamePad.ClickReleased()) {
				if (EndDrag != null) {
					EndDrag(this, new ItemSlotEvents(Item, Position));
				}

				Position = oldPosition;

				currentItem = null;
				oldPosition = Vector2.Zero;
				offset = Vector2.Zero;
			}
		}

		public Rectangle GetDimension() {
			return PositionRec;
		}

		public void PerformClick(Vector2 position) {
			if (Item != null) {

			}
		}

		public bool IsEnable() {
			return true;
		}

		public bool IsClickable() {
			return true;
		}

		public bool SetItem(Item item) {
			//Ausgerüsteter Gegenstand soll entfernt werden
			if (Item != null && item == null) {
				Item = null;
				return true;
				//Ein Gegenstand wird ausgerüstet
			} else if (item != null && SlotType == item.Type) {
				Item = item;
				return true;
			} else {
				return false;
			}
		}

		public object GetObjectFromElement() {
			throw new NotImplementedException();
		}

		public void SetIsEnable(bool enable) {
			throw new NotImplementedException();
		}

		public void SetOnClickEvent(Action onClickEvent) {
			throw new NotImplementedException();
		}
	}

	public class ItemSlotEvents : EventArgs {
		public Item Item { get; private set; }
		public Vector2 Position { get; private set; }

		public ItemSlotEvents(Item item, Vector2 position) {
			Item = item;
			Position = position;
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpDX.Toolkit.Graphics;
using SharpDX;
using RPG.Items;
using RPG.Items.Enum;
using GUI.Interface;
using SharpDX.Toolkit;
using RPG;

namespace DungeonCrawler.Controls {
	public class ToolTipText : IControl {
		private Item item;
		private Texture2D backGround;
		public Vector2 Position { get; set; }
		private List<Tuple<Vector2, string, Color>> texts;
		private SpriteFont font;

		public ToolTipText(Item item, Vector2 position, SpriteFont font, bool forShop) {
			this.item = item;
			this.Position = position;
			this.font = font;
			texts = new List<Tuple<Vector2, string, Color>>();

			if (forShop) {
				CreateShopText(item, font);
			} else {
				CreateTexts(item, font);
			}
		}

		private void CreateShopText(Item item, SpriteFont font) {
			//create Texts
			int borderTop = 5;
			float HeaderHight = font.MeasureString(item.Title).Y;

			texts.Add(new Tuple<Vector2, string, Color>(new Vector2(5, borderTop), "Kosten: " +  String.Format("{0:n0}", item.Cost), Color.White)); //Kosten des Gegenstandes
			texts.Add(new Tuple<Vector2, string, Color>(new Vector2(5, borderTop * 2 + HeaderHight), item.Title, Color.White)); //Name des Gegenstandes
			if (item.Type == ItemType.Weapon) { //Ausgabe des Schadens oder Rüstungs-Wertes
				texts.Add(new Tuple<Vector2, string, Color>(new Vector2(5, borderTop * 3 + HeaderHight * 2), "Schaden: " + ((Weapon)item).MinDamage.ToString() + " bis " + ((Weapon)item).MaxDamage.ToString(), Color.White));
			} else {
				texts.Add(new Tuple<Vector2, string, Color>(new Vector2(5, borderTop * 3 + HeaderHight * 2), "Rüstung: " + ((Armor)item).Defence.ToString(), Color.White));
			}
		}

		private void CreateTexts(Item item, SpriteFont font) {
			//create Texts
			int borderTop = 5;
			float HeaderHight = font.MeasureString(item.SpecialTitle).Y;

			Color headLine;
			switch (item.Quality) {
				case Quality.Normal: headLine = Color.White; break;
				case Quality.Magic: headLine = Color.DodgerBlue; break;
				case Quality.Rare: headLine = Color.Yellow; break;
				case Quality.Legendary: headLine = Color.Orange; break;
				default: throw new Exception("missing color");
			}

			texts.Add(new Tuple<Vector2, string, Color>(new Vector2(5, borderTop), item.SpecialTitle, headLine)); //Uniq Titel des besten Skills
			texts.Add(new Tuple<Vector2, string, Color>(new Vector2(5, borderTop * 2 + HeaderHight), item.Title, headLine)); //Name des Gegenstandes
			if (item.Type == ItemType.Weapon) { //Ausgabe des Schadens oder Rüstungs-Wertes
				texts.Add(new Tuple<Vector2, string, Color>(new Vector2(5, borderTop * 3 + HeaderHight * 2), "Schaden: " + ((Weapon)item).MinDamage.ToString() + " bis " + ((Weapon)item).MaxDamage.ToString(), Color.White));
			} else {
				texts.Add(new Tuple<Vector2, string, Color>(new Vector2(5, borderTop * 3 + HeaderHight * 2), "Rüstung: " + ((Armor)item).Defence.ToString(), Color.White));
			}

			int i = 1;
			foreach (Skill skill in item.AvaiableSkills) {//Ausgabe aller Fähigkeiten des Items
				texts.Add(new Tuple<Vector2, string, Color>(new Vector2(5, borderTop * (3 + i) + HeaderHight * (2 + i)), skill.Title.Replace("#value", skill.Value.ToString()), Color.LightBlue));
				i += 1;
			}
		}

		public void Draw(SpriteBatch spriteBatch) {
			if (backGround == null) {
				float maxTextWidth = texts.Max(t => font.MeasureString(t.Item2).X);
				float maxHeight = texts.Max(t => t.Item1.Y) + font.MeasureString(texts[0].Item2).Y;
				backGround = TextureHelper.CreateTexture((int)maxTextWidth + 10, (int)maxHeight + 5, Color.Black, spriteBatch.GraphicsDevice);

				List<Tuple<Vector2, string, Color>> tmp = new List<Tuple<Vector2, string, Color>>();
				foreach (Tuple<Vector2, string, Color> element in texts) {
					tmp.Add(new Tuple<Vector2, string, Color>(new Vector2(backGround.Width / 2 - font.MeasureString(element.Item2).X / 2, element.Item1.Y), element.Item2, element.Item3));
				}
				texts = tmp;
			} else {
				spriteBatch.Draw(backGround, Position, Color.White * 0.7f);
				foreach (Tuple<Vector2, string, Color> element in texts) {
					spriteBatch.DrawString(font, element.Item2, element.Item1 + Position, element.Item3);
				}
			}
		}

		public void Refresh() {
			backGround = null;
			texts.Clear();
			CreateTexts(item, font);
		}

		public void Update(GameTime gameTime) {
			throw new NotImplementedException();
		}

		public Rectangle GetDimension() {
			if (backGround == null) {
				return new Rectangle((int)Position.X, (int)Position.Y, 400, 300);
			}
			return new Rectangle((int)Position.X, (int)Position.Y, backGround.Width, backGround.Height);
		}

		public object GetObjectFromElement() {
			throw new NotImplementedException();
		}

		public void PerformClick(Vector2 position) {
			throw new NotImplementedException();
		}

		public bool IsEnable() {
			throw new NotImplementedException();
		}

		public bool IsClickable() {
			throw new NotImplementedException();
		}

		public void SetIsEnable(bool enable) {
			throw new NotImplementedException();
		}

		public void SetOnClickEvent(Action onClickEvent) {
			throw new NotImplementedException();
		}
	}
}

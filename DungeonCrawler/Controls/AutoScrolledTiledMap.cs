﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TiledMap;
using SharpDX.Toolkit.Content;
using GUI.Interface;
using SharpDX;
using SharpDX.Toolkit;
using SharpDX.Toolkit.Graphics;
using System.IO;
using GUI;


namespace DungeonCrawler.Controls {
	public class AutoScrolledTiledMap : TiledMap.TiledMap, IControl {

		private new Camera Cam { get; set; }
		public float Speed { set { Random rnd = new Random(); Violency = new Vector2(rnd.NextFloat(-value, value), rnd.NextFloat(-value, value)); } }
		private Vector2 Violency { get; set; }

		public AutoScrolledTiledMap(string path, ContentManager content, string tileSetPathFolder, Camera cam) : base(path, content, tileSetPathFolder, cam) {
			this.Cam = cam;

			Speed = 250f;
		}

		public override void Draw(SpriteBatch spriteBatch) {
			base.Draw(spriteBatch);
		}

//http://www.indiedev.de/wiki/SpriteBatch_Magic:_Grayscale
//        float4 PixelShaderFunction(VertexShaderOutput input) : COLOR0
//{
//    float4 color = tex2D(textureSampler, input.texCoord);
//    float gray = color.r * 0.299 + color.g * 0.587 + color.b * 0.114;
 
//    return float4(gray, gray, gray, color.a);
//}

//http://www.indiedev.de/wiki/Kreisf%C3%B6rmige_Fortschrittsanzeige

//http://www.indiedev.de/wiki/Farbverlauf
//"new Texture2D(.....)" width,height and a graphics device is needed
//Then you assign the colors
//"newTexture.SetData(colors);"

		public void Update(GameTime gameTime) {
			if (Cam.Position.X < 0) {
				Violency *= new Vector2(-1f, 1f);
			}

			if (Cam.Position.Y < 0) {
				Violency *= new Vector2(1f, -1f);
			}

			if (Cam.Position.X + Cam.Width > MapWidthPx) {
				Violency *= new Vector2(-1f, 1f);
			}

			if (Cam.Position.Y + Cam.Height > MapHeightPx) {
				Violency *= new Vector2(1f, -1f);
			}
			
			Cam.Position = Cam.Position + Violency * (float)gameTime.ElapsedGameTime.TotalSeconds;
		}

		public Rectangle GetDimension() {
			return new Rectangle(0, 0, Cam.Width, Cam.Height);
		}

		public object GetObjectFromElement() {
			throw new NotImplementedException();
		}

		public void PerformClick(Vector2 position) {
			throw new NotImplementedException();
		}

		public bool IsEnable() {
			return true;
		}

		public bool IsClickable() {
			return false;
		}

		public void SetIsEnable(bool enable) {
			throw new NotImplementedException();
		}

		public void SetOnClickEvent(Action onClickEvent) {
			throw new NotImplementedException();
		}
	}
}

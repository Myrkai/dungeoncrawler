﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GUI.Interface;
using SharpDX.Toolkit.Graphics;
using SharpDX.Toolkit;
using SharpDX;
using Core;
using GUI.Controls;
using GUI;
using SharpDX.Toolkit.Content;
using Core.Enums;
using RPG.Sprites;
using RPG.Chars;
using DungeonCrawler.CoreElements;
using Core.Map;
using GUI.Screens;
using Core.BattleSystem;
using RPG.Items;

namespace DungeonCrawler.Controls {
	public class EngineControl : IControl {
		private Engine EngineRef { get; set; }
		private ScreenManager ScreenManager { get; set; }

		private List<ImageControl> Labels { get; set; }
		private List<LinkControl> StaticTexts { get; set; }

		private ProgressbarControl NextFightProgressBar { get; set; }
		private ProgressbarControl EXPProgressBar { get; set; }
		private LinkControl EXPString { get; set; }
		private LinkControl LayerString { get; set; }
		private LinkControl LevelString { get; set; }
		private LinkControl GameModeString { get; set; }
		private LinkControl DurationString { get; set; }

		private Dictionary<Monsterzone, Texture2D> ZoneTextures { get; set; }
		private CorePlayer Player { get; set; }

		public EngineControl(Engine engineRef, GraphicsDevice graphics, ContentManager content, ScreenManager screenManager, CorePlayer player) {
			EngineRef = engineRef;
			ScreenManager = screenManager;
			Player = player;
			EngineRef.CurrentMap.Zones.ForEach(z => z.CurrentLayer = Player.GetLayer());
			EngineRef.CurrentMap.BossZone.CurrentLayer = Player.GetLayer();

			ZoneTextures = new Dictionary<Monsterzone, Texture2D>();

			Labels = new List<ImageControl>();
			Labels.Add(new ImageControl(TextureHelper.CreateTextureWidthBoarder(225, 50, Color.LightBlue, 2, Color.Black, graphics), new Vector2(0f, (Resolution.TargetHeight - 122f)), 0.85f));
			Labels.Add(new ImageControl(TextureHelper.CreateTextureWidthBoarder(225, 75, Color.LightBlue, 2, Color.Black, graphics), new Vector2(0f, (Resolution.TargetHeight - 75f)), 0.85f));
			Labels.Add(new ImageControl(TextureHelper.CreateTextureWidthBoarder(225, 122, Color.LightBlue, 2, Color.Black, graphics), new Vector2((Resolution.TargetWidth - 225f), (Resolution.TargetHeight - 122f)), 0.85f));
			Labels.Add(new ImageControl(TextureHelper.CreateTextureWidthBoarder((int)((Resolution.TargetWidth - 225f) - 217f), 50, Color.LightBlue, 2, Color.Black, graphics), new Vector2(222f, (Resolution.TargetHeight - 122f)), 0.85f));
			Labels.Add(new ImageControl(TextureHelper.CreateTextureWidthBoarder((int)((Resolution.TargetWidth - 225f) - 217f), 75, Color.LightBlue, 2, Color.Black, graphics), new Vector2(222f, (Resolution.TargetHeight - 75f)), 0.85f));

			StaticTexts = new List<LinkControl>();
			StaticTexts.Add(new LinkControl("Ebene", new Vector2(6f, (Resolution.TargetHeight - 113f)), content.Load<SpriteFont>(@"Fonts\Areal20.tk"), Color.Black, null));
			StaticTexts.Add(new LinkControl("Level", new Vector2(6f, (Resolution.TargetHeight - 55f)), content.Load<SpriteFont>(@"Fonts\Areal20.tk"), Color.Black, null));
			StaticTexts.Add(new LinkControl("EXP", new Vector2(233f, (Resolution.TargetHeight - 70f)), content.Load<SpriteFont>(@"Fonts\Areal20.tk"), Color.Black, null));
			StaticTexts.Add(new LinkControl("Kampfwahrscheinlichkeit", new Vector2(233f, (Resolution.TargetHeight - 117f)), content.Load<SpriteFont>(@"Fonts\Areal10.tk"), Color.Black, null));

			//dynamische Texte
			Texture2D fullBarNextFight = content.Load<Texture2D>(@"GUI\battleProgressBar.png");
			Texture2D emptyBar = TextureHelper.CreateTextureWidthBoarder((int)(Resolution.TargetWidth - 470f), 15, Color.Gray, 2, Color.Gray, graphics);
			double progressFight = engineRef.Player.GetTimeToNextBattle().TotalSeconds / engineRef.Player.GetNextBattleTime().TotalSeconds;
			NextFightProgressBar = new ProgressbarControl((int)(Resolution.TargetWidth - 470f), 15, new Vector2(233f, (Resolution.TargetHeight - 97f)), fullBarNextFight, emptyBar, (float)progressFight, 2.47f);

			Texture2D fullBarEXP = content.Load<Texture2D>(@"GUI\expProgressBar.png");
			float progressEXP = (engineRef.Player.GetCurrentEXP() * 1f) / (engineRef.Player.GetNextLevelEXP() + engineRef.Player.GetBeforeLevelEXP()); //* 1f = damit die rechnung als float angesehen wird
			EXPProgressBar = new ProgressbarControl((int)(Resolution.TargetWidth - 470f), 15, new Vector2(233f, (Resolution.TargetHeight - 33f)), fullBarEXP, emptyBar, progressEXP, 2.47f);

			string _expString = String.Format("{0:n0}", engineRef.Player.GetCurrentEXP()) + " / " + String.Format("{0:n0}", engineRef.Player.GetNextLevelEXP());
			float _expStringWidth = content.Load<SpriteFont>(@"Fonts\Areal20.tk").MeasureString(_expString).X;
			EXPString = new LinkControl(_expString, new Vector2((Resolution.TargetWidth - 237f) - _expStringWidth, (Resolution.TargetHeight - 70f)), content.Load<SpriteFont>(@"Fonts\Areal20.tk"), Color.Black, null);

			float layerStringWidth = content.Load<SpriteFont>(@"Fonts\Areal20.tk").MeasureString(engineRef.Player.GetLayer().ToString()).X;
			LayerString = new LinkControl(engineRef.Player.GetLayer().ToString(), new Vector2(215f - layerStringWidth, (Resolution.TargetHeight - 113f)), content.Load<SpriteFont>(@"Fonts\Areal20.tk"), Color.Black, null);

			float levelStringWidth = content.Load<SpriteFont>(@"Fonts\Areal20.tk").MeasureString(engineRef.Player.GetLevel().ToString()).X;
			LevelString = new LinkControl(engineRef.Player.GetLevel().ToString(), new Vector2(215f - levelStringWidth, (Resolution.TargetHeight - 55f)), content.Load<SpriteFont>(@"Fonts\Areal20.tk"), Color.Black, null);

			string _gameModeString = engineRef.Player.GetGameMode() == GameModes.TimeAttack ? "Zeit" : "Kampfrunden";
			float gameModeWidth = content.Load<SpriteFont>(@"Fonts\Areal20.tk").MeasureString(_gameModeString).X;
			GameModeString = new LinkControl(_gameModeString, new Vector2((Resolution.TargetWidth - 225f) + 225f / 2 - gameModeWidth / 2, (Resolution.TargetHeight - 116f)), content.Load<SpriteFont>(@"Fonts\Areal20.tk"), Color.Black, null);

			string _durationString = "(^_^)";
			if (EngineRef.Player.GetGameMode() == GameModes.RoundAttack) {
				_durationString = EngineRef.Player.GetBattleRounds().ToString();
			} else if (EngineRef.Player.GetGameMode() == GameModes.TimeAttack) {
				_durationString = EngineRef.Player.GetBattleTime().Minutes.ToString("00") + ":" + EngineRef.Player.GetBattleTime().Seconds.ToString("00");
			}
			float duarationWidth = content.Load<SpriteFont>(@"Fonts\Areal48.tk").MeasureString(_durationString).X;
			DurationString = new LinkControl(_durationString, new Vector2((Resolution.TargetWidth - 225f) + 225f / 2 - duarationWidth / 2, (Resolution.TargetHeight - 84f)), content.Load<SpriteFont>(@"Fonts\Areal48.tk"), Color.Black, null);

			//List<Item> items = new List<Item>();
			//foreach (var a in Enumerable.Range(1, 100000)) { 
			//    items.Add((Item)EngineRef.ItemManager.GenerateItem(10, Core.Items.Enum.ItemType.Shoe, Core.Items.Enum.Quality.Magic));
			//}
			//var aa = 1;
		}

		public void Draw(SpriteBatch spriteBatch) {
			if (ZoneTextures.Count == 0) {//geht nun ratten schnell
				EngineRef.CurrentMap.Zones.ForEach(s => ZoneTextures.Add(s, TextureHelper.CreateTextureWidthBoarderAndSmooth(s.Dimension.Width - 10, s.Dimension.Height - 10, Color.Transparent, 1, Color.Red, 3, true, spriteBatch.GraphicsDevice)));
				ZoneTextures.Add(EngineRef.CurrentMap.BossZone, TextureHelper.CreateTextureWidthBoarderAndSmooth(EngineRef.CurrentMap.BossZone.Dimension.Width, EngineRef.CurrentMap.BossZone.Dimension.Height, Color.Transparent, 1, Color.Red, 3, true, spriteBatch.GraphicsDevice));
			}

			EngineRef.CurrentMap.TiledMap.Draw(spriteBatch);
			EngineRef.CurrentMap.CurrentVisibleZones.ForEach(z => spriteBatch.Draw(ZoneTextures[z], z.Dimension.ToVector2() - EngineRef.CurrentMap.Camera.GetPosition() + 5f, Color.White));
			EngineRef.CurrentMap.CurrentVisibleZones.ForEach(z => spriteBatch.DrawString(GameModeString.Font, z.GetDescreiption(), z.Dimension.ToVector2() - EngineRef.CurrentMap.Camera.GetPosition() + z.Size / 2, Color.White));
			if (EngineRef.CurrentMap.IsBossZoneVisible()) {
				spriteBatch.Draw(ZoneTextures[EngineRef.CurrentMap.BossZone], EngineRef.CurrentMap.BossZone.Dimension.ToVector2() - EngineRef.CurrentMap.Camera.GetPosition(), Color.White);
				Vector2 fontSize = GameModeString.Font.MeasureString("Boss");
				spriteBatch.DrawString(GameModeString.Font, "Boss", EngineRef.CurrentMap.BossZone.Dimension.ToVector2() - EngineRef.CurrentMap.Camera.GetPosition() + EngineRef.CurrentMap.BossZone.Dimension.Size() / 2 - fontSize / 2, Color.White);
			}

			EngineRef.Player.Draw(spriteBatch, EngineRef.Player.GetPosition() - EngineRef.CurrentMap.TiledMap.GetCamera().GetPosition());
			Labels.ForEach(l => l.Draw(spriteBatch));
			StaticTexts.ForEach(s => s.Draw(spriteBatch));

			//rendern der dynamischen Texte
			NextFightProgressBar.Draw(spriteBatch);
			EXPProgressBar.Draw(spriteBatch);
			EXPString.Draw(spriteBatch);
			LayerString.Draw(spriteBatch);
			LevelString.Draw(spriteBatch);
			GameModeString.Draw(spriteBatch);
			DurationString.Draw(spriteBatch);
		}

		public void Update(GameTime gameTime) {
			EngineRef.Update(gameTime);

			//dynamische Texte updaten (nur die Texte ändern, die sich auch verändert haben)
			double progressFight = EngineRef.Player.GetTimeToNextBattle().TotalSeconds / EngineRef.Player.GetNextBattleTime().TotalSeconds;
			NextFightProgressBar.ProgressFilled = 1f - (float)progressFight;

			float progressEXP = (EngineRef.Player.GetCurrentEXP() * 1f - EngineRef.Player.GetBeforeLevelEXP() * 1f) / (EngineRef.Player.GetNextLevelEXP() * 1f - EngineRef.Player.GetBeforeLevelEXP() * 1f); //(EngineRef.Player.GetBeforeLevelEXP() * 1f) / (EngineRef.Player.GetNextLevelEXP() * 1f); //* 1f = damit die rechnung als float angesehen wird
			EXPProgressBar.ProgressFilled = progressEXP;

			string _expString = String.Format("{0:n0}", EngineRef.Player.GetCurrentEXP()) + " / " + String.Format("{0:n0}", EngineRef.Player.GetNextLevelEXP());
			if (EXPString.Text != _expString) {
				float _expStringWidth = EXPString.Font.MeasureString(_expString).X;
				EXPString.Position = new Vector2((Resolution.TargetWidth - 237f) - _expStringWidth, (Resolution.TargetHeight - 70f));
				EXPString.Text = _expString;
			}

			if (LayerString.Text != EngineRef.Player.GetLayer().ToString()) {
				float layerStringWidth = LayerString.Font.MeasureString(EngineRef.Player.GetLayer().ToString()).X;
				LayerString.Position = new Vector2(215f - layerStringWidth, (Resolution.TargetHeight - 113f));
				LayerString.Text = EngineRef.Player.GetLayer().ToString();
			}

			if (LevelString.Text != EngineRef.Player.GetLevel().ToString()) {
				float levelStringWidth = LevelString.Font.MeasureString(EngineRef.Player.GetLevel().ToString()).X;
				LevelString.Position = new Vector2(215f - levelStringWidth, (Resolution.TargetHeight - 55f));
				LevelString.Text = EngineRef.Player.GetLevel().ToString();
			}

			if (GameModeString.Text != (EngineRef.Player.GetGameMode() == GameModes.TimeAttack ? "Zeit" : "Kampfrunden")) {
				float gameModeWidth = GameModeString.Font.MeasureString((EngineRef.Player.GetGameMode() == GameModes.TimeAttack ? "Zeit" : "Kampfrunden")).X;
				GameModeString.Position = new Vector2((Resolution.TargetWidth - 225f) + 225f / 2 - gameModeWidth / 2, (Resolution.TargetHeight - 116f));
				GameModeString.Text = (EngineRef.Player.GetGameMode() == GameModes.TimeAttack ? "Zeit" : "Kampfrunden");
			}

			string _durationString = "(^_^)";
			if (EngineRef.Player.GetGameMode() == GameModes.RoundAttack) {
				_durationString = EngineRef.Player.GetBattleRounds().ToString();
			} else if (EngineRef.Player.GetGameMode() == GameModes.TimeAttack) {
				_durationString = EngineRef.Player.GetBattleTime().Minutes.ToString("00") + ":" + EngineRef.Player.GetBattleTime().Seconds.ToString("00");
			}

			if (DurationString.Text != _durationString) {
				float duarationWidth = DurationString.Font.MeasureString(_durationString).X;
				DurationString.Position = new Vector2((Resolution.TargetWidth - 225f) + 225f / 2 - duarationWidth / 2, (Resolution.TargetHeight - 84f));
				DurationString.Text = _durationString;
			}

			//InGame-Menü aufrufen
			if (VirtualGamePad.GetGameKeyState(GameKeys.Menue) == GameKeysState.Released) {
				ScreenManager.GetScreen(Game1.GAMESCREEN).SetDoUpdate(false);
				ScreenManager.AddScreen(Game1.INGAMEMENU, false);
			}

			//Spielerbewegung
			if (VirtualGamePad.ClickHolded()) {
				Vector2 moveDirection = EngineRef.GetPlayerMoveDirection(VirtualGamePad.GetClickPoint());
				EngineRef.MovePlayer(moveDirection);
				double rotation = MathHelper.GetRotation(moveDirection);

				if (rotation >= 315d && rotation <= 360d || rotation >= 0d && rotation < 45d) {
					Player.PlayerAnimation.SetDirection(AnimationDirection.UP);
				} else if (rotation >= 45d && rotation < 135d) {
					Player.PlayerAnimation.SetDirection(AnimationDirection.RIGHT);
				} else if (rotation >= 135d && rotation < 225d) {
					Player.PlayerAnimation.SetDirection(AnimationDirection.DOWN);
				} else if (rotation >= 225d && rotation < 315d) {
					Player.PlayerAnimation.SetDirection(AnimationDirection.LEFT);
				}

				Player.ReduceNextBattleTime((int)gameTime.ElapsedGameTime.TotalMilliseconds);
				if (Player.GetTimeToNextBattle().TotalSeconds <= 0f) {
					Player.SetNextBattleTime(Engine.Instance.Randomizer);

					CoreMonster monster = (CoreMonster)EngineRef.StartBattle();
					List<BattleStep> fullBattle = EngineRef.CalculateBattle(monster);

					((BattleScreen)ScreenManager.GetScreen(Game1.BATTLESCREEN)).Restart(Player, monster, fullBattle);
					ScreenManager.AddScreen(Game1.BATTLESCREEN, false);
					ScreenManager.GetScreen(Game1.GAMESCREEN).SetDoUpdate(false);
				}
			}
		}

		public void PerformClick(Vector2 position) {

		}

		public void Reset() {
			ZoneTextures.Clear();

			EngineRef.CurrentMap.Zones.ForEach(z => z.CurrentLayer = Player.GetLayer());
			EngineRef.CurrentMap.BossZone.CurrentLayer = Player.GetLayer();
		}

		public Rectangle GetDimension() { return new Rectangle(0, 0, Resolution.TargetWidth, Resolution.TargetHeight); }
		public object GetObjectFromElement() { return null; }
		public bool IsEnable() { return true; }
		public bool IsClickable() { return true; }
		public void SetIsEnable(bool enable) { }
		public void SetOnClickEvent(Action onClickEvent) { }

		internal void ResetEngine(CorePlayer player) {
			Player = player;
			EngineRef.ResetEngine(player);
			Reset();
		}
	}
}

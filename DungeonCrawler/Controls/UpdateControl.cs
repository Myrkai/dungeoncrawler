﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GUI.Interface;
using SharpDX.Toolkit;
using SharpDX.Toolkit.Graphics;
using SharpDX;

namespace DungeonCrawler.Controls {
	/// <summary>
	/// Dieses Kontrol ist nur dazu da um die Update-Methode von den Screens zu überschreiben, ohne dabei einen individuellen Screen anlegen zu müssen.
	/// </summary>
	public class UpdateControl : IControl {
		public Action updateAction;

		public UpdateControl(Action updateAction) {
			this.updateAction = updateAction;
		}
		
		public void Draw(SpriteBatch spriteBatch) {

		}

		public void Update(GameTime gameTime) {
			if (updateAction != null) {
				updateAction();
			}
		}

		public Rectangle GetDimension() {
			return new Rectangle(0, 0, 1, 1);
		}

		public object GetObjectFromElement() {
			return null;
		}

		public void PerformClick(Vector2 position) {
			
		}

		public bool IsEnable() {
			return false;
		}

		public bool IsClickable() {
			return false;
		}

		public void SetIsEnable(bool enable) {
			
		}

		public void SetOnClickEvent(Action onClickEvent) {
			
		}
	}
}

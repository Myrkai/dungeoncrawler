﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpDX.Toolkit.Content;

namespace DungeonCrawler.SaveLoad {
	public interface ISave {
		void Save(string filename);
		void Load(ContentManager content, string filename);
	}
}

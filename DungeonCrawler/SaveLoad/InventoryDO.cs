﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpDX;
using RPG.Items.Inventory;
using RPG.Items;
using RPG.Items.Enum;

namespace DungeonCrawler.SaveLoad {
	public class FakeVector2 {
		public float X { get; set; }
		public float Y { get; set; }

		public FakeVector2() {

		}
	}

	public class InventoryDO {
		public int MaxWidth { get; set; }
		public int MaxHeight { get; set; }

		public int Width { get; set; }
		public int Height { get; set; }

		public float PositionX { get; set; }
		public float PositionY { get; set; }

		public List<InventoryPositionDO> Items { get; set; }
		public List<FakeVector2> CellPositionsOffsets { get; set; }


		public InventoryDO() {
			Items = new List<InventoryPositionDO>();
			CellPositionsOffsets = new List<FakeVector2>();
		}
	}

	public class InventoryPositionDO {
		public float PositionX { get; set; }
		public float PositionY { get; set; }
		public ItemDO Item { get; set; }

		public InventoryPositionDO() {

		}
	}

	public class ItemDO {
		public List<SkillDO> AvaiableSkills { get; set; }

		public ItemType Type { get; set; }
		public Quality Quality { get; set; }

		public int Itemlevel { get; set; }
		public string Title { get; set; }
		public string SpecialTitle { get; set; }
		public uint Cost { get; set; }

		public int MinDamage { get; set; }
		public int MaxDamage { get; set; }
		public int MinDefence { get; set; }
		public int MaxDefence { get; set; }
		public int Defence { get; set; }

		public string TextureAsset { get; set; }

		public ItemDO() {
			AvaiableSkills = new List<SkillDO>();
		}
	}

	public class SkillDO {
		public SkillType Type { get; set; }
		public int Level { get; set; }
		public string Description { get; set; }
		public string Title { get; set; }
		public string Look { get; set; }
		public string OriginLook { get; set; }
		public int MinimalItemLevel { get; set; }

		public string Prefix { get; set; }
		public string Surfix { get; set; }

		public List<ItemType> AvailableItemTyps { get; set; }

		public Quality MinimalQualityType { get; set; }
		public AffectType AffectType { get; set; }

		public float Value { get; set; }
		public float Chance { get; set; }

		public int GridWidth { get; set; }
		public int GridHeight { get; set; }
		public int StoneColor { get; set; }
		public int BoarderColor { get; set; }

		public SkillDO() {
			AvailableItemTyps = new List<ItemType>();
		}
	}
}

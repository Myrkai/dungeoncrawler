﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RPG.Chars;
using Core.Interface;
using Core.BattleSystem;
using RPG.Items.Inventory;
using Core.Enums;
using RPG;
using RPG.Sprites;
using RPG.Chars.Enum;
using RPG.Items.Enum;
using SharpDX;
using MathEval;
using DungeonCrawler.Controls;

namespace DungeonCrawler.SaveLoad {
	public class CorePlayerDO : PlayerDO {
		public int Layer { get; set; }

		public int BattleRounds { get; set; }
		public TimeSpan BattleTime { get; set; }
		public GameModes GameMode { get; set; }
		public int LayersToBeat { get; set; }

		public InventoryDO Inventory { get; set; }

		public float PlayerMovementSpeed { get; set; }
		public float MovementSpeedModifier { get; set; }
		public float NextBattleFightModifier { get; set; }

		public NextBattleChanceDO NextBattleTime { get; set; }

		public List<SkillGridEntityControlDO> ActiveSkillsFromGrid { get; set; }

		public CorePlayerDO() {
			ActiveSkillsFromGrid = new List<SkillGridEntityControlDO>();
		}
	}

	public class SkillGridEntityControlDO {
		public SkillDO Skill { get; set; }
		public FakeVector2 Position { get; set; }
		public float Rotation { get; set; }
	}

	public class NextBattleChanceDO { 
		public TimeSpan MaxTimeForNextBattle { get; set; }
		public TimeSpan MinTimeForNextBattle { get; set; }
		public TimeSpan TimeToNextBattle { get; set; }
		public TimeSpan CurrentTimeNextBattle { get; set; }

		public NextBattleChanceDO() {

		}
	}

	public class PlayerDO : EntityDO { 
		public int CurrentEXP { get; set; }
		public int StatPoints { get; set; }
		public int StatPointsPerLevelUp { get; set; }
		public uint Gold { get; set; }
		public AnimationDO PlayerAnimation { get; set; }

		public SerializableDictionary<StatisticType, UInt64> Statistics { get; set; }

		public EXPTableDO EXPTable { get; set; }

		public PlayerDO() { 
		
		}
	}

	public class AnimationDO {
		public string TextureAsset { get; set; }

		public int AnimationStepsPerAnimation { get; set; }
		public int AnimationCount { get; set; }
		public float AnimationSpeed { get; set; }

		public AnimationDirection CurrentAnimationDirection { get; set; }
		public int CurrentAnimationStepIndex { get; set; }

		public float Timer { get; set; }
		public bool DoAnimationLooping { get; set; }

		public int Step { get; set; }
		public int SpriteSingleWidth { get; set; }
		public int SpriteSingleHeight { get; set; }
		public int AnimationStopIndex { get; set; }

		public SerializableDictionary<AnimationDirection, int> AnimationIndexes { get; set; }

		public AnimationDO() {
			AnimationIndexes = new SerializableDictionary<AnimationDirection, int>();
		}
	}

	public class LevelEntityDO {
		public int Level { get; set; }
		public int NextLevelEXP { get; set; }
		public int EXPStep { get; set; }

		public LevelEntityDO() {

		}
	}

	public class EXPTableDO {
		public List<LevelEntityDO> LevelUpStats { get; set; }

		public EXPTableDO() {
			LevelUpStats = new List<LevelEntityDO>();
		}
	}

	public class EntityDO {
		public SerializableDictionary<StatType, float> Stats { get; set; }
		public List<SkillDO> AvaiableSkills { get; set; }
		public List<SkillDO> CurrentSkills { get; set; }
		public SerializableDictionary<ItemType, ItemDO> UsedItems { get; set; }

		public int Level { get; set; }

		public ClassTypeDO ClassType { get; set; }
		public FakeVector2 Position { get; set; }
		public string Name { get; set; }
		public float CurrentHP { get; set; }

		public EntityDO() {
			Stats = new SerializableDictionary<StatType, float>();
			AvaiableSkills = new List<SkillDO>();
			CurrentSkills = new List<SkillDO>();
			UsedItems = new SerializableDictionary<ItemType, ItemDO>();
		}
	}

	public class ClassTypeDO {
		public SerializableDictionary<StatType, float> StatBonus { get; set; }

		public string Title { get; set; }

		public ClassTypeDO() {
			StatBonus = new SerializableDictionary<StatType, float>();
		}
	}
}

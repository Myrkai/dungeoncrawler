﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DungeonCrawler.CoreElements.Shop.Enum;

namespace DungeonCrawler.SaveLoad {
	public class ShopSkillDO {
		public bool Buyed { get; set; } //ob dieser erworben wurde oder nicht
		public BuyableSkillTyps Typ { get; set; } //Zum Filtern
		public BuyableSkills Skill { get; set; } //Der eigentliche permanente Skill
		public uint Cost { get; set; }

		public ShopSkillDO() {
			
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RPG.Items.Inventory;
using DungeonCrawler.CoreElements.Shop;
using DungeonCrawler.CoreElements;
using System.Xml.Serialization;

namespace DungeonCrawler.SaveLoad {
	public class PlayerAccountDO {
		public SerializableDictionary<string, InventoryDO> BankInventories { get; set; }
		public List<ShopSkillDO> ShopSkills { get; set; }
		public List<CorePlayerDO> AvaiablePlayers { get; set; }

		public PlayerAccountDO() {
			ShopSkills = new List<ShopSkillDO>();
			BankInventories = new SerializableDictionary<string, InventoryDO>();
			AvaiablePlayers = new List<CorePlayerDO>();
		}
	}

	[XmlRoot("dictionary")]
	public class SerializableDictionary<TKey, TValue>
		: Dictionary<TKey, TValue>, IXmlSerializable {
		#region IXmlSerializable Members
		public System.Xml.Schema.XmlSchema GetSchema() {
			return null;
		}

		public void ReadXml(System.Xml.XmlReader reader) {
			XmlSerializer keySerializer = new XmlSerializer(typeof(TKey));
			XmlSerializer valueSerializer = new XmlSerializer(typeof(TValue));

			bool wasEmpty = reader.IsEmptyElement;
			reader.Read();

			if (wasEmpty)
				return;

			while (reader.NodeType != System.Xml.XmlNodeType.EndElement) {
				reader.ReadStartElement("item");

				reader.ReadStartElement("key");
				TKey key = (TKey)keySerializer.Deserialize(reader);
				reader.ReadEndElement();

				reader.ReadStartElement("value");
				TValue value = (TValue)valueSerializer.Deserialize(reader);
				reader.ReadEndElement();

				this.Add(key, value);

				reader.ReadEndElement();
				reader.MoveToContent();
			}
			reader.ReadEndElement();
		}

		public void WriteXml(System.Xml.XmlWriter writer) {
			XmlSerializer keySerializer = new XmlSerializer(typeof(TKey));
			XmlSerializer valueSerializer = new XmlSerializer(typeof(TValue));

			foreach (TKey key in this.Keys) {
				writer.WriteStartElement("item");

				writer.WriteStartElement("key");
				keySerializer.Serialize(writer, key);
				writer.WriteEndElement();

				writer.WriteStartElement("value");
				TValue value = this[key];
				valueSerializer.Serialize(writer, value);
				writer.WriteEndElement();

				writer.WriteEndElement();
			}
		}
		#endregion
	}
}

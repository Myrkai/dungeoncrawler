﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace DungeonCrawler.Sound {
	public class MediaManager {
		private static MediaManager _mediaManager;
		public static MediaManager Instance { get { if (_mediaManager == null) { _mediaManager = new MediaManager(); } return _mediaManager; } }

		private Dictionary<string, MediaPlayer> AvaiableSounds { get; set; }

		private MediaManager() {
			AvaiableSounds = new Dictionary<string, MediaPlayer>();
		}

		public bool RegisterSound(string key, MediaPlayer mediaPlayer) {
			if (AvaiableSounds.ContainsKey(key)) {
				return false;
			} else {
				AvaiableSounds.Add(key, mediaPlayer);
				return true;
			}
		}

		public bool Play(string key, bool looping = false) {
			if (!AvaiableSounds.ContainsKey(key)) {
				return false;
			} else {
				AvaiableSounds[key].Play();
				if (looping) {
					AvaiableSounds[key].Stopped -= MediaManager_Stopped;
					AvaiableSounds[key].Stopped += new EventHandler(MediaManager_Stopped);
				}
				return true;
			}
		}

		void MediaManager_Stopped(object sender, EventArgs e) {
			if (AvaiableSounds.Values.Contains(sender)) {
				Play(AvaiableSounds.Single(s => s.Value == sender).Key, true);
			}
		}

		public void Dispose() {
			foreach (MediaPlayer player in AvaiableSounds.Values) {
				player.Dispos();
			}
		}
	}
}

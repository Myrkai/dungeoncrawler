﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.InteropServices;
using System.Media;
using System.Net;
using System.Threading;
using CSCore.Codecs;
using CSCore.XAudio2;
using CSCore;
using SharpDX.MediaFoundation;
using SharpDX.Multimedia;
using NVorbis;
using CSCore.SoundOut;
using DungeonCrawler.Sound;

namespace DungeonCrawler {
	public class MediaPlayer {

		#region Player
		private IWaveSource _source;
		private XAudio2 _xaudio2;
		private XAudio2MasteringVoice _masteringVoice;
		private StreamingSourceVoice _streamingSourceVoice;
		public event EventHandler Stopped;
		private Uri uri;
		private string filename;

		public MediaPlayer(Uri uri) {
			if (!CodecFactory.Instance.GetSupportedFileExtensions().Contains(".ogg")) {
				CodecFactory.Instance.Register("ogg-vorbis", new CodecFactoryEntry(s => new NVorbisSource(s).ToWaveSource(), ".ogg"));
			}

			this.uri = uri;
		}

		public MediaPlayer(string filename) {
			if (!CodecFactory.Instance.GetSupportedFileExtensions().Contains(".ogg")) {
				CodecFactory.Instance.Register("ogg-vorbis", new CodecFactoryEntry(s => new NVorbisSource(s).ToWaveSource(), ".ogg"));
			}

			this.filename = filename;
		}

		public void Play() {
			Dispos();

			if (uri != null) {
				_source = CodecFactory.Instance.GetCodec(uri);

				_xaudio2 = XAudio2.CreateXAudio2();
				_masteringVoice = _xaudio2.CreateMasteringVoice();
				_streamingSourceVoice = StreamingSourceVoice.Create(_xaudio2, _source);
				_streamingSourceVoice.Stopped += new EventHandler(_streamingSourceVoice_Stopped);

				StreamingSourceVoiceListener.Default.Add(_streamingSourceVoice);

				_streamingSourceVoice.Refill();
			} else if (filename != null) {
				_source = CodecFactory.Instance.GetCodec(filename);

				_xaudio2 = XAudio2.CreateXAudio2();
				_masteringVoice = _xaudio2.CreateMasteringVoice();
				_streamingSourceVoice = StreamingSourceVoice.Create(_xaudio2, _source);
				_streamingSourceVoice.Stopped += new EventHandler(_streamingSourceVoice_Stopped);

				StreamingSourceVoiceListener.Default.Add(_streamingSourceVoice);
			}

			_streamingSourceVoice.SetVolume(GameOptions.Volume, XAudio2.CommitNow);
			_streamingSourceVoice.Start();
		}

		void _streamingSourceVoice_Stopped(object sender, EventArgs e) {
			if (Stopped != null) {
				Stopped(this, e);
			}
		}

		public void Dispos() {
			StreamingSourceVoiceListener.Default.Remove(_streamingSourceVoice);
			if (_streamingSourceVoice != null) {
				try {
					_streamingSourceVoice.Stop();
				} catch { }

				try {
					_streamingSourceVoice.Dispose();
				} catch { }
			}

			if (_masteringVoice != null)
				_masteringVoice.Dispose();
			//if (_xaudio2 != null) {
			//    _xaudio2.StopEngine();
			//    _xaudio2.Dispose();
			//}
			if (_source != null)
				_source.Dispose();
		}
		#endregion

		#region static Player
		static IWaveSource source;
		static XAudio2 xaudio2;
		static XAudio2MasteringVoice masteringVoice;
		static StreamingSourceVoice streamingSourceVoice;

		public static void Init() {
			if (!CodecFactory.Instance.GetSupportedFileExtensions().Contains(".ogg")) {
				CodecFactory.Instance.Register("ogg-vorbis", new CodecFactoryEntry(s => new NVorbisSource(s).ToWaveSource(), ".ogg"));
			}
		}

		public static void PlayDirect(Uri uri) {
			source = CodecFactory.Instance.GetCodec(uri);
			Playmedia();
		}

		public static void PlayDirect(string filename) {
			source = CodecFactory.Instance.GetCodec(filename);
			Playmedia();
		}

		private static void Playmedia() {
			xaudio2 = XAudio2.CreateXAudio2();
			masteringVoice = xaudio2.CreateMasteringVoice();
			streamingSourceVoice = StreamingSourceVoice.Create(xaudio2, source);

			StreamingSourceVoiceListener.Default.Add(streamingSourceVoice);
			streamingSourceVoice.SetVolume(GameOptions.Volume, XAudio2.CommitNow);
			streamingSourceVoice.Start();
		}

		public static void ConvertToWave(string fileName) {
			string name = Path.GetFileNameWithoutExtension(fileName);

			FileStream stream = new FileStream(fileName, FileMode.Open, FileAccess.Read);
			AudioDecoder audioDecoder = new AudioDecoder(stream);

			var outputWavStream = new FileStream(@"Temp\" + name + ".wav", FileMode.Create, FileAccess.Write);

			var wavWriter = new WavWriter(outputWavStream);

			// Write the WAV file
			wavWriter.Begin(audioDecoder.WaveFormat);

			// Decode the samples from the input file and output PCM raw data to the WAV stream.
			wavWriter.AppendData(audioDecoder.GetSamples());

			// Close the wav writer.
			wavWriter.End();

			audioDecoder.Dispose();
			outputWavStream.Close();
			stream.Close();
		}

		public static void Dispose() {
			StreamingSourceVoiceListener.Default.Remove(streamingSourceVoice);
			if (streamingSourceVoice != null) {
				streamingSourceVoice.Stop();
				streamingSourceVoice.Dispose();
			}

			if (masteringVoice != null)
				masteringVoice.Dispose();
			if (xaudio2 != null)
				xaudio2.Dispose();
			if (source != null)
				source.Dispose();

			StreamingSourceVoiceListener.Default.Dispose();
		}
		#endregion
	}
}

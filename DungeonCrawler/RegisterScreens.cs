﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GUI;
using SharpDX.Toolkit.Graphics;
using GUI.Screens;
using SharpDX;
using DungeonCrawler.Controls;
using GUI.Controls;
using Core;
using Core.Map;
using DungeonCrawler.CoreElements;
using TiledMap;
using RPG.Sprites;
using RPG.Chars;
using RPG.Items.Enum;
using RPG.Items;
using RPG.Chars.Enum;
using Core.Items;
using DungeonCrawler.CoreElements.Items.Skills;
using Core.Items.Interface;
using DungeonCrawler.CoreElements.Shop;
using DungeonCrawler.CoreElements.Shop.Enum;
using System.Threading;
using System.Windows.Forms;
using CSCore.Codecs;
using CSCore.XAudio2;
using Tiled_Map = TiledMap.TiledMap;
using Core.BattleSystem;
using CoreItemType = Core.Items.Enum.ItemType;
using CoreQuality = Core.Items.Enum.Quality;
using Core.Enums;
using SideProjekt.Mechanic.Log;
using DungeonCrawler.Sound;
using Animation = RPG.Sprites.Animation;
using System.IO;

namespace DungeonCrawler {
	public partial class Game1 {

		/*
		 * IDEE Charakter Screen / Auswahl:
		 * Es gibt einen SpielerAccount:
		 * - Beinhaltet die Bank
		 * - Beinhaltet die Shop Items
		 * - Behinhaltet alle Verfügbaren CorePlayer (wobei jeder CorePlayer nur einmal Verfügabr sein kann)
		 * 
		 * Es gibt des normalen Spieler der zum Charakter wird (CorePlayer):
		 * - Ein Coreplayer besitzt eine Spezielle Klasse (gibt es schon)
		 * - Ein Acc darf keine 2 Klassen auf einmal besitzen
		 * 
		 * Hardcore Mode löscht dann den Charakter.
		 * Damit stehen später dann trotzdem alle ShopSkills und Bankfächer zur Verfügung
		 */

		/* Umsetzung Ring Menu:
		 * Nutze MathHelper.RotateAPoint
		 * Unten ist 0°.
		 * 
		 * Aktueller Winkel	/ Winkeländerung bei 9 Elementen im Menu / Skalierungsfaktor
			0	40	1 ((ABS(C5-180)/360)+0,5) -> C5 ist der aktuelle Winkel
			40		0,888888889
			80		0,777777778
			120		0,666666667
			160		0,555555556
			200		0,555555556
			240		0,666666667
			280		0,777777778
			320		0,888888889
			360		1
		*/

		public const string TITLESCREEN = "titlescreen";
		public const string MAINSCREEN = "mainscreen";
		public const string OPTIONSCREEN = "optionScreen";
		public const string GAMESCREEN = "gameScreen";
		public const string INGAMEMENU = "inGameMenu";
		public const string STATUSSCREEN = "statusScreen";
		public const string DETAILSSCREEN = "detailsscreen";
		public const string SKILLSCREEN = "skillscreen";
		public const string STATISTIKSCREEN = "statistikScreen";
		public const string INVENTORYSCREEN = "inventoryScreen";
		public const string SHOPSCREEN = "shopscreen";
		public const string UPGRADESSCREEN = "upgradesScreen";
		public const string BANKSCREEN = "bankscreen";
		public const string BATTLESCREEN = "battlescreen";
		public const string CHARSELECTEDSCREEN = "charSelectedScreen";

		partial void RegisterAllScreens(ScreenManager screenManager) {
			int hash = Guid.NewGuid().GetHashCode();
			Random rnd = new Random(hash);
			this.Window.Title = "Seed: " + hash.ToString();

			//-> Titelbildschirm anlegen
			TitleScreen<Game1> titleScreen = new TitleScreen<Game1>(this, screenManager);
			SpriteFont font48 = Content.Load<SpriteFont>(@"Fonts\Areal48.tk");
			titleScreen.SetTitle("Dungeon Crawler", new Vector2(Resolution.TargetWidth / 2 - font48.MeasureString("Dungeon Crawler").X / 2, 20f), font48, Color.DarkBlue);

			AutoScrolledTiledMap map = new AutoScrolledTiledMap(@"Content\TiledMaps\test mit terrain.tmx", Content, "Tilesets", new Camera(new Vector2(111.5f, 100f), graphics.PreferredBackBufferWidth, graphics.PreferredBackBufferHeight));
			Texture2D buttonTexture = Content.Load<Texture2D>(@"GUI\buttonSample.png");

			titleScreen.SetButton("Starte Spiel", new Vector2(Resolution.TargetWidth / 2 - buttonTexture.Width / 2, 650f), Content.Load<SpriteFont>(@"Fonts\Areal16.tk"), Color.White, buttonTexture, () => { screenManager.Transfer(MAINSCREEN); map.Speed = 0f; }, Color.Blue);
			titleScreen.AddCustomControl("map", map, true);

			//-> Hauptmenü anlegen
			MainMenuScreen<Game1> mainScreen = new MainMenuScreen<Game1>(this, screenManager);
			mainScreen.SetTitle("Dungeon Crawler", new Vector2(Resolution.TargetWidth / 2 - font48.MeasureString("Dungeon Crawler").X / 2, 20f), Content.Load<SpriteFont>(@"Fonts\Areal48.tk"), Color.DarkBlue);
			mainScreen.AddCustomControl("map", map, true);

			Texture2D black = TextureHelper.CreateTexture(graphics.PreferredBackBufferWidth, graphics.PreferredBackBufferHeight, Color.Black, GraphicsDevice);
			mainScreen.AddCustomControl("backGround", new ImageControl(black, Vector2.Zero, 0.5f), true);

			mainScreen.AddButton("Neues Spiel", new Vector2(Resolution.TargetWidth / 2 - buttonTexture.Width / 2, 200f), Content.Load<SpriteFont>(@"Fonts\Areal16.tk"), Color.White, buttonTexture, () => screenManager.Transfer(CHARSELECTEDSCREEN), Color.Blue);
			mainScreen.AddButton("Spiel laden", new Vector2(Resolution.TargetWidth / 2 - buttonTexture.Width / 2, 270f), Content.Load<SpriteFont>(@"Fonts\Areal16.tk"), Color.White, buttonTexture, null, Color.Blue);
			mainScreen.AddButton("Optionen", new Vector2(Resolution.TargetWidth / 2 - buttonTexture.Width / 2, 340f), Content.Load<SpriteFont>(@"Fonts\Areal16.tk"), Color.White, buttonTexture, () => screenManager.Transfer(OPTIONSCREEN), Color.Blue);
			mainScreen.AddButton("Exit", new Vector2(Resolution.TargetWidth / 2 - buttonTexture.Width / 2, 410f), Content.Load<SpriteFont>(@"Fonts\Areal16.tk"), Color.White, buttonTexture, () => this.Exit(), Color.Blue);

			//-> Optionen anlegen
			OptionsScreen<Game1> optionScreen = new OptionsScreen<Game1>(this, screenManager);
			optionScreen.LoadOptions(() => GameOptions.Load2("options"));
			optionScreen.SetTitle("Optionen", new Vector2(Resolution.TargetWidth / 2 - font48.MeasureString("Optionen").X / 2, 20f), Content.Load<SpriteFont>(@"Fonts\Areal48.tk"), Color.White);
			optionScreen.SetBackground(Content.Load<Texture2D>(@"Backgrounds\options.png"), Resolution.Scale);

			Texture2D unterline = TextureHelper.CreateTexture(275, 4, Color.White, GraphicsDevice);
			optionScreen.AddControl("underline", new ImageControl(unterline, new Vector2(Resolution.TargetWidth / 2 - unterline.Width / 2, 100f)));
			optionScreen.AddControl("CanelButton", new ButtonControl("Abbrechen", new Vector2(Resolution.TargetWidth - 20 - buttonTexture.Width, Resolution.TargetHeight - 60f), Content.Load<SpriteFont>(@"Fonts\Areal16.tk"), buttonTexture, Color.White, () => screenManager.Transfer(MAINSCREEN), Color.Blue));

			optionScreen.AddControl("DifficultyTitle", new LinkControl("Schwierigkeitsgrad", new Vector2(20f, 140f), Content.Load<SpriteFont>(@"Fonts\Areal16.tk"), Color.White, null));
			optionScreen.AddControl("FullscreenTitle", new LinkControl("Vollbild", new Vector2(20f, 200f), Content.Load<SpriteFont>(@"Fonts\Areal16.tk"), Color.White, null));
			optionScreen.AddControl("VolumeTitle", new LinkControl("Musiklautstärke (%)", new Vector2(20f, 260f), Content.Load<SpriteFont>(@"Fonts\Areal16.tk"), Color.White, null));
			optionScreen.AddControl("deleteTempSoundTitle", new LinkControl("Lösche temporäre Dateien", new Vector2(20f, 320f), Content.Load<SpriteFont>(@"Fonts\Areal16.tk"), Color.White, null));
			optionScreen.AddControl("showFPSTitle", new LinkControl("Zeige FPS", new Vector2(20f, 380f), Content.Load<SpriteFont>(@"Fonts\Areal16.tk"), Color.White, null));
			optionScreen.AddControl("FPSLimitTitle", new LinkControl("FPS Limit", new Vector2(20f, 440f), Content.Load<SpriteFont>(@"Fonts\Areal16.tk"), Color.White, null));
			optionScreen.AddControl("showLiveDebuggerTitle", new LinkControl("Zeige Live Debugger", new Vector2(20f, 500f), Content.Load<SpriteFont>(@"Fonts\Areal16.tk"), Color.White, null));
			optionScreen.AddControl("showVirtualESCTitle", new LinkControl("Virtuelle ESC-Taste", new Vector2(20f, 560f), Content.Load<SpriteFont>(@"Fonts\Areal16.tk"), Color.White, null));
			optionScreen.AddControl("autoContinueTitle", new LinkControl("Automatisches Fortsetzen", new Vector2(20f, 620f), Content.Load<SpriteFont>(@"Fonts\Areal16.tk"), Color.White, null));

			SelectBoxControl difficulty = new SelectBoxControl(new List<string>() { "Normal" }, Content.Load<SpriteFont>(@"Fonts\Areal16.tk"), Color.White, 0);
			difficulty.SetPosition(new Vector2(Resolution.TargetWidth - 90, 140f));
			SelectBoxControl volume = new SelectBoxControl(new List<string>() { "0", "5", "10", "15", "20", "25", "30", "35", "40", "45", "50", "55", "60", "65", "70", "75", "80", "85", "90", "95", "100" }, Content.Load<SpriteFont>(@"Fonts\Areal16.tk"), Color.White, 0);
			volume.SetPosition(new Vector2(Resolution.TargetWidth - 90, 260f));
			volume.Text = Convert.ToInt32(GameOptions.Volume * 100f).ToString();
			CheckBoxControl fullscreen = new CheckBoxControl(Content.Load<SpriteFont>(@"Fonts\Areal16.tk"), Color.White, new Vector2(Resolution.TargetWidth - 90, 200f));
			fullscreen.SetValue(GameOptions.Fullscreen);
			CheckBoxControl deleteTempSound = new CheckBoxControl(Content.Load<SpriteFont>(@"Fonts\Areal16.tk"), Color.White, new Vector2(Resolution.TargetWidth - 90, 320f));
			deleteTempSound.SetValue(GameOptions.DeleteTempSound);
			CheckBoxControl showFPS = new CheckBoxControl(Content.Load<SpriteFont>(@"Fonts\Areal16.tk"), Color.White, new Vector2(Resolution.TargetWidth - 90, 380f));
			showFPS.SetValue(GameOptions.ShowFPS);
			CheckBoxControl FPSLimit = new CheckBoxControl(Content.Load<SpriteFont>(@"Fonts\Areal16.tk"), Color.White, new Vector2(Resolution.TargetWidth - 90, 440f));
			FPSLimit.SetValue(GameOptions.ForceFPSLimiter);
			CheckBoxControl showLiveDebugger = new CheckBoxControl(Content.Load<SpriteFont>(@"Fonts\Areal16.tk"), Color.White, new Vector2(Resolution.TargetWidth - 90, 500f));
			showLiveDebugger.SetValue(GameOptions.ShowLiveDebuger);
			CheckBoxControl showVirtuelESC = new CheckBoxControl(Content.Load<SpriteFont>(@"Fonts\Areal16.tk"), Color.White, new Vector2(Resolution.TargetWidth - 90, 560f));
			showVirtuelESC.SetValue(GameOptions.ShowVirtualESCButton);
			CheckBoxControl autoContinueCheckbox = new CheckBoxControl(Content.Load<SpriteFont>(@"Fonts\Areal16.tk"), Color.White, new Vector2(Resolution.TargetWidth - 90, 620f));
			autoContinueCheckbox.SetValue(GameOptions.autoContinue);

			optionScreen.AddControl("OKButton",
				new ButtonControl("OK",
					new Vector2(20f, Resolution.TargetHeight - 60f),
					Content.Load<SpriteFont>(@"Fonts\Areal16.tk"),
					buttonTexture,
					Color.White,
					() => {
						GameOptions.DeleteTempSound = deleteTempSound.GetValue();
						GameOptions.ForceFPSLimiter = FPSLimit.GetValue();
						GameOptions.Fullscreen = fullscreen.GetValue();
						GameOptions.ShowFPS = showFPS.GetValue();
						GameOptions.ShowLiveDebuger = showLiveDebugger.GetValue();
						GameOptions.Volume = Convert.ToSingle(volume.Text) / 100f;
						GameOptions.ShowVirtualESCButton = showVirtuelESC.GetValue();
						GameOptions.autoContinue = autoContinueCheckbox.GetValue();
						GameOptions.Save2("options");
						graphics.IsFullScreen = GameOptions.Fullscreen;
						graphics.ApplyChanges();
						this.IsFixedTimeStep = GameOptions.ForceFPSLimiter;
						screenManager.Transfer(MAINSCREEN);
					},
					Color.Blue
				)
			);

			optionScreen.AddControl("difficulty", difficulty);
			optionScreen.AddControl("fullscreen", fullscreen);
			optionScreen.AddControl("volume", volume);
			optionScreen.AddControl("deleteTempSound", deleteTempSound);
			optionScreen.AddControl("showFPS", showFPS);
			optionScreen.AddControl("FPSLimit", FPSLimit);
			optionScreen.AddControl("showLiveDebugger", showLiveDebugger);
			optionScreen.AddControl("showVirtuelESC", showVirtuelESC);
			optionScreen.AddControl("autoContinueCheckbox", autoContinueCheckbox);

			//-> GameScreen anlegen
			GameScreen<Game1> gameScreen = new GameScreen<Game1>(this, screenManager);

			MapManager mapManager = new MapManager();
			foreach (string mapPath in System.IO.Directory.GetFiles(@"Content\TiledMaps\")) {
				Map map1 = new Map(new CoreTiledMap(new Tiled_Map(mapPath, Content, "Tilesets", new Camera(Vector2.Zero, graphics.PreferredBackBufferWidth, graphics.PreferredBackBufferHeight))));
				map1.AddMonsterzone(new Rectangle(0 * 32, 0 * 32, 50 * 32, 50 * 32), 0, 1);
				map1.AddMonsterzone(new Rectangle(0 * 32, 50 * 32, 50 * 32, 50 * 32), 1, 3);
				map1.AddMonsterzone(new Rectangle(50 * 32, 0 * 32, 50 * 32, 50 * 32), 1, 3);
				map1.AddMonsterzone(new Rectangle(50 * 32, 50 * 32, 50 * 32, 50 * 32), 2, 4);
				map1.SetBossZone(85 * 32, 85 * 32, 5 * 32, 5 * 32);
				mapManager.RegisterMap(map1);
			}

			ItemManager itemManager = new ItemManager(rnd);
			CreateBaseItems(itemManager);
			CreateBaseSkills(itemManager);

			PlayerAccount acc = PlayerAccount.CreateNew(CreateUnBuyedShopSkills(), Content, rnd, itemManager);
			//acc.Save("default.sav");
			//acc.Load(Content, "default.sav");
			//acc.Save("load.sav");

			if (File.Exists("acc.sav")) {
				acc.Load(Content);
			} else {
				acc.Save();
			}

			CorePlayer player = acc.GetPlayer(0); //default data

			MonsterManager.Instance.Init(CreateCoreExpTable(), new CoreMonster(false, null, null, 0f, 0f, 0f, "", 1, Vector2.Zero, Content), itemManager);
			MonsterManager monsterManager = MonsterManager.Instance;
			monsterManager.AddSkill(itemManager.BaseSkills.Where(s => s.Item3.GetAffectedType() == (int)AffectType.StatusChangeSkill || s.Item3.GetAffectedType() == (int)AffectType.BattleSkill).ToList());
			CreateBaseMonsterSources(monsterManager);

			Engine engine = Engine.Instance;
			Engine.Instance.Init(player, mapManager, itemManager, monsterManager, rnd);
			EngineControl engineControl = new EngineControl(engine, GraphicsDevice, Content, screenManager, player);

			gameScreen.AddCustomControl("engine", engineControl, true);

			//-> InGame-Menü anlegen
			MainMenuScreen<Game1> inGameMenu = new MainMenuScreen<Game1>(this, screenManager);
			Texture2D inGameMenuBackground = TextureHelper.CreateTextureWidthBoarder(225, 370, Color.Black, 2, Color.White, GraphicsDevice);

			inGameMenu.AddCustomControl("background", new ImageControl(black, Vector2.Zero, 0.7f), true);
			inGameMenu.AddCustomControl("menuBackground", new ImageControl(inGameMenuBackground, new Vector2(Resolution.TargetWidth / 2 - inGameMenuBackground.Width / 2, 720 / 2 - inGameMenuBackground.Height / 2)), true);
			inGameMenu.AddButton("Status", new Vector2(Resolution.TargetWidth / 2 - buttonTexture.Width / 2, 215f - 30f), Content.Load<SpriteFont>(@"Fonts\Areal16.tk"), Color.White, buttonTexture, () => { screenManager.RemoveScreen(); screenManager.AddScreen(STATUSSCREEN, false); ((StatusScreen)screenManager.GetScreen(STATUSSCREEN)).RefreshTextboxes(); }, Color.Blue);
			inGameMenu.AddButton("Inventar", new Vector2(Resolution.TargetWidth / 2 - buttonTexture.Width / 2, 275f - 30f), Content.Load<SpriteFont>(@"Fonts\Areal16.tk"), Color.White, buttonTexture, () => { screenManager.RemoveScreen(); screenManager.AddScreen(INVENTORYSCREEN, false); ((InventoryScreen)screenManager.GetScreen(INVENTORYSCREEN)).CreateEvents(); }, Color.Blue);

			if (player.Owner.ShopSkills.Where(s => s.Typ == BuyableSkillTyps.BankSlotExtension && s.Buyed).Count() == 0 || player.GetGameModeAddons().Contains(GameModeAddons.Ironman)) {
				inGameMenu.AddButton("Bank", new Vector2(Resolution.TargetWidth / 2 - buttonTexture.Width / 2, 335f - 30f), Content.Load<SpriteFont>(@"Fonts\Areal16.tk"), Color.White, buttonTexture, null, Color.Gray);
			} else {
				inGameMenu.AddButton("Bank", new Vector2(Resolution.TargetWidth / 2 - buttonTexture.Width / 2, 335f - 30f), Content.Load<SpriteFont>(@"Fonts\Areal16.tk"), Color.White, buttonTexture, () => { screenManager.RemoveScreen(); screenManager.AddScreen(BANKSCREEN, false); ((BankScreen)screenManager.GetScreen(BANKSCREEN)).CreateEvents(); }, Color.Blue);
			}

			inGameMenu.AddButton("Shop", new Vector2(Resolution.TargetWidth / 2 - buttonTexture.Width / 2, 395f - 30f), Content.Load<SpriteFont>(@"Fonts\Areal16.tk"), Color.White, buttonTexture, () => { screenManager.RemoveScreen(); screenManager.AddScreen(SHOPSCREEN, false); ((ShopScreen)screenManager.GetScreen(SHOPSCREEN)).CreateEvents(); ((ShopScreen)screenManager.GetScreen(SHOPSCREEN)).RefreshItems(); }, Color.Blue);
			inGameMenu.AddButton("Speichern", new Vector2(Resolution.TargetWidth / 2 - buttonTexture.Width / 2, 455f - 30f), Content.Load<SpriteFont>(@"Fonts\Areal16.tk"), Color.White, buttonTexture, () => { player.Owner.Save(); }, Color.Blue);
			inGameMenu.AddButton("Exit", new Vector2(Resolution.TargetWidth / 2 - buttonTexture.Width / 2, 515f - 30f), Content.Load<SpriteFont>(@"Fonts\Areal16.tk"), Color.White, buttonTexture, () => { player.Owner.Save(); screenManager.GetScreen(GAMESCREEN).SetDoUpdate(true); screenManager.Transfer(MAINSCREEN); }, Color.Blue);
			inGameMenu.AddCustomControl("update", new UpdateControl(() => { if (VirtualGamePad.GetGameKeyState(GameKeys.Menue) == GameKeysState.Released) { screenManager.GetScreen(GAMESCREEN).SetDoUpdate(true); screenManager.RemoveScreen(); } }), false);

			//-> StatusScreen anlegen
			StatusScreen statusScreen = new StatusScreen(this, screenManager, Content);
			statusScreen.AddCustomControl("blackBG", new ImageControl(black, Vector2.Zero, 0.6f), true);
			ImageControl statusBackground = new ImageControl(Content.Load<Texture2D>(@"Backgrounds\StatusWithSkillPoints.png"), Vector2.Zero);
			statusBackground.Scale = Resolution.Scale - Resolution.ScaleAddon4_3;

			statusScreen.AddCustomControl("background", statusBackground, true);
			statusScreen.AddCustomControl("update", new UpdateControl(() => { if (VirtualGamePad.GetGameKeyState(GameKeys.Menue) == GameKeysState.Released) { screenManager.RemoveScreen(); screenManager.AddScreen(INGAMEMENU, false); } }), false);

			statusScreen.AddCustomControl("Details", new ButtonControl("Details", new Vector2(10f, Resolution.TargetHeight - 60f), Content.Load<SpriteFont>(@"Fonts\Areal16.tk"), buttonTexture, Color.White, () => { screenManager.RemoveScreen(); screenManager.AddScreen(DETAILSSCREEN, false); ((DetailsScreen)screenManager.GetScreen(DETAILSSCREEN)).RefreshInformations(); }, Color.DarkBlue), false);
			statusScreen.AddCustomControl("Fähigkeiten", new ButtonControl("Fähigkeiten", new Vector2((statusBackground.Texture.Width * (Resolution.Scale - Resolution.ScaleAddon4_3)) / 2f - buttonTexture.Width / 2f, Resolution.TargetHeight - 60f), Content.Load<SpriteFont>(@"Fonts\Areal16.tk"), buttonTexture, Color.White, () => { screenManager.RemoveScreen(); screenManager.AddScreen(SKILLSCREEN, false); ((SkillScreen)screenManager.GetScreen(SKILLSCREEN)).Refresh(); }, Color.DarkBlue), false);
			statusScreen.AddCustomControl("Statistik", new ButtonControl("Statistik", new Vector2((statusBackground.Texture.Width * (Resolution.Scale - Resolution.ScaleAddon4_3)) - 10f - buttonTexture.Width, Resolution.TargetHeight - 60f), Content.Load<SpriteFont>(@"Fonts\Areal16.tk"), buttonTexture, Color.White, () => { screenManager.RemoveScreen(); screenManager.AddScreen(STATISTIKSCREEN, false); ((StatistikScreen)screenManager.GetScreen(STATISTIKSCREEN)).RefreshInformations(); }, Color.DarkBlue), false);
			statusScreen.CreateTextboxes(player);

			//-> Details anlegen
			DetailsScreen detailsScreen = new DetailsScreen(this, screenManager, Content);
			detailsScreen.AddCustomControl("blackBG", new ImageControl(black, Vector2.Zero, 0.6f), true);
			ImageControl detailsBackground = new ImageControl(Content.Load<Texture2D>(@"Backgrounds\Details.png"), Vector2.Zero);
			detailsBackground.Scale = Resolution.Scale - Resolution.ScaleAddon4_3;

			detailsScreen.AddCustomControl("background", detailsBackground, true);
			detailsScreen.AddCustomControl("update", new UpdateControl(() => { if (VirtualGamePad.GetGameKeyState(GameKeys.Menue) == GameKeysState.Released) { screenManager.RemoveScreen(); screenManager.AddScreen(STATUSSCREEN, false); } }), false);
			detailsScreen.CreateInformations(player);

			//-> Fähigkeiten anlegen
			SkillScreen skillScreen = new SkillScreen(this, screenManager, player);
			skillScreen.AddCustomControl("blackBG", new ImageControl(black, Vector2.Zero, 0.6f), true);
			ImageControl skillBackground = new ImageControl(Content.Load<Texture2D>(@"Backgrounds\Empty.png"), Vector2.Zero);
			skillBackground.Scale = Resolution.Scale - Resolution.ScaleAddon4_3;

			skillScreen.AddCustomControl("background", skillBackground, true);
			skillScreen.AddCustomControl("update", new UpdateControl(() => { if (VirtualGamePad.GetGameKeyState(GameKeys.Menue) == GameKeysState.Released) { screenManager.RemoveScreen(); screenManager.AddScreen(STATUSSCREEN, false); ((StatusScreen)screenManager.GetScreen(STATUSSCREEN)).RefreshTextboxes(); } }), false);

			SkillListControl listControl = new SkillListControl(Vector2.Zero);
			player.AvaiableSkills.ToList().ForEach(e => listControl.AddSkillControl(new SkillListEntityControl(e, Content.Load<SpriteFont>(@"Fonts\Areal12.tk"), Content.Load<SpriteFont>(@"Fonts\Areal8Normal.tk"), new Vector2(50f, 50f))));

			int gridUpgradeCount = player.Owner.ShopSkills.Where(s => s.Typ == BuyableSkillTyps.SkillGridExtension && s.Buyed).Count();
			SkillGridControl skillGrid = new SkillGridControl(new Vector2(345f, 65f), 4 + gridUpgradeCount, 4 + gridUpgradeCount, listControl.GetDimension());

			skillScreen.AddCustomControl("skillList", listControl, false);
			skillScreen.AddCustomControl("skillGrid", skillGrid, false);

			//-> Statistiken anlegen
			StatistikScreen statistikScreen = new StatistikScreen(this, screenManager, Content);
			statistikScreen.AddCustomControl("blackBG", new ImageControl(black, Vector2.Zero, 0.6f), true);
			ImageControl statisticBackground = new ImageControl(Content.Load<Texture2D>(@"Backgrounds\Details.png"), Vector2.Zero);
			statisticBackground.Scale = Resolution.Scale - Resolution.ScaleAddon4_3;

			statistikScreen.AddCustomControl("background", statisticBackground, true);
			statistikScreen.AddCustomControl("update", new UpdateControl(() => { if (VirtualGamePad.GetGameKeyState(GameKeys.Menue) == GameKeysState.Released) { screenManager.RemoveScreen(); screenManager.AddScreen(STATUSSCREEN, false); } }), false);
			statistikScreen.CreateInformations(player);

			//-> InventarScreen anlegen
			InventoryScreen inventoryScreen = new InventoryScreen(this, screenManager, player);
			ImageControl inventoryBackground = new ImageControl(Content.Load<Texture2D>(@"Backgrounds\Inventory.png"), Vector2.Zero);
			inventoryBackground.Scale = Resolution.Scale - Resolution.ScaleAddon4_3;
			inventoryBackground.Position = new Vector2(Resolution.TargetWidth - inventoryBackground.GetDimension().Width, 0f);
			inventoryScreen.AddCustomControl("blackBG", new ImageControl(black, Vector2.Zero, 0.6f), true);
			inventoryScreen.AddCustomControl("back", inventoryBackground, true);
			inventoryScreen.AddCustomControl("update", new UpdateControl(() => { if (VirtualGamePad.GetGameKeyState(GameKeys.Menue) == GameKeysState.Released) { screenManager.RemoveScreen(); screenManager.AddScreen(INGAMEMENU, false); ((InventoryScreen)screenManager.GetScreen(INVENTORYSCREEN)).RemoveEvents(); } }), false);

			//-> ShopScreen anlegen
			ShopScreen shopScreen = new ShopScreen(this, screenManager, player, itemManager, rnd);
			shopScreen.AddCustomControl("blackBG", new ImageControl(black, Vector2.Zero, 0.6f), true);

			ImageControl shopBackground = new ImageControl(Content.Load<Texture2D>(@"Backgrounds\Empty.png"), Vector2.Zero);
			shopBackground.Scale = Resolution.Scale - Resolution.ScaleAddon4_3;
			shopBackground.Position = new Vector2(Resolution.TargetWidth - inventoryBackground.GetDimension().Width, 0f);
			shopScreen.AddCustomControl("background", shopBackground, true);
			shopScreen.AddCustomControl("update", new UpdateControl(() => { if (VirtualGamePad.GetGameKeyState(GameKeys.Menue) == GameKeysState.Released) { screenManager.RemoveScreen(); screenManager.AddScreen(INGAMEMENU, false); ((ShopScreen)screenManager.GetScreen(SHOPSCREEN)).RemoveEvents(); } }), false);

			Texture2D goldBoxTexture = TextureHelper.CreateTextureWidthBoarder(buttonTexture.Width + 50, buttonTexture.Height, Color.Black, 2, Color.DarkGray, GraphicsDevice);
			Vector2 goldBoxPosition = new Vector2(shopBackground.Position.X + 7f, 415f);
			shopScreen.AddCustomControl("Gold", new ButtonControl("Gold: " + String.Format("{0:n0}", player.Gold), goldBoxPosition, Content.Load<SpriteFont>(@"Fonts\Areal16.tk"), goldBoxTexture, Color.White, null), false);
			((ButtonControl)shopScreen.CustomControls.Where(s => s.Item1 == "Gold").First().Item2).SetOffSetPositionText(new Vector2(10f, ((ButtonControl)shopScreen.CustomControls.Where(s => s.Item1 == "Gold").First().Item2).OffSetPositionText.Y));

			shopScreen.AddCustomControl("Upgrades", new ButtonControl("Upgrades", new Vector2(shopBackground.Position.X + shopBackground.Texture.Width - 7f - buttonTexture.Width, 415f), Content.Load<SpriteFont>(@"Fonts\Areal16.tk"), buttonTexture, Color.White, () => { screenManager.RemoveScreen(); screenManager.AddScreen(UPGRADESSCREEN, false); ((UpgradesScreen)screenManager.GetScreen(UPGRADESSCREEN)).AddEvents(); ((ShopScreen)screenManager.GetScreen(SHOPSCREEN)).RemoveEvents(); }, Color.Blue), false);

			//-> Upgrades anlegen
			UpgradesScreen upgradesScreen = new UpgradesScreen(this, screenManager, player);
			upgradesScreen.AddCustomControl("blackBG", new ImageControl(black, Vector2.Zero, 0.6f), true);
			upgradesScreen.AddCustomControl("background", shopBackground, true);
			upgradesScreen.AddCustomControl("update", new UpdateControl(() => { if (VirtualGamePad.GetGameKeyState(GameKeys.Menue) == GameKeysState.Released) { screenManager.RemoveScreen(); screenManager.AddScreen(INGAMEMENU, false); ((UpgradesScreen)screenManager.GetScreen(UPGRADESSCREEN)).RemoveEvents(); } }), false);

			//-> Bank anlegen
			BankScreen bankScreen = new BankScreen(this, screenManager, player);
			bankScreen.AddCustomControl("blackBG", new ImageControl(black, Vector2.Zero, 0.6f), true);
			bankScreen.AddCustomControl("background", shopBackground, true);
			bankScreen.AddCustomControl("update", new UpdateControl(() => { if (VirtualGamePad.GetGameKeyState(GameKeys.Menue) == GameKeysState.Released) { screenManager.RemoveScreen(); screenManager.AddScreen(INGAMEMENU, false); ((BankScreen)screenManager.GetScreen(BANKSCREEN)).RemoveEvents(); } }), false);
			bankScreen.RemoveEvents();

			//-> BattleScreen anlegen
			BattleScreen battleScreen = new BattleScreen(this, screenManager, player, null);
			battleScreen.AddCustomControl("blackBG", new ImageControl(black, Vector2.Zero, 0.6f), true);

			// -> Charakterauswahl Screen
			CharSelectedScreen charSelectedScreen = new CharSelectedScreen(this, screenManager, acc);
			charSelectedScreen.AddCustomControl("map", map, true);
			charSelectedScreen.AddCustomControl("backGround", new ImageControl(black, Vector2.Zero, 0.75f), true);

			Texture2D arrowRight = Content.Load<Texture2D>(@"GUI\ArrowRight.png");
			Texture2D arrowLeft = Content.Load<Texture2D>(@"GUI\ArrowLeft.png");
			Vector2 center = Resolution.Size / 2f - new Vector2(16f * 2f, 16f * 2f + 100);
			charSelectedScreen.AddCustomControl("NextCharButton", new ButtonControl(" ", center + new Vector2(170, 290), Content.Load<SpriteFont>(@"Fonts\Areal16.tk"), arrowRight, Color.White, () => ((CharSelectedScreen)screenManager.GetScreen(CHARSELECTEDSCREEN)).PreviousCharacter(), Color.Blue), false);
			charSelectedScreen.AddCustomControl("PreviousCharButton", new ButtonControl(" ", center + new Vector2(-190, 290), Content.Load<SpriteFont>(@"Fonts\Areal16.tk"), arrowLeft, Color.White, () => ((CharSelectedScreen)screenManager.GetScreen(CHARSELECTEDSCREEN)).NextCharacter(), Color.Blue), false);
			charSelectedScreen.AddCustomControl("ChooseCharButton", new ButtonControl("Play", center + new Vector2(-70, 420), Content.Load<SpriteFont>(@"Fonts\Areal16.tk"), buttonTexture, Color.White, () => ((CharSelectedScreen)screenManager.GetScreen(CHARSELECTEDSCREEN)).ChooseCharacter(), Color.Blue), false);
			charSelectedScreen.NextCharacter();

			//registieren aller Screens
			screenManager.RegisterScreen(TITLESCREEN, titleScreen);
			screenManager.RegisterScreen(MAINSCREEN, mainScreen);
			screenManager.RegisterScreen(OPTIONSCREEN, optionScreen);
			screenManager.RegisterScreen(GAMESCREEN, gameScreen);
			screenManager.RegisterScreen(INGAMEMENU, inGameMenu);
			screenManager.RegisterScreen(STATUSSCREEN, statusScreen);
			screenManager.RegisterScreen(DETAILSSCREEN, detailsScreen);
			screenManager.RegisterScreen(SKILLSCREEN, skillScreen);
			screenManager.RegisterScreen(STATISTIKSCREEN, statistikScreen);
			screenManager.RegisterScreen(INVENTORYSCREEN, inventoryScreen);
			screenManager.RegisterScreen(SHOPSCREEN, shopScreen);
			screenManager.RegisterScreen(UPGRADESSCREEN, upgradesScreen);
			screenManager.RegisterScreen(BANKSCREEN, bankScreen);
			screenManager.RegisterScreen(BATTLESCREEN, battleScreen);
			screenManager.RegisterScreen(CHARSELECTEDSCREEN, charSelectedScreen);

			//mit was startet die Anwendung
			screenManager.Transfer(TITLESCREEN);

			//Init Sound at last
			MediaManager.Instance.RegisterSound("hit1", new MediaPlayer(@"Content\Sounds\Hit1.wav"));
			MediaManager.Instance.RegisterSound("hit2", new MediaPlayer(@"Content\Sounds\Hit2.wav"));
			MediaManager.Instance.RegisterSound("backGround", new MediaPlayer(new Uri(GameOptions.RadioStream)));
			MediaManager.Instance.Play("backGround");
		}

		private void CreateBaseSkills(ItemManager itemManager) {
			//Status-Change-Skills//
			//stärke
			CoreStatusChangeSkill absStrI = new CoreStatusChangeSkill("+ #value Stärke", "Lunas", "der Stärke", SkillType.AbsoluteStrength, 1, 1, Quality.Magic, "0, 1, 5, 6", "description", ItemType.Weapon, ItemType.Shoe, ItemType.Schild, ItemType.Head, ItemType.Body);
			CoreStatusChangeSkill absStrII = new CoreStatusChangeSkill("+ #value Stärke", "Lunas", "der Stärke", SkillType.AbsoluteStrength, 20, 2, Quality.Magic, "0, 1, 5, 6", "description", ItemType.Weapon, ItemType.Shoe, ItemType.Schild, ItemType.Head, ItemType.Body);
			CoreStatusChangeSkill absStrIII = new CoreStatusChangeSkill("+ #value Stärke", "Lunas", "der Stärke", SkillType.AbsoluteStrength, 40, 3, Quality.Magic, "0, 1, 5, 6", "description", ItemType.Weapon, ItemType.Shoe, ItemType.Schild, ItemType.Head, ItemType.Body);
			CoreStatusChangeSkill absStrIV = new CoreStatusChangeSkill("+ #value Stärke", "Lunas", "der Stärke", SkillType.AbsoluteStrength, 60, 4, Quality.Magic, "0, 1, 5, 6", "description", ItemType.Weapon, ItemType.Shoe, ItemType.Schild, ItemType.Head, ItemType.Body);
			CoreStatusChangeSkill absStrV = new CoreStatusChangeSkill("+ #value Stärke", "Lunas", "der Stärke", SkillType.AbsoluteStrength, 80, 5, Quality.Rare, "0, 1, 5, 6", "description", ItemType.Weapon, ItemType.Shoe, ItemType.Schild, ItemType.Head, ItemType.Body);
			itemManager.AddBaseSkill(1, 24, absStrI);
			itemManager.AddBaseSkill(25, 48, absStrII);
			itemManager.AddBaseSkill(49, 72, absStrIII);
			itemManager.AddBaseSkill(73, 95, absStrIV);
			itemManager.AddBaseSkill(96, 120, absStrV);

			CoreStatusChangeSkill percStrI = new CoreStatusChangeSkill("+ #value% Stärke", "Lunas", "der Stärke", SkillType.PercentageStrength, 1, 1, Quality.Magic, "0, 1, 5, 6", "description", ItemType.Weapon, ItemType.Shoe, ItemType.Schild, ItemType.Head, ItemType.Body);
			CoreStatusChangeSkill percStrII = new CoreStatusChangeSkill("+ #value% Stärke", "Lunas", "der Stärke", SkillType.PercentageStrength, 20, 2, Quality.Magic, "0, 1, 5, 6", "description", ItemType.Weapon, ItemType.Shoe, ItemType.Schild, ItemType.Head, ItemType.Body);
			CoreStatusChangeSkill percStrIII = new CoreStatusChangeSkill("+ #value% Stärke", "Lunas", "der Stärke", SkillType.PercentageStrength, 40, 3, Quality.Magic, "0, 1, 5, 6", "description", ItemType.Weapon, ItemType.Shoe, ItemType.Schild, ItemType.Head, ItemType.Body);
			CoreStatusChangeSkill percStrIV = new CoreStatusChangeSkill("+ #value% Stärke", "Lunas", "der Stärke", SkillType.PercentageStrength, 60, 4, Quality.Magic, "0, 1, 5, 6", "description", ItemType.Weapon, ItemType.Shoe, ItemType.Schild, ItemType.Head, ItemType.Body);
			CoreStatusChangeSkill percStrV = new CoreStatusChangeSkill("+ #value% Stärke", "Lunas", "der Stärke", SkillType.PercentageStrength, 80, 5, Quality.Rare, "0, 1, 5, 6", "description", ItemType.Weapon, ItemType.Shoe, ItemType.Schild, ItemType.Head, ItemType.Body);
			itemManager.AddBaseSkill(1, 30, percStrI);
			itemManager.AddBaseSkill(31, 60, percStrII);
			itemManager.AddBaseSkill(61, 90, percStrIII);
			itemManager.AddBaseSkill(91, 120, percStrIV);
			itemManager.AddBaseSkill(121, 150, percStrV);

			//Geschick
			CoreStatusChangeSkill absDexI = new CoreStatusChangeSkill("+ #value Geschick", "Willows", "des Geschicks", SkillType.AbsoluteDexterity, 1, 1, Quality.Magic, "0, 1, 5, 6", "description", ItemType.Weapon, ItemType.Shoe, ItemType.Schild, ItemType.Head, ItemType.Body);
			CoreStatusChangeSkill absDexII = new CoreStatusChangeSkill("+ #value Geschick", "Willows", "des Geschicks", SkillType.AbsoluteDexterity, 20, 2, Quality.Magic, "0, 1, 5, 6", "description", ItemType.Weapon, ItemType.Shoe, ItemType.Schild, ItemType.Head, ItemType.Body);
			CoreStatusChangeSkill absDexIII = new CoreStatusChangeSkill("+ #value Geschick", "Willows", "des Geschicks", SkillType.AbsoluteDexterity, 40, 3, Quality.Magic, "0, 1, 5, 6", "description", ItemType.Weapon, ItemType.Shoe, ItemType.Schild, ItemType.Head, ItemType.Body);
			CoreStatusChangeSkill absDexIV = new CoreStatusChangeSkill("+ #value Geschick", "Willows", "des Geschicks", SkillType.AbsoluteDexterity, 60, 4, Quality.Magic, "0, 1, 5, 6", "description", ItemType.Weapon, ItemType.Shoe, ItemType.Schild, ItemType.Head, ItemType.Body);
			CoreStatusChangeSkill absDexV = new CoreStatusChangeSkill("+ #value Geschick", "Willows", "des Geschicks", SkillType.AbsoluteDexterity, 80, 5, Quality.Rare, "0, 1, 5, 6", "description", ItemType.Weapon, ItemType.Shoe, ItemType.Schild, ItemType.Head, ItemType.Body);
			itemManager.AddBaseSkill(1, 24, absDexI);
			itemManager.AddBaseSkill(25, 48, absDexII);
			itemManager.AddBaseSkill(49, 72, absDexIII);
			itemManager.AddBaseSkill(73, 95, absDexIV);
			itemManager.AddBaseSkill(96, 120, absDexV);

			CoreStatusChangeSkill percDexI = new CoreStatusChangeSkill("+ #value% Geschick", "Willows", "des Geschicks", SkillType.PercentageDexterity, 1, 1, Quality.Magic, "0, 1, 5, 6", "description", ItemType.Weapon, ItemType.Shoe, ItemType.Schild, ItemType.Head, ItemType.Body);
			CoreStatusChangeSkill percDexII = new CoreStatusChangeSkill("+ #value% Geschick", "Willows", "des Geschicks", SkillType.PercentageDexterity, 20, 2, Quality.Magic, "0, 1, 5, 6", "description", ItemType.Weapon, ItemType.Shoe, ItemType.Schild, ItemType.Head, ItemType.Body);
			CoreStatusChangeSkill percDexIII = new CoreStatusChangeSkill("+ #value% Geschick", "Willows", "des Geschicks", SkillType.PercentageDexterity, 40, 3, Quality.Magic, "0, 1, 5, 6", "description", ItemType.Weapon, ItemType.Shoe, ItemType.Schild, ItemType.Head, ItemType.Body);
			CoreStatusChangeSkill percDexIV = new CoreStatusChangeSkill("+ #value% Geschick", "Willows", "des Geschicks", SkillType.PercentageDexterity, 60, 4, Quality.Magic, "0, 1, 5, 6", "description", ItemType.Weapon, ItemType.Shoe, ItemType.Schild, ItemType.Head, ItemType.Body);
			CoreStatusChangeSkill percDexV = new CoreStatusChangeSkill("+ #value% Geschick", "Willows", "des Geschicks", SkillType.PercentageDexterity, 80, 5, Quality.Rare, "0, 1, 5, 6", "description", ItemType.Weapon, ItemType.Shoe, ItemType.Schild, ItemType.Head, ItemType.Body);
			itemManager.AddBaseSkill(1, 30, percDexI);
			itemManager.AddBaseSkill(31, 60, percDexII);
			itemManager.AddBaseSkill(61, 90, percDexIII);
			itemManager.AddBaseSkill(91, 120, percDexIV);
			itemManager.AddBaseSkill(121, 150, percDexV);

			//Ausdauer
			CoreStatusChangeSkill absStaI = new CoreStatusChangeSkill("+ #value Ausdauer", "Jaels", "der Ausdauer", SkillType.AbsoluteStamina, 1, 1, Quality.Magic, "0, 1, 5, 6", "description", ItemType.Weapon, ItemType.Shoe, ItemType.Schild, ItemType.Head, ItemType.Body);
			CoreStatusChangeSkill absStaII = new CoreStatusChangeSkill("+ #value Ausdauer", "Jaels", "der Ausdauer", SkillType.AbsoluteStamina, 20, 2, Quality.Magic, "0, 1, 5, 6", "description", ItemType.Weapon, ItemType.Shoe, ItemType.Schild, ItemType.Head, ItemType.Body);
			CoreStatusChangeSkill absStaIII = new CoreStatusChangeSkill("+ #value Ausdauer", "Jaels", "der Ausdauer", SkillType.AbsoluteStamina, 40, 3, Quality.Magic, "0, 1, 5, 6", "description", ItemType.Weapon, ItemType.Shoe, ItemType.Schild, ItemType.Head, ItemType.Body);
			CoreStatusChangeSkill absStaIV = new CoreStatusChangeSkill("+ #value Ausdauer", "Jaels", "der Ausdauer", SkillType.AbsoluteStamina, 60, 4, Quality.Magic, "0, 1, 5, 6", "description", ItemType.Weapon, ItemType.Shoe, ItemType.Schild, ItemType.Head, ItemType.Body);
			CoreStatusChangeSkill absStaV = new CoreStatusChangeSkill("+ #value Ausdauer", "Jaels", "der Ausdauer", SkillType.AbsoluteStamina, 80, 5, Quality.Rare, "0, 1, 5, 6", "description", ItemType.Weapon, ItemType.Shoe, ItemType.Schild, ItemType.Head, ItemType.Body);
			itemManager.AddBaseSkill(1, 24, absStaI);
			itemManager.AddBaseSkill(25, 48, absStaII);
			itemManager.AddBaseSkill(49, 72, absStaIII);
			itemManager.AddBaseSkill(73, 95, absStaIV);
			itemManager.AddBaseSkill(96, 120, absStaV);

			CoreStatusChangeSkill percStaI = new CoreStatusChangeSkill("+ #value% Ausdauer", "Jaels", "der Ausdauer", SkillType.PercentageStamina, 1, 1, Quality.Magic, "0, 1, 5, 6", "description", ItemType.Weapon, ItemType.Shoe, ItemType.Schild, ItemType.Head, ItemType.Body);
			CoreStatusChangeSkill percStaII = new CoreStatusChangeSkill("+ #value% Ausdauer", "Jaels", "der Ausdauer", SkillType.PercentageStamina, 20, 2, Quality.Magic, "0, 1, 5, 6", "description", ItemType.Weapon, ItemType.Shoe, ItemType.Schild, ItemType.Head, ItemType.Body);
			CoreStatusChangeSkill percStaIII = new CoreStatusChangeSkill("+ #value% Ausdauer", "Jaels", "der Ausdauer", SkillType.PercentageStamina, 40, 3, Quality.Magic, "0, 1, 5, 6", "description", ItemType.Weapon, ItemType.Shoe, ItemType.Schild, ItemType.Head, ItemType.Body);
			CoreStatusChangeSkill percStaIV = new CoreStatusChangeSkill("+ #value% Ausdauer", "Jaels", "der Ausdauer", SkillType.PercentageStamina, 60, 4, Quality.Magic, "0, 1, 5, 6", "description", ItemType.Weapon, ItemType.Shoe, ItemType.Schild, ItemType.Head, ItemType.Body);
			CoreStatusChangeSkill percStaV = new CoreStatusChangeSkill("+ #value% Ausdauer", "Jaels", "der Ausdauer", SkillType.PercentageStamina, 80, 5, Quality.Rare, "0, 1, 5, 6", "description", ItemType.Weapon, ItemType.Shoe, ItemType.Schild, ItemType.Head, ItemType.Body);
			itemManager.AddBaseSkill(1, 30, percStaI);
			itemManager.AddBaseSkill(31, 60, percStaII);
			itemManager.AddBaseSkill(61, 90, percStaIII);
			itemManager.AddBaseSkill(91, 120, percStaIV);
			itemManager.AddBaseSkill(121, 150, percStaV);

			//Glück
			CoreStatusChangeSkill absLuckI = new CoreStatusChangeSkill("+ #value Glück", "Gabriels", "des Glücks", SkillType.AbsoluteLuck, 1, 1, Quality.Magic, "0, 1, 5, 6", "description", ItemType.Weapon, ItemType.Shoe, ItemType.Schild, ItemType.Head, ItemType.Body);
			CoreStatusChangeSkill absLuckII = new CoreStatusChangeSkill("+ #value Glück", "Gabriels", "des Glücks", SkillType.AbsoluteLuck, 20, 2, Quality.Magic, "0, 1, 5, 6", "description", ItemType.Weapon, ItemType.Shoe, ItemType.Schild, ItemType.Head, ItemType.Body);
			CoreStatusChangeSkill absLuckIII = new CoreStatusChangeSkill("+ #value Glück", "Gabriels", "des Glücks", SkillType.AbsoluteLuck, 40, 3, Quality.Magic, "0, 1, 5, 6", "description", ItemType.Weapon, ItemType.Shoe, ItemType.Schild, ItemType.Head, ItemType.Body);
			CoreStatusChangeSkill absLuckIV = new CoreStatusChangeSkill("+ #value Glück", "Gabriels", "des Glücks", SkillType.AbsoluteLuck, 60, 4, Quality.Magic, "0, 1, 5, 6", "description", ItemType.Weapon, ItemType.Shoe, ItemType.Schild, ItemType.Head, ItemType.Body);
			CoreStatusChangeSkill absLuckV = new CoreStatusChangeSkill("+ #value Glück", "Gabriels", "des Glücks", SkillType.AbsoluteLuck, 80, 5, Quality.Rare, "0, 1, 5, 6", "description", ItemType.Weapon, ItemType.Shoe, ItemType.Schild, ItemType.Head, ItemType.Body);
			itemManager.AddBaseSkill(1, 24, absLuckI);
			itemManager.AddBaseSkill(25, 48, absLuckII);
			itemManager.AddBaseSkill(49, 72, absLuckIII);
			itemManager.AddBaseSkill(73, 95, absLuckIV);
			itemManager.AddBaseSkill(96, 120, absLuckV);

			CoreStatusChangeSkill percLuckI = new CoreStatusChangeSkill("+ #value% Glück", "Gabriels", "des Glücks", SkillType.PercentageLuck, 1, 1, Quality.Magic, "0, 1, 5, 6", "description", ItemType.Weapon, ItemType.Shoe, ItemType.Schild, ItemType.Head, ItemType.Body);
			CoreStatusChangeSkill percLuckII = new CoreStatusChangeSkill("+ #value% Glück", "Gabriels", "des Glücks", SkillType.PercentageLuck, 20, 2, Quality.Magic, "0, 1, 5, 6", "description", ItemType.Weapon, ItemType.Shoe, ItemType.Schild, ItemType.Head, ItemType.Body);
			CoreStatusChangeSkill percLuckIII = new CoreStatusChangeSkill("+ #value% Glück", "Gabriels", "des Glücks", SkillType.PercentageLuck, 40, 3, Quality.Magic, "0, 1, 5, 6", "description", ItemType.Weapon, ItemType.Shoe, ItemType.Schild, ItemType.Head, ItemType.Body);
			CoreStatusChangeSkill percLuckIV = new CoreStatusChangeSkill("+ #value% Glück", "Gabriels", "des Glücks", SkillType.PercentageLuck, 60, 4, Quality.Magic, "0, 1, 5, 6", "description", ItemType.Weapon, ItemType.Shoe, ItemType.Schild, ItemType.Head, ItemType.Body);
			CoreStatusChangeSkill percLuckV = new CoreStatusChangeSkill("+ #value% Glück", "Gabriels", "des Glücks", SkillType.PercentageLuck, 80, 5, Quality.Rare, "0, 1, 5, 6", "description", ItemType.Weapon, ItemType.Shoe, ItemType.Schild, ItemType.Head, ItemType.Body);
			itemManager.AddBaseSkill(1, 30, percLuckI);
			itemManager.AddBaseSkill(31, 60, percLuckII);
			itemManager.AddBaseSkill(61, 90, percLuckIII);
			itemManager.AddBaseSkill(91, 120, percLuckIV);
			itemManager.AddBaseSkill(121, 150, percLuckV);

			//all stats
			CoreStatusChangeSkill absAllStatsI = new CoreStatusChangeSkill("+ #value zu allen Statuswerten", "Zeus", "des Könners", SkillType.AbsoluteAllStats, 30, 1, Quality.Legendary, "1, 4, 5, 6, 9", "description", ItemType.Weapon, ItemType.Shoe, ItemType.Schild, ItemType.Head, ItemType.Body);
			CoreStatusChangeSkill absAllStatsII = new CoreStatusChangeSkill("+ #value zu allen Statuswerten", "Zeus", "des Könners", SkillType.AbsoluteAllStats, 50, 2, Quality.Legendary, "1, 4, 5, 6, 9", "description", ItemType.Weapon, ItemType.Shoe, ItemType.Schild, ItemType.Head, ItemType.Body);
			CoreStatusChangeSkill absAllStatsIII = new CoreStatusChangeSkill("+ #value zu allen Statuswerten", "Zeus", "des Könners", SkillType.AbsoluteAllStats, 60, 3, Quality.Legendary, "1, 4, 5, 6, 9", "description", ItemType.Weapon, ItemType.Shoe, ItemType.Schild, ItemType.Head, ItemType.Body);
			CoreStatusChangeSkill absAllStatsIV = new CoreStatusChangeSkill("+ #value zu allen Statuswerten", "Zeus", "des Könners", SkillType.AbsoluteAllStats, 70, 4, Quality.Legendary, "1, 4, 5, 6, 9", "description", ItemType.Weapon, ItemType.Shoe, ItemType.Schild, ItemType.Head, ItemType.Body);
			CoreStatusChangeSkill absAllStatsV = new CoreStatusChangeSkill("+ #value zu allen Statuswerten", "Zeus", "des Könners", SkillType.AbsoluteAllStats, 80, 5, Quality.Legendary, "1, 4, 5, 6, 9", "description", ItemType.Weapon, ItemType.Shoe, ItemType.Schild, ItemType.Head, ItemType.Body);
			itemManager.AddBaseSkill(1, 10, absAllStatsI);
			itemManager.AddBaseSkill(11, 30, absAllStatsII);
			itemManager.AddBaseSkill(31, 50, absAllStatsIII);
			itemManager.AddBaseSkill(51, 70, absAllStatsIV);
			itemManager.AddBaseSkill(71, 90, absAllStatsV);

			//Rüstung
			CoreStatusChangeSkill absDefI = new CoreStatusChangeSkill("+ #value Rüstung", "Cains", "des Schutzes", SkillType.AbsoluteDefence, 1, 1, Quality.Magic, "1, 2, 4, 5", "description", ItemType.Shoe, ItemType.Schild, ItemType.Head, ItemType.Body);
			CoreStatusChangeSkill absDefII = new CoreStatusChangeSkill("+ #value Rüstung", "Cains", "des Schutzes", SkillType.AbsoluteDefence, 20, 2, Quality.Magic, "1, 2, 4, 5", "description", ItemType.Shoe, ItemType.Schild, ItemType.Head, ItemType.Body);
			CoreStatusChangeSkill absDefIII = new CoreStatusChangeSkill("+ #value Rüstung", "Cains", "des Schutzes", SkillType.AbsoluteDefence, 40, 3, Quality.Magic, "1, 2, 4, 5", "description", ItemType.Shoe, ItemType.Schild, ItemType.Head, ItemType.Body);
			CoreStatusChangeSkill absDefIV = new CoreStatusChangeSkill("+ #value Rüstung", "Cains", "des Schutzes", SkillType.AbsoluteDefence, 60, 4, Quality.Magic, "1, 2, 4, 5", "description", ItemType.Shoe, ItemType.Schild, ItemType.Head, ItemType.Body);
			CoreStatusChangeSkill absDefV = new CoreStatusChangeSkill("+ #value Rüstung", "Cains", "des Schutzes", SkillType.AbsoluteDefence, 80, 5, Quality.Rare, "1, 2, 4, 5", "description", ItemType.Shoe, ItemType.Schild, ItemType.Head, ItemType.Body);
			itemManager.AddBaseSkill(1, 40, absDefI);
			itemManager.AddBaseSkill(41, 80, absDefII);
			itemManager.AddBaseSkill(81, 120, absDefIII);
			itemManager.AddBaseSkill(121, 160, absDefIV);
			itemManager.AddBaseSkill(161, 200, absDefV);

			CoreStatusChangeSkill percDefI = new CoreStatusChangeSkill("+ #value% Rüstung", "Cains", "des Schutzes", SkillType.PercentageDefence, 1, 1, Quality.Magic, "1, 2, 4, 5", "description", ItemType.Shoe, ItemType.Schild, ItemType.Head, ItemType.Body);
			CoreStatusChangeSkill percDefII = new CoreStatusChangeSkill("+ #value% Rüstung", "Cains", "des Schutzes", SkillType.PercentageDefence, 20, 2, Quality.Magic, "1, 2, 4, 5", "description", ItemType.Shoe, ItemType.Schild, ItemType.Head, ItemType.Body);
			CoreStatusChangeSkill percDefIII = new CoreStatusChangeSkill("+ #value% Rüstung", "Cains", "des Schutzes", SkillType.PercentageDefence, 40, 3, Quality.Magic, "1, 2, 4, 5", "description", ItemType.Shoe, ItemType.Schild, ItemType.Head, ItemType.Body);
			CoreStatusChangeSkill percDefIV = new CoreStatusChangeSkill("+ #value% Rüstung", "Cains", "des Schutzes", SkillType.PercentageDefence, 60, 4, Quality.Magic, "1, 2, 4, 5", "description", ItemType.Shoe, ItemType.Schild, ItemType.Head, ItemType.Body);
			CoreStatusChangeSkill percDefV = new CoreStatusChangeSkill("+ #value% Rüstung", "Cains", "des Schutzes", SkillType.PercentageDefence, 80, 5, Quality.Rare, "1, 2, 4, 5", "description", ItemType.Shoe, ItemType.Schild, ItemType.Head, ItemType.Body);
			itemManager.AddBaseSkill(1, 20, percDefI);
			itemManager.AddBaseSkill(21, 40, percDefII);
			itemManager.AddBaseSkill(41, 60, percDefIII);
			itemManager.AddBaseSkill(61, 80, percDefIV);
			itemManager.AddBaseSkill(81, 100, percDefV);

			//Rüstungswert
			CoreStatusChangeSkill absDefValueI = new CoreStatusChangeSkill("+ #value Rüstungswert", "Skys", "des Schutzsuchenden", SkillType.AbsoluteDefenceValue, 1, 1, Quality.Magic, "1, 2, 4, 5", "description", ItemType.Shoe, ItemType.Schild, ItemType.Head, ItemType.Body);
			CoreStatusChangeSkill absDefValueII = new CoreStatusChangeSkill("+ #value Rüstungswert", "Skys", "des Schutzsuchenden", SkillType.AbsoluteDefenceValue, 20, 2, Quality.Magic, "1, 2, 4, 5", "description", ItemType.Shoe, ItemType.Schild, ItemType.Head, ItemType.Body);
			CoreStatusChangeSkill absDefValueIII = new CoreStatusChangeSkill("+ #value Rüstungswert", "Skys", "des Schutzsuchenden", SkillType.AbsoluteDefenceValue, 40, 3, Quality.Magic, "1, 2, 4, 5", "description", ItemType.Shoe, ItemType.Schild, ItemType.Head, ItemType.Body);
			CoreStatusChangeSkill absDefValueIV = new CoreStatusChangeSkill("+ #value Rüstungswert", "Skys", "des Schutzsuchenden", SkillType.AbsoluteDefenceValue, 60, 4, Quality.Magic, "1, 2, 4, 5", "description", ItemType.Shoe, ItemType.Schild, ItemType.Head, ItemType.Body);
			CoreStatusChangeSkill absDefValueV = new CoreStatusChangeSkill("+ #value Rüstungswert", "Skys", "des Schutzsuchenden", SkillType.AbsoluteDefenceValue, 80, 5, Quality.Rare, "1, 2, 4, 5", "description", ItemType.Shoe, ItemType.Schild, ItemType.Head, ItemType.Body);
			itemManager.AddBaseSkill(1, 50, absDefValueI);
			itemManager.AddBaseSkill(51, 400, absDefValueII);
			itemManager.AddBaseSkill(401, 900, absDefValueIII);
			itemManager.AddBaseSkill(901, 1300, absDefValueIV);
			itemManager.AddBaseSkill(1301, 2000, absDefValueV);

			CoreStatusChangeSkill percDefValueI = new CoreStatusChangeSkill("+ #value% Rüstungswert", "Skys", "des Schutzsuchenden", SkillType.PercentageDefenceValue, 1, 1, Quality.Magic, "1, 2, 4, 5", "description", ItemType.Shoe, ItemType.Schild, ItemType.Head, ItemType.Body);
			CoreStatusChangeSkill percDefValueII = new CoreStatusChangeSkill("+ #value% Rüstungswert", "Skys", "des Schutzsuchenden", SkillType.PercentageDefenceValue, 20, 2, Quality.Magic, "1, 2, 4, 5", "description", ItemType.Shoe, ItemType.Schild, ItemType.Head, ItemType.Body);
			CoreStatusChangeSkill percDefValueIII = new CoreStatusChangeSkill("+ #value% Rüstungswert", "Skys", "des Schutzsuchenden", SkillType.PercentageDefenceValue, 40, 3, Quality.Magic, "1, 2, 4, 5", "description", ItemType.Shoe, ItemType.Schild, ItemType.Head, ItemType.Body);
			CoreStatusChangeSkill percDefValueIV = new CoreStatusChangeSkill("+ #value% Rüstungswert", "Skys", "des Schutzsuchenden", SkillType.PercentageDefenceValue, 60, 4, Quality.Magic, "1, 2, 4, 5", "description", ItemType.Shoe, ItemType.Schild, ItemType.Head, ItemType.Body);
			CoreStatusChangeSkill percDefValueV = new CoreStatusChangeSkill("+ #value% Rüstungswert", "Skys", "des Schutzsuchenden", SkillType.PercentageDefenceValue, 80, 5, Quality.Rare, "1, 2, 4, 5", "description", ItemType.Shoe, ItemType.Schild, ItemType.Head, ItemType.Body);
			itemManager.AddBaseSkill(1, 14, percDefValueI);
			itemManager.AddBaseSkill(15, 28, percDefValueII);
			itemManager.AddBaseSkill(29, 42, percDefValueIII);
			itemManager.AddBaseSkill(43, 56, percDefValueIV);
			itemManager.AddBaseSkill(57, 70, percDefValueV);

			//Kritische Treffer Chance
			CoreStatusChangeSkill absCritChanceI = new CoreStatusChangeSkill("+ #value% kritische Trefferchance", "Eves", "der Chance", SkillType.AbsoluteCriticalHitChance, 1, 1, Quality.Magic, "1, 2, 4, 5", "description", ItemType.Weapon, ItemType.Body, ItemType.Schild);
			CoreStatusChangeSkill absCritChanceII = new CoreStatusChangeSkill("+ #value% kritische Trefferchance", "Eves", "der Chance", SkillType.AbsoluteCriticalHitChance, 20, 2, Quality.Magic, "1, 2, 4, 5", "description", ItemType.Weapon, ItemType.Body, ItemType.Schild);
			CoreStatusChangeSkill absCritChanceIII = new CoreStatusChangeSkill("+ #value% kritische Trefferchance", "Eves", "der Chance", SkillType.AbsoluteCriticalHitChance, 40, 3, Quality.Magic, "1, 2, 4, 5", "description", ItemType.Weapon, ItemType.Body, ItemType.Schild);
			CoreStatusChangeSkill absCritChanceIV = new CoreStatusChangeSkill("+ #value% kritische Trefferchance", "Eves", "der Chance", SkillType.AbsoluteCriticalHitChance, 60, 4, Quality.Magic, "1, 2, 4, 5", "description", ItemType.Weapon, ItemType.Body, ItemType.Schild);
			CoreStatusChangeSkill absCritChanceV = new CoreStatusChangeSkill("+ #value% kritische Trefferchance", "Eves", "der Chance", SkillType.AbsoluteCriticalHitChance, 80, 5, Quality.Rare, "1, 2, 4, 5", "description", ItemType.Weapon, ItemType.Body, ItemType.Schild);
			itemManager.AddBaseSkill(1, 4, absCritChanceI);
			itemManager.AddBaseSkill(5, 8, absCritChanceII);
			itemManager.AddBaseSkill(9, 12, absCritChanceIII);
			itemManager.AddBaseSkill(13, 16, absCritChanceIV);
			itemManager.AddBaseSkill(17, 20, absCritChanceV);

			//MF
			CoreStatusChangeSkill absMFI = new CoreStatusChangeSkill("+ #value% Magic Find", "Magiers", "des Suchenden", SkillType.PercentageMagicFind, 1, 1, Quality.Magic, "1, 4, 5, 6", "description", ItemType.Shoe, ItemType.Head, ItemType.Body);
			CoreStatusChangeSkill absMFII = new CoreStatusChangeSkill("+ #value% Magic Find", "Magiers", "des Suchenden", SkillType.PercentageMagicFind, 20, 2, Quality.Magic, "1, 4, 5, 6", "description", ItemType.Shoe, ItemType.Head, ItemType.Body);
			CoreStatusChangeSkill absMFIII = new CoreStatusChangeSkill("+ #value% Magic Find", "Magiers", "des Suchenden", SkillType.PercentageMagicFind, 40, 3, Quality.Magic, "1, 4, 5, 6", "description", ItemType.Shoe, ItemType.Head, ItemType.Body);
			CoreStatusChangeSkill absMFIV = new CoreStatusChangeSkill("+ #value% Magic Find", "Magiers", "des Suchenden", SkillType.PercentageMagicFind, 60, 4, Quality.Magic, "1, 4, 5, 6", "description", ItemType.Shoe, ItemType.Head, ItemType.Body);
			CoreStatusChangeSkill absMFV = new CoreStatusChangeSkill("+ #value% Magic Find", "Magiers", "des Suchenden", SkillType.PercentageMagicFind, 80, 5, Quality.Rare, "1, 4, 5, 6", "description", ItemType.Shoe, ItemType.Head, ItemType.Body);
			itemManager.AddBaseSkill(1, 50, absMFI);
			itemManager.AddBaseSkill(51, 100, absMFII);
			itemManager.AddBaseSkill(101, 150, absMFIII);
			itemManager.AddBaseSkill(151, 200, absMFIV);
			itemManager.AddBaseSkill(201, 250, absMFV);

			//HP
			CoreStatusChangeSkill absHPI = new CoreStatusChangeSkill("+ #value Leben", "Erzengels", "des Lebens", SkillType.AbsoluteHP, 1, 1, Quality.Magic, "1, 4, 5, 6", "description", ItemType.Weapon, ItemType.Shoe, ItemType.Schild, ItemType.Head, ItemType.Body);
			CoreStatusChangeSkill absHPII = new CoreStatusChangeSkill("+ #value Leben", "Erzengels", "des Lebens", SkillType.AbsoluteHP, 20, 2, Quality.Magic, "1, 4, 5, 6", "description", ItemType.Weapon, ItemType.Shoe, ItemType.Schild, ItemType.Head, ItemType.Body);
			CoreStatusChangeSkill absHPIII = new CoreStatusChangeSkill("+ #value Leben", "Erzengels", "des Lebens", SkillType.AbsoluteHP, 40, 3, Quality.Magic, "1, 4, 5, 6", "description", ItemType.Weapon, ItemType.Shoe, ItemType.Schild, ItemType.Head, ItemType.Body);
			CoreStatusChangeSkill absHPIV = new CoreStatusChangeSkill("+ #value Leben", "Erzengels", "des Lebens", SkillType.AbsoluteHP, 60, 4, Quality.Magic, "1, 4, 5, 6", "description", ItemType.Weapon, ItemType.Shoe, ItemType.Schild, ItemType.Head, ItemType.Body);
			CoreStatusChangeSkill absHPV = new CoreStatusChangeSkill("+ #value Leben", "Erzengels", "des Lebens", SkillType.AbsoluteHP, 80, 5, Quality.Rare, "1, 4, 5, 6", "description", ItemType.Weapon, ItemType.Shoe, ItemType.Schild, ItemType.Head, ItemType.Body);
			itemManager.AddBaseSkill(50, 500, absHPI);
			itemManager.AddBaseSkill(501, 1000, absHPII);
			itemManager.AddBaseSkill(1001, 1500, absHPIII);
			itemManager.AddBaseSkill(1501, 3000, absHPIV);
			itemManager.AddBaseSkill(3001, 6000, absHPV);

			CoreStatusChangeSkill percHPI = new CoreStatusChangeSkill("+ #value% Leben", "Erzengels", "des Lebens", SkillType.PercentageHP, 1, 1, Quality.Magic, "1, 4, 5, 6", "description", ItemType.Weapon, ItemType.Shoe, ItemType.Schild, ItemType.Head, ItemType.Body);
			CoreStatusChangeSkill percHPII = new CoreStatusChangeSkill("+ #value% Leben", "Erzengels", "des Lebens", SkillType.PercentageHP, 20, 2, Quality.Magic, "1, 4, 5, 6", "description", ItemType.Weapon, ItemType.Shoe, ItemType.Schild, ItemType.Head, ItemType.Body);
			CoreStatusChangeSkill percHPIII = new CoreStatusChangeSkill("+ #value% Leben", "Erzengels", "des Lebens", SkillType.PercentageHP, 40, 3, Quality.Magic, "1, 4, 5, 6", "description", ItemType.Weapon, ItemType.Shoe, ItemType.Schild, ItemType.Head, ItemType.Body);
			CoreStatusChangeSkill percHPIV = new CoreStatusChangeSkill("+ #value% Leben", "Erzengels", "des Lebens", SkillType.PercentageHP, 60, 4, Quality.Magic, "1, 4, 5, 6", "description", ItemType.Weapon, ItemType.Shoe, ItemType.Schild, ItemType.Head, ItemType.Body);
			CoreStatusChangeSkill percHPV = new CoreStatusChangeSkill("+ #value% Leben", "Erzengels", "des Lebens", SkillType.PercentageHP, 80, 5, Quality.Rare, "1, 4, 5, 6", "description", ItemType.Weapon, ItemType.Shoe, ItemType.Schild, ItemType.Head, ItemType.Body);
			itemManager.AddBaseSkill(1, 25, percHPI);
			itemManager.AddBaseSkill(26, 50, percHPII);
			itemManager.AddBaseSkill(51, 75, percHPIII);
			itemManager.AddBaseSkill(76, 100, percHPIV);
			itemManager.AddBaseSkill(101, 125, percHPV);

			//Ausweichen
			CoreStatusChangeSkill absEvadeI = new CoreStatusChangeSkill("+ #value% Ausweichen", "Merlins", "der Flinkheit", SkillType.AbsoluteEvade, 1, 1, Quality.Magic, "0, 1, 4, 5", "description", ItemType.Shoe, ItemType.Head, ItemType.Body);
			CoreStatusChangeSkill absEvadeII = new CoreStatusChangeSkill("+ #value% Ausweichen", "Merlins", "der Flinkheit", SkillType.AbsoluteEvade, 20, 2, Quality.Magic, "0, 1, 4, 5", "description", ItemType.Shoe, ItemType.Head, ItemType.Body);
			CoreStatusChangeSkill absEvadeIII = new CoreStatusChangeSkill("+ #value% Ausweichen", "Merlins", "der Flinkheit", SkillType.AbsoluteEvade, 40, 3, Quality.Magic, "0, 1, 4, 5", "description", ItemType.Shoe, ItemType.Head, ItemType.Body);
			CoreStatusChangeSkill absEvadeIV = new CoreStatusChangeSkill("+ #value% Ausweichen", "Merlins", "der Flinkheit", SkillType.AbsoluteEvade, 60, 4, Quality.Magic, "0, 1, 4, 5", "description", ItemType.Shoe, ItemType.Head, ItemType.Body);
			CoreStatusChangeSkill absEvadeV = new CoreStatusChangeSkill("+ #value% Ausweichen", "Merlins", "der Flinkheit", SkillType.AbsoluteEvade, 80, 5, Quality.Rare, "0, 1, 4, 5", "description", ItemType.Shoe, ItemType.Head, ItemType.Body);
			itemManager.AddBaseSkill(1, 4, absEvadeI);
			itemManager.AddBaseSkill(5, 8, absEvadeII);
			itemManager.AddBaseSkill(9, 12, absEvadeIII);
			itemManager.AddBaseSkill(13, 16, absEvadeIV);
			itemManager.AddBaseSkill(17, 20, absEvadeV);

			//Min Weapon Dmg
			CoreStatusChangeSkill absMinWeapDmgI = new CoreStatusChangeSkill("+ #value min Waffenschaden", "Enigmas", "des Schadens", SkillType.AbsoluteMinWeaponDmg, 1, 1, Quality.Magic, "0, 1, 2, 3", "description", ItemType.Weapon);
			CoreStatusChangeSkill absMinWeapDmgII = new CoreStatusChangeSkill("+ #value min Waffenschaden", "Enigmas", "des Schadens", SkillType.AbsoluteMinWeaponDmg, 20, 2, Quality.Magic, "0, 1, 2, 3", "description", ItemType.Weapon);
			CoreStatusChangeSkill absMinWeapDmgIII = new CoreStatusChangeSkill("+ #value min Waffenschaden", "Enigmas", "des Schadens", SkillType.AbsoluteMinWeaponDmg, 40, 3, Quality.Magic, "0, 1, 2, 3", "description", ItemType.Weapon);
			CoreStatusChangeSkill absMinWeapDmgIV = new CoreStatusChangeSkill("+ #value min Waffenschaden", "Enigmas", "des Schadens", SkillType.AbsoluteMinWeaponDmg, 60, 4, Quality.Magic, "0, 1, 2, 3", "description", ItemType.Weapon);
			CoreStatusChangeSkill absMinWeapDmgV = new CoreStatusChangeSkill("+ #value min Waffenschaden", "Enigmas", "des Schadens", SkillType.AbsoluteMinWeaponDmg, 80, 5, Quality.Rare, "0, 1, 2, 3", "description", ItemType.Weapon);
			itemManager.AddBaseSkill(1, 30, absMinWeapDmgI);
			itemManager.AddBaseSkill(31, 60, absMinWeapDmgII);
			itemManager.AddBaseSkill(61, 90, absMinWeapDmgIII);
			itemManager.AddBaseSkill(91, 120, absMinWeapDmgIV);
			itemManager.AddBaseSkill(121, 150, absMinWeapDmgV);

			CoreStatusChangeSkill percMinWeapDmgI = new CoreStatusChangeSkill("+ #value% min Waffenschaden", "Enigmas", "des Schadens", SkillType.PercentageMinWeaponDmg, 1, 1, Quality.Magic, "0, 1, 2, 3", "description", ItemType.Weapon);
			CoreStatusChangeSkill percMinWeapDmgII = new CoreStatusChangeSkill("+ #value% min Waffenschaden", "Enigmas", "des Schadens", SkillType.PercentageMinWeaponDmg, 20, 2, Quality.Magic, "0, 1, 2, 3", "description", ItemType.Weapon);
			CoreStatusChangeSkill percMinWeapDmgIII = new CoreStatusChangeSkill("+ #value% min Waffenschaden", "Enigmas", "des Schadens", SkillType.PercentageMinWeaponDmg, 40, 3, Quality.Magic, "0, 1, 2, 3", "description", ItemType.Weapon);
			CoreStatusChangeSkill percMinWeapDmgIV = new CoreStatusChangeSkill("+ #value% min Waffenschaden", "Enigmas", "des Schadens", SkillType.PercentageMinWeaponDmg, 60, 4, Quality.Magic, "0, 1, 2, 3", "description", ItemType.Weapon);
			CoreStatusChangeSkill percMinWeapDmgV = new CoreStatusChangeSkill("+ #value% min Waffenschaden", "Enigmas", "des Schadens", SkillType.PercentageMinWeaponDmg, 80, 5, Quality.Rare, "0, 1, 2, 3", "description", ItemType.Weapon);
			itemManager.AddBaseSkill(1, 36, percMinWeapDmgI);
			itemManager.AddBaseSkill(37, 72, percMinWeapDmgII);
			itemManager.AddBaseSkill(73, 108, percMinWeapDmgIII);
			itemManager.AddBaseSkill(109, 144, percMinWeapDmgIV);
			itemManager.AddBaseSkill(145, 180, percMinWeapDmgV);

			//Max Weapon Dmg
			CoreStatusChangeSkill absMaxWeapDmgI = new CoreStatusChangeSkill("+ #value Max Waffenschaden", "Baals", "des Schadens", SkillType.AbsoluteMaxWeaponDmg, 1, 1, Quality.Magic, "0, 1, 2, 3", "description", ItemType.Weapon);
			CoreStatusChangeSkill absMaxWeapDmgII = new CoreStatusChangeSkill("+ #value Max Waffenschaden", "Baals", "des Schadens", SkillType.AbsoluteMaxWeaponDmg, 20, 2, Quality.Magic, "0, 1, 2, 3", "description", ItemType.Weapon);
			CoreStatusChangeSkill absMaxWeapDmgIII = new CoreStatusChangeSkill("+ #value Max Waffenschaden", "Baals", "des Schadens", SkillType.AbsoluteMaxWeaponDmg, 40, 3, Quality.Magic, "0, 1, 2, 3", "description", ItemType.Weapon);
			CoreStatusChangeSkill absMaxWeapDmgIV = new CoreStatusChangeSkill("+ #value Max Waffenschaden", "Baals", "des Schadens", SkillType.AbsoluteMaxWeaponDmg, 60, 4, Quality.Magic, "0, 1, 2, 3", "description", ItemType.Weapon);
			CoreStatusChangeSkill absMaxWeapDmgV = new CoreStatusChangeSkill("+ #value Max Waffenschaden", "Baals", "des Schadens", SkillType.AbsoluteMaxWeaponDmg, 80, 5, Quality.Rare, "0, 1, 2, 3", "description", ItemType.Weapon);
			itemManager.AddBaseSkill(1, 67, absMaxWeapDmgI);
			itemManager.AddBaseSkill(68, 134, absMaxWeapDmgII);
			itemManager.AddBaseSkill(135, 201, absMaxWeapDmgIII);
			itemManager.AddBaseSkill(202, 268, absMaxWeapDmgIV);
			itemManager.AddBaseSkill(269, 336, absMaxWeapDmgV);

			CoreStatusChangeSkill percMaxWeapDmgI = new CoreStatusChangeSkill("+ #value% Max Waffenschaden", "Baals", "des Schadens", SkillType.PercentageMaxWeaponDmg, 1, 1, Quality.Magic, "0, 1, 2, 3", "description", ItemType.Weapon);
			CoreStatusChangeSkill percMaxWeapDmgII = new CoreStatusChangeSkill("+ #value% Max Waffenschaden", "Baals", "des Schadens", SkillType.PercentageMaxWeaponDmg, 20, 2, Quality.Magic, "0, 1, 2, 3", "description", ItemType.Weapon);
			CoreStatusChangeSkill percMaxWeapDmgIII = new CoreStatusChangeSkill("+ #value% Max Waffenschaden", "Baals", "des Schadens", SkillType.PercentageMaxWeaponDmg, 40, 3, Quality.Magic, "0, 1, 2, 3", "description", ItemType.Weapon);
			CoreStatusChangeSkill percMaxWeapDmgIV = new CoreStatusChangeSkill("+ #value% Max Waffenschaden", "Baals", "des Schadens", SkillType.PercentageMaxWeaponDmg, 60, 4, Quality.Magic, "0, 1, 2, 3", "description", ItemType.Weapon);
			CoreStatusChangeSkill percMaxWeapDmgV = new CoreStatusChangeSkill("+ #value% Max Waffenschaden", "Baals", "des Schadens", SkillType.PercentageMaxWeaponDmg, 80, 5, Quality.Rare, "0, 1, 2, 3", "description", ItemType.Weapon);
			itemManager.AddBaseSkill(1, 36, percMaxWeapDmgI);
			itemManager.AddBaseSkill(37, 72, percMaxWeapDmgII);
			itemManager.AddBaseSkill(73, 108, percMaxWeapDmgIII);
			itemManager.AddBaseSkill(109, 144, percMaxWeapDmgIV);
			itemManager.AddBaseSkill(145, 180, percMaxWeapDmgV);

			//Min Dmg
			CoreStatusChangeSkill absMinDmgI = new CoreStatusChangeSkill("+ #value min Schaden", "Teufels", "des Schadens", SkillType.AbsoluteMinDamage, 1, 1, Quality.Magic, "2, 4, 5, 6", "description", ItemType.Weapon, ItemType.Schild);
			CoreStatusChangeSkill absMinDmgII = new CoreStatusChangeSkill("+ #value min Schaden", "Teufels", "des Schadens", SkillType.AbsoluteMinDamage, 20, 2, Quality.Magic, "2, 4, 5, 6", "description", ItemType.Weapon, ItemType.Schild);
			CoreStatusChangeSkill absMinDmgIII = new CoreStatusChangeSkill("+ #value min Schaden", "Teufels", "des Schadens", SkillType.AbsoluteMinDamage, 40, 3, Quality.Magic, "2, 4, 5, 6", "description", ItemType.Weapon, ItemType.Schild);
			CoreStatusChangeSkill absMinDmgIV = new CoreStatusChangeSkill("+ #value min Schaden", "Teufels", "des Schadens", SkillType.AbsoluteMinDamage, 60, 4, Quality.Magic, "2, 4, 5, 6", "description", ItemType.Weapon, ItemType.Schild);
			CoreStatusChangeSkill absMinDmgV = new CoreStatusChangeSkill("+ #value min Schaden", "Teufels", "des Schadens", SkillType.AbsoluteMinDamage, 80, 5, Quality.Rare, "2, 4, 5, 6", "description", ItemType.Weapon, ItemType.Schild);
			itemManager.AddBaseSkill(1, 60, absMinDmgI);
			itemManager.AddBaseSkill(61, 120, absMinDmgII);
			itemManager.AddBaseSkill(121, 180, absMinDmgIII);
			itemManager.AddBaseSkill(181, 240, absMinDmgIV);
			itemManager.AddBaseSkill(241, 300, absMinDmgV);

			CoreStatusChangeSkill percMinDmgI = new CoreStatusChangeSkill("+ #value% min Schaden", "Teufels", "des Schadens", SkillType.PercentageMinDamage, 1, 1, Quality.Magic, "2, 4, 5, 6", "description", ItemType.Weapon, ItemType.Schild);
			CoreStatusChangeSkill percMinDmgII = new CoreStatusChangeSkill("+ #value% min Schaden", "Teufels", "des Schadens", SkillType.PercentageMinDamage, 20, 2, Quality.Magic, "2, 4, 5, 6", "description", ItemType.Weapon, ItemType.Schild);
			CoreStatusChangeSkill percMinDmgIII = new CoreStatusChangeSkill("+ #value% min Schaden", "Teufels", "des Schadens", SkillType.PercentageMinDamage, 40, 3, Quality.Magic, "2, 4, 5, 6", "description", ItemType.Weapon, ItemType.Schild);
			CoreStatusChangeSkill percMinDmgIV = new CoreStatusChangeSkill("+ #value% min Schaden", "Teufels", "des Schadens", SkillType.PercentageMinDamage, 60, 4, Quality.Magic, "2, 4, 5, 6", "description", ItemType.Weapon, ItemType.Schild);
			CoreStatusChangeSkill percMinDmgV = new CoreStatusChangeSkill("+ #value% min Schaden", "Teufels", "des Schadens", SkillType.PercentageMinDamage, 80, 5, Quality.Rare, "2, 4, 5, 6", "description", ItemType.Weapon, ItemType.Schild);
			itemManager.AddBaseSkill(1, 16, percMinDmgI);
			itemManager.AddBaseSkill(17, 32, percMinDmgII);
			itemManager.AddBaseSkill(33, 48, percMinDmgIII);
			itemManager.AddBaseSkill(49, 64, percMinDmgIV);
			itemManager.AddBaseSkill(65, 80, percMinDmgV);

			//Max Dmg
			CoreStatusChangeSkill absMaxDmgI = new CoreStatusChangeSkill("+ #value Max Schaden", "Diablos", "des Schadens", SkillType.AbsoluteMaxDamage, 1, 1, Quality.Magic, "2, 4, 5, 6", "description", ItemType.Weapon, ItemType.Schild);
			CoreStatusChangeSkill absMaxDmgII = new CoreStatusChangeSkill("+ #value Max Schaden", "Diablos", "des Schadens", SkillType.AbsoluteMaxDamage, 20, 2, Quality.Magic, "2, 4, 5, 6", "description", ItemType.Weapon, ItemType.Schild);
			CoreStatusChangeSkill absMaxDmgIII = new CoreStatusChangeSkill("+ #value Max Schaden", "Diablos", "des Schadens", SkillType.AbsoluteMaxDamage, 40, 3, Quality.Magic, "2, 4, 5, 6", "description", ItemType.Weapon, ItemType.Schild);
			CoreStatusChangeSkill absMaxDmgIV = new CoreStatusChangeSkill("+ #value Max Schaden", "Diablos", "des Schadens", SkillType.AbsoluteMaxDamage, 60, 4, Quality.Magic, "2, 4, 5, 6", "description", ItemType.Weapon, ItemType.Schild);
			CoreStatusChangeSkill absMaxDmgV = new CoreStatusChangeSkill("+ #value Max Schaden", "Diablos", "des Schadens", SkillType.AbsoluteMaxDamage, 80, 5, Quality.Rare, "2, 4, 5, 6", "description", ItemType.Weapon, ItemType.Schild);
			itemManager.AddBaseSkill(1, 120, absMaxDmgI);
			itemManager.AddBaseSkill(121, 240, absMaxDmgII);
			itemManager.AddBaseSkill(241, 360, absMaxDmgIII);
			itemManager.AddBaseSkill(361, 480, absMaxDmgIV);
			itemManager.AddBaseSkill(481, 600, absMaxDmgV);

			CoreStatusChangeSkill percMaxDmgI = new CoreStatusChangeSkill("+ #value% Max Schaden", "Diablos", "des Schadens", SkillType.PercentageMaxDamage, 1, 1, Quality.Magic, "2, 4, 5, 6", "description", ItemType.Weapon, ItemType.Schild);
			CoreStatusChangeSkill percMaxDmgII = new CoreStatusChangeSkill("+ #value% Max Schaden", "Diablos", "des Schadens", SkillType.PercentageMaxDamage, 20, 2, Quality.Magic, "2, 4, 5, 6", "description", ItemType.Weapon, ItemType.Schild);
			CoreStatusChangeSkill percMaxDmgIII = new CoreStatusChangeSkill("+ #value% Max Schaden", "Diablos", "des Schadens", SkillType.PercentageMaxDamage, 40, 3, Quality.Magic, "2, 4, 5, 6", "description", ItemType.Weapon, ItemType.Schild);
			CoreStatusChangeSkill percMaxDmgIV = new CoreStatusChangeSkill("+ #value% Max Schaden", "Diablos", "des Schadens", SkillType.PercentageMaxDamage, 60, 4, Quality.Magic, "2, 4, 5, 6", "description", ItemType.Weapon, ItemType.Schild);
			CoreStatusChangeSkill percMaxDmgV = new CoreStatusChangeSkill("+ #value% Max Schaden", "Diablos", "des Schadens", SkillType.PercentageMaxDamage, 80, 5, Quality.Rare, "2, 4, 5, 6", "description", ItemType.Weapon, ItemType.Schild);
			itemManager.AddBaseSkill(1, 16, percMaxDmgI);
			itemManager.AddBaseSkill(17, 32, percMaxDmgII);
			itemManager.AddBaseSkill(33, 48, percMaxDmgIII);
			itemManager.AddBaseSkill(49, 64, percMaxDmgIV);
			itemManager.AddBaseSkill(65, 80, percMaxDmgV);

			//Weapondmg
			CoreStatusChangeSkill absWeaponDmgI = new CoreStatusChangeSkill("+ #value Waffenschaden", "Ritters", "des Schadens", SkillType.AbsoluteWeaponDamage, 1, 1, Quality.Magic, "0, 4, 5, 6", "description", ItemType.Weapon);
			CoreStatusChangeSkill absWeaponDmgII = new CoreStatusChangeSkill("+ #value Waffenschaden", "Ritters", "des Schadens", SkillType.AbsoluteWeaponDamage, 20, 2, Quality.Magic, "0, 4, 5, 6", "description", ItemType.Weapon);
			CoreStatusChangeSkill absWeaponDmgIII = new CoreStatusChangeSkill("+ #value Waffenschaden", "Ritters", "des Schadens", SkillType.AbsoluteWeaponDamage, 40, 3, Quality.Magic, "0, 4, 5, 6", "description", ItemType.Weapon);
			CoreStatusChangeSkill absWeaponDmgIV = new CoreStatusChangeSkill("+ #value Waffenschaden", "Ritters", "des Schadens", SkillType.AbsoluteWeaponDamage, 60, 4, Quality.Magic, "0, 4, 5, 6", "description", ItemType.Weapon);
			CoreStatusChangeSkill absWeaponDmgV = new CoreStatusChangeSkill("+ #value Waffenschaden", "Ritters", "des Schadens", SkillType.AbsoluteWeaponDamage, 80, 5, Quality.Rare, "0, 4, 5, 6", "description", ItemType.Weapon);
			itemManager.AddBaseSkill(1, 48, absWeaponDmgI);
			itemManager.AddBaseSkill(49, 97, absWeaponDmgII);
			itemManager.AddBaseSkill(98, 145, absWeaponDmgIII);
			itemManager.AddBaseSkill(146, 194, absWeaponDmgIV);
			itemManager.AddBaseSkill(195, 243, absWeaponDmgV);

			CoreStatusChangeSkill percWeaponDmgI = new CoreStatusChangeSkill("+ #value% Waffenschaden", "Ritters", "des Schadens", SkillType.PercentageWeaponDamage, 1, 1, Quality.Magic, "0, 4, 5, 6", "description", ItemType.Weapon);
			CoreStatusChangeSkill percWeaponDmgII = new CoreStatusChangeSkill("+ #value% Waffenschaden", "Ritters", "des Schadens", SkillType.PercentageWeaponDamage, 20, 2, Quality.Magic, "0, 4, 5, 6", "description", ItemType.Weapon);
			CoreStatusChangeSkill percWeaponDmgIII = new CoreStatusChangeSkill("+ #value% Waffenschaden", "Ritters", "des Schadens", SkillType.PercentageWeaponDamage, 40, 3, Quality.Magic, "0, 4, 5, 6", "description", ItemType.Weapon);
			CoreStatusChangeSkill percWeaponDmgIV = new CoreStatusChangeSkill("+ #value% Waffenschaden", "Ritters", "des Schadens", SkillType.PercentageWeaponDamage, 60, 4, Quality.Magic, "0, 4, 5, 6", "description", ItemType.Weapon);
			CoreStatusChangeSkill percWeaponDmgV = new CoreStatusChangeSkill("+ #value% Waffenschaden", "Ritters", "des Schadens", SkillType.PercentageWeaponDamage, 80, 5, Quality.Rare, "0, 4, 5, 6", "description", ItemType.Weapon);
			itemManager.AddBaseSkill(1, 36, percWeaponDmgI);
			itemManager.AddBaseSkill(37, 72, percWeaponDmgII);
			itemManager.AddBaseSkill(73, 108, percWeaponDmgIII);
			itemManager.AddBaseSkill(109, 144, percWeaponDmgIV);
			itemManager.AddBaseSkill(145, 180, percWeaponDmgV);

			//dmg
			CoreStatusChangeSkill absDmgI = new CoreStatusChangeSkill("+ #value Schaden", "Königs", "des Schadens", SkillType.AbsoluteDamage, 1, 1, Quality.Magic, "0, 4, 5, 6", "description", ItemType.Weapon, ItemType.Schild);
			CoreStatusChangeSkill absDmgII = new CoreStatusChangeSkill("+ #value Schaden", "Königs", "des Schadens", SkillType.AbsoluteDamage, 20, 2, Quality.Magic, "0, 4, 5, 6", "description", ItemType.Weapon, ItemType.Schild);
			CoreStatusChangeSkill absDmgIII = new CoreStatusChangeSkill("+ #value Schaden", "Königs", "des Schadens", SkillType.AbsoluteDamage, 40, 3, Quality.Magic, "0, 4, 5, 6", "description", ItemType.Weapon, ItemType.Schild);
			CoreStatusChangeSkill absDmgIV = new CoreStatusChangeSkill("+ #value Schaden", "Königs", "des Schadens", SkillType.AbsoluteDamage, 60, 4, Quality.Magic, "0, 4, 5, 6", "description", ItemType.Weapon, ItemType.Schild);
			CoreStatusChangeSkill absDmgV = new CoreStatusChangeSkill("+ #value Schaden", "Königs", "des Schadens", SkillType.AbsoluteDamage, 80, 5, Quality.Rare, "0, 4, 5, 6", "description", ItemType.Weapon, ItemType.Schild);
			itemManager.AddBaseSkill(1, 63, absDmgI);
			itemManager.AddBaseSkill(64, 127, absDmgII);
			itemManager.AddBaseSkill(128, 190, absDmgIII);
			itemManager.AddBaseSkill(191, 254, absDmgIV);
			itemManager.AddBaseSkill(255, 318, absDmgV);

			CoreStatusChangeSkill percDmgI = new CoreStatusChangeSkill("+ #value% Schaden", "Königs", "des Schadens", SkillType.PercentageDamage, 1, 1, Quality.Magic, "0, 4, 5, 6", "description", ItemType.Weapon, ItemType.Schild);
			CoreStatusChangeSkill percDmgII = new CoreStatusChangeSkill("+ #value% Schaden", "Königs", "des Schadens", SkillType.PercentageDamage, 20, 2, Quality.Magic, "0, 4, 5, 6", "description", ItemType.Weapon, ItemType.Schild);
			CoreStatusChangeSkill percDmgIII = new CoreStatusChangeSkill("+ #value% Schaden", "Königs", "des Schadens", SkillType.PercentageDamage, 40, 3, Quality.Magic, "0, 4, 5, 6", "description", ItemType.Weapon, ItemType.Schild);
			CoreStatusChangeSkill percDmgIV = new CoreStatusChangeSkill("+ #value% Schaden", "Königs", "des Schadens", SkillType.PercentageDamage, 60, 4, Quality.Magic, "0, 4, 5, 6", "description", ItemType.Weapon, ItemType.Schild);
			CoreStatusChangeSkill percDmgV = new CoreStatusChangeSkill("+ #value% Schaden", "Königs", "des Schadens", SkillType.PercentageDamage, 80, 5, Quality.Rare, "0, 4, 5, 6", "description", ItemType.Weapon, ItemType.Schild);
			itemManager.AddBaseSkill(1, 26, percDmgI);
			itemManager.AddBaseSkill(27, 52, percDmgII);
			itemManager.AddBaseSkill(53, 78, percDmgIII);
			itemManager.AddBaseSkill(79, 104, percDmgIV);
			itemManager.AddBaseSkill(105, 130, percDmgV);

			//Kritischer Schaden
			CoreStatusChangeSkill absCritDmgI = new CoreStatusChangeSkill("+ #value% Kritischerschaden", "Königs", "des Schadens", SkillType.AbsoluteCriticalDamage, 1, 1, Quality.Magic, "0, 1, 4, 5", "description", ItemType.Shoe, ItemType.Head, ItemType.Body);
			CoreStatusChangeSkill absCritDmgII = new CoreStatusChangeSkill("+ #value% Kritischerschaden", "Königs", "des Schadens", SkillType.AbsoluteCriticalDamage, 20, 2, Quality.Magic, "0, 1, 4, 5", "description", ItemType.Shoe, ItemType.Head, ItemType.Body);
			CoreStatusChangeSkill absCritDmgIII = new CoreStatusChangeSkill("+ #value% Kritischerschaden", "Königs", "des Schadens", SkillType.AbsoluteCriticalDamage, 40, 3, Quality.Magic, "0, 1, 4, 5", "description", ItemType.Shoe, ItemType.Head, ItemType.Body);
			CoreStatusChangeSkill absCritDmgIV = new CoreStatusChangeSkill("+ #value% Kritischerschaden", "Königs", "des Schadens", SkillType.AbsoluteCriticalDamage, 60, 4, Quality.Magic, "0, 1, 4, 5", "description", ItemType.Shoe, ItemType.Head, ItemType.Body);
			CoreStatusChangeSkill absCritDmgV = new CoreStatusChangeSkill("+ #value% Kritischerschaden", "Königs", "des Schadens", SkillType.AbsoluteCriticalDamage, 80, 5, Quality.Rare, "0, 1, 4, 5", "description", ItemType.Shoe, ItemType.Head, ItemType.Body);
			itemManager.AddBaseSkill(1, 63, absCritDmgI);
			itemManager.AddBaseSkill(64, 127, absCritDmgII);
			itemManager.AddBaseSkill(128, 190, absCritDmgIII);
			itemManager.AddBaseSkill(191, 254, absCritDmgIV);
			itemManager.AddBaseSkill(255, 318, absCritDmgV);

			//Battle-Skills//
			//Heal
			CoreBattleSkill absHealI = new CoreBattleSkill("5% Chance auf #value Heilung", "Heilers", "der Heilung", SkillType.AbsoluteHealInRound, 1, 1, Quality.Magic, "1, 4, 5, 6", "description", ItemType.Weapon, ItemType.Shoe, ItemType.Schild, ItemType.Head, ItemType.Body);
			CoreBattleSkill absHealII = new CoreBattleSkill("5% Chance auf #value Heilung", "Heilers", "der Heilung", SkillType.AbsoluteHealInRound, 20, 2, Quality.Magic, "1, 4, 5, 6", "description", ItemType.Weapon, ItemType.Shoe, ItemType.Schild, ItemType.Head, ItemType.Body);
			CoreBattleSkill absHealIII = new CoreBattleSkill("10% Chance auf #value Heilung", "Heilers", "der Heilung", SkillType.AbsoluteHealInRound, 40, 3, Quality.Magic, "1, 4, 5, 6", "description", ItemType.Weapon, ItemType.Shoe, ItemType.Schild, ItemType.Head, ItemType.Body);
			CoreBattleSkill absHealIV = new CoreBattleSkill("10% Chance auf #value Heilung", "Heilers", "der Heilung", SkillType.AbsoluteHealInRound, 60, 4, Quality.Magic, "1, 4, 5, 6", "description", ItemType.Weapon, ItemType.Shoe, ItemType.Schild, ItemType.Head, ItemType.Body);
			CoreBattleSkill absHealV = new CoreBattleSkill("15% Chance auf #value Heilung", "Heilers", "der Heilung", SkillType.AbsoluteHealInRound, 80, 5, Quality.Rare, "1, 4, 5, 6", "description", ItemType.Weapon, ItemType.Shoe, ItemType.Schild, ItemType.Head, ItemType.Body);
			itemManager.AddBaseSkill(5, 75, absHealI);
			itemManager.AddBaseSkill(125, 375, absHealII);
			itemManager.AddBaseSkill(75, 425, absHealIII);
			itemManager.AddBaseSkill(275, 675, absHealIV);
			itemManager.AddBaseSkill(575, 1540, absHealV);

			//Heal
			CoreBattleSkill percHealI = new CoreBattleSkill("5% Chance auf #value% Heilung", "Heilers", "der Heilung", SkillType.PercentageHealInRound, 1, 1, Quality.Rare, "1, 4, 5, 6", "description", ItemType.Weapon, ItemType.Shoe, ItemType.Schild, ItemType.Head, ItemType.Body);
			CoreBattleSkill percHealII = new CoreBattleSkill("5% Chance auf #value% Heilung", "Heilers", "der Heilung", SkillType.PercentageHealInRound, 20, 2, Quality.Rare, "1, 4, 5, 6", "description", ItemType.Weapon, ItemType.Shoe, ItemType.Schild, ItemType.Head, ItemType.Body);
			CoreBattleSkill percHealIII = new CoreBattleSkill("10% Chance auf #value% Heilung", "Heilers", "der Heilung", SkillType.PercentageHealInRound, 40, 3, Quality.Legendary, "1, 4, 5, 6", "description", ItemType.Weapon, ItemType.Shoe, ItemType.Schild, ItemType.Head, ItemType.Body);
			CoreBattleSkill percHealIV = new CoreBattleSkill("10% Chance auf #value% Heilung", "Heilers", "der Heilung", SkillType.PercentageHealInRound, 60, 4, Quality.Legendary, "1, 4, 5, 6", "description", ItemType.Weapon, ItemType.Shoe, ItemType.Schild, ItemType.Head, ItemType.Body);
			CoreBattleSkill percHealV = new CoreBattleSkill("15% Chance auf #value% Heilung", "Heilers", "der Heilung", SkillType.PercentageHealInRound, 80, 5, Quality.Legendary, "1, 4, 5, 6", "description", ItemType.Weapon, ItemType.Shoe, ItemType.Schild, ItemType.Head, ItemType.Body);
			itemManager.AddBaseSkill(1, 4, percHealI);
			itemManager.AddBaseSkill(5, 8, percHealII);
			itemManager.AddBaseSkill(9, 12, percHealIII);
			itemManager.AddBaseSkill(13, 16, percHealIV);
			itemManager.AddBaseSkill(17, 20, percHealV);

			//dmg
			CoreBattleSkill absRoundDmgI = new CoreBattleSkill("5% Chance auf #value mehr Schaden", "Teufels", "des Schadens", SkillType.AbsoluteDamageInRound, 1, 1, Quality.Rare, "0,1,2,3", "description", ItemType.Weapon, ItemType.Body);
			CoreBattleSkill absRoundDmgII = new CoreBattleSkill("5% Chance auf #value mehr Schaden", "Teufels", "des Schadens", SkillType.AbsoluteDamageInRound, 20, 2, Quality.Rare, "0,1,2,3", "description", ItemType.Weapon, ItemType.Body);
			CoreBattleSkill absRoundDmgIII = new CoreBattleSkill("5% Chance auf #value mehr Schaden", "Teufels", "des Schadens", SkillType.AbsoluteDamageInRound, 40, 3, Quality.Rare, "0,1,2,3", "description", ItemType.Weapon, ItemType.Body);
			CoreBattleSkill absRoundDmgIV = new CoreBattleSkill("10% Chance auf #value mehr Schaden", "Teufels", "des Schadens", SkillType.AbsoluteDamageInRound, 60, 4, Quality.Rare, "0,1,2,3", "description", ItemType.Weapon, ItemType.Body);
			CoreBattleSkill absRoundDmgV = new CoreBattleSkill("10% Chance auf #value mehr Schaden", "Teufels", "des Schadens", SkillType.AbsoluteDamageInRound, 80, 5, Quality.Rare, "0,1,2,3", "description", ItemType.Weapon, ItemType.Body);
			itemManager.AddBaseSkill(25, 175, absRoundDmgI);
			itemManager.AddBaseSkill(125, 425, absRoundDmgII);
			itemManager.AddBaseSkill(215, 625, absRoundDmgIII);
			itemManager.AddBaseSkill(125, 875, absRoundDmgIV);
			itemManager.AddBaseSkill(350, 1200, absRoundDmgV);

			//battleRound
			CoreBattleSkill absBattleRoundI = new CoreBattleSkill("5% Chance auf +#value Kampfrunden", "Orcs", "der Kampfeslust", SkillType.AbsoluteBattleRoundsInRound, 1, 1, Quality.Rare, "0, 1, 4, 5", "description", ItemType.Head, ItemType.Shoe);
			CoreBattleSkill absBattleRoundII = new CoreBattleSkill("10% Chance auf +#value Kampfrunden", "Orcs", "der Kampfeslust", SkillType.AbsoluteBattleRoundsInRound, 20, 2, Quality.Rare, "0, 1, 4, 5", "description", ItemType.Head, ItemType.Shoe);
			CoreBattleSkill absBattleRoundIII = new CoreBattleSkill("5% Chance auf +#value Kampfrunden", "Orcs", "der Kampfeslust", SkillType.AbsoluteBattleRoundsInRound, 40, 3, Quality.Rare, "0, 1, 4, 5", "description", ItemType.Head, ItemType.Shoe);
			CoreBattleSkill absBattleRoundIV = new CoreBattleSkill("5% Chance auf +#value Kampfrunden", "Orcs", "der Kampfeslust", SkillType.AbsoluteBattleRoundsInRound, 60, 4, Quality.Rare, "0, 1, 4, 5", "description", ItemType.Head, ItemType.Shoe);
			CoreBattleSkill absBattleRoundV = new CoreBattleSkill("10% Chance auf +#value Kampfrunden", "Orcs", "der Kampfeslust", SkillType.AbsoluteBattleRoundsInRound, 80, 5, Quality.Rare, "0, 1, 4, 5", "description", ItemType.Head, ItemType.Shoe);
			itemManager.AddBaseSkill(1, 2, absBattleRoundI);
			itemManager.AddBaseSkill(1, 2, absBattleRoundII);
			itemManager.AddBaseSkill(2, 4, absBattleRoundIII);
			itemManager.AddBaseSkill(3, 6, absBattleRoundIV);
			itemManager.AddBaseSkill(3, 6, absBattleRoundV);

			//battleTime
			CoreBattleSkill absBattleTimeI = new CoreBattleSkill("5% Chance auf +#valuemin Kampfzeit", "Orcs", "der Kampfeslust", SkillType.AbsoluteBattleRoundsInRound, 1, 1, Quality.Rare, "0, 1, 4, 5", "description", ItemType.Head, ItemType.Schild);
			CoreBattleSkill absBattleTimeII = new CoreBattleSkill("10% Chance auf +#valuemin Kampfzeit", "Orcs", "der Kampfeslust", SkillType.AbsoluteBattleRoundsInRound, 20, 2, Quality.Rare, "0, 1, 4, 5", "description", ItemType.Head, ItemType.Schild);
			CoreBattleSkill absBattleTimeIII = new CoreBattleSkill("5% Chance auf +#valuemin Kampfzeit", "Orcs", "der Kampfeslust", SkillType.AbsoluteBattleRoundsInRound, 40, 3, Quality.Rare, "0, 1, 4, 5", "description", ItemType.Head, ItemType.Schild);
			CoreBattleSkill absBattleTimeIV = new CoreBattleSkill("5% Chance auf +#valuemin Kampfzeit", "Orcs", "der Kampfeslust", SkillType.AbsoluteBattleRoundsInRound, 60, 4, Quality.Rare, "0, 1, 4, 5", "description", ItemType.Head, ItemType.Schild);
			CoreBattleSkill absBattleTimeV = new CoreBattleSkill("10% Chance auf +#valuemin Kampfzeit", "Orcs", "der Kampfeslust", SkillType.AbsoluteBattleRoundsInRound, 80, 5, Quality.Rare, "0, 1, 4, 5", "description", ItemType.Head, ItemType.Schild);
			itemManager.AddBaseSkill(1, 1, absBattleTimeI);
			itemManager.AddBaseSkill(1, 1, absBattleTimeII);
			itemManager.AddBaseSkill(1, 3, absBattleTimeIII);
			itemManager.AddBaseSkill(2, 4, absBattleTimeIV);
			itemManager.AddBaseSkill(2, 4, absBattleTimeV);

			//After-Battle-Skills//
			//No Death
			CoreAfterBattleSkill absNoDeathI = new CoreAfterBattleSkill("3% Chance auf Wiederbelegung", "Deaths", "des Glücklichen", SkillType.AbsoluteNoDeath, 1, 1, Quality.Rare, "0, 4, 5, 6", "description", ItemType.Body, ItemType.Head, ItemType.Schild, ItemType.Shoe);
			CoreAfterBattleSkill absNoDeathII = new CoreAfterBattleSkill("6% Chance auf Wiederbelegung", "Deaths", "des Glücklichen", SkillType.AbsoluteNoDeath, 20, 2, Quality.Rare, "0, 4, 5, 6", "description", ItemType.Body, ItemType.Head, ItemType.Schild, ItemType.Shoe);
			CoreAfterBattleSkill absNoDeathIII = new CoreAfterBattleSkill("9% Chance auf Wiederbelegung", "Deaths", "des Glücklichen", SkillType.AbsoluteNoDeath, 40, 3, Quality.Rare, "0, 4, 5, 6", "description", ItemType.Body, ItemType.Head, ItemType.Schild, ItemType.Shoe);
			CoreAfterBattleSkill absNoDeathIV = new CoreAfterBattleSkill("12% Chance auf Wiederbelegung", "Deaths", "des Glücklichen", SkillType.AbsoluteNoDeath, 60, 4, Quality.Rare, "0, 4, 5, 6", "description", ItemType.Body, ItemType.Head, ItemType.Schild, ItemType.Shoe);
			CoreAfterBattleSkill absNoDeathV = new CoreAfterBattleSkill("15% Chance auf Wiederbelegung", "Deaths", "des Glücklichen", SkillType.AbsoluteNoDeath, 80, 5, Quality.Rare, "0, 4, 5, 6", "description", ItemType.Body, ItemType.Head, ItemType.Schild, ItemType.Shoe);
			itemManager.AddBaseSkill(1, 1, absNoDeathI);
			itemManager.AddBaseSkill(1, 1, absNoDeathII);
			itemManager.AddBaseSkill(1, 1, absNoDeathIII);
			itemManager.AddBaseSkill(1, 1, absNoDeathIV);
			itemManager.AddBaseSkill(1, 1, absNoDeathV);

			//more gold
			CoreAfterBattleSkill absMoreGoldI = new CoreAfterBattleSkill("Basis-Gold-Factor +#value", "Zockers", "des Reichtums", SkillType.AbsoluteMoreGold, 1, 1, Quality.Magic, "0, 1, 2, 3", "description", ItemType.Body, ItemType.Head, ItemType.Schild, ItemType.Shoe, ItemType.Weapon);
			CoreAfterBattleSkill absMoreGoldII = new CoreAfterBattleSkill("Basis-Gold-Factor +#value", "Zockers", "des Reichtums", SkillType.AbsoluteMoreGold, 20, 2, Quality.Magic, "0, 1, 2, 3", "description", ItemType.Body, ItemType.Head, ItemType.Schild, ItemType.Shoe, ItemType.Weapon);
			CoreAfterBattleSkill absMoreGoldIII = new CoreAfterBattleSkill("Basis-Gold-Factor +#value", "Zockers", "des Reichtums", SkillType.AbsoluteMoreGold, 40, 3, Quality.Magic, "0, 1, 2, 3", "description", ItemType.Body, ItemType.Head, ItemType.Schild, ItemType.Shoe, ItemType.Weapon);
			CoreAfterBattleSkill absMoreGoldIV = new CoreAfterBattleSkill("Basis-Gold-Factor +#value", "Zockers", "des Reichtums", SkillType.AbsoluteMoreGold, 60, 4, Quality.Magic, "0, 1, 2, 3", "description", ItemType.Body, ItemType.Head, ItemType.Schild, ItemType.Shoe, ItemType.Weapon);
			CoreAfterBattleSkill absMoreGoldV = new CoreAfterBattleSkill("Basis-Gold-Factor +#value", "Zockers", "des Reichtums", SkillType.AbsoluteMoreGold, 80, 5, Quality.Rare, "0, 1, 2, 3", "description", ItemType.Body, ItemType.Head, ItemType.Schild, ItemType.Shoe, ItemType.Weapon);
			itemManager.AddBaseSkill(1, 2, absMoreGoldI);
			itemManager.AddBaseSkill(1, 3, absMoreGoldII);
			itemManager.AddBaseSkill(3, 5, absMoreGoldIII);
			itemManager.AddBaseSkill(3, 6, absMoreGoldIV);
			itemManager.AddBaseSkill(4, 7, absMoreGoldV);

			CoreAfterBattleSkill percMoreGoldI = new CoreAfterBattleSkill("Erhöht das Gold um #value%", "Zockers", "des Reichtums", SkillType.PercentageMoreGold, 1, 1, Quality.Magic, "0, 1, 2, 3", "description", ItemType.Body, ItemType.Head, ItemType.Schild, ItemType.Shoe, ItemType.Weapon);
			CoreAfterBattleSkill percMoreGoldII = new CoreAfterBattleSkill("Erhöht das Gold um #value%", "Zockers", "des Reichtums", SkillType.PercentageMoreGold, 20, 2, Quality.Magic, "0, 1, 2, 3", "description", ItemType.Body, ItemType.Head, ItemType.Schild, ItemType.Shoe, ItemType.Weapon);
			CoreAfterBattleSkill percMoreGoldIII = new CoreAfterBattleSkill("Erhöht das Gold um #value%", "Zockers", "des Reichtums", SkillType.PercentageMoreGold, 40, 3, Quality.Magic, "0, 1, 2, 3", "description", ItemType.Body, ItemType.Head, ItemType.Schild, ItemType.Shoe, ItemType.Weapon);
			CoreAfterBattleSkill percMoreGoldIV = new CoreAfterBattleSkill("Erhöht das Gold um #value%", "Zockers", "des Reichtums", SkillType.PercentageMoreGold, 60, 4, Quality.Magic, "0, 1, 2, 3", "description", ItemType.Body, ItemType.Head, ItemType.Schild, ItemType.Shoe, ItemType.Weapon);
			CoreAfterBattleSkill percMoreGoldV = new CoreAfterBattleSkill("Erhöht das Gold um #value%", "Zockers", "des Reichtums", SkillType.PercentageMoreGold, 80, 5, Quality.Rare, "0, 1, 2, 3", "description", ItemType.Body, ItemType.Head, ItemType.Schild, ItemType.Shoe, ItemType.Weapon);
			itemManager.AddBaseSkill(1, 30, percMoreGoldI);
			itemManager.AddBaseSkill(31, 60, percMoreGoldII);
			itemManager.AddBaseSkill(61, 90, percMoreGoldIII);
			itemManager.AddBaseSkill(91, 120, percMoreGoldIV);
			itemManager.AddBaseSkill(121, 150, percMoreGoldV);

			//more exp
			CoreAfterBattleSkill absMoreExpI = new CoreAfterBattleSkill("Basis-Exp-Factor +#value", "Einsteins", "des Levels", SkillType.AbsoluteMoreEXP, 1, 1, Quality.Rare, "0, 1, 4, 5", "description", ItemType.Weapon, ItemType.Schild, ItemType.Body);
			CoreAfterBattleSkill absMoreExpII = new CoreAfterBattleSkill("Basis-Exp-Factor +#value", "Einsteins", "des Levels", SkillType.AbsoluteMoreEXP, 20, 2, Quality.Rare, "0, 1, 4, 5", "description", ItemType.Weapon, ItemType.Schild, ItemType.Body);
			CoreAfterBattleSkill absMoreExpIII = new CoreAfterBattleSkill("Basis-Exp-Factor +#value", "Einsteins", "des Levels", SkillType.AbsoluteMoreEXP, 40, 3, Quality.Rare, "0, 1, 4, 5", "description", ItemType.Weapon, ItemType.Schild, ItemType.Body);
			CoreAfterBattleSkill absMoreExpIV = new CoreAfterBattleSkill("Basis-Exp-Factor +#value", "Einsteins", "des Levels", SkillType.AbsoluteMoreEXP, 60, 4, Quality.Rare, "0, 1, 4, 5", "description", ItemType.Weapon, ItemType.Schild, ItemType.Body);
			CoreAfterBattleSkill absMoreExpV = new CoreAfterBattleSkill("Basis-Exp-Factor +#value", "Einsteins", "des Levels", SkillType.AbsoluteMoreEXP, 80, 5, Quality.Rare, "0, 1, 4, 5", "description", ItemType.Weapon, ItemType.Schild, ItemType.Body);
			itemManager.AddBaseSkill(1, 10, absMoreExpI);
			itemManager.AddBaseSkill(11, 20, absMoreExpII);
			itemManager.AddBaseSkill(21, 30, absMoreExpIII);
			itemManager.AddBaseSkill(31, 40, absMoreExpIV);
			itemManager.AddBaseSkill(41, 50, absMoreExpV);

			CoreAfterBattleSkill percMoreExpI = new CoreAfterBattleSkill("Erhöht die Exp #value%", "Einsteins", "des Levels", SkillType.PercentageMoreEXP, 1, 1, Quality.Rare, "0, 1, 4, 5", "description", ItemType.Weapon, ItemType.Schild, ItemType.Body);
			CoreAfterBattleSkill percMoreExpII = new CoreAfterBattleSkill("Erhöht die Exp #value%", "Einsteins", "des Levels", SkillType.PercentageMoreEXP, 20, 2, Quality.Rare, "0, 1, 4, 5", "description", ItemType.Weapon, ItemType.Schild, ItemType.Body);
			CoreAfterBattleSkill percMoreExpIII = new CoreAfterBattleSkill("Erhöht die Exp #value%", "Einsteins", "des Levels", SkillType.PercentageMoreEXP, 40, 3, Quality.Rare, "0, 1, 4, 5", "description", ItemType.Weapon, ItemType.Schild, ItemType.Body);
			CoreAfterBattleSkill percMoreExpIV = new CoreAfterBattleSkill("Erhöht die Exp #value%", "Einsteins", "des Levels", SkillType.PercentageMoreEXP, 60, 4, Quality.Rare, "0, 1, 4, 5", "description", ItemType.Weapon, ItemType.Schild, ItemType.Body);
			CoreAfterBattleSkill percMoreExpV = new CoreAfterBattleSkill("Erhöht die Exp #value%", "Einsteins", "des Levels", SkillType.PercentageMoreEXP, 80, 5, Quality.Rare, "0, 1, 4, 5", "description", ItemType.Weapon, ItemType.Schild, ItemType.Body);
			itemManager.AddBaseSkill(1, 20, percMoreExpI);
			itemManager.AddBaseSkill(21, 40, percMoreExpII);
			itemManager.AddBaseSkill(41, 60, percMoreExpIII);
			itemManager.AddBaseSkill(61, 80, percMoreExpIV);
			itemManager.AddBaseSkill(81, 100, percMoreExpV);

			//PlayRoundSkills
			//Kampfrunden
			CorePlayRoundSkill absBattleRoundsI = new CorePlayRoundSkill("+#value Kampfrunden", "Kämpfers", "der Schlacht", SkillType.AbsoluteBattleRounds, 1, 1, Quality.Rare, "1, 2, 4, 5", "description", ItemType.Shoe, ItemType.Schild, ItemType.Head, ItemType.Body);
			CorePlayRoundSkill absBattleRoundsII = new CorePlayRoundSkill("+#value Kampfrunden", "Kämpfers", "der Schlacht", SkillType.AbsoluteBattleRounds, 20, 2, Quality.Rare, "1, 2, 4, 5", "description", ItemType.Shoe, ItemType.Schild, ItemType.Head, ItemType.Body);
			CorePlayRoundSkill absBattleRoundsIII = new CorePlayRoundSkill("+#value Kampfrunden", "Kämpfers", "der Schlacht", SkillType.AbsoluteBattleRounds, 40, 3, Quality.Rare, "1, 2, 4, 5", "description", ItemType.Shoe, ItemType.Schild, ItemType.Head, ItemType.Body);
			CorePlayRoundSkill absBattleRoundsIV = new CorePlayRoundSkill("+#value Kampfrunden", "Kämpfers", "der Schlacht", SkillType.AbsoluteBattleRounds, 60, 4, Quality.Rare, "1, 2, 4, 5", "description", ItemType.Shoe, ItemType.Schild, ItemType.Head, ItemType.Body);
			CorePlayRoundSkill absBattleRoundsV = new CorePlayRoundSkill("+#value Kampfrunden", "Kämpfers", "der Schlacht", SkillType.AbsoluteBattleRounds, 80, 5, Quality.Rare, "1, 2, 4, 5", "description", ItemType.Shoe, ItemType.Schild, ItemType.Head, ItemType.Body);
			itemManager.AddBaseSkill(1, 9, absBattleRoundsI);
			itemManager.AddBaseSkill(10, 18, absBattleRoundsII);
			itemManager.AddBaseSkill(19, 27, absBattleRoundsIII);
			itemManager.AddBaseSkill(26, 36, absBattleRoundsIV);
			itemManager.AddBaseSkill(35, 45, absBattleRoundsV);

			//Kampfzeit
			CorePlayRoundSkill abssBattleTimeI = new CorePlayRoundSkill("+#valuemin Kampfzeit", "Kämpfers", "der Schlacht", SkillType.AbsoluteBattleTime, 1, 1, Quality.Rare, "1, 4, 5, 6", "description", ItemType.Weapon, ItemType.Schild);
			CorePlayRoundSkill abssBattleTimeII = new CorePlayRoundSkill("+#valuemin Kampfzeit", "Kämpfers", "der Schlacht", SkillType.AbsoluteBattleTime, 20, 2, Quality.Rare, "1, 4, 5, 6", "description", ItemType.Weapon, ItemType.Schild);
			CorePlayRoundSkill abssBattleTimeIII = new CorePlayRoundSkill("+#valuemin Kampfzeit", "Kämpfers", "der Schlacht", SkillType.AbsoluteBattleTime, 40, 3, Quality.Rare, "1, 4, 5, 6", "description", ItemType.Weapon, ItemType.Schild);
			CorePlayRoundSkill abssBattleTimeIV = new CorePlayRoundSkill("+#valuemin Kampfzeit", "Kämpfers", "der Schlacht", SkillType.AbsoluteBattleTime, 60, 4, Quality.Rare, "1, 4, 5, 6", "description", ItemType.Weapon, ItemType.Schild);
			CorePlayRoundSkill abssBattleTimeV = new CorePlayRoundSkill("+#valuemin Kampfzeit", "Kämpfers", "der Schlacht", SkillType.AbsoluteBattleTime, 80, 5, Quality.Rare, "1, 4, 5, 6", "description", ItemType.Weapon, ItemType.Schild);
			itemManager.AddBaseSkill(1, 4, abssBattleTimeI);
			itemManager.AddBaseSkill(5, 8, abssBattleTimeII);
			itemManager.AddBaseSkill(9, 12, abssBattleTimeIII);
			itemManager.AddBaseSkill(13, 16, abssBattleTimeIV);
			itemManager.AddBaseSkill(17, 20, abssBattleTimeV);

			//Faster Walk
			CorePlayRoundSkill fasterWalkI = new CorePlayRoundSkill("+#value% schneller Laufen", "Hermes", "des Läufers", SkillType.PercentageFasterWalk, 1, 1, Quality.Rare, "0, 4, 5, 6", "description", ItemType.Body, ItemType.Head, ItemType.Schild, ItemType.Shoe, ItemType.Weapon);
			CorePlayRoundSkill fasterWalkII = new CorePlayRoundSkill("+#value% schneller Laufen", "Hermes", "des Läufers", SkillType.PercentageFasterWalk, 20, 2, Quality.Rare, "0, 4, 5, 6", "description", ItemType.Body, ItemType.Head, ItemType.Schild, ItemType.Shoe, ItemType.Weapon);
			CorePlayRoundSkill fasterWalkIII = new CorePlayRoundSkill("+#value% schneller Laufen", "Hermes", "des Läufers", SkillType.PercentageFasterWalk, 40, 3, Quality.Rare, "0, 4, 5, 6", "description", ItemType.Body, ItemType.Head, ItemType.Schild, ItemType.Shoe, ItemType.Weapon);
			CorePlayRoundSkill fasterWalkIV = new CorePlayRoundSkill("+#value% schneller Laufen", "Hermes", "des Läufers", SkillType.PercentageFasterWalk, 60, 4, Quality.Rare, "0, 4, 5, 6", "description", ItemType.Body, ItemType.Head, ItemType.Schild, ItemType.Shoe, ItemType.Weapon);
			CorePlayRoundSkill fasterWalkV = new CorePlayRoundSkill("+#value% schneller Laufen", "Hermes", "des Läufers", SkillType.PercentageFasterWalk, 80, 5, Quality.Rare, "0, 4, 5, 6", "description", ItemType.Body, ItemType.Head, ItemType.Schild, ItemType.Shoe, ItemType.Weapon);
			itemManager.AddBaseSkill(1, 20, fasterWalkI);
			itemManager.AddBaseSkill(21, 40, fasterWalkII);
			itemManager.AddBaseSkill(41, 60, fasterWalkIII);
			itemManager.AddBaseSkill(61, 80, fasterWalkIV);
			itemManager.AddBaseSkill(81, 100, fasterWalkV);

			//Slower Walk
			CorePlayRoundSkill slowerWalkI = new CorePlayRoundSkill("+#value% langsamer Laufen", "Schildis", "der Schnecke", SkillType.PercentageSlowerWalk, 1, 1, Quality.Rare, "2, 4, 5, 6", "description", ItemType.Body, ItemType.Shoe, ItemType.Schild);
			CorePlayRoundSkill slowerWalkII = new CorePlayRoundSkill("+#value% langsamer Laufen", "Schildis", "der Schnecke", SkillType.PercentageSlowerWalk, 20, 2, Quality.Rare, "2, 4, 5, 6", "description", ItemType.Body, ItemType.Shoe, ItemType.Schild);
			CorePlayRoundSkill slowerWalkIII = new CorePlayRoundSkill("+#value% langsamer Laufen", "Schildis", "der Schnecke", SkillType.PercentageSlowerWalk, 40, 3, Quality.Rare, "2, 4, 5, 6", "description", ItemType.Body, ItemType.Shoe, ItemType.Schild);
			CorePlayRoundSkill slowerWalkIV = new CorePlayRoundSkill("+#value% langsamer Laufen", "Schildis", "der Schnecke", SkillType.PercentageSlowerWalk, 60, 4, Quality.Rare, "2, 4, 5, 6", "description", ItemType.Body, ItemType.Shoe, ItemType.Schild);
			CorePlayRoundSkill slowerWalkV = new CorePlayRoundSkill("+#value% langsamer Laufen", "Schildis", "der Schnecke", SkillType.PercentageSlowerWalk, 80, 5, Quality.Rare, "2, 4, 5, 6", "description", ItemType.Body, ItemType.Shoe, ItemType.Schild);
			itemManager.AddBaseSkill(1, 10, slowerWalkI);
			itemManager.AddBaseSkill(11, 20, slowerWalkII);
			itemManager.AddBaseSkill(21, 30, slowerWalkIII);
			itemManager.AddBaseSkill(31, 40, slowerWalkIV);
			itemManager.AddBaseSkill(41, 50, slowerWalkV);

			//faster fights
			CorePlayRoundSkill fasterFightsI = new CorePlayRoundSkill("+#value% häufigere Kämpfe", "Masters", "des Kampfes", SkillType.PercentageFastMonsterfights, 1, 1, Quality.Rare, "1, 4, 5, 6", "description", ItemType.Body, ItemType.Shoe, ItemType.Schild);
			CorePlayRoundSkill fasterFightsII = new CorePlayRoundSkill("+#value% häufigere Kämpfe", "Masters", "des Kampfes", SkillType.PercentageFastMonsterfights, 20, 2, Quality.Rare, "1, 4, 5, 6", "description", ItemType.Body, ItemType.Shoe, ItemType.Schild);
			CorePlayRoundSkill fasterFightsIII = new CorePlayRoundSkill("+#value% häufigere Kämpfe", "Masters", "des Kampfes", SkillType.PercentageFastMonsterfights, 40, 3, Quality.Rare, "1, 4, 5, 6", "description", ItemType.Body, ItemType.Shoe, ItemType.Schild);
			CorePlayRoundSkill fasterFightsIV = new CorePlayRoundSkill("+#value% häufigere Kämpfe", "Masters", "des Kampfes", SkillType.PercentageFastMonsterfights, 60, 4, Quality.Rare, "1, 4, 5, 6", "description", ItemType.Body, ItemType.Shoe, ItemType.Schild);
			CorePlayRoundSkill fasterFightsV = new CorePlayRoundSkill("+#value% häufigere Kämpfe", "Masters", "des Kampfes", SkillType.PercentageFastMonsterfights, 80, 5, Quality.Rare, "1, 4, 5, 6", "description", ItemType.Body, ItemType.Shoe, ItemType.Schild);
			itemManager.AddBaseSkill(1, 15, fasterFightsI);
			itemManager.AddBaseSkill(16, 30, fasterFightsII);
			itemManager.AddBaseSkill(31, 45, fasterFightsIII);
			itemManager.AddBaseSkill(46, 60, fasterFightsIV);
			itemManager.AddBaseSkill(61, 75, fasterFightsV);

			//slower fights
			CorePlayRoundSkill slowerFightsI = new CorePlayRoundSkill("+#value% seltenere Kämpfe", "Masters", "der Feigheit", SkillType.PercentageSlowMonsterfights, 1, 1, Quality.Rare, "0, 4, 5, 6", "description", ItemType.Body, ItemType.Head, ItemType.Schild, ItemType.Shoe, ItemType.Weapon);
			CorePlayRoundSkill slowerFightsII = new CorePlayRoundSkill("+#value% seltenere Kämpfe", "Masters", "der Feigheit", SkillType.PercentageSlowMonsterfights, 20, 2, Quality.Rare, "0, 4, 5, 6", "description", ItemType.Body, ItemType.Head, ItemType.Schild, ItemType.Shoe, ItemType.Weapon);
			CorePlayRoundSkill slowerFightsIII = new CorePlayRoundSkill("+#value% seltenere Kämpfe", "Masters", "der Feigheit", SkillType.PercentageSlowMonsterfights, 40, 3, Quality.Rare, "0, 4, 5, 6", "description", ItemType.Body, ItemType.Head, ItemType.Schild, ItemType.Shoe, ItemType.Weapon);
			CorePlayRoundSkill slowerFightsIV = new CorePlayRoundSkill("+#value% seltenere Kämpfe", "Masters", "der Feigheit", SkillType.PercentageSlowMonsterfights, 60, 4, Quality.Rare, "0, 4, 5, 6", "description", ItemType.Body, ItemType.Head, ItemType.Schild, ItemType.Shoe, ItemType.Weapon);
			CorePlayRoundSkill slowerFightsV = new CorePlayRoundSkill("+#value% seltenere Kämpfe", "Masters", "der Feigheit", SkillType.PercentageSlowMonsterfights, 80, 5, Quality.Rare, "0, 4, 5, 6", "description", ItemType.Body, ItemType.Head, ItemType.Schild, ItemType.Shoe, ItemType.Weapon);
			itemManager.AddBaseSkill(1, 15, slowerFightsI);
			itemManager.AddBaseSkill(16, 30, slowerFightsII);
			itemManager.AddBaseSkill(31, 45, slowerFightsIII);
			itemManager.AddBaseSkill(46, 60, slowerFightsIV);
			itemManager.AddBaseSkill(61, 75, slowerFightsV);

			itemManager.BaseSkills.ToList().ForEach(s => ((Skill)s.Item3).Description = "Skill der Stufe " + ((Skill)s.Item3).Level.ToString() + " im Bereich von " + s.Item1.ToString() + " bis " + s.Item2.ToString() + ".");

			Random rnd = new Random();
			//ändern der Farben der Skills, und zwar bekommt jeder Gruppe die gleiche zufällige Farbe
			itemManager.BaseSkills.GroupBy(s => ((Skill)s.Item3).Type).Select(s => new { baseSkillList = s.ToList(), newColor = new Color(rnd.Next(0, 255), rnd.Next(0, 255), rnd.Next(0, 255)) }).AsParallel().ForAll(t => t.baseSkillList.ForEach(b => ((Skill)b.Item3).ChangeStoneRenderingColor(t.newColor)));
		}

		private void CreateBaseItems(ItemManager itemManager) {
			itemManager.AddBaseItem(new CoreWeapon(1, 4, "Kurzschwert", 1, Quality.Normal, Content.Load<Texture2D>(@"Items\Weapon\Weapon.png"), @"Items\Weapon\Weapon.png", 1000));
			itemManager.AddBaseItem(new CoreWeapon(2, 6, "Langschwert", 5, Quality.Normal, Content.Load<Texture2D>(@"Items\Weapon\Weapon.png"), @"Items\Weapon\Weapon.png", 8800));
			itemManager.AddBaseItem(new CoreWeapon(1, 10, "Säbel", 11, Quality.Normal, Content.Load<Texture2D>(@"Items\Weapon\Weapon.png"), @"Items\Weapon\Weapon.png", 42000));
			itemManager.AddBaseItem(new CoreWeapon(4, 16, "Breitschwert", 17, Quality.Normal, Content.Load<Texture2D>(@"Items\Weapon\Weapon.png"), @"Items\Weapon\Weapon.png", 140000));
			itemManager.AddBaseItem(new CoreWeapon(8, 24, "Falchion", 24, Quality.Normal, Content.Load<Texture2D>(@"Items\Weapon\Weapon.png"), @"Items\Weapon\Weapon.png", 375000));
			itemManager.AddBaseItem(new CoreWeapon(12, 32, "Kriegsschwert", 30, Quality.Normal, Content.Load<Texture2D>(@"Items\Weapon\Weapon.png"), @"Items\Weapon\Weapon.png", 1000000));
			itemManager.AddBaseItem(new CoreWeapon(15, 40, "Bastardschwert", 36, Quality.Normal, Content.Load<Texture2D>(@"Items\Weapon\Weapon.png"), @"Items\Weapon\Weapon.png", 2500000));
			itemManager.AddBaseItem(new CoreWeapon(19, 56, "Gladius", 42, Quality.Normal, Content.Load<Texture2D>(@"Items\Weapon\Weapon.png"), @"Items\Weapon\Weapon.png", 5000000));
			itemManager.AddBaseItem(new CoreWeapon(21, 63, "Machete", 48, Quality.Normal, Content.Load<Texture2D>(@"Items\Weapon\Weapon.png"), @"Items\Weapon\Weapon.png", 9200000));
			itemManager.AddBaseItem(new CoreWeapon(26, 71, "Kampfschwert", 55, Quality.Normal, Content.Load<Texture2D>(@"Items\Weapon\Weapon.png"), @"Items\Weapon\Weapon.png", 15000000));
			itemManager.AddBaseItem(new CoreWeapon(30, 80, "Runenschwert", 61, Quality.Normal, Content.Load<Texture2D>(@"Items\Weapon\Weapon.png"), @"Items\Weapon\Weapon.png", 25000000));
			itemManager.AddBaseItem(new CoreWeapon(36, 87, "Saif", 67, Quality.Normal, Content.Load<Texture2D>(@"Items\Weapon\Weapon.png"), @"Items\Weapon\Weapon.png", 64000000));
			itemManager.AddBaseItem(new CoreWeapon(40, 92, "Tulwar", 73, Quality.Normal, Content.Load<Texture2D>(@"Items\Weapon\Weapon.png"), @"Items\Weapon\Weapon.png", 166000000));
			itemManager.AddBaseItem(new CoreWeapon(46, 102, "Königsklinge", 78, Quality.Normal, Content.Load<Texture2D>(@"Items\Weapon\Weapon.png"), @"Items\Weapon\Weapon.png", 362000000));
			itemManager.AddBaseItem(new CoreWeapon(53, 120, "Meisterschwert", 85, Quality.Normal, Content.Load<Texture2D>(@"Items\Weapon\Weapon.png"), @"Items\Weapon\Weapon.png", 500000000));

			itemManager.AddBaseItem(new CoreArmor(5, 11, "Leichte Rüstung", 1, Quality.Normal, ItemType.Body, Content.Load<Texture2D>(@"Items\Body\Body.png"), @"Items\Body\Body.png", 1000));
			itemManager.AddBaseItem(new CoreArmor(11, 17, "Lederrüstung", 5, Quality.Normal, ItemType.Body, Content.Load<Texture2D>(@"Items\Body\Body.png"), @"Items\Body\Body.png", 8800));
			itemManager.AddBaseItem(new CoreArmor(21, 24, "gehärtetes Leder", 11, Quality.Normal, ItemType.Body, Content.Load<Texture2D>(@"Items\Body\Body.png"), @"Items\Body\Body.png", 42000));
			itemManager.AddBaseItem(new CoreArmor(28, 35, "beschlagenes Leder", 17, Quality.Normal, ItemType.Body, Content.Load<Texture2D>(@"Items\Body\Body.png"), @"Items\Body\Body.png", 140000));
			itemManager.AddBaseItem(new CoreArmor(39, 48, "Ringpanzer", 24, Quality.Normal, ItemType.Body, Content.Load<Texture2D>(@"Items\Body\Body.png"), @"Items\Body\Body.png", 375000));
			itemManager.AddBaseItem(new CoreArmor(50, 60, "Schuppenpanzer", 30, Quality.Normal, ItemType.Body, Content.Load<Texture2D>(@"Items\Body\Body.png"), @"Items\Body\Body.png", 1000000));
			itemManager.AddBaseItem(new CoreArmor(62, 75, "Kettenpanzer", 36, Quality.Normal, ItemType.Body, Content.Load<Texture2D>(@"Items\Body\Body.png"), @"Items\Body\Body.png", 2500000));
			itemManager.AddBaseItem(new CoreArmor(71, 81, "Brustpanzer", 42, Quality.Normal, ItemType.Body, Content.Load<Texture2D>(@"Items\Body\Body.png"), @"Items\Body\Body.png", 5000000));
			itemManager.AddBaseItem(new CoreArmor(81, 95, "Bänderrüstung", 48, Quality.Normal, ItemType.Body, Content.Load<Texture2D>(@"Items\Body\Body.png"), @"Items\Body\Body.png", 9200000));
			itemManager.AddBaseItem(new CoreArmor(92, 105, "Plattenharnisch", 55, Quality.Normal, ItemType.Body, Content.Load<Texture2D>(@"Items\Body\Body.png"), @"Items\Body\Body.png", 15000000));
			itemManager.AddBaseItem(new CoreArmor(103, 116, "Feldharnisch", 61, Quality.Normal, ItemType.Body, Content.Load<Texture2D>(@"Items\Body\Body.png"), @"Items\Body\Body.png", 25000000));
			itemManager.AddBaseItem(new CoreArmor(121, 135, "Prunkharnisch", 67, Quality.Normal, ItemType.Body, Content.Load<Texture2D>(@"Items\Body\Body.png"), @"Items\Body\Body.png", 64000000));
			itemManager.AddBaseItem(new CoreArmor(141, 161, "Vollharnisch", 73, Quality.Normal, ItemType.Body, Content.Load<Texture2D>(@"Items\Body\Body.png"), @"Items\Body\Body.png", 166000000));
			itemManager.AddBaseItem(new CoreArmor(173, 201, "leichte Plattenrüstung", 78, Quality.Normal, ItemType.Body, Content.Load<Texture2D>(@"Items\Body\Body.png"), @"Items\Body\Body.png", 362000000));
			itemManager.AddBaseItem(new CoreArmor(201, 233, "schwere Plattenrüstung", 85, Quality.Normal, ItemType.Body, Content.Load<Texture2D>(@"Items\Body\Body.png"), @"Items\Body\Body.png", 500000000));

			itemManager.AddBaseItem(new CoreArmor(3, 7, "Lederkapuze", 1, Quality.Normal, ItemType.Head, Content.Load<Texture2D>(@"Items\Helm\Helm.png"), @"Items\Helm\Helm.png", 1000));
			itemManager.AddBaseItem(new CoreArmor(7, 11, "Haube", 5, Quality.Normal, ItemType.Head, Content.Load<Texture2D>(@"Items\Helm\Helm.png"), @"Items\Helm\Helm.png", 8800));
			itemManager.AddBaseItem(new CoreArmor(13, 15, "Kriegshaube", 11, Quality.Normal, ItemType.Head, Content.Load<Texture2D>(@"Items\Helm\Helm.png"), @"Items\Helm\Helm.png", 42000));
			itemManager.AddBaseItem(new CoreArmor(18, 22, "Helm", 17, Quality.Normal, ItemType.Head, Content.Load<Texture2D>(@"Items\Helm\Helm.png"), @"Items\Helm\Helm.png", 140000));
			itemManager.AddBaseItem(new CoreArmor(25, 31, "Plattenhelm", 24, Quality.Normal, ItemType.Head, Content.Load<Texture2D>(@"Items\Helm\Helm.png"), @"Items\Helm\Helm.png", 375000));
			itemManager.AddBaseItem(new CoreArmor(32, 39, "Krone", 30, Quality.Normal, ItemType.Head, Content.Load<Texture2D>(@"Items\Helm\Helm.png"), @"Items\Helm\Helm.png", 1000000));
			itemManager.AddBaseItem(new CoreArmor(40, 48, "Basinet", 36, Quality.Normal, ItemType.Head, Content.Load<Texture2D>(@"Items\Helm\Helm.png"), @"Items\Helm\Helm.png", 2500000));
			itemManager.AddBaseItem(new CoreArmor(46, 52, "Klappvisier", 42, Quality.Normal, ItemType.Head, Content.Load<Texture2D>(@"Items\Helm\Helm.png"), @"Items\Helm\Helm.png", 5000000));
			itemManager.AddBaseItem(new CoreArmor(52, 61, "Kaskett", 48, Quality.Normal, ItemType.Head, Content.Load<Texture2D>(@"Items\Helm\Helm.png"), @"Items\Helm\Helm.png", 9200000));
			itemManager.AddBaseItem(new CoreArmor(59, 68, "Großhelm", 55, Quality.Normal, ItemType.Head, Content.Load<Texture2D>(@"Items\Helm\Helm.png"), @"Items\Helm\Helm.png", 15000000));
			itemManager.AddBaseItem(new CoreArmor(66, 75, "Hundsgugel", 61, Quality.Normal, ItemType.Head, Content.Load<Texture2D>(@"Items\Helm\Helm.png"), @"Items\Helm\Helm.png", 25000000));
			itemManager.AddBaseItem(new CoreArmor(78, 87, "Stechhelm", 67, Quality.Normal, ItemType.Head, Content.Load<Texture2D>(@"Items\Helm\Helm.png"), @"Items\Helm\Helm.png", 64000000));
			itemManager.AddBaseItem(new CoreArmor(91, 104, "Zischägge", 73, Quality.Normal, ItemType.Head, Content.Load<Texture2D>(@"Items\Helm\Helm.png"), @"Items\Helm\Helm.png", 166000000));
			itemManager.AddBaseItem(new CoreArmor(112, 130, "Höllenmaske", 78, Quality.Normal, ItemType.Head, Content.Load<Texture2D>(@"Items\Helm\Helm.png"), @"Items\Helm\Helm.png", 362000000));
			itemManager.AddBaseItem(new CoreArmor(130, 151, "Archonkrone", 85, Quality.Normal, ItemType.Head, Content.Load<Texture2D>(@"Items\Helm\Helm.png"), @"Items\Helm\Helm.png", 500000000));

			itemManager.AddBaseItem(new CoreArmor(2, 6, "Schuhe", 1, Quality.Normal, ItemType.Shoe, Content.Load<Texture2D>(@"Items\Shoes\Shoes.png"), @"Items\Shoes\Shoes.png", 1000));
			itemManager.AddBaseItem(new CoreArmor(6, 9, "Stiefel", 5, Quality.Normal, ItemType.Shoe, Content.Load<Texture2D>(@"Items\Shoes\Shoes.png"), @"Items\Shoes\Shoes.png", 8800));
			itemManager.AddBaseItem(new CoreArmor(11, 13, "schwere Stiefel", 11, Quality.Normal, ItemType.Shoe, Content.Load<Texture2D>(@"Items\Shoes\Shoes.png"), @"Items\Shoes\Shoes.png", 42000));
			itemManager.AddBaseItem(new CoreArmor(15, 19, "Kettenstiefel", 17, Quality.Normal, ItemType.Shoe, Content.Load<Texture2D>(@"Items\Shoes\Shoes.png"), @"Items\Shoes\Shoes.png", 140000));
			itemManager.AddBaseItem(new CoreArmor(21, 26, "Sabatons", 24, Quality.Normal, ItemType.Shoe, Content.Load<Texture2D>(@"Items\Shoes\Shoes.png"), @"Items\Shoes\Shoes.png", 375000));
			itemManager.AddBaseItem(new CoreArmor(27, 33, "Beinschienen", 30, Quality.Normal, ItemType.Shoe, Content.Load<Texture2D>(@"Items\Shoes\Shoes.png"), @"Items\Shoes\Shoes.png", 1000000));
			itemManager.AddBaseItem(new CoreArmor(34, 41, "Seidenschuhe", 36, Quality.Normal, ItemType.Shoe, Content.Load<Texture2D>(@"Items\Shoes\Shoes.png"), @"Items\Shoes\Shoes.png", 2500000));
			itemManager.AddBaseItem(new CoreArmor(39, 44, "Soldatenstiefel", 42, Quality.Normal, ItemType.Shoe, Content.Load<Texture2D>(@"Items\Shoes\Shoes.png"), @"Items\Shoes\Shoes.png", 5000000));
			itemManager.AddBaseItem(new CoreArmor(44, 52, "Treter", 48, Quality.Normal, ItemType.Shoe, Content.Load<Texture2D>(@"Items\Shoes\Shoes.png"), @"Items\Shoes\Shoes.png", 9200000));
			itemManager.AddBaseItem(new CoreArmor(50, 57, "schwere Sabatons", 55, Quality.Normal, ItemType.Shoe, Content.Load<Texture2D>(@"Items\Shoes\Shoes.png"), @"Items\Shoes\Shoes.png", 15000000));
			itemManager.AddBaseItem(new CoreArmor(56, 63, "Schlachtbeinschienen", 61, Quality.Normal, ItemType.Shoe, Content.Load<Texture2D>(@"Items\Shoes\Shoes.png"), @"Items\Shoes\Shoes.png", 25000000));
			itemManager.AddBaseItem(new CoreArmor(66, 74, "schwere Treter", 67, Quality.Normal, ItemType.Shoe, Content.Load<Texture2D>(@"Items\Shoes\Shoes.png"), @"Items\Shoes\Shoes.png", 64000000));
			itemManager.AddBaseItem(new CoreArmor(77, 88, "gepanzerte Kettenstiefel", 73, Quality.Normal, ItemType.Shoe, Content.Load<Texture2D>(@"Items\Shoes\Shoes.png"), @"Items\Shoes\Shoes.png", 166000000));
			itemManager.AddBaseItem(new CoreArmor(95, 110, "schwere Soldatenstiefel", 78, Quality.Normal, ItemType.Shoe, Content.Load<Texture2D>(@"Items\Shoes\Shoes.png"), @"Items\Shoes\Shoes.png", 362000000));
			itemManager.AddBaseItem(new CoreArmor(110, 128, "schwere Schlachtbeinschienen", 85, Quality.Normal, ItemType.Shoe, Content.Load<Texture2D>(@"Items\Shoes\Shoes.png"), @"Items\Shoes\Shoes.png", 500000000));

			itemManager.AddBaseItem(new CoreArmor(1, 3, "Schild 1", 1, Quality.Normal, ItemType.Schild, Content.Load<Texture2D>(@"Items\Schild\Schild.png"), @"Items\Schild\Schild.png", 1000));
			itemManager.AddBaseItem(new CoreArmor(3, 5, "Schild 2", 5, Quality.Normal, ItemType.Schild, Content.Load<Texture2D>(@"Items\Schild\Schild.png"), @"Items\Schild\Schild.png", 8800));
			itemManager.AddBaseItem(new CoreArmor(7, 8, "Schild 3", 11, Quality.Normal, ItemType.Schild, Content.Load<Texture2D>(@"Items\Schild\Schild.png"), @"Items\Schild\Schild.png", 42000));
			itemManager.AddBaseItem(new CoreArmor(9, 12, "Schild 4", 17, Quality.Normal, ItemType.Schild, Content.Load<Texture2D>(@"Items\Schild\Schild.png"), @"Items\Schild\Schild.png", 140000));
			itemManager.AddBaseItem(new CoreArmor(13, 16, "Schild 5", 24, Quality.Normal, ItemType.Schild, Content.Load<Texture2D>(@"Items\Schild\Schild.png"), @"Items\Schild\Schild.png", 375000));
			itemManager.AddBaseItem(new CoreArmor(17, 21, "Schild 6", 30, Quality.Normal, ItemType.Schild, Content.Load<Texture2D>(@"Items\Schild\Schild.png"), @"Items\Schild\Schild.png", 1000000));
			itemManager.AddBaseItem(new CoreArmor(21, 26, "Schild 7", 36, Quality.Normal, ItemType.Schild, Content.Load<Texture2D>(@"Items\Schild\Schild.png"), @"Items\Schild\Schild.png", 2500000));
			itemManager.AddBaseItem(new CoreArmor(24, 28, "Schild 8", 42, Quality.Normal, ItemType.Schild, Content.Load<Texture2D>(@"Items\Schild\Schild.png"), @"Items\Schild\Schild.png", 5000000));
			itemManager.AddBaseItem(new CoreArmor(28, 33, "Schild 9", 48, Quality.Normal, ItemType.Schild, Content.Load<Texture2D>(@"Items\Schild\Schild.png"), @"Items\Schild\Schild.png", 9200000));
			itemManager.AddBaseItem(new CoreArmor(32, 36, "Schild 10", 55, Quality.Normal, ItemType.Schild, Content.Load<Texture2D>(@"Items\Schild\Schild.png"), @"Items\Schild\Schild.png", 15000000));
			itemManager.AddBaseItem(new CoreArmor(36, 40, "Schild 11", 61, Quality.Normal, ItemType.Schild, Content.Load<Texture2D>(@"Items\Schild\Schild.png"), @"Items\Schild\Schild.png", 25000000));
			itemManager.AddBaseItem(new CoreArmor(42, 47, "Schild 12", 67, Quality.Normal, ItemType.Schild, Content.Load<Texture2D>(@"Items\Schild\Schild.png"), @"Items\Schild\Schild.png", 64000000));
			itemManager.AddBaseItem(new CoreArmor(49, 56, "Schild 13", 73, Quality.Normal, ItemType.Schild, Content.Load<Texture2D>(@"Items\Schild\Schild.png"), @"Items\Schild\Schild.png", 166000000));
			itemManager.AddBaseItem(new CoreArmor(60, 70, "Schild 14", 78, Quality.Normal, ItemType.Schild, Content.Load<Texture2D>(@"Items\Schild\Schild.png"), @"Items\Schild\Schild.png", 362000000));
			itemManager.AddBaseItem(new CoreArmor(70, 81, "Schild 15", 85, Quality.Normal, ItemType.Schild, Content.Load<Texture2D>(@"Items\Schild\Schild.png"), @"Items\Schild\Schild.png", 500000000));
		}

		private Core.BattleSystem.EXPTable CreateCoreExpTable() {
			Core.BattleSystem.EXPTable expTable = new Core.BattleSystem.EXPTable();
			expTable.AddLevelUpStat(0, 0, 0);
			expTable.AddLevelUpStat(1, 1000, 1000);
			expTable.AddLevelUpStat(2, 2000, 1100);
			expTable.AddLevelUpStat(3, 3100, 1105);
			expTable.AddLevelUpStat(4, 4205, 1105);
			expTable.AddLevelUpStat(5, 5310, 1105);
			expTable.AddLevelUpStat(6, 6415, 1105);
			expTable.AddLevelUpStat(7, 7520, 1105);
			expTable.AddLevelUpStat(8, 8625, 1105);
			expTable.AddLevelUpStat(9, 9730, 1105);
			expTable.AddLevelUpStat(10, 10835, 1105);
			expTable.AddLevelUpStat(11, 11940, 1394);
			expTable.AddLevelUpStat(12, 13334, 1727);
			expTable.AddLevelUpStat(13, 15061, 2092);
			expTable.AddLevelUpStat(14, 17153, 2484);
			expTable.AddLevelUpStat(15, 19637, 2902);
			expTable.AddLevelUpStat(16, 22539, 3345);
			expTable.AddLevelUpStat(17, 25884, 3810);
			expTable.AddLevelUpStat(18, 29694, 4297);
			expTable.AddLevelUpStat(19, 33991, 4805);
			expTable.AddLevelUpStat(20, 38796, 5332);
			expTable.AddLevelUpStat(21, 44128, 5876);
			expTable.AddLevelUpStat(22, 50004, 6438);
			expTable.AddLevelUpStat(23, 56442, 7015);
			expTable.AddLevelUpStat(24, 63457, 7608);
			expTable.AddLevelUpStat(25, 71065, 8216);
			expTable.AddLevelUpStat(26, 79281, 8837);
			expTable.AddLevelUpStat(27, 88118, 9471);
			expTable.AddLevelUpStat(28, 97589, 10117);
			expTable.AddLevelUpStat(29, 107706, 10775);
			expTable.AddLevelUpStat(30, 118481, 11444);
			expTable.AddLevelUpStat(31, 129925, 12124);
			expTable.AddLevelUpStat(32, 142049, 12814);
			expTable.AddLevelUpStat(33, 154863, 13513);
			expTable.AddLevelUpStat(34, 168376, 14222);
			expTable.AddLevelUpStat(35, 182598, 14940);
			expTable.AddLevelUpStat(36, 197538, 15666);
			expTable.AddLevelUpStat(37, 213204, 16400);
			expTable.AddLevelUpStat(38, 229604, 17142);
			expTable.AddLevelUpStat(39, 246746, 17890);
			expTable.AddLevelUpStat(40, 264636, 18646);
			expTable.AddLevelUpStat(41, 283282, 19409);
			expTable.AddLevelUpStat(42, 302691, 20178);
			expTable.AddLevelUpStat(43, 322869, 20953);
			expTable.AddLevelUpStat(44, 343822, 21735);
			expTable.AddLevelUpStat(45, 365557, 22522);
			expTable.AddLevelUpStat(46, 388079, 23316);
			expTable.AddLevelUpStat(47, 411395, 24113);
			expTable.AddLevelUpStat(48, 435508, 24917);
			expTable.AddLevelUpStat(49, 460425, 25725);
			expTable.AddLevelUpStat(50, 486150, 26538);
			expTable.AddLevelUpStat(51, 512688, 27355);
			expTable.AddLevelUpStat(52, 540043, 28177);
			expTable.AddLevelUpStat(53, 568220, 29002);
			expTable.AddLevelUpStat(54, 597222, 29833);
			expTable.AddLevelUpStat(55, 627055, 30667);
			expTable.AddLevelUpStat(56, 657722, 31505);
			expTable.AddLevelUpStat(57, 689227, 32346);
			expTable.AddLevelUpStat(58, 721573, 33191);
			expTable.AddLevelUpStat(59, 754764, 34039);
			expTable.AddLevelUpStat(60, 788803, 34890);
			expTable.AddLevelUpStat(61, 823693, 35745);
			expTable.AddLevelUpStat(62, 859438, 36603);
			expTable.AddLevelUpStat(63, 896041, 37464);
			expTable.AddLevelUpStat(64, 933505, 38327);
			expTable.AddLevelUpStat(65, 971832, 39193);
			expTable.AddLevelUpStat(66, 1011025, 40062);
			expTable.AddLevelUpStat(67, 1051087, 40934);
			expTable.AddLevelUpStat(68, 1092021, 41807);
			expTable.AddLevelUpStat(69, 1133828, 42684);
			expTable.AddLevelUpStat(70, 1176512, 43563);
			expTable.AddLevelUpStat(71, 1220075, 44444);
			expTable.AddLevelUpStat(72, 1264519, 45326);
			expTable.AddLevelUpStat(73, 1309845, 46211);
			expTable.AddLevelUpStat(74, 1356056, 47098);
			expTable.AddLevelUpStat(75, 1403154, 47987);
			expTable.AddLevelUpStat(76, 1451141, 48878);
			expTable.AddLevelUpStat(77, 1500019, 49770);
			expTable.AddLevelUpStat(78, 1549789, 50665);
			expTable.AddLevelUpStat(79, 1600454, 51562);
			expTable.AddLevelUpStat(80, 1652016, 52460);
			expTable.AddLevelUpStat(81, 1704476, 53359);
			expTable.AddLevelUpStat(82, 1757835, 54259);
			expTable.AddLevelUpStat(83, 1812094, 55161);
			expTable.AddLevelUpStat(84, 1867255, 56066);
			expTable.AddLevelUpStat(85, 1923321, 56971);
			expTable.AddLevelUpStat(86, 1980292, 57878);
			expTable.AddLevelUpStat(87, 2038170, 58785);
			expTable.AddLevelUpStat(88, 2096955, 59695);
			expTable.AddLevelUpStat(89, 2156650, 60605);
			expTable.AddLevelUpStat(90, 2217255, 61517);
			expTable.AddLevelUpStat(91, 2278772, 62429);
			expTable.AddLevelUpStat(92, 2341201, 63343);
			expTable.AddLevelUpStat(93, 2404544, 64258);
			expTable.AddLevelUpStat(94, 2468802, 65173);
			expTable.AddLevelUpStat(95, 2533975, 66090);
			expTable.AddLevelUpStat(96, 2600065, 67008);
			expTable.AddLevelUpStat(97, 2667073, 67926);
			expTable.AddLevelUpStat(98, 2734999, 68846);
			expTable.AddLevelUpStat(99, 2803845, 69766);
			expTable.AddLevelUpStat(100, 2873611, 70688);
			expTable.AddLevelUpStat(101, 2944299, 71609);
			expTable.AddLevelUpStat(102, 3015908, 72532);
			expTable.AddLevelUpStat(103, 3088440, 73455);
			expTable.AddLevelUpStat(104, 3161895, 74379);
			expTable.AddLevelUpStat(105, 3236274, 75304);
			expTable.AddLevelUpStat(106, 3311578, 76230);
			expTable.AddLevelUpStat(107, 3387808, 77156);
			expTable.AddLevelUpStat(108, 3464964, 78083);
			expTable.AddLevelUpStat(109, 3543047, 79011);
			expTable.AddLevelUpStat(110, 3622058, 79938);
			expTable.AddLevelUpStat(111, 3701996, 80866);
			expTable.AddLevelUpStat(112, 3782862, 81796);
			expTable.AddLevelUpStat(113, 3864658, 82725);
			expTable.AddLevelUpStat(114, 3947383, 83655);
			expTable.AddLevelUpStat(115, 4031038, 84585);
			expTable.AddLevelUpStat(116, 4115623, 85516);
			expTable.AddLevelUpStat(117, 4201139, 86447);
			expTable.AddLevelUpStat(118, 4287586, 87379);
			expTable.AddLevelUpStat(119, 4374965, 88311);
			expTable.AddLevelUpStat(120, 4463276, 89243);
			expTable.AddLevelUpStat(121, 4552519, 90176);
			expTable.AddLevelUpStat(122, 4642695, 91109);
			expTable.AddLevelUpStat(123, 4733804, 92043);
			expTable.AddLevelUpStat(124, 4825847, 92977);
			expTable.AddLevelUpStat(125, 4918824, 93910);
			expTable.AddLevelUpStat(126, 5012734, 94844);
			expTable.AddLevelUpStat(127, 5107578, 95779);
			expTable.AddLevelUpStat(128, 5203357, 96713);
			expTable.AddLevelUpStat(129, 5300070, 97648);
			expTable.AddLevelUpStat(130, 5397718, 98584);
			expTable.AddLevelUpStat(131, 5496302, 99519);
			expTable.AddLevelUpStat(132, 5595821, 100454);
			expTable.AddLevelUpStat(133, 5696275, 101390);
			expTable.AddLevelUpStat(134, 5797665, 102326);
			expTable.AddLevelUpStat(135, 5899991, 103262);
			expTable.AddLevelUpStat(136, 6003253, 104198);
			expTable.AddLevelUpStat(137, 6107451, 105134);
			expTable.AddLevelUpStat(138, 6212585, 106070);
			expTable.AddLevelUpStat(139, 6318655, 107007);
			expTable.AddLevelUpStat(140, 6425662, 107944);
			expTable.AddLevelUpStat(141, 6533606, 108881);
			expTable.AddLevelUpStat(142, 6642487, 109817);
			expTable.AddLevelUpStat(143, 6752304, 110753);
			expTable.AddLevelUpStat(144, 6863057, 111690);
			expTable.AddLevelUpStat(145, 6974747, 112627);
			expTable.AddLevelUpStat(146, 7087374, 113564);
			expTable.AddLevelUpStat(147, 7200938, 114501);
			expTable.AddLevelUpStat(148, 7315439, 115438);
			expTable.AddLevelUpStat(149, 7430877, 116375);
			expTable.AddLevelUpStat(150, 7547252, 117312);
			expTable.AddLevelUpStat(151, 7664564, 118249);
			expTable.AddLevelUpStat(152, 7782813, 119186);
			expTable.AddLevelUpStat(153, 7901999, 120123);
			expTable.AddLevelUpStat(154, 8022122, 121060);
			expTable.AddLevelUpStat(155, 8143182, 121997);
			expTable.AddLevelUpStat(156, 8265179, 122932);
			expTable.AddLevelUpStat(157, 8388111, 123869);
			expTable.AddLevelUpStat(158, 8511980, 124806);
			expTable.AddLevelUpStat(159, 8636786, 125743);
			expTable.AddLevelUpStat(160, 8762529, 126679);
			expTable.AddLevelUpStat(161, 8889208, 127615);
			expTable.AddLevelUpStat(162, 9016823, 128551);
			expTable.AddLevelUpStat(163, 9145374, 129488);
			expTable.AddLevelUpStat(164, 9274862, 130424);
			expTable.AddLevelUpStat(165, 9405286, 131360);
			expTable.AddLevelUpStat(166, 9536646, 132296);
			expTable.AddLevelUpStat(167, 9668942, 133231);
			expTable.AddLevelUpStat(168, 9802173, 134167);
			expTable.AddLevelUpStat(169, 9936340, 135102);
			expTable.AddLevelUpStat(170, 10071442, 136038);
			expTable.AddLevelUpStat(171, 10207480, 136972);
			expTable.AddLevelUpStat(172, 10344452, 137908);
			expTable.AddLevelUpStat(173, 10482360, 138843);
			expTable.AddLevelUpStat(174, 10621203, 139778);
			expTable.AddLevelUpStat(175, 10760981, 140711);
			expTable.AddLevelUpStat(176, 10901692, 141646);
			expTable.AddLevelUpStat(177, 11043338, 142581);
			expTable.AddLevelUpStat(178, 11185919, 143515);
			expTable.AddLevelUpStat(179, 11329434, 144448);
			expTable.AddLevelUpStat(180, 11473882, 145382);
			expTable.AddLevelUpStat(181, 11619264, 146316);
			expTable.AddLevelUpStat(182, 11765580, 147248);
			expTable.AddLevelUpStat(183, 11912828, 148182);
			expTable.AddLevelUpStat(184, 12061010, 149115);
			expTable.AddLevelUpStat(185, 12210125, 150047);
			expTable.AddLevelUpStat(186, 12360172, 150979);
			expTable.AddLevelUpStat(187, 12511151, 151911);
			expTable.AddLevelUpStat(188, 12663062, 152843);
			expTable.AddLevelUpStat(189, 12815905, 153776);
			expTable.AddLevelUpStat(190, 12969681, 154707);
			expTable.AddLevelUpStat(191, 13124388, 155638);
			expTable.AddLevelUpStat(192, 13280026, 156569);
			expTable.AddLevelUpStat(193, 13436595, 157500);
			expTable.AddLevelUpStat(194, 13594095, 158431);
			expTable.AddLevelUpStat(195, 13752526, 159361);
			expTable.AddLevelUpStat(196, 13911887, 160291);
			expTable.AddLevelUpStat(197, 14072178, 161221);
			expTable.AddLevelUpStat(198, 14233399, 162151);
			expTable.AddLevelUpStat(199, 14395550, 163080);
			expTable.AddLevelUpStat(200, 14558630, 164009);

			return expTable;
		}

		private void CreateBaseMonsterSources(MonsterManager monsterManager) {
			monsterManager.AddMonster("Drache", @"Sprites\Monster\Dragon.png");
			monsterManager.AddMonster("grüner Drache", @"Sprites\Monster\Dragon2.png");
			monsterManager.AddMonster("Gumba", @"Sprites\Monster\gumba.png");
		}

		private List<ShopSkill> CreateUnBuyedShopSkills() {
			List<ShopSkill> tmp = new List<ShopSkill>();

			tmp.Add(new ShopSkill(BuyableSkillTyps.BankInventoryExtension, BuyableSkills.BankInventoryI_I, false, 20000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.BankInventoryExtension, BuyableSkills.BankInventoryI_II, false, 40000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.BankInventoryExtension, BuyableSkills.BankInventoryI_III, false, 80000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.BankInventoryExtension, BuyableSkills.BankInventoryI_IV, false, 160000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.BankInventoryExtension, BuyableSkills.BankInventoryI_V, false, 320000));

			tmp.Add(new ShopSkill(BuyableSkillTyps.BankInventoryExtension, BuyableSkills.BankInventoryII_I, false, 200000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.BankInventoryExtension, BuyableSkills.BankInventoryII_II, false, 400000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.BankInventoryExtension, BuyableSkills.BankInventoryII_III, false, 800000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.BankInventoryExtension, BuyableSkills.BankInventoryII_IV, false, 1600000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.BankInventoryExtension, BuyableSkills.BankInventoryII_V, false, 3200000));

			tmp.Add(new ShopSkill(BuyableSkillTyps.BankInventoryExtension, BuyableSkills.BankInventoryIII_I, false, 2000000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.BankInventoryExtension, BuyableSkills.BankInventoryIII_II, false, 4000000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.BankInventoryExtension, BuyableSkills.BankInventoryIII_III, false, 8000000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.BankInventoryExtension, BuyableSkills.BankInventoryIII_IV, false, 16000000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.BankInventoryExtension, BuyableSkills.BankInventoryIII_V, false, 32000000));

			tmp.Add(new ShopSkill(BuyableSkillTyps.BankInventoryExtension, BuyableSkills.BankInventoryIV_I, false, 20000000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.BankInventoryExtension, BuyableSkills.BankInventoryIV_II, false, 40000000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.BankInventoryExtension, BuyableSkills.BankInventoryIV_III, false, 80000000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.BankInventoryExtension, BuyableSkills.BankInventoryIV_IV, false, 160000000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.BankInventoryExtension, BuyableSkills.BankInventoryIV_V, false, 320000000));

			tmp.Add(new ShopSkill(BuyableSkillTyps.BankInventoryExtension, BuyableSkills.BankInventoryV_I, false, 200000000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.BankInventoryExtension, BuyableSkills.BankInventoryV_II, false, 400000000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.BankInventoryExtension, BuyableSkills.BankInventoryV_III, false, 800000000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.BankInventoryExtension, BuyableSkills.BankInventoryV_IV, false, 1600000000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.BankInventoryExtension, BuyableSkills.BankInventoryV_V, false, 3200000000));

			tmp.Add(new ShopSkill(BuyableSkillTyps.BankSlotExtension, BuyableSkills.BankSlotI, false, 100000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.BankSlotExtension, BuyableSkills.BankSlotII, false, 1000000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.BankSlotExtension, BuyableSkills.BankSlotIII, false, 10000000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.BankSlotExtension, BuyableSkills.BankSlotIV, false, 100000000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.BankSlotExtension, BuyableSkills.BankSlotV, false, 1000000000));

			tmp.Add(new ShopSkill(BuyableSkillTyps.InventoryExtension, BuyableSkills.InventoryI, false, 10000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.InventoryExtension, BuyableSkills.InventoryII, false, 100000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.InventoryExtension, BuyableSkills.InventoryIII, false, 1000000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.InventoryExtension, BuyableSkills.InventoryIV, false, 10000000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.InventoryExtension, BuyableSkills.InventoryV, false, 100000000));

			tmp.Add(new ShopSkill(BuyableSkillTyps.ItemDropable, BuyableSkills.Langschwert, false, 44000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.ItemDropable, BuyableSkills.Säbel, false, 210000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.ItemDropable, BuyableSkills.Breitschwert, false, 700000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.ItemDropable, BuyableSkills.Falchion, false, 1875000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.ItemDropable, BuyableSkills.Kriegsschwert, false, 5000000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.ItemDropable, BuyableSkills.Bastardschwert, false, 12500000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.ItemDropable, BuyableSkills.Gladius, false, 25000000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.ItemDropable, BuyableSkills.Machete, false, 46000000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.ItemDropable, BuyableSkills.Kampfschwert, false, 75000000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.ItemDropable, BuyableSkills.Runenschwert, false, 125000000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.ItemDropable, BuyableSkills.Saif, false, 320000000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.ItemDropable, BuyableSkills.Tulwar, false, 830000000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.ItemDropable, BuyableSkills.Königsklinge, false, 1810000000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.ItemDropable, BuyableSkills.Meisterschwert, false, 2500000000));

			tmp.Add(new ShopSkill(BuyableSkillTyps.ItemDropable, BuyableSkills.Lederrüstung, false, 44000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.ItemDropable, BuyableSkills.gehärtetesLeder, false, 210000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.ItemDropable, BuyableSkills.BeschlagenesLeder, false, 700000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.ItemDropable, BuyableSkills.Ringpanzer, false, 1875000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.ItemDropable, BuyableSkills.Schuppenpanzer, false, 5000000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.ItemDropable, BuyableSkills.Kettenpanzer, false, 12500000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.ItemDropable, BuyableSkills.Brustpanzer, false, 25000000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.ItemDropable, BuyableSkills.Bänderrüstung, false, 46000000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.ItemDropable, BuyableSkills.Plattenharnisch, false, 75000000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.ItemDropable, BuyableSkills.Feldharnisch, false, 125000000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.ItemDropable, BuyableSkills.Prunkharnisch, false, 320000000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.ItemDropable, BuyableSkills.Vollharnisch, false, 830000000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.ItemDropable, BuyableSkills.LeichtePlattenrüstung, false, 1810000000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.ItemDropable, BuyableSkills.SchwerePlattenrüstung, false, 2500000000));

			tmp.Add(new ShopSkill(BuyableSkillTyps.ItemDropable, BuyableSkills.Haube, false, 44000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.ItemDropable, BuyableSkills.Kriegshaube, false, 210000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.ItemDropable, BuyableSkills.Helm, false, 700000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.ItemDropable, BuyableSkills.Plattenhelm, false, 1875000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.ItemDropable, BuyableSkills.Krone, false, 5000000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.ItemDropable, BuyableSkills.Basinet, false, 12500000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.ItemDropable, BuyableSkills.Klappvisier, false, 25000000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.ItemDropable, BuyableSkills.Kaskett, false, 46000000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.ItemDropable, BuyableSkills.Großhelm, false, 75000000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.ItemDropable, BuyableSkills.Hundsgugel, false, 125000000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.ItemDropable, BuyableSkills.Stechhelm, false, 320000000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.ItemDropable, BuyableSkills.Zischägge, false, 830000000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.ItemDropable, BuyableSkills.Höllenmaske, false, 1810000000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.ItemDropable, BuyableSkills.Archonkrone, false, 2500000000));

			tmp.Add(new ShopSkill(BuyableSkillTyps.ItemDropable, BuyableSkills.Stiefel, false, 44000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.ItemDropable, BuyableSkills.SchwereStiefel, false, 210000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.ItemDropable, BuyableSkills.Kettenstiefel, false, 700000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.ItemDropable, BuyableSkills.Sabatons, false, 1875000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.ItemDropable, BuyableSkills.Beinschienen, false, 5000000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.ItemDropable, BuyableSkills.Seidenschuhe, false, 12500000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.ItemDropable, BuyableSkills.Soldatenstiefel, false, 25000000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.ItemDropable, BuyableSkills.Treter, false, 46000000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.ItemDropable, BuyableSkills.SchwereSabatons, false, 75000000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.ItemDropable, BuyableSkills.Schlachtbeinschienen, false, 125000000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.ItemDropable, BuyableSkills.SchwereTreter, false, 320000000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.ItemDropable, BuyableSkills.GepanzerteKettenstiefel, false, 830000000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.ItemDropable, BuyableSkills.SchwereSoldatenstiefel, false, 1810000000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.ItemDropable, BuyableSkills.SchwereSchlachtbeinschienen, false, 2500000000));

			tmp.Add(new ShopSkill(BuyableSkillTyps.ItemDropable, BuyableSkills.Schild2, false, 44000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.ItemDropable, BuyableSkills.Schild3, false, 210000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.ItemDropable, BuyableSkills.Schild4, false, 700000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.ItemDropable, BuyableSkills.Schild5, false, 1875000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.ItemDropable, BuyableSkills.Schild6, false, 5000000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.ItemDropable, BuyableSkills.Schild7, false, 12500000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.ItemDropable, BuyableSkills.Schild8, false, 25000000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.ItemDropable, BuyableSkills.Schild9, false, 46000000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.ItemDropable, BuyableSkills.Schild10, false, 75000000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.ItemDropable, BuyableSkills.Schild11, false, 125000000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.ItemDropable, BuyableSkills.Schild12, false, 320000000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.ItemDropable, BuyableSkills.Schild13, false, 830000000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.ItemDropable, BuyableSkills.Schild14, false, 1810000000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.ItemDropable, BuyableSkills.Schild15, false, 2500000000));

			tmp.Add(new ShopSkill(BuyableSkillTyps.QualityDropable, BuyableSkills.MagicBody, false, 1000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.QualityDropable, BuyableSkills.MagicHead, false, 1000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.QualityDropable, BuyableSkills.MagicSchild, false, 1000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.QualityDropable, BuyableSkills.MagicShoe, false, 1000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.QualityDropable, BuyableSkills.MagicWeapon, false, 1000));

			tmp.Add(new ShopSkill(BuyableSkillTyps.QualityDropable, BuyableSkills.RareBody, false, 5000000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.QualityDropable, BuyableSkills.RareHead, false, 5000000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.QualityDropable, BuyableSkills.RareSchild, false, 5000000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.QualityDropable, BuyableSkills.RareShoe, false, 5000000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.QualityDropable, BuyableSkills.RareWeapon, false, 5000000));

			tmp.Add(new ShopSkill(BuyableSkillTyps.QualityDropable, BuyableSkills.LegendaryBody, false, 175000000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.QualityDropable, BuyableSkills.LegendaryHead, false, 175000000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.QualityDropable, BuyableSkills.LegendarySchild, false, 175000000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.QualityDropable, BuyableSkills.LegendaryShoe, false, 175000000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.QualityDropable, BuyableSkills.LegendaryWeapon, false, 175000000));

			tmp.Add(new ShopSkill(BuyableSkillTyps.SkillGridExtension, BuyableSkills.SkillGridI, false, 100000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.SkillGridExtension, BuyableSkills.SkillGridII, false, 1000000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.SkillGridExtension, BuyableSkills.SkillGridIII, false, 10000000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.SkillGridExtension, BuyableSkills.SkillGridIV, false, 100000000));
			tmp.Add(new ShopSkill(BuyableSkillTyps.SkillGridExtension, BuyableSkills.SkillGridV, false, 1000000000));

			return tmp;
		}

		private void ResetIngameMenuShopButton(CorePlayer player) {
			Texture2D buttonTexture = Content.Load<Texture2D>(@"GUI\buttonSample.png");

			if (player.Owner.ShopSkills.Where(s => s.Typ == BuyableSkillTyps.BankSlotExtension && s.Buyed).Count() == 0 || player.GetGameModeAddons().Contains(GameModeAddons.Ironman)) {
				((MainMenuScreen<Game1>)screenManager.GetScreen(INGAMEMENU)).AddButton("Bank", new Vector2(Resolution.TargetWidth / 2 - buttonTexture.Width / 2, 335f - 30f), Content.Load<SpriteFont>(@"Fonts\Areal16.tk"), Color.White, buttonTexture, null, Color.Gray);
			} else {
				((MainMenuScreen<Game1>)screenManager.GetScreen(INGAMEMENU)).AddButton("Bank", new Vector2(Resolution.TargetWidth / 2 - buttonTexture.Width / 2, 335f - 30f), Content.Load<SpriteFont>(@"Fonts\Areal16.tk"), Color.White, buttonTexture, () => { screenManager.RemoveScreen(); screenManager.AddScreen(BANKSCREEN, false); ((BankScreen)screenManager.GetScreen(BANKSCREEN)).RemoveEvents(); ((BankScreen)screenManager.GetScreen(BANKSCREEN)).CreateEvents(); }, Color.Blue);
			}
		}
	}
}

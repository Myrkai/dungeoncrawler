﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.Interface;
using Core.BattleSystem;
using SharpDX;
using Core.Enums;
using RPG.Chars;
using RPG.Sprites;
using SharpDX.Toolkit.Graphics;
using SharpDX.Toolkit;
using RPG.Items.Inventory;
using DungeonCrawler.CoreElements.Shop;
using RPG.Chars.Enum;
using Core.BattleSystem.Interface;
using RPG.Items.Enum;
using DungeonCrawler.CoreElements.Items.Skills;
using RPG;
using DungeonCrawler.Controls;

namespace DungeonCrawler.CoreElements {
	public class CorePlayer : Player, IPlayer {
		#region fields
		private int Layer { get; set; }

		private int BattleRounds { get; set; }
		private TimeSpan BattleTime { get; set; }
		private GameModes GameMode { get; set; }
		private int LayersToBeat { get; set; }

		public Inventory Inventory { get; private set; }

		private float PlayerMovementSpeed { get; set; }
		public float MovementSpeedModifier { get; set; }
		public float NextBattleFightModifier { get; set; }

		public NextBattleChance NextBattleTime { get; private set; }
		public PlayerAccount Owner { get; private set; }

		public List<SkillGridEntityControl> ActiveSkillsFromGrid { get; set; }
		#endregion

		public CorePlayer(PlayerAccount owner, Animation animation, RPG.Chars.EXPTable expTable, string name, int level, ClassType classType, Vector2 position, int currentHP, Inventory inventory)
			: base(animation, 2, expTable, name, level, classType, position, currentHP) {
			Layer = 1;

			BattleRounds = 25;
			BattleTime = new TimeSpan(0, 15, 0);
			GameMode = GameModes.Normal;

			PlayerMovementSpeed = 200f;
			MovementSpeedModifier = 1f;
			NextBattleFightModifier = 1f;

			Inventory = inventory;
			Owner = owner;

			CurrentHP = MaxHP;

			LayersToBeat = 1;

			ActiveSkillsFromGrid = new List<SkillGridEntityControl>();
		}

		public override void RefreshAllStatValue() {
			PlayerMovementSpeed = 200f;
			MovementSpeedModifier = 1f;
			NextBattleFightModifier = 1f;

			base.RefreshAllStatValue();
		}

		public float GetDamage(Random rnd, out bool criticalHit) {
			float dmg = base.GetDamage(rnd);
			List<KeyValuePair<float, bool>> critChance = new List<KeyValuePair<float, bool>>();
			critChance.Add(new KeyValuePair<float, bool>(Stats[RPG.Chars.Enum.StatType.CriticalHitChance] / 100f, true));
			critChance.Add(new KeyValuePair<float, bool>(1f - Stats[RPG.Chars.Enum.StatType.CriticalHitChance] / 100f, false));

			bool chance = Chance<bool>(critChance, rnd);
			criticalHit = chance;
			if (chance) {
				dmg = dmg * (1f + Stats[RPG.Chars.Enum.StatType.CriticalDmg] / 100f);
			}

			return dmg;
		}

		public object Clone() {
			CorePlayer tmp = (CorePlayer)this.MemberwiseClone();
			tmp.PlayerAnimation = new Animation(this.PlayerAnimation.Texture, this.PlayerAnimation.TextureAsset, this.PlayerAnimation.AnimationStepsPerAnimation, this.PlayerAnimation.AnimationCount, this.PlayerAnimation.AnimationSpeed);

			return tmp;
		}

		public void SetNextBattleTime(Random rnd) {
			NextBattleTime = new NextBattleChance(NextBattleChance.ConvertToTimespan(15), NextBattleChance.ConvertToTimespan(5), rnd);
		}

		public void SetNextBattleTime(NextBattleChance chance) {
			NextBattleTime = chance;
		}

		public override void Draw(SpriteBatch spriteBatch) {
			base.Draw(spriteBatch);
		}

		public void Draw(SpriteBatch spriteBatch, Vector2 position) {
			PlayerAnimation.Draw(spriteBatch, position);
		}

		public override void Update(GameTime gameTime) {
			base.Update(gameTime);
		}

		public Vector2 GetPosition() {
			return Position;
		}

		public int GetLevel() {
			return base.Level;
		}

		public int GetLayer() {
			return Layer;
		}

		public void SetLayer(int layer) {
			Layer = layer;
		}

		public int GetBeforeLevelEXP() {
			return base.EXPTable.GetLevelUpStats(Math.Max(base.Level - 1, 0)).NextLevelEXP;
		}

		public int GetCurrentEXP() {
			return base.CurrentEXP;
		}

		public int GetNextLevelEXP() {
			return base.NextLevelEXP;
		}

		public int GetBattleRounds() {
			return BattleRounds;
		}

		public TimeSpan GetNextBattleTime() {
			return NextBattleTime.TimeToNextBattle;
		}

		public TimeSpan GetTimeToNextBattle() {
			return NextBattleTime.CurrentTimeNextBattle;
		}

		public void ReduceNextBattleTime(int totalMilliseconds) {
			NextBattleTime.ReduceTimeToNextBattle(new TimeSpan(0, 0, 0, 0, (int)(totalMilliseconds * NextBattleFightModifier)));
		}

		public GameModes GetGameMode() {
			return GameMode;
		}

		public void SetGameMode(GameModes mode) {
			GameMode = mode;
		}

		public List<GameModeAddons> GetGameModeAddons() {
			return new List<GameModeAddons>();
		}

		public int GetLayersToBeat() {
			return LayersToBeat;
		}

		public void SetLayersToBeat(int value) {
			LayersToBeat = value;
		}

		public Dictionary<string, bool> GetShopSkills() {
			Dictionary<string, bool> tmp = new Dictionary<string, bool>();

			foreach (ShopSkill skill in Owner.ShopSkills) {
				tmp.Add(skill.Skill.ToString().ToLower().Replace(" ", ""), skill.Buyed);
			}

			return tmp;
		}

		public void SetPosition(Vector2 position) {
			Position = position;
		}

		public RectangleF GetDimension() {
			return new RectangleF(Position.X + 1, Position.Y + (PlayerAnimation.CurrentTextureRectangle().Height / 3), PlayerAnimation.CurrentTextureRectangle().Width - 2, PlayerAnimation.CurrentTextureRectangle().Height / 3 * 2);
			// return new RectangleF(Position.X + 1, Position.Y + 1, PlayerAnimation.CurrentTextureRectangle().Width - 2, PlayerAnimation.CurrentTextureRectangle().Height - 2);
		}

		public float GetMovementSpeed() {
			return PlayerMovementSpeed * MovementSpeedModifier;
		}

		public void AddEXP(int exp) {
			base.AddExp(exp);
		}

		public void AddGold(uint gold) {
			Gold += gold;
		}

		public float GetCurrentHP() {
			return CurrentHP;
		}

		/// <summary>
		/// Gibt zurück ob der Spieler Schaden erlitten hat.
		/// </summary>
		public bool TakeDamage(float damage, int monsterLevel, Random rnd) {
			List<KeyValuePair<float, bool>> evadeChance = new List<KeyValuePair<float, bool>>();
			evadeChance.Add(new KeyValuePair<float, bool>(Stats[RPG.Chars.Enum.StatType.Evade] / 100f, true));
			evadeChance.Add(new KeyValuePair<float, bool>(1f - Stats[RPG.Chars.Enum.StatType.Evade] / 100, false));
			bool evadeC = Chance<bool>(evadeChance, rnd);

			if (!evadeC) {
				CurrentHP -= damage * (1f - base.GetDamageReduce(monsterLevel) / 100f);
				return true;
			} else {
				return false;
			}
		}

		private static T Chance<T>(List<KeyValuePair<float, T>> list, Random rnd) {
			float dice = (float)rnd.NextDouble();

			//korrigiere die Chancen auf einen Gesamtwert von 100%
			float sumChance = list.Sum(d => d.Key);
			List<KeyValuePair<float, T>> items;
			if (sumChance != 1f) {
				items = list.Select(e => new KeyValuePair<float, T>(e.Key / sumChance, e.Value)).ToList();
			} else {
				items = list;
			}

			//bestimmte das Zufällige Item anhand der Chancen. Die Chancen werden aufsummiert. 
			//z.B. A=10, B=30, C=60; Dice=90; Erg=A+B+C; 90 < 100 = C;
			//z.B. A=10, B=30, C=60; Dice=39; Erg=A+B; 39 < 40 = B
			float cumulative = 0.0f;
			foreach (KeyValuePair<float, T> item in items.OrderBy(di => di.Key)) {
				cumulative += item.Key;

				if (dice < cumulative) {
					return item.Value;
				}
			}

			return default(T);
		}

		public void SetCurrentHealth(float value) {
			base.CurrentHP = value;
		}

		public float GetMagicFind() {
			return Stats[StatType.MagicFind];
		}

		public List<string> PerformAllBattleActions(IPlayer owner, IMonster target, Random rnd) {
			List<string> tmp = new List<string>();
			((CorePlayer)owner).CurrentSkills.Where(s => s.AffectType == AffectType.BattleSkill).Select(s => (CoreBattleSkill)s).ToList().ForEach(s => tmp.Add(s.PerformAction((Entity)owner, (Entity)target, rnd)));

			return tmp.Where(s => !String.IsNullOrEmpty(s)).ToList();
		}

		public List<string> PerformAllAfterBattleActions(Random Randomizer) {
			List<string> tmp = new List<string>();
			CurrentSkills.Where(s => s.AffectType == AffectType.AfterBattleSkill).Select(s => (CoreAfterBattleSkill)s).ToList().ForEach(s => tmp.Add(s.PerformAction(this, Randomizer)));

			return tmp.Where(s => !String.IsNullOrEmpty(s)).ToList();
		}

		internal void AddBattleTime(int min) {
			BattleTime = BattleTime.Add(new TimeSpan(0, min, 0));
		}

		internal void SetBattleTime(TimeSpan min) {
			BattleTime = min;
		}

		internal void AddBattleRound(int p) {
			BattleRounds += p;
		}

		public void Heal() {
			SetCurrentHealth(MaxHP);
		}

		internal void RedurceBattleTime(int milliseconds) {
			BattleTime = BattleTime.Subtract(new TimeSpan(0, 0, 0, 0, milliseconds));
		}

		public void SetBattleRounds(int value) {
			BattleRounds = value;
		}

		public TimeSpan GetBattleTime() {
			return BattleTime;
		}

		internal void SetPlayerMovementSpeedAndModifier(float p, float p_2) {
			PlayerMovementSpeed = p;
			MovementSpeedModifier = p_2;
		}

		internal void SetCurrentEXPAndStatPoints(int p, int p_2, int p_3) {
			base.SetExpAndLevel(Level, p);
			StatPoints = p_2;
			SetStatPointsPerLevelup(p_3);
		}

		internal void Reset(Random rnd) {
			Level = 1;
			Layer = 1;
			SetCurrentEXPAndStatPoints(0, 0, 2);
			CurrentHP = MaxHP;
			Gold = 0;
			StatPoints = 0;

			foreach (var skill in ActiveSkillsFromGrid.ToList()) {
				if (AvaiableSkills.FirstOrDefault(s => s.Equals(skill.Skill)) == null) {
					ActiveSkillsFromGrid.Remove(skill);
				}
			}

			BattleRounds = 25;
			BattleTime = new TimeSpan(0, 15, 0);

			PlayerMovementSpeed = 200f;
			MovementSpeedModifier = 1f;
			NextBattleFightModifier = 1f;

			SetStat(StatType.BaseStrength, 1f);
			SetStat(StatType.BaseDexterity, 1f);
			SetStat(StatType.BaseStamina, 1f);
			SetStat(StatType.BaseLuck, 1f);
			RefreshAllStatValue();

			SetNextBattleTime(rnd);
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RPG.Chars;
using Core.BattleSystem.Interface;
using Core.Items.Interface;
using SharpDX;
using SharpDX.Toolkit.Graphics;
using RPG.Chars.Enum;
using RPG.Items;
using SharpDX.Toolkit;
using SharpDX.Toolkit.Content;
using RPG.Items.Enum;
using Core.Interface;
using DungeonCrawler.CoreElements.Items.Skills;
using RPG;

namespace DungeonCrawler.CoreElements {
	public class CoreMonster : Monster, IMonster {

		private ContentManager ContentManager { get; set; }

		public CoreMonster(bool isBoss, Texture2D texture, EXPTable expTable, float dropChanceMagic, float dropChanceRare, float dropChanceLegendary, string name, int level, Vector2 position, ContentManager content)
			: base(isBoss, texture, expTable, dropChanceMagic, dropChanceRare, dropChanceLegendary, name, level, position) {
				ContentManager = content;
		}

		#region Interface IMonster
		public IMonster Clone() {
			CoreMonster monster = new CoreMonster(IsBoss, Texture, EXPTable, DropChanceMagic, DropChanceRare, DropChanceLegendary, Name, Level, Position, ContentManager);
			CurrentSkills.ToList().ForEach(s => AddUseSkill(s));
			UsedSkills.ToList().ForEach(s => AddSkill(s));
			Stats.ToList().ForEach(s => SetStat(s.Key, s.Value));
			monster.AddBaseDropableItem(DropAbleBaseItems.ToList());

			return monster;
		}

		public float GetDamage(Random rnd, out bool criticalHit) {
			float dmg = base.GetDamage(rnd);
			List<KeyValuePair<float, bool>> critChance = new List<KeyValuePair<float, bool>>();
			critChance.Add(new KeyValuePair<float, bool>(Stats[RPG.Chars.Enum.StatType.CriticalHitChance] / 100f, true));
			critChance.Add(new KeyValuePair<float, bool>(1f - Stats[RPG.Chars.Enum.StatType.CriticalHitChance] / 100f, false));

			bool chance = Chance<bool>(critChance, rnd);
			criticalHit = chance;
			if (chance) {
				dmg = dmg * (1f + Stats[RPG.Chars.Enum.StatType.CriticalDmg] / 100f);
			}

			return dmg;
		}

		public bool GetIsBoss() {
			return IsBoss;
		}

		public int GetGold() {
			return Gold();
		}

		public int GetGold(int addonBaseGoldMultiplier) {
			return Gold(addonBaseGoldMultiplier);
		}

		public int GetEXP() {
			return EXP();
		}

		public int GetExp(int addonBaseExpMultiplier) {
			return EXP(addonBaseExpMultiplier);
		}

		public float GetStrength() {
			return Strength;
		}

		public float GetDexterity() {
			return Dexterity;
		}

		public float GetStamina() {
			return Stamina;
		}

		public float GetLuck() {
			return Luck;
		}

		public List<ISkill> GetUsedSkills() {
			return UsedSkills.Select(s => (ISkill)s).ToList();
		}

		public List<IItem> GetDropableItems() {
			return DropAbleBaseItems.Select(i => (IItem)i).ToList();
		}

		public string GetName() {
			return Name;
		}

		public int GetLevel() {
			return Level;
		}

		public float GetCriticalHitChance() {
			return CriticalHitChance;
		}

		public float GetCriticalDamage() {
			return CriticalHitChance;
		}

		public float GetEvade() {
			return Evade;
		}

		public float GetCurrentHP() {
			return CurrentHP;
		}

		public float GetMaxHP() {
			return Stats[StatType.HP];
		}

		public void SetIsBoss(bool value) {
			IsBoss = value;
		}

		public void LoadNewTexture(string textureAsset) {
			base.Texture = ContentManager.Load<Texture2D>(textureAsset);
		}

		public void SetExpTable(Core.BattleSystem.EXPTable expTable) {
			EXPTable tmp = new EXPTable();
			foreach (Core.BattleSystem.LevelEntity entity in expTable.LevelUpStats) {
				tmp.AddLevelUpStat(entity.Level, entity.NextLevelEXP, entity.EXPStep);
			}

			EXPTable = tmp;
		}

		public void SetName(string name) {
			Name = name;
		}

		public void SetLevel(int level) {
			Level = level;
		}

		public void SetPosition(Vector2 position) {
			Position = position;
		}

		public void SetCurrentHP(float hp) {
			CurrentHP = hp;
		}

		public void AddSkill(ISkill skill) {
			base.AddSkill((Skill)skill);
		}

		public void AddBaseDropableItem(IItem item) {
			base.AddBaseDropableItem((Item)item);
		}
		#endregion

		public override void Draw(SpriteBatch spriteBatch) {
			base.Draw(spriteBatch);

			spriteBatch.Draw(base.Texture, Position, Color.White);
		}

		public override void Update(GameTime gameTime) {
			base.Update(gameTime);
		}

		public void CalcAllStatValues() {
			base.SetStat(StatType.BaseStrength, GetStrength());
			base.SetStat(StatType.BaseDexterity, GetDexterity());
			base.SetStat(StatType.BaseStamina, GetStamina());
			base.SetStat(StatType.BaseLuck, GetLuck());
			base.RefreshAllStatValue();
		}

		public void EquipSword(IItem item) {
			base.WearItem(ItemType.Weapon, (Item)item);
		}


		public void EquipArmor(IItem item, Core.Items.Enum.ItemType type) {
			RPG.Items.Enum.ItemType t = ItemType.Body;

			switch (type) {
				case Core.Items.Enum.ItemType.Body:
					t = ItemType.Body;
					break;
				case Core.Items.Enum.ItemType.Head:
					t = ItemType.Head;
					break;
				case Core.Items.Enum.ItemType.Schild:
					t = ItemType.Schild;
					break;
				case Core.Items.Enum.ItemType.Shoe:
					t = ItemType.Shoe;
					break;
				default:
					throw new NotSupportedException(type.ToString());
			}
			
			base.WearItem(t, (Item)item);
		}

		/// <summary>
		/// Gibt zurück ob das Monster Schaden erlitten hat.
		/// </summary>
		public bool TakeDamage(float damage, int monsterLevel, Random rnd) {
			List<KeyValuePair<float, bool>> evadeChance = new List<KeyValuePair<float, bool>>();
			evadeChance.Add(new KeyValuePair<float, bool>(Stats[RPG.Chars.Enum.StatType.Evade] / 100f, true));
			evadeChance.Add(new KeyValuePair<float, bool>(1f - Stats[RPG.Chars.Enum.StatType.Evade] / 100, false));
			bool evadeC = Chance<bool>(evadeChance, rnd);

			if (!evadeC) {
				CurrentHP -= damage * (1f - base.GetDamageReduce(monsterLevel) / 100f);
				return true;
			} else {
				return false;
			}
		}

		public void SetMaxHP(float hp) {
			base.SetStat(StatType.HP, hp);
		}

		private static T Chance<T>(List<KeyValuePair<float, T>> list, Random rnd) {
			float dice = (float)rnd.NextDouble();

			//korrigiere die Chancen auf einen Gesamtwert von 100%
			float sumChance = list.Sum(d => d.Key);
			List<KeyValuePair<float, T>> items;
			if (sumChance != 1f) {
				items = list.Select(e => new KeyValuePair<float, T>(e.Key / sumChance, e.Value)).ToList();
			} else {
				items = list;
			}

			//bestimmte das Zufällige Item anhand der Chancen. Die Chancen werden aufsummiert. 
			//z.B. A=10, B=30, C=60; Dice=90; Erg=A+B+C; 90 < 100 = C;
			//z.B. A=10, B=30, C=60; Dice=39; Erg=A+B; 39 < 40 = B
			float cumulative = 0.0f;
			foreach (KeyValuePair<float, T> item in items.OrderBy(di => di.Key)) {
				cumulative += item.Key;

				if (dice < cumulative) {
					return item.Value;
				}
			}

			return default(T);
		}

		public List<string> PerformAllBattleActions(IMonster owner, IPlayer target, Random rnd) {
			List<string> tmp = new List<string>();
			((CoreMonster)owner).UsedSkills.Where(s => s.AffectType == AffectType.BattleSkill).Select(s => (CoreBattleSkill)s).ToList().ForEach(s => tmp.Add(s.PerformAction((Entity)owner, (Entity)target, rnd)));

			return tmp.Where(s => !String.IsNullOrEmpty(s)).ToList();
		}
	}
}

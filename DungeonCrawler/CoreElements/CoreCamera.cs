﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.Interface;
using TiledMap;
using SharpDX;

namespace DungeonCrawler.CoreElements {
	public class CoreCamera : ICamera {
		private Camera Camera { get; set; }

		public CoreCamera(Camera cam) {
			Camera = cam;
		}

		public Vector2 GetPosition() {
			return Camera.Position;
		}

		public int GetWidth() {
			return Camera.Width;
		}

		public int GetHeight() {
			return Camera.Height;
		}

		public void SetPosition(Vector2 position) {
			Camera.Position = position;
		}

		public Rectangle GetDimension() {
			return new Rectangle((int)Camera.Position.X, (int)Camera.Position.Y, Camera.Width, Camera.Height);
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DungeonCrawler.CoreElements.Shop.Enum {
	public enum BuyableSkillTyps {
		SkillGridExtension,
		InventoryExtension,
		BankSlotExtension,
		BankInventoryExtension,
		ItemDropable,
		QualityDropable
	}

	public enum BuyableSkills { 
		SkillGridI,
		SkillGridII,
		SkillGridIII,
		SkillGridIV,
		SkillGridV,
		
		InventoryI,
		InventoryII,
		InventoryIII,
		InventoryIV,
		InventoryV,
		
		BankSlotI,
		BankInventoryI_I,
		BankInventoryI_II,
		BankInventoryI_III,
		BankInventoryI_IV,
		BankInventoryI_V,
		
		BankSlotII,
		BankInventoryII_I,
		BankInventoryII_II,
		BankInventoryII_III,
		BankInventoryII_IV,
		BankInventoryII_V,
		
		BankSlotIII,
		BankInventoryIII_I,
		BankInventoryIII_II,
		BankInventoryIII_III,
		BankInventoryIII_IV,
		BankInventoryIII_V,
		
		BankSlotIV,
		BankInventoryIV_I,
		BankInventoryIV_II,
		BankInventoryIV_III,
		BankInventoryIV_IV,
		BankInventoryIV_V,
		
		BankSlotV,
		BankInventoryV_I,
		BankInventoryV_II,
		BankInventoryV_III,
		BankInventoryV_IV,
		BankInventoryV_V,

		Langschwert,
		Säbel,
		Breitschwert,
		Falchion,
		Kriegsschwert,
		Bastardschwert,
		Gladius,
		Machete,
		Kampfschwert,
		Runenschwert,
		Saif,
		Tulwar,
		Königsklinge,
		Meisterschwert,

		Lederrüstung,
		gehärtetesLeder,
		BeschlagenesLeder,
		Ringpanzer,
		Schuppenpanzer,
		Kettenpanzer,
		Brustpanzer,
		Bänderrüstung,
		Plattenharnisch,
		Feldharnisch,
		Prunkharnisch,
		Vollharnisch,
		LeichtePlattenrüstung,
		SchwerePlattenrüstung,

		Haube,
		Kriegshaube,
		Helm,
		Plattenhelm,
		Krone,
		Basinet,
		Klappvisier,
		Kaskett,
		Großhelm,
		Hundsgugel,
		Stechhelm,
		Zischägge,
		Höllenmaske,
		Archonkrone,

		Stiefel,
		SchwereStiefel,
		Kettenstiefel,
		Sabatons,
		Beinschienen,
		Seidenschuhe,
		Soldatenstiefel,
		Treter,
		SchwereSabatons,
		Schlachtbeinschienen,
		SchwereTreter,
		GepanzerteKettenstiefel,
		SchwereSoldatenstiefel,
		SchwereSchlachtbeinschienen,

		Schild2,
		Schild3,
		Schild4,
		Schild5,
		Schild6,
		Schild7,
		Schild8,
		Schild9,
		Schild10,
		Schild11,
		Schild12,
		Schild13,
		Schild14,
		Schild15,

		MagicWeapon,
		MagicHead,
		MagicBody,
		MagicShoe,
		MagicSchild,

		RareWeapon,
		RareHead,
		RareBody,
		RareShoe,
		RareSchild,

		LegendaryWeapon,
		LegendaryHead,
		LegendaryBody,
		LegendaryShoe,
		LegendarySchild
	}
}

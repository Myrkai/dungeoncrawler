﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DungeonCrawler.CoreElements.Shop.Enum;
using Core.Items.Interface;

namespace DungeonCrawler.CoreElements.Shop {
	public class ShopSkill {
		public bool Buyed { get; set; } //ob dieser erworben wurde oder nicht
		public BuyableSkillTyps Typ { get; private set; } //Zum Filtern
		public BuyableSkills Skill { get; private set; } //Der eigentliche permanente Skill
		public uint Cost { get; private set; }

		public ShopSkill(BuyableSkillTyps typ, BuyableSkills skill, bool buyed, uint cost) {
			this.Typ = typ;
			this.Skill = skill;
			this.Buyed = buyed;
			this.Cost = cost;
		}
	}
}

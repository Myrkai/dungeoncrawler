﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RPG.Items.Implemented_Skills;
using RPG;
using RPG.Items.Enum;
using Core.Items.Interface;

namespace DungeonCrawler.CoreElements.Items.Skills {
	public class CoreAfterBattleSkill : AfterBattleSkill, ISkill  {

		#region Konstruktor
		public CoreAfterBattleSkill(float value, string title, string prefix, string surfix, SkillType type, int minItemLevel, int level, Quality minQuality, string look, string description, List<ItemType> availableItemTyps) 
			: base(value, title, prefix, surfix, type, level, minItemLevel, minQuality, look, description, availableItemTyps) {

		}

		public CoreAfterBattleSkill(float value, string title, string prefix, string surfix, SkillType type, int minItemLevel, int level, Quality minQuality, string look, string description, params ItemType[] availableItemTyps)
			: base(value, title, prefix, surfix, type, level, minItemLevel, minQuality, look, description, availableItemTyps) {

		}

		public CoreAfterBattleSkill(float value, float chance, string title, string prefix, string surfix, SkillType type, int minItemLevel, int level, Quality minQuality, string look, string description, List<ItemType> availableItemTyps)
			: base(value, chance, title, prefix, surfix, type, level, minItemLevel, minQuality, look, description, availableItemTyps) {

		}

		public CoreAfterBattleSkill(float value, float chance, string title, string prefix, string surfix, SkillType type, int minItemLevel, int level, Quality minQuality, string look, string description, params ItemType[] availableItemTyps)
			: base(value, chance, title, prefix, surfix, type, level, minItemLevel, minQuality, look, description, availableItemTyps) {

		}

		public CoreAfterBattleSkill(string title, string prefix, string surfix, SkillType type, int minItemLevel, int level, Quality minQuality, string look, string description, List<ItemType> availableItemTyps)
			: base(title, prefix, surfix, type, level, minItemLevel, minQuality, look, description, availableItemTyps) {

		}

		public CoreAfterBattleSkill(string title, string prefix, string surfix, SkillType type, int minItemLevel, int level, Quality minQuality, string look, string description, params ItemType[] availableItemTyps)
			: base(title, prefix, surfix, type, level, minItemLevel, minQuality, look, description, availableItemTyps) {

		}
		#endregion

		public override void PerformAction(Entity player) {
			//base.PerformAction(player);
		}

		public int GetSkillType() {
			return (int)base.Type;
		}

		public List<Core.Items.Enum.ItemType> GetAvailableItemTyps() {
			List<Core.Items.Enum.ItemType> tmp = new List<Core.Items.Enum.ItemType>();

			foreach (ItemType type in base.AvailableItemTyps) {
				switch (type) {
					case ItemType.Body: tmp.Add(Core.Items.Enum.ItemType.Body); break;
					case ItemType.Head: tmp.Add(Core.Items.Enum.ItemType.Head); break;
					case ItemType.Schild: tmp.Add(Core.Items.Enum.ItemType.Schild); break;
					case ItemType.Shoe: tmp.Add(Core.Items.Enum.ItemType.Shoe); break;
					case ItemType.Weapon: tmp.Add(Core.Items.Enum.ItemType.Weapon); break;
					default: throw new NotSupportedException();
				}
			}

			return tmp;
		}

		public int GetMinQualityType() {
			return (int)MinimalQualityType;
		}

		public string GetPrefix() {
			return Prefix;
		}

		public string GetSurfix() {
			return Surfix;
		}

		public int GetLevel() {
			return Level;
		}

		public int GetAffectedType() {
			return (int)AffectType;
		}

		public new ISkill Clone() {
			return (ISkill)base.Clone();
		}
	}
}

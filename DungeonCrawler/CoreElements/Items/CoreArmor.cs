﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RPG.Items;
using RPG.Items.Enum;
using Core.Items.Interface;
using SharpDX.Toolkit.Graphics;

namespace DungeonCrawler.CoreElements {
	public class CoreArmor : Armor, IArmor {

		public int MinDefence { get; set; }
		public int MaxDefence { get; set; }

		public CoreArmor(int defence, string title, int ilvl, Quality quality, ItemType type, Texture2D texture, string textureAsset, uint cost)
			: base(defence, title, ilvl, quality, type, texture, textureAsset, cost) {
			//hier skill-liste clonen für AvaiableSkills
		}

		public CoreArmor(int minDefence, int maxDefence, string title, int ilvl, Quality quality, ItemType type, Texture2D texture, string textureAsset, uint cost)
			: base(minDefence, title, ilvl, quality, type, texture, textureAsset, cost) {
			//hier skill-liste clonen für AvaiableSkills
			MinDefence = minDefence;
			MaxDefence = maxDefence;
		}

		public new int Defence() {
			return base.Defence;
		}

		public new Core.Items.Enum.ItemType Type() {
			switch (base.Type) {
				case ItemType.Body: return Core.Items.Enum.ItemType.Body;
				case ItemType.Head: return Core.Items.Enum.ItemType.Head;
				case ItemType.Schild: return Core.Items.Enum.ItemType.Schild;
				case ItemType.Shoe: return Core.Items.Enum.ItemType.Shoe;
				case ItemType.Weapon: return Core.Items.Enum.ItemType.Weapon;
				default: throw new NotSupportedException();
			}
		}

		public new Core.Items.Enum.Quality Quality() {
			switch (base.Quality) {
				case RPG.Items.Enum.Quality.Normal: return Core.Items.Enum.Quality.Normal;
				case RPG.Items.Enum.Quality.Magic: return Core.Items.Enum.Quality.Magic;
				case RPG.Items.Enum.Quality.Rare: return Core.Items.Enum.Quality.Rare;
				case RPG.Items.Enum.Quality.Legendary: return Core.Items.Enum.Quality.Legendary;
				default: throw new NotSupportedException();
			}
		}

		public void SetQuality(Core.Items.Enum.Quality quality) {
			switch (quality) {
				case Core.Items.Enum.Quality.Normal: base.Quality = RPG.Items.Enum.Quality.Normal; break;
				case Core.Items.Enum.Quality.Magic: base.Quality = RPG.Items.Enum.Quality.Magic; break;
				case Core.Items.Enum.Quality.Rare: base.Quality = RPG.Items.Enum.Quality.Rare; break;
				case Core.Items.Enum.Quality.Legendary: base.Quality = RPG.Items.Enum.Quality.Legendary; break;
				default: throw new NotSupportedException();
			}
		}

		public new int Itemlevel() {
			return base.Itemlevel;
		}

		public new string Title() {
			return base.Title;
		}

		public new List<ISkill> AvaiableSkills() {
			return base.AvaiableSkills.Select(s => (ISkill)s).ToList();
		}

		public void AddSkill(ISkill skill) {
			base.AddSkill((Skill)skill);
		}

		public new IItem Clone() {
			CoreArmor tmp = new CoreArmor(this.MinDefence, this.MaxDefence, this.Title(), this.Itemlevel(), base.Quality, base.Type, this.Texture, this.TextureAsset, this.Cost);
			tmp.AddSkill(base.AvaiableSkills.ToList());
			tmp.SpecialTitle = this.SpecialTitle;

			return (IItem)tmp;
		}

		public void SetSpecialTitle(string title) {
			base.SpecialTitle = title;
		}

		public void SetDefence(int defence) {
			base.Defence = defence;
		}

		int IArmor.MinDefence() {
			return MinDefence;
		}

		int IArmor.MaxDefence() {
			return MaxDefence;
		}
	}
}

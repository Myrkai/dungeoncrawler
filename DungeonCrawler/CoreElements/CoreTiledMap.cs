﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.Interface;
using SharpDX.Toolkit.Graphics;
using SharpDX.Toolkit;
using SharpDX;

namespace DungeonCrawler.CoreElements {
	public class CoreTiledMap : IMap {
		private TiledMap.TiledMap TiledMap { get; set; }
		private CoreCamera Camera { get; set; }

		public CoreTiledMap(TiledMap.TiledMap map) {
			TiledMap = map;
			Camera = new CoreCamera(map.Cam);
		}

		public List<bool> GetCollisionLayer() {
			return TiledMap.CollisitionLayer.layerInformation;
		}

		public bool IsWalkable(int indexX, int indexY) {
			return TiledMap.CollisitionLayer.GetCell(indexX, indexY);
		}

		public bool IsWalkable(RectangleF rec) {
			int cellWidthPx = MapWidthPx() / GetLayerWidth();
			int cellHeightPx = MapHeightPx() / GetLayerHeight();

			bool topLeft = TiledMap.CollisitionLayer.GetCell((int)(rec.TopLeft.X / cellWidthPx), (int)(rec.TopLeft.Y / cellHeightPx));
			bool topRight = TiledMap.CollisitionLayer.GetCell((int)(rec.TopRight.X / cellWidthPx), (int)(rec.TopRight.Y / cellHeightPx));
			bool bottomLeft = TiledMap.CollisitionLayer.GetCell((int)(rec.BottomLeft.X / cellWidthPx), (int)(rec.BottomLeft.Y / cellHeightPx));
			bool bottomRight = TiledMap.CollisitionLayer.GetCell((int)(rec.BottomRight.X / cellWidthPx), (int)(rec.BottomRight.Y / cellHeightPx));

			return topLeft && topRight && bottomLeft && bottomRight;
		}

		public int GetLayerWidth() {
			return TiledMap.CollisitionLayer.Width;
		}

		public int GetLayerHeight() {
			return TiledMap.CollisitionLayer.Height;
		}

		public void Draw(SpriteBatch spriteBatch) {
			TiledMap.Draw(spriteBatch);
		}

		public void Update(GameTime gameTime) {
			
		}

		public ICamera GetCamera() {
			return Camera;
		}

		public int MapWidthPx() {
			return TiledMap.MapWidthPx;
		}

		public int MapHeightPx() {
			return TiledMap.MapHeightPx;
		}
	}
}

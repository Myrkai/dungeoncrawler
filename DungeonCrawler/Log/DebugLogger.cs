﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SideProjekt.Mechanic.Log;

namespace SideProjekt.Mechanic.Log {
	public class DebugLogger : iLogOutput {

		System.IO.StreamWriter output;

		public void Log(LogEntity l) {
			if (l.Type == LogType.Debug) {
				output.WriteLine(String.Format("{0}: {1}", l.TimeStamp.ToString("yyyy-MM-dd HH:mm:ss.fff"), l.Text));
			}
		}

		public void Init() {
			output = new System.IO.StreamWriter("debug" + DateTime.Now.ToString("yyyy-MM-dd HH_mm_ss.fff") + ".log", true, Encoding.UTF8);
		}

		public void GetAllEntity(List<LogEntity> l) {
			foreach (LogEntity e in l) {
				Log(e);
			}
		}

		public void Dispose() {
			try {
				output.WriteLine("Close Output-Log");
				output.Flush();
				output.Close();
				output.Dispose();
			} catch { 
				// nichts zu tun
			}
		}
	}
}

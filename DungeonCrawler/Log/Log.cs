﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SideProjekt.Mechanic.Log {
	/// <summary>
	/// Der eigentliche Logger.
	/// Hier wird alles eingetragen was man speichern möchte.
	/// Die Ausgabe erfolgt über die jeweilige Schnittstelle, welche sich auch zur Laufzeit ändern lässt.
	/// </summary>
	public static class Log {
		private static List<LogEntity> LogDataBase = new List<LogEntity>();
		private static List<iLogOutput> Outputs = new List<iLogOutput>();

		public static void Add(string text, LogType type) {
			LogEntity l = new LogEntity(text, type);
			LogDataBase.Add(l);
			if (Outputs != null && Outputs.Count > 0) {
				foreach (var output in Outputs) {
					output.Log(l);
				}
			}
		}

		public static void Add(string text) {
			Add(text, LogType.Normal);
		}

		public static void AddOutput(iLogOutput outp) {
			Outputs.Add(outp);
			outp.Init();
			outp.GetAllEntity(LogDataBase);
		}

		public static void Dispose() {
			if (Outputs != null && Outputs.Count > 0) {
				foreach (var output in Outputs) {
					output.Dispose();
				}
			}
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SideProjekt.Mechanic.Log {
	public enum LogType { 
		Normal,
		Info,
		Debug,
		Error,
		Warning
	}
	
	/// <summary>
	/// der jeweilige Log-Eintrag. Wann dieser Log erfolgt wird automatisch generiert.
	/// </summary>
	public class LogEntity {
		public DateTime TimeStamp { get; private set; }
		public string Text { get; private set; }
		public LogType Type { get; private set; }

		public LogEntity(string text, LogType type) {
			TimeStamp = DateTime.Now;
			Text = text;
			Type = type;
		}
	}
}

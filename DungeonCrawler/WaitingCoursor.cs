﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DungeonCrawler {
	public class WaitingCoursor : IDisposable {
		public WaitingCoursor() {
			Cursor.Current = Cursors.WaitCursor;
		}

		public void Dispose() {
			Cursor.Current = Cursors.Default;
		}
	}
}

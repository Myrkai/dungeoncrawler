﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GUI.Screens;
using SharpDX.Toolkit.Graphics;
using SharpDX.Toolkit;
using GUI;
using DungeonCrawler.CoreElements;
using GUI.Controls;
using SharpDX.Toolkit.Content;
using SharpDX;
using RPG.Chars.Enum;
using DungeonCrawler;
using GUI.Interface;
using SideProjekt.Mechanic.Log;

namespace GUI.Screens {
	public class StatusScreen : BaseScreen<Game1> {

		List<Tuple<string, string>> textboxesSources;
		Dictionary<string, Rectangle> textBoxes;
		ContentManager content;
		CorePlayer player;

		ButtonControl AddStrength;
		ButtonControl AddDexterity;
		ButtonControl AddStamina;
		ButtonControl AddLuck;

		public StatusScreen(Game1 game, ScreenManager manager, ContentManager content)
			: base(game, manager) {
			this.content = content;

			textboxesSources = new List<Tuple<string, string>>();
			textboxesSources.Add(new Tuple<string, string>("13,23,404,60", "Name"));
			textboxesSources.Add(new Tuple<string, string>("423,23,625,60", "Klasse"));
			textboxesSources.Add(new Tuple<string, string>("14,77,70,133", "Level"));
			textboxesSources.Add(new Tuple<string, string>("86,78,404,133", "exp"));
			textboxesSources.Add(new Tuple<string, string>("424,79,625,133", "exp next level"));
			textboxesSources.Add(new Tuple<string, string>("15,167,154,204", "stärke title"));
			textboxesSources.Add(new Tuple<string, string>("158,167,267,204", "stärke"));
			textboxesSources.Add(new Tuple<string, string>("16,285,155,322", "Geschick title"));
			textboxesSources.Add(new Tuple<string, string>("159,285,268,322", "Geschick"));
			textboxesSources.Add(new Tuple<string, string>("17,396,156,433", "Ausdauer Title"));
			textboxesSources.Add(new Tuple<string, string>("160,396,269,433", "Ausdauer"));
			textboxesSources.Add(new Tuple<string, string>("17,449,156,486", "Glück Title"));
			textboxesSources.Add(new Tuple<string, string>("160,449,269,486", "Glück"));
			textboxesSources.Add(new Tuple<string, string>("375,167,514,204", "Schaden Title"));
			textboxesSources.Add(new Tuple<string, string>("518,167,627,204", "Schaden"));
			textboxesSources.Add(new Tuple<string, string>("375,220,514,257", "Waffentitel"));
			textboxesSources.Add(new Tuple<string, string>("518,220,627,257", "Waffe"));
			textboxesSources.Add(new Tuple<string, string>("376,286,515,323", "Rüstungswert title"));
			textboxesSources.Add(new Tuple<string, string>("519,286,628,323", "Rüstung"));
			textboxesSources.Add(new Tuple<string, string>("376,338,515,375", "defence title"));
			textboxesSources.Add(new Tuple<string, string>("519,338,628,375", "defence"));
			textboxesSources.Add(new Tuple<string, string>("377,397,516,434", "hp title"));
			textboxesSources.Add(new Tuple<string, string>("520,397,629,434", "hp"));
			textboxesSources.Add(new Tuple<string, string>("377,449,516,486", "kritische Treffer Title"));
			textboxesSources.Add(new Tuple<string, string>("520,449,629,486", "kritische Treffer"));
			textboxesSources.Add(new Tuple<string, string>("16,503,156,540", "stat point Title"));
			textboxesSources.Add(new Tuple<string, string>("160,503,269,540", "stat point"));

			textBoxes = new Dictionary<string, Rectangle>();
			foreach (Tuple<string, string> box in textboxesSources) {
				int[] cord = box.Item1.Split(',').Select(b => Convert.ToInt32(b)).ToArray();
				textBoxes.Add(box.Item2, new Rectangle((int)(cord[0] * (Resolution.Scale - Resolution.ScaleAddon4_3)), (int)(cord[1] * (Resolution.Scale - Resolution.ScaleAddon4_3)), (int)((cord[2] - cord[0]) * (Resolution.Scale - Resolution.ScaleAddon4_3)), (int)((cord[3] - cord[1]) * (Resolution.Scale - Resolution.ScaleAddon4_3))));
			}

			Texture2D buttonTexture = TextureHelper.CreateTextureWidthBoarderAndSmooth(35, 35, Color.DarkRed, 1, Color.White, 4, false, GameRef.GraphicsDevice);
			AddStrength = new ButtonControl("+", Vector2.Zero, content.Load<SpriteFont>(@"Fonts\Areal16.tk"), buttonTexture, Color.White, () => { if (player.StatPoints > 0) { player.AddStat(StatType.BaseStrength, 1); player.RefreshAllStatValue(); player.StatPoints -= 1; RefreshTextboxes(); } });
			AddStrength.SetPosition(textBoxes["stärke"].TopRight.ToVector2() + new Vector2(5f, textBoxes["stärke"].Size.Height / 2 - buttonTexture.Height / 2));
			AddCustomControl("StrButton", AddStrength, false);

			AddDexterity = new ButtonControl("+", Vector2.Zero, content.Load<SpriteFont>(@"Fonts\Areal16.tk"), buttonTexture, Color.White, () => { if (player.StatPoints > 0) { player.AddStat(StatType.BaseDexterity, 1); player.RefreshAllStatValue(); player.StatPoints -= 1; RefreshTextboxes(); } });
			AddDexterity.SetPosition(textBoxes["Geschick"].TopRight.ToVector2() + new Vector2(5f, textBoxes["Geschick"].Size.Height / 2 - buttonTexture.Height / 2));
			AddCustomControl("DexButton", AddDexterity, false);

			AddStamina = new ButtonControl("+", Vector2.Zero, content.Load<SpriteFont>(@"Fonts\Areal16.tk"), buttonTexture, Color.White, () => { if (player.StatPoints > 0) { player.AddStat(StatType.BaseStamina, 1); player.RefreshAllStatValue(); player.StatPoints -= 1; RefreshTextboxes(); } });
			AddStamina.SetPosition(textBoxes["Ausdauer"].TopRight.ToVector2() + new Vector2(5f, textBoxes["Ausdauer"].Size.Height / 2 - buttonTexture.Height / 2));
			AddCustomControl("StamButton", AddStamina, false);

			AddLuck = new ButtonControl("+", Vector2.Zero, content.Load<SpriteFont>(@"Fonts\Areal16.tk"), buttonTexture, Color.White, () => { if (player.StatPoints > 0) { player.AddStat(StatType.BaseLuck, 1); player.RefreshAllStatValue(); player.StatPoints -= 1; RefreshTextboxes(); } });
			AddLuck.SetPosition(textBoxes["Glück"].TopRight.ToVector2() + new Vector2(5f, textBoxes["Glück"].Size.Height / 2 - buttonTexture.Height / 2));
			AddCustomControl("LuckButton", AddLuck, false);
		}

		public override void Draw(SpriteBatch spriteBatch) {
			if (IsActiv && PerformDraw) {
				foreach (IControl control in CustomControls.Where(c => c.Item3).Select(c => c.Item2)) {
					control.Draw(spriteBatch);
				}

				foreach (IControl control in CustomControls.Where(c => !c.Item3).Select(c => c.Item2)) {
					control.Draw(spriteBatch);
				}
			}
		}

		public override void Update(GameTime gameTime) {
			if (IsActiv && PerformUpdate) {
				ObjektManager.Update(gameTime);
				foreach (IControl control in CustomControls.Select(c => c.Item2)) {
					control.Update(gameTime);
				}
			}
		}

		public override void Initialize() {

		}

		public override void LoadContent() {

		}

		public void CreateTextboxes(CorePlayer player) {
			this.player = player;

			AddCustomControl("Name", CreateTextControl(textBoxes, player.Name, "Name", "20", true), false);
			AddCustomControl("Klasse", CreateTextControl(textBoxes, player.ClassType.Title, "Klasse", "20", true), false);
			AddCustomControl("LevelTitle", CreateTextControl(textBoxes, "Level", "Level", "9", true, -17), false);
			AddCustomControl("Level", CreateTextControl(textBoxes, player.Level.ToString(), "Level", "22", true, 6), false);
			AddCustomControl("EXPTitle", CreateTextControl(textBoxes, "Erfahrungspunkte", "exp", "9", true, -17), false);
			AddCustomControl("EXP", CreateTextControl(textBoxes, string.Format("{0:n0}", player.GetCurrentEXP()), "exp", "22", true, 6), false);
			AddCustomControl("NextEXPTitle", CreateTextControl(textBoxes, "nächstes Level", "exp next level", "9", true, -17), false);
			AddCustomControl("NextEXP", CreateTextControl(textBoxes, string.Format("{0:n0}", player.GetNextLevelEXP()), "exp next level", "22", true, 6), false);
			AddCustomControl("stärke title", CreateTextControl(textBoxes, "Stärke", "stärke title", "16", true), false);
			AddCustomControl("stärke", CreateTextControl(textBoxes, player.Stats[StatType.Strength].ToString(), "stärke", "16", true), false);
			AddCustomControl("Geschick title", CreateTextControl(textBoxes, "Geschick", "Geschick title", "16", true), false);
			AddCustomControl("Geschick", CreateTextControl(textBoxes, player.Stats[StatType.Dexterity].ToString(), "Geschick", "16", true), false);
			AddCustomControl("Ausdauer Title", CreateTextControl(textBoxes, "Ausdauer", "Ausdauer Title", "16", true), false);
			AddCustomControl("Ausdauer", CreateTextControl(textBoxes, player.Stats[StatType.Stamina].ToString(), "Ausdauer", "16", true), false);
			AddCustomControl("Glück Title", CreateTextControl(textBoxes, "Glück", "Glück Title", "16", true), false);
			AddCustomControl("Glück", CreateTextControl(textBoxes, player.Stats[StatType.Luck].ToString(), "Glück", "16", true), false);
			AddCustomControl("Schaden Title", CreateTextControl(textBoxes, "Schaden", "Schaden Title", "10", true), false);
			AddCustomControl("Schaden", CreateTextControl(textBoxes, player.Stats[StatType.MinDamage].ToString() + " - " + player.Stats[StatType.MaxDamage].ToString(), "Schaden", "10", true), false);
			AddCustomControl("Waffentitel", CreateTextControl(textBoxes, "Waffenschaden", "Waffentitel", "10", true), false);
			AddCustomControl("Waffe", CreateTextControl(textBoxes, player.Stats[StatType.MinWeaponDmg].ToString() + " - " + player.Stats[StatType.MaxWeaponDmg].ToString(), "Waffe", "10", true), false);
			AddCustomControl("Rüstungswert title", CreateTextControl(textBoxes, "Rüstungswert", "Rüstungswert title", "10", true), false);
			AddCustomControl("Rüstung", CreateTextControl(textBoxes, player.Stats[StatType.DefenceValue].ToString(), "Rüstung", "10", true), false);
			AddCustomControl("defence title", CreateTextControl(textBoxes, "Rüstung", "defence title", "10", true), false);
			AddCustomControl("defence", CreateTextControl(textBoxes, player.Stats[StatType.Defence].ToString(), "defence", "10", true), false);
			AddCustomControl("hp title", CreateTextControl(textBoxes, "Leben", "hp title", "10", true), false);
			AddCustomControl("hp", CreateTextControl(textBoxes, String.Format("{0:n0}", player.Stats[StatType.HP]), "hp", "10", true), false);
			AddCustomControl("kritische Treffer Title", CreateTextControl(textBoxes, "kritische Trefferchance", "kritische Treffer Title", "8", true), false);
			AddCustomControl("kritische Treffer", CreateTextControl(textBoxes, player.Stats[StatType.CriticalHitChance].ToString() + "%", "kritische Treffer", "10", true), false);
			AddCustomControl("stat point Title", CreateTextControl(textBoxes, "verbleibende Statuspunkte", "stat point Title", "8", true, 0, Color.Red), false);
			AddCustomControl("stat point", CreateTextControl(textBoxes, player.StatPoints.ToString(), "stat point", "16", true), false);

			if (player.StatPoints > 0) {
				CustomControls.Single(s => s.Item1 == "StrButton").Item2.SetIsEnable(true);
				CustomControls.Single(s => s.Item1 == "DexButton").Item2.SetIsEnable(true);
				CustomControls.Single(s => s.Item1 == "StamButton").Item2.SetIsEnable(true);
				CustomControls.Single(s => s.Item1 == "LuckButton").Item2.SetIsEnable(true);
			} else {
				CustomControls.Single(s => s.Item1 == "StrButton").Item2.SetIsEnable(false);
				CustomControls.Single(s => s.Item1 == "DexButton").Item2.SetIsEnable(false);
				CustomControls.Single(s => s.Item1 == "StamButton").Item2.SetIsEnable(false);
				CustomControls.Single(s => s.Item1 == "LuckButton").Item2.SetIsEnable(false);
			}
		}

		public void RefreshTextboxes() {
			CreateTextboxes(player);

			Log.Add("In StatusScreen", LogType.Debug);
			foreach (var item in player.UsedItems.Where(s => s.Value != null)) {
				Log.Add("UsedItem: " + item.ToString(), LogType.Debug);
			}

			foreach (var item in player.Inventory.Items.Select(s => s.Item)) {
				Log.Add("InvItem: " + item.ToString(), LogType.Debug);
			}
		}

		private LinkControl CreateTextControl(Dictionary<string, Rectangle> textBoxes, string text, string key, string fontHeight, bool centerName, int borderTop = 0) {
			float widthAddon = 3;
			float heightAddon = 3 + borderTop;
			SpriteFont font = content.Load<SpriteFont>(@"Fonts\Areal" + fontHeight + ".tk");

			if (centerName) {
				widthAddon = textBoxes[key].Width / 2f - font.MeasureString(text).X / 2f;
				heightAddon = textBoxes[key].Height / 2f - font.MeasureString(text).Y / 2f + borderTop;
			}

			LinkControl textControl;
			textControl = new LinkControl(text, textBoxes[key].TopLeft.ToVector2() + new Vector2(widthAddon, heightAddon), font, Color.White, null);
			return textControl;
		}

		private LinkControl CreateTextControl(Dictionary<string, Rectangle> textBoxes, string text, string key, string fontHeight, bool centerName, int borderTop, Color color) {
			float widthAddon = 3;
			float heightAddon = 3 + borderTop;
			SpriteFont font = content.Load<SpriteFont>(@"Fonts\Areal" + fontHeight + ".tk");

			if (centerName) {
				widthAddon = textBoxes[key].Width / 2f - font.MeasureString(text).X / 2f;
				heightAddon = textBoxes[key].Height / 2f - font.MeasureString(text).Y / 2f + borderTop;
			}

			LinkControl textControl;
			textControl = new LinkControl(text, textBoxes[key].TopLeft.ToVector2() + new Vector2(widthAddon, heightAddon), font, color, null);
			return textControl;
		}
	}
}

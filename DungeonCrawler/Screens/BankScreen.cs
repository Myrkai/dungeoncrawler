﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DungeonCrawler;
using SharpDX.Toolkit;
using SharpDX.Toolkit.Graphics;
using DungeonCrawler.CoreElements;
using GUI.Interface;
using SharpDX;
using DungeonCrawler.Controls;
using RPG.Items.Inventory;
using RPG.Items;
using DungeonCrawler.CoreElements.Shop.Enum;
using GUI.Controls;
using DungeonCrawler.CoreElements.Shop;

namespace GUI.Screens {
	public class BankScreen : BaseScreen<Game1> {

		private CorePlayer Player { get; set; }
		private InventoryControl PlayerInventory { get; set; }
		private InventoryControl CurrentBankInventory { get; set; }
		private Dictionary<string, InventoryControl> BankInventorys { get; set; }

		private ButtonControl BankSlot1 { get; set; }
		private ButtonControl BankSlot2 { get; set; }
		private ButtonControl BankSlot3 { get; set; }
		private ButtonControl BankSlot4 { get; set; }
		private ButtonControl BankSlot5 { get; set; }

		private Vector2 Position { get; set; }

		private InventoryPosition CurrentDragedItem { get; set; }
		private Vector2 CurrentDragedPosition { get; set; }

		public BankScreen(Game1 game, ScreenManager manager, CorePlayer player)
			: base(game, manager) {
			Player = player;
			PlayerInventory = (InventoryControl)Player.Inventory;
			AddCustomControl("PlayerInventory", PlayerInventory, false);

			Texture2D buttonTexture = TextureHelper.CreateTextureWidthBoarder(100, 40, Color.White, 2, Color.Black, GameRef.GraphicsDevice);
			BankSlot1 = new ButtonControl("Fach 1", Vector2.Zero, GameRef.Content.Load<SpriteFont>(@"Fonts\Areal14.tk"), buttonTexture, Color.White, null, Color.Blue);
			BankSlot1.ObjectForButton = BuyableSkills.BankSlotI.ToString();
			BankSlot1.SetIsEnable(false);
			BankSlot2 = new ButtonControl("Fach 2", Vector2.Zero, GameRef.Content.Load<SpriteFont>(@"Fonts\Areal14.tk"), buttonTexture, Color.White, null, Color.Gray);
			BankSlot2.ObjectForButton = BuyableSkills.BankSlotII.ToString();
			BankSlot2.SetIsEnable(false);
			BankSlot3 = new ButtonControl("Fach 3", Vector2.Zero, GameRef.Content.Load<SpriteFont>(@"Fonts\Areal14.tk"), buttonTexture, Color.White, null, Color.Gray);
			BankSlot3.ObjectForButton = BuyableSkills.BankSlotIII.ToString();
			BankSlot3.SetIsEnable(false);
			BankSlot4 = new ButtonControl("Fach 4", Vector2.Zero, GameRef.Content.Load<SpriteFont>(@"Fonts\Areal14.tk"), buttonTexture, Color.White, null, Color.Gray);
			BankSlot4.ObjectForButton = BuyableSkills.BankSlotIV.ToString();
			BankSlot4.SetIsEnable(false);
			BankSlot5 = new ButtonControl("Fach 5", Vector2.Zero, GameRef.Content.Load<SpriteFont>(@"Fonts\Areal14.tk"), buttonTexture, Color.White, null, Color.Gray);
			BankSlot5.ObjectForButton = BuyableSkills.BankSlotV.ToString();
			BankSlot5.SetIsEnable(false);

			BankInventorys = new Dictionary<string, InventoryControl>();
			foreach (KeyValuePair<string, Inventory> inv in Player.Owner.BankInventories) {
				BankInventorys.Add(inv.Key, (InventoryControl)inv.Value);

				// "leere" Klick-Actions ermöglichen die Eventauslösung ButtonClicked
				switch (inv.Key) {
					case "BankSlotI":
						BankSlot1.SetOnClickEvent(() => { });
						BankSlot1.SetIsEnable(true);
						break;
					case "BankSlotII":
						BankSlot2.SetOnClickEvent(() => { });
						BankSlot2.SetIsEnable(true);
						break;
					case "BankSlotIII":
						BankSlot3.SetOnClickEvent(() => { });
						BankSlot3.SetIsEnable(true);
						break;
					case "BankSlotIV":
						BankSlot4.SetOnClickEvent(() => { });
						BankSlot4.SetIsEnable(true);
						break;
					case "BankSlotV":
						BankSlot5.SetOnClickEvent(() => { });
						BankSlot5.SetIsEnable(true);
						break;
					default:
						throw new NotImplementedException(inv.Key);
				}
			}

			// Es wird davon ausgegangen, das wenn der Bank Screen aufgerufen wird immer mindestens ein Bankfach vorhanden ist
			if (BankInventorys.Count > 0) {
				KeyValuePair<string, InventoryControl> tmp = BankInventorys.First();
				SwitchBankSlot(tmp.Key);
			}

			AddCustomControl("buttonSlot1", BankSlot1, false);
			AddCustomControl("buttonSlot2", BankSlot2, false);
			AddCustomControl("buttonSlot3", BankSlot3, false);
			AddCustomControl("buttonSlot4", BankSlot4, false);
			AddCustomControl("buttonSlot5", BankSlot5, false);

			Position = Vector2.Zero;
		}

		public override void Draw(SpriteBatch spriteBatch) {
			if (IsActiv && PerformDraw) {
				foreach (IControl control in CustomControls.Where(c => c.Item3).Select(c => c.Item2)) {
					control.Draw(spriteBatch);
				}

				foreach (IControl control in CustomControls.Where(c => !c.Item3).Select(c => c.Item2)) {
					control.Draw(spriteBatch);
				}

				//werden immer über allem anderen gezeichnet
				if (currentInvToolTip != null) {
					currentInvToolTip.Draw(spriteBatch);
				}

				if (CurrentDragedItem != null) {
					spriteBatch.Draw(CurrentDragedItem.back, CurrentDragedItem.Position + CurrentDragedPosition, Color.White * 0.45f);
					spriteBatch.Draw(CurrentDragedItem.Item.Texture, CurrentDragedItem.Position + CurrentDragedPosition, Color.White);
				}
			}
		}

		public override void Update(GameTime gameTime) {
			if (IsActiv && PerformUpdate) {
				ObjektManager.Update(gameTime);
				foreach (IControl control in CustomControls.Select(c => c.Item2)) {
					control.Update(gameTime);
				}

				if (currentInvToolTip != null) {
					currentInvToolTip.Position = VirtualGamePad.GetClickPoint() - new Vector2(currentInvToolTip.GetDimension().Width / 2, -10);
					if (currentInvToolTip.GetDimension().BottomLeft.Y > Resolution.TargetHeight) {
						currentInvToolTip.Position = VirtualGamePad.GetClickPoint() - new Vector2(currentInvToolTip.GetDimension().Width / 2, currentInvToolTip.GetDimension().Height + 10);
					}
				}
			}
		}

		#region PlayerInventoryEvents
		void PlayerInventory_ItemMouseHoverEnd(object sender, InventoryEvent e) {
			currentInvToolTip = null;
		}

		void PlayerInventory_ItemMouseHoverBegin(object sender, InventoryEvent e) {
			if (CurrentDragedItem == null) {
				SetInvToolTip(e.Item);
			}
		}

		void PlayerInventory_ItemEndDrag(object sender, InventoryEvent e) {
			CurrentDragedItem = null;

			//prüfen ob item über bankinventar ist
			if (CurrentBankInventory.GetActiveDimension().Intersects((e.Position + PlayerInventory.Position).ToRectangle())) {
				currentInvToolTip = null;
				CurrentDragedItem = null;

				//prüfen ob bankplatz belegt ist
					//wenn ja austausch vornehmen
						//prüfen ob austausch möglich
							//wenn ja, denn nehme ihn vor
							//wenn nein, tue nix
					//wenn nein prüfen ob item hinzugefügt werden kann
						//wenn ja füge es hinzu
						//wenn nein tue nix

				InventoryPosition bankItem = CurrentBankInventory.GetItemInventoryPosition(e.Position + PlayerInventory.Position);
				if (bankItem != null) {
					if (bankItem.Item.Type == e.Item.Type) {//bei gleichem Typ kann der Austausch immer vorgenommen werden
						CurrentBankInventory.RemoveItem(bankItem.Item);
						PlayerInventory.RemoveItem(e.Item);

						bool bankResult = CurrentBankInventory.AddItem(e.Item, bankItem.Position + CurrentBankInventory.Position);
						bool playerResult = PlayerInventory.AddItem(bankItem.Item, e.OldPosition + PlayerInventory.Position);

						if (!bankResult || !playerResult) {
							throw new Exception("BankScreen - Itemaustausch fehlgeschlagen");
						}
					} else { //prüfen ob trotzdem Platz ist und der Austausch funktionieren kann
						CurrentBankInventory.RemoveItem(bankItem.Item);
						PlayerInventory.RemoveItem(e.Item);

						bool bankResult = CurrentBankInventory.AddItem(e.Item, bankItem.Position + CurrentBankInventory.Position);
						bool playerResult = PlayerInventory.AddItem(bankItem.Item, e.OldPosition + PlayerInventory.Position);

						if (!bankResult || !playerResult) {
							if (bankResult) {
								CurrentBankInventory.RemoveItem(e.Item);
							}

							if (playerResult) {
								PlayerInventory.RemoveItem(bankItem.Item);
							}

							bool newBankResult = CurrentBankInventory.AddItem(bankItem.Item, bankItem.Position + CurrentBankInventory.Position);
							bool newPlayerResult = PlayerInventory.AddItem(e.Item, e.OldPosition + PlayerInventory.Position);

							if (!newBankResult || !newPlayerResult) {
								throw new Exception("BankScreen - Itemaustausch fehlgeschlagen");
							}
						}
					}
				} else {
					PlayerInventory.RemoveItem(e.Item);
					bool bankResult = CurrentBankInventory.AddItem(e.Item, e.Position + PlayerInventory.Position);

					if (!bankResult) {
						bool newPlayerResult = PlayerInventory.AddItem(e.Item, e.OldPosition + PlayerInventory.Position);

						if (!newPlayerResult) {
							throw new Exception("BankScreen - Itemaustausch fehlgeschlagen");
						}
					}
				}
			}
		}

		void PlayerInventory_ItemDoubleClicked(object sender, InventoryEvent e) {
			currentInvToolTip = null;
			CurrentDragedItem = null;

			if (CurrentBankInventory.AddItem(e.Item)) {
				PlayerInventory.RemoveItem(e.Item);
			}
		}

		void PlayerInventory_ItemBeginDrag(object sender, InventoryEvent e) {
			CurrentDragedItem = e.InventoryPosition;
			CurrentDragedPosition = PlayerInventory.Position;
			currentInvToolTip = null;
		}
		#endregion

		#region BankInventoryEvents
		void CurrentBankInventory_ItemMouseHoverEnd(object sender, InventoryEvent e) {
			currentInvToolTip = null;
		}

		void CurrentBankInventory_ItemMouseHoverBegin(object sender, InventoryEvent e) {
			if (CurrentDragedItem == null) {
				SetInvToolTip(e.Item);
			}
		}

		void CurrentBankInventory_ItemEndDrag(object sender, InventoryEvent e) {
			CurrentDragedItem = null;

			//prüfen ob item über bankinventar ist
			if (PlayerInventory.GetActiveDimension().Intersects((e.Position + CurrentBankInventory.Position).ToRectangle())) {
				currentInvToolTip = null;
				CurrentDragedItem = null;

				//prüfen ob bankplatz belegt ist
					//wenn ja austausch vornehmen
						//prüfen ob austausch möglich
							//wenn ja, denn nehme ihn vor
							//wenn nein, tue nix
					//wenn nein prüfen ob item hinzugefügt werden kann
						//wenn ja füge es hinzu
						//wenn nein tue nix

				InventoryPosition bankItem = PlayerInventory.GetItemInventoryPosition(e.Position + CurrentBankInventory.Position);
				if (bankItem != null) {
					if (bankItem.Item.Type == e.Item.Type) {//bei gleichem Typ kann der Austausch immer vorgenommen werden
						PlayerInventory.RemoveItem(bankItem.Item);
						CurrentBankInventory.RemoveItem(e.Item);

						bool bankResult = PlayerInventory.AddItem(e.Item, bankItem.Position + PlayerInventory.Position);
						bool playerResult = CurrentBankInventory.AddItem(bankItem.Item, e.OldPosition + CurrentBankInventory.Position);

						if (!bankResult || !playerResult) {
							throw new Exception("BankScreen - Itemaustausch fehlgeschlagen");
						}
					} else { //prüfen ob trotzdem Platz ist und der Austausch funktionieren kann
						PlayerInventory.RemoveItem(bankItem.Item);
						CurrentBankInventory.RemoveItem(e.Item);

						bool bankResult = PlayerInventory.AddItem(e.Item, bankItem.Position + PlayerInventory.Position);
						bool playerResult = CurrentBankInventory.AddItem(bankItem.Item, e.OldPosition + CurrentBankInventory.Position);

						if (!bankResult || !playerResult) {
							if (playerResult) {
								PlayerInventory.RemoveItem(e.Item);
							}

							if (bankResult) {
								CurrentBankInventory.RemoveItem(bankItem.Item);
							}

							bool newBankResult = PlayerInventory.AddItem(bankItem.Item, bankItem.Position + PlayerInventory.Position);
							bool newPlayerResult = CurrentBankInventory.AddItem(e.Item, e.OldPosition + CurrentBankInventory.Position);

							if (!newBankResult || !newPlayerResult) {
								throw new Exception("BankScreen - Itemaustausch fehlgeschlagen");
							}
						}
					}
				} else {
					CurrentBankInventory.RemoveItem(e.Item);
					bool bankResult = PlayerInventory.AddItem(e.Item, e.Position + CurrentBankInventory.Position);

					if (!bankResult) {
						bool newPlayerResult = CurrentBankInventory.AddItem(e.Item, e.OldPosition + CurrentBankInventory.Position);

						if (!newPlayerResult) {
							throw new Exception("BankScreen - Itemaustausch fehlgeschlagen");
						}
					}
				}
			}
		}

		void CurrentBankInventory_ItemDoubleClicked(object sender, InventoryEvent e) {
			currentInvToolTip = null;
			CurrentDragedItem = null;

			if (PlayerInventory.AddItem(e.Item)) {
				CurrentBankInventory.RemoveItem(e.Item);
			}
		}

		void CurrentBankInventory_ItemBeginDrag(object sender, InventoryEvent e) {
			CurrentDragedItem = e.InventoryPosition;
			CurrentDragedPosition = CurrentBankInventory.Position;
			currentInvToolTip = null;
		}
		#endregion

		#region BankSlotEvents
		void BankSlot5_ButtonClicked(object sender, EventArgs e) {
			SwitchBankSlot((string)BankSlot5.GetObjectFromElement());
		}

		void BankSlot4_ButtonClicked(object sender, EventArgs e) {
			SwitchBankSlot((string)BankSlot4.GetObjectFromElement());
		}

		void BankSlot3_ButtonClicked(object sender, EventArgs e) {
			SwitchBankSlot((string)BankSlot3.GetObjectFromElement());
		}

		void BankSlot2_ButtonClicked(object sender, EventArgs e) {
			SwitchBankSlot((string)BankSlot2.GetObjectFromElement());
		}

		void BankSlot1_ButtonClicked(object sender, EventArgs e) {
			SwitchBankSlot((string)BankSlot1.GetObjectFromElement());
		}
		#endregion

		#region tooltip
		Dictionary<Item, ToolTipText> InvToolTips = new Dictionary<Item, ToolTipText>();
		ToolTipText currentInvToolTip;
		private void SetInvToolTip(Item item) {
			if (InvToolTips.ContainsKey(item)) {
				currentInvToolTip = InvToolTips[item];
			} else {
				InvToolTips.Add(item, new ToolTipText(item, Vector2.Zero, GameRef.Content.Load<SpriteFont>(@"Fonts\Areal11.tk"), false));
				currentInvToolTip = InvToolTips[item];
			}
		}
		#endregion

		public override void Initialize() {

		}

		public override void LoadContent() {

		}

		public override void AddCustomControl(string key, IControl control, bool renderingFirst) {
			base.AddCustomControl(key, control, renderingFirst);

			if (key == "background") {
				Position = new Vector2(control.GetDimension().X, control.GetDimension().Y);
				PlayerInventory.Position = new Vector2(Position.X + (control.GetDimension().Width / 2 - PlayerInventory.GetDimension().Width / 2), control.GetDimension().Height - PlayerInventory.GetDimension().Height - 10f);

				BankSlot1.SetPosition(new Vector2(Position.X + 10f, Position.Y + 10f));
				BankSlot2.SetPosition(new Vector2(BankSlot1.GetDimension().TopRight.X + 5f, Position.Y + 10f));
				BankSlot3.SetPosition(new Vector2(BankSlot2.GetDimension().TopRight.X + 5f, Position.Y + 10f));
				BankSlot4.SetPosition(new Vector2(BankSlot3.GetDimension().TopRight.X + 5f, Position.Y + 10f));
				BankSlot5.SetPosition(new Vector2(BankSlot4.GetDimension().TopRight.X + 5f, Position.Y + 10f));

				if (CurrentBankInventory != null) {
					CurrentBankInventory.Position = new Vector2(Position.X + (base.GetCustomControl("background").GetDimension().Width / 2 - CurrentBankInventory.GetDimension().Width / 2), 60f);
				}
			}
		}

		public void Reset(CorePlayer player) {
			this.Player = player;
			InvToolTips.Clear();

			Vector2 playerPos = PlayerInventory.Position;
			PlayerInventory = (InventoryControl)Player.Inventory;
			AddCustomControl("PlayerInventory", PlayerInventory, false);
			PlayerInventory.Position = playerPos;

			Texture2D buttonTexture = TextureHelper.CreateTextureWidthBoarder(100, 40, Color.White, 2, Color.Black, GameRef.GraphicsDevice);
			BankSlot1 = new ButtonControl("Fach 1", Vector2.Zero, GameRef.Content.Load<SpriteFont>(@"Fonts\Areal14.tk"), buttonTexture, Color.White, null, Color.Blue);
			BankSlot1.ObjectForButton = BuyableSkills.BankSlotI.ToString();
			BankSlot1.SetIsEnable(false);
			BankSlot2 = new ButtonControl("Fach 2", Vector2.Zero, GameRef.Content.Load<SpriteFont>(@"Fonts\Areal14.tk"), buttonTexture, Color.White, null, Color.Gray);
			BankSlot2.ObjectForButton = BuyableSkills.BankSlotII.ToString();
			BankSlot2.SetIsEnable(false);
			BankSlot3 = new ButtonControl("Fach 3", Vector2.Zero, GameRef.Content.Load<SpriteFont>(@"Fonts\Areal14.tk"), buttonTexture, Color.White, null, Color.Gray);
			BankSlot3.ObjectForButton = BuyableSkills.BankSlotIII.ToString();
			BankSlot3.SetIsEnable(false);
			BankSlot4 = new ButtonControl("Fach 4", Vector2.Zero, GameRef.Content.Load<SpriteFont>(@"Fonts\Areal14.tk"), buttonTexture, Color.White, null, Color.Gray);
			BankSlot4.ObjectForButton = BuyableSkills.BankSlotIV.ToString();
			BankSlot4.SetIsEnable(false);
			BankSlot5 = new ButtonControl("Fach 5", Vector2.Zero, GameRef.Content.Load<SpriteFont>(@"Fonts\Areal14.tk"), buttonTexture, Color.White, null, Color.Gray);
			BankSlot5.ObjectForButton = BuyableSkills.BankSlotV.ToString();
			BankSlot5.SetIsEnable(false);

			BankInventorys = new Dictionary<string, InventoryControl>();
			foreach (KeyValuePair<string, Inventory> inv in Player.Owner.BankInventories) {
				BankInventorys.Add(inv.Key, (InventoryControl)inv.Value);

				// "leere" Klick-Actions ermöglichen die Eventauslösung ButtonClicked
				switch (inv.Key) {
					case "BankSlotI":
						BankSlot1.SetOnClickEvent(() => { });
						BankSlot1.SetIsEnable(true);
						break;
					case "BankSlotII":
						BankSlot2.SetOnClickEvent(() => { });
						BankSlot2.SetIsEnable(true);
						break;
					case "BankSlotIII":
						BankSlot3.SetOnClickEvent(() => { });
						BankSlot3.SetIsEnable(true);
						break;
					case "BankSlotIV":
						BankSlot4.SetOnClickEvent(() => { });
						BankSlot4.SetIsEnable(true);
						break;
					case "BankSlotV":
						BankSlot5.SetOnClickEvent(() => { });
						BankSlot5.SetIsEnable(true);
						break;
					default:
						throw new NotImplementedException(inv.Key);
				}
			}

			// Es wird davon ausgegangen, das wenn der Bank Screen aufgerufen wird immer mindestens ein Bankfach vorhanden ist
			if (BankInventorys.Count > 0) {
				KeyValuePair<string, InventoryControl> tmp = BankInventorys.First();
				SwitchBankSlot(tmp.Key);
			}

			AddCustomControl("buttonSlot1", BankSlot1, false);
			AddCustomControl("buttonSlot2", BankSlot2, false);
			AddCustomControl("buttonSlot3", BankSlot3, false);
			AddCustomControl("buttonSlot4", BankSlot4, false);
			AddCustomControl("buttonSlot5", BankSlot5, false);

			BankSlot1.SetPosition(new Vector2(Position.X + 10f, Position.Y + 10f));
			BankSlot2.SetPosition(new Vector2(BankSlot1.GetDimension().TopRight.X + 5f, Position.Y + 10f));
			BankSlot3.SetPosition(new Vector2(BankSlot2.GetDimension().TopRight.X + 5f, Position.Y + 10f));
			BankSlot4.SetPosition(new Vector2(BankSlot3.GetDimension().TopRight.X + 5f, Position.Y + 10f));
			BankSlot5.SetPosition(new Vector2(BankSlot4.GetDimension().TopRight.X + 5f, Position.Y + 10f));

			if (CurrentBankInventory != null) {
				CurrentBankInventory.Position = new Vector2(Position.X + (base.GetCustomControl("background").GetDimension().Width / 2 - CurrentBankInventory.GetDimension().Width / 2), 60f);
			}

			CreateEvents();
			RemoveEvents();
		}

		#region EventHandling
		public void SwitchBankSlot(string key) {
			if (CurrentBankInventory != null) {
				CurrentBankInventory.ItemBeginDrag -= CurrentBankInventory_ItemBeginDrag;
				CurrentBankInventory.ItemDoubleClicked -= CurrentBankInventory_ItemDoubleClicked;
				CurrentBankInventory.ItemEndDrag -= CurrentBankInventory_ItemEndDrag;
				CurrentBankInventory.ItemMouseHoverBegin -= CurrentBankInventory_ItemMouseHoverBegin;
				CurrentBankInventory.ItemMouseHoverEnd -= CurrentBankInventory_ItemMouseHoverEnd;

				RemoveCustomControl(CurrentBankInventory);
			}

			CurrentBankInventory = BankInventorys[key];
			if (base.CustomControls.Where(c => c.Item1 == "background").Count() > 0) {
				CurrentBankInventory.Position = new Vector2(Position.X + (base.GetCustomControl("background").GetDimension().Width / 2 - CurrentBankInventory.GetDimension().Width / 2), 60f);
			}

			CurrentBankInventory.ItemBeginDrag += new EventHandler<InventoryEvent>(CurrentBankInventory_ItemBeginDrag);
			CurrentBankInventory.ItemDoubleClicked += new EventHandler<InventoryEvent>(CurrentBankInventory_ItemDoubleClicked);
			CurrentBankInventory.ItemEndDrag += new EventHandler<InventoryEvent>(CurrentBankInventory_ItemEndDrag);
			CurrentBankInventory.ItemMouseHoverBegin += new EventHandler<InventoryEvent>(CurrentBankInventory_ItemMouseHoverBegin);
			CurrentBankInventory.ItemMouseHoverEnd += new EventHandler<InventoryEvent>(CurrentBankInventory_ItemMouseHoverEnd);

			switch (key) {
				case "BankSlotI":
					BankSlot1.SetBackgroundRenderColor(Color.Blue);
					BankSlot2.SetBackgroundRenderColor(Color.Gray);
					BankSlot3.SetBackgroundRenderColor(Color.Gray);
					BankSlot4.SetBackgroundRenderColor(Color.Gray);
					BankSlot5.SetBackgroundRenderColor(Color.Gray);
					break;
				case "BankSlotII":
					BankSlot2.SetBackgroundRenderColor(Color.Blue);
					BankSlot1.SetBackgroundRenderColor(Color.Gray);
					BankSlot3.SetBackgroundRenderColor(Color.Gray);
					BankSlot4.SetBackgroundRenderColor(Color.Gray);
					BankSlot5.SetBackgroundRenderColor(Color.Gray);
					break;
				case "BankSlotIII":
					BankSlot3.SetBackgroundRenderColor(Color.Blue);
					BankSlot1.SetBackgroundRenderColor(Color.Gray);
					BankSlot2.SetBackgroundRenderColor(Color.Gray);
					BankSlot4.SetBackgroundRenderColor(Color.Gray);
					BankSlot5.SetBackgroundRenderColor(Color.Gray);
					break;
				case "BankSlotIV":
					BankSlot4.SetBackgroundRenderColor(Color.Blue);
					BankSlot1.SetBackgroundRenderColor(Color.Gray);
					BankSlot2.SetBackgroundRenderColor(Color.Gray);
					BankSlot3.SetBackgroundRenderColor(Color.Gray);
					BankSlot5.SetBackgroundRenderColor(Color.Gray);
					break;
				case "BankSlotV":
					BankSlot5.SetBackgroundRenderColor(Color.Blue);
					BankSlot1.SetBackgroundRenderColor(Color.Gray);
					BankSlot2.SetBackgroundRenderColor(Color.Gray);
					BankSlot3.SetBackgroundRenderColor(Color.Gray);
					BankSlot4.SetBackgroundRenderColor(Color.Gray);
					break;
				default:
					throw new NotImplementedException(key);
			}

			AddCustomControl(key, CurrentBankInventory, false);
		}

		public void CreateEvents() {
			PlayerInventory.ItemBeginDrag += new EventHandler<InventoryEvent>(PlayerInventory_ItemBeginDrag);
			PlayerInventory.ItemDoubleClicked += new EventHandler<InventoryEvent>(PlayerInventory_ItemDoubleClicked);
			PlayerInventory.ItemEndDrag += new EventHandler<InventoryEvent>(PlayerInventory_ItemEndDrag);
			PlayerInventory.ItemMouseHoverBegin += new EventHandler<InventoryEvent>(PlayerInventory_ItemMouseHoverBegin);
			PlayerInventory.ItemMouseHoverEnd += new EventHandler<InventoryEvent>(PlayerInventory_ItemMouseHoverEnd);

			int inventoryUpgradeCount = Player.Owner.ShopSkills.Where(s => s.Typ == BuyableSkillTyps.InventoryExtension && s.Buyed).Count();
			PlayerInventory.SetInventoryDimension(4 + 4 * inventoryUpgradeCount, 6);

			BankSlot1.ButtonClicked += new EventHandler(BankSlot1_ButtonClicked);
			BankSlot2.ButtonClicked += new EventHandler(BankSlot2_ButtonClicked);
			BankSlot3.ButtonClicked += new EventHandler(BankSlot3_ButtonClicked);
			BankSlot4.ButtonClicked += new EventHandler(BankSlot4_ButtonClicked);
			BankSlot5.ButtonClicked += new EventHandler(BankSlot5_ButtonClicked);

			if (CurrentBankInventory != null) {
				CurrentBankInventory.ItemBeginDrag += new EventHandler<InventoryEvent>(CurrentBankInventory_ItemBeginDrag);
				CurrentBankInventory.ItemDoubleClicked += new EventHandler<InventoryEvent>(CurrentBankInventory_ItemDoubleClicked);
				CurrentBankInventory.ItemEndDrag += new EventHandler<InventoryEvent>(CurrentBankInventory_ItemEndDrag);
				CurrentBankInventory.ItemMouseHoverBegin += new EventHandler<InventoryEvent>(CurrentBankInventory_ItemMouseHoverBegin);
				CurrentBankInventory.ItemMouseHoverEnd += new EventHandler<InventoryEvent>(CurrentBankInventory_ItemMouseHoverEnd);
			}

			//todo expand bankinventory after buy
			foreach (KeyValuePair<string, InventoryControl> pair in BankInventorys) {
				pair.Value.SetInventoryDimension(4 + 4 * GetBankSlotInventoryUpgradeCount(pair.Key.ToEnum<BuyableSkills>()), 6);
			}

			//add new slots after buy
			List<ShopSkill> slotsBuyed = Player.Owner.ShopSkills.Where(s => s.Typ == BuyableSkillTyps.BankSlotExtension && s.Buyed).ToList();
			foreach (ShopSkill skill in slotsBuyed) {
				if (!BankInventorys.ContainsKey(skill.Skill.ToString())) {
					BankInventorys.Add(skill.Skill.ToString(), new InventoryControl(24, 6, 4 + GetBankSlotInventoryUpgradeCount(skill.Skill) * 4, 6));
				}

				switch (skill.Skill) {
					case BuyableSkills.BankSlotI:
						BankSlot1.SetIsEnable(true);
						BankSlot1.SetOnClickEvent(() => { });
						break;
					case BuyableSkills.BankSlotII:
						BankSlot2.SetIsEnable(true);
						BankSlot2.SetOnClickEvent(() => { });
						break;
					case BuyableSkills.BankSlotIII:
						BankSlot3.SetIsEnable(true);
						BankSlot3.SetOnClickEvent(() => { });
						break;
					case BuyableSkills.BankSlotIV:
						BankSlot4.SetIsEnable(true);
						BankSlot4.SetOnClickEvent(() => { });
						break;
					case BuyableSkills.BankSlotV:
						BankSlot5.SetIsEnable(true);
						BankSlot5.SetOnClickEvent(() => { });
						break;
					default:
						throw new NotImplementedException(skill.Skill.ToString());
				}
			}
		}

		private int GetBankSlotInventoryUpgradeCount(BuyableSkills slotType) {
			int count = 0;

			switch (slotType) {
				case BuyableSkills.BankSlotI:
					count = Player.Owner.ShopSkills.Where(s => s.Skill.ToString().StartsWith("BankInventoryI_") && s.Buyed).Count();
					break;
				case BuyableSkills.BankSlotII:
					count = Player.Owner.ShopSkills.Where(s => s.Skill.ToString().StartsWith("BankInventoryII_") && s.Buyed).Count();
					break;
				case BuyableSkills.BankSlotIII:
					count = Player.Owner.ShopSkills.Where(s => s.Skill.ToString().StartsWith("BankInventoryIII_") && s.Buyed).Count();
					break;
				case BuyableSkills.BankSlotIV:
					count = Player.Owner.ShopSkills.Where(s => s.Skill.ToString().StartsWith("BankInventoryIV_") && s.Buyed).Count();
					break;
				case BuyableSkills.BankSlotV:
					count = Player.Owner.ShopSkills.Where(s => s.Skill.ToString().StartsWith("BankInventoryV_") && s.Buyed).Count();
					break;
				default:
					throw new NotImplementedException(slotType.ToString());
			}

			return count;
		}

		public void RemoveEvents() {
			PlayerInventory.ItemBeginDrag -= PlayerInventory_ItemBeginDrag;
			PlayerInventory.ItemDoubleClicked -= PlayerInventory_ItemDoubleClicked;
			PlayerInventory.ItemEndDrag -= PlayerInventory_ItemEndDrag;
			PlayerInventory.ItemMouseHoverBegin -= PlayerInventory_ItemMouseHoverBegin;
			PlayerInventory.ItemMouseHoverEnd -= PlayerInventory_ItemMouseHoverEnd;

			if (CurrentBankInventory != null) {
				CurrentBankInventory.ItemBeginDrag -= CurrentBankInventory_ItemBeginDrag;
				CurrentBankInventory.ItemDoubleClicked -= CurrentBankInventory_ItemDoubleClicked;
				CurrentBankInventory.ItemEndDrag -= CurrentBankInventory_ItemEndDrag;
				CurrentBankInventory.ItemMouseHoverBegin -= CurrentBankInventory_ItemMouseHoverBegin;
				CurrentBankInventory.ItemMouseHoverEnd -= CurrentBankInventory_ItemMouseHoverEnd;
			}

			BankSlot1.ButtonClicked -= BankSlot1_ButtonClicked;
			BankSlot2.ButtonClicked -= BankSlot2_ButtonClicked;
			BankSlot3.ButtonClicked -= BankSlot3_ButtonClicked;
			BankSlot4.ButtonClicked -= BankSlot4_ButtonClicked;
			BankSlot5.ButtonClicked -= BankSlot5_ButtonClicked;

			foreach (var bankInv in BankInventorys) {
				foreach (var item in bankInv.Value.Items) {
					if (!Player.Owner.BankInventories.ContainsKey(bankInv.Key)) {
						Player.Owner.BankInventories.Add(bankInv.Key, new InventoryControl(bankInv.Value.MaxWidth, bankInv.Value.MaxHeight, bankInv.Value.Width, bankInv.Value.Height));
					}

					if (!Player.Owner.BankInventories[bankInv.Key].Items.Any(i => i.Item.ID == item.Item.ID)) {
						if (!Player.Owner.BankInventories[bankInv.Key].AddItem(item.Item)) {
							throw new Exception("SpielerBank ist voll, obwohl das nicht möglich sein sollte.");
						}
					}
				}
			}
		}
		#endregion

	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DungeonCrawler;
using SharpDX.Toolkit.Graphics;
using SharpDX.Toolkit;
using DungeonCrawler.CoreElements;
using GUI.Interface;
using RPG.Items.Inventory;
using SharpDX;
using RPG.Items;
using DungeonCrawler.Controls;
using Core.Items.Enum;
using ItemType = RPG.Items.Enum.ItemType;
using DungeonCrawler.CoreElements.Shop.Enum;
using SideProjekt.Mechanic.Log;

namespace GUI.Screens {
	public class InventoryScreen : BaseScreen<Game1> {

		private CorePlayer Player { get; set; }
		private InventoryControl Inventory { get; set; }
		private Vector2 Position { get; set; }

		private List<Tuple<string, string>> boxesSources;
		private Dictionary<string, Rectangle> boxes;
		private List<ItemSlot> ItemSlots { get; set; }
		private bool RenderToolTipText;
		private bool InventoryItemIsDraging;

		private InventoryPosition CurrentDragedItem { get; set; }
		private Vector2 CurrentDragedPosition { get; set; }

		public InventoryScreen(Game1 game, ScreenManager manager, CorePlayer player)
			: base(game, manager) {
			Player = player;

			RenderToolTipText = false;
			InventoryItemIsDraging = false;

			boxesSources = new List<Tuple<string, string>>();
			boxesSources.Add(new Tuple<string, string>("270,53,370,153", "Helm"));
			boxesSources.Add(new Tuple<string, string>("108,202,208,352", "Sword"));
			boxesSources.Add(new Tuple<string, string>("271,202,371,352", "Body"));
			boxesSources.Add(new Tuple<string, string>("434,202,534,352", "Schild"));
			boxesSources.Add(new Tuple<string, string>("270,400,370,500", "Shoe"));

			boxes = new Dictionary<string, Rectangle>();
			foreach (Tuple<string, string> box in boxesSources) {
				int[] cord = box.Item1.Split(',').Select(b => Convert.ToInt32(b)).ToArray();
				boxes.Add(box.Item2, new Rectangle((int)(cord[0] * (Resolution.Scale - Resolution.ScaleAddon4_3)), (int)(cord[1] * (Resolution.Scale - Resolution.ScaleAddon4_3)), (int)((cord[2] - cord[0]) * (Resolution.Scale - Resolution.ScaleAddon4_3)), (int)((cord[3] - cord[1]) * (Resolution.Scale - Resolution.ScaleAddon4_3))));
			}

			ItemSlots = new List<ItemSlot>();
			ItemSlots.Add(new ItemSlot(boxes["Helm"], ItemType.Head));
			ItemSlots.Add(new ItemSlot(boxes["Sword"], ItemType.Weapon));
			ItemSlots.Add(new ItemSlot(boxes["Body"], ItemType.Body));
			ItemSlots.Add(new ItemSlot(boxes["Schild"], ItemType.Schild));
			ItemSlots.Add(new ItemSlot(boxes["Shoe"], ItemType.Shoe));

			foreach (KeyValuePair<ItemType, Item> item in Player.UsedItems) {
				ItemSlots.Single(i => i.SlotType == item.Key).SetItem(item.Value);
			}

			Inventory = (InventoryControl)Player.Inventory;
			AddCustomControl("inventory", Inventory, false);

			Position = Vector2.Zero;
		}

		#region InventorySlot Events
		void slot_MouseHoverEnd(object sender, ItemSlotEvents e) {
			RenderToolTipText = false;
			currentToolTip = null;
		}

		void slot_MouseHoverBegin(object sender, ItemSlotEvents e) {
			RenderToolTipText = true;
			SetToolTip(e.Item);
		}

		void slot_BeginDrag(object sender, ItemSlotEvents e) {
			InventoryItemIsDraging = true;
			RenderToolTipText = false;
		}

		void slot_EndDrag(object sender, ItemSlotEvents e) {
			currentToolTip = null;
			InventoryItemIsDraging = false;
			RenderToolTipText = false;

			//prüfen ob Item sich innerhalb des aktiven Bereichs des Inventars befindet
			if (Inventory.GetActiveDimension().Intersects(e.Position.ToRectangle())) {
				// wenn ja, prüfen ob sich auf dieser Positon ein Item befindet
				var tmp = Inventory.Items.Where(i => new RectangleF((i.Position + Inventory.Position).X, (i.Position + Inventory.Position).Y, i.Item.Texture.Width, i.Item.Texture.Height).Intersects(e.Position.ToRectangle())).FirstOrDefault();
				if (tmp != null) {
					//  wenn ja, prüfen welcher Typ der Gegenstand ist
					if (tmp.Item.Type == e.Item.Type) {
						//   wenn typ identisch, austausch vornehmen
						Item inventoryItem = tmp.Item;
						Vector2 inventoryItemPos = tmp.Position + Inventory.Position;
						Inventory.RemoveItem(inventoryItem);
						Inventory.AddItem(e.Item, inventoryItemPos);
						((ItemSlot)sender).SetItem(inventoryItem);
						Player.WearItem(e.Item.Type, inventoryItem);
						Player.RefreshAllStatValue();
					} //   wenn typ nicht identisch, tue nichts
				} else {
					// wenn nein, verschiebe das item
					if (Inventory.AddItem(e.Item, e.Position)) {
						Player.WearItem(e.Item.Type, null);
						((ItemSlot)sender).SetItem(null);
						Player.RefreshAllStatValue();
					}
				}
			}
		}

		void slot_DoubleClicked(object sender, ItemSlotEvents e) {
			currentToolTip = null;

			if (Inventory.AddItem(e.Item)) {
				ItemSlots.Single(x => x == sender).SetItem(null);
				Player.WearItem(e.Item.Type, null);
				Player.RefreshAllStatValue();
			}//else gibt es nicht, denn wenn das Iventar voll ist gibt die Funktion false zurück
		}
		#endregion

		#region Inventory Events
		void Inventory_ItemMouseHoverEnd(object sender, InventoryEvent e) {
			RenderToolTipText = false;
			currentToolTip = null;
		}

		void Inventory_ItemMouseHoverBegin(object sender, InventoryEvent e) {
			RenderToolTipText = true;
			SetToolTip(e.Item);
		}

		void Inventory_ItemBeginDrag(object sender, InventoryEvent e) {
			InventoryItemIsDraging = true;
			RenderToolTipText = false;
			CurrentDragedItem = e.InventoryPosition;
			CurrentDragedPosition = Inventory.Position;
		}

		void Inventory_ItemEndDrag(object sender, InventoryEvent e) {
			currentToolTip = null;
			CurrentDragedItem = null;
			InventoryItemIsDraging = false;
			RenderToolTipText = false;

			var tmp = ItemSlots.Where(i => i.PositionRec.Intersects((e.Position + Inventory.Position).ToRectangle()) && i.SlotType == e.Item.Type).FirstOrDefault();

			//prüfen ob Item sich überhalb eines ausrüstungsslots befindet
			// ja, prüfen ob itemTyp zu SlotTyp passt
			if (tmp != null) {
				Item oldItem = null;
				if (tmp.Item != null) {
					oldItem = tmp.Item;
				}

				//  Ja, Rüste den Gegenstand aus und entferne ihn aus dem Iventar
				tmp.SetItem(e.Item);
				Inventory.RemoveItem(e.Item);
				Player.WearItem(e.Item.Type, e.Item);
				Player.RefreshAllStatValue();

				if (oldItem != null) {
					Inventory.AddItem(oldItem);
				}
			}
			//  nein, verschieben abbrechen, also tu nix
			// nein, verschieben abbrechen, also tu nix
		}

		void Inventory_ItemDoubleClicked(object sender, InventoryEvent e) {
			currentToolTip = null;
			CurrentDragedItem = null;
			InventoryItemIsDraging = false;
			RenderToolTipText = false;

			//Item wird ausgerüstet
			if (Player.UsedItems[e.Item.Type] == null) {
				Player.WearItem(e.Item.Type, e.Item);
				Inventory.RemoveItem(e.Item);
				Player.RefreshAllStatValue();
				ItemSlots.Single(i => i.SlotType == e.Item.Type).SetItem(e.Item);
			} else {//nehme einen austausch vor 
				Item playerItem = Player.UsedItems[e.Item.Type];
				Inventory.RemoveItem(e.Item);
				Inventory.AddItem(playerItem, e.Position);
				Player.WearItem(e.Item.Type, e.Item);
				Player.RefreshAllStatValue();
				ItemSlots.Single(i => i.SlotType == e.Item.Type).SetItem(e.Item);
			}
		}
		#endregion

		public override void Draw(SpriteBatch spriteBatch) {
			if (IsActiv && PerformDraw) {
				foreach (IControl control in CustomControls.Where(c => c.Item3).Select(c => c.Item2)) {
					control.Draw(spriteBatch);
				}

				foreach (IControl control in CustomControls.Where(c => !c.Item3).Select(c => c.Item2)) {
					control.Draw(spriteBatch);
				}

				foreach (ItemSlot item in ItemSlots) {
					item.Draw(spriteBatch);
				}

				if (RenderToolTipText && !InventoryItemIsDraging) {
					currentToolTip.Draw(spriteBatch);
				}

				if (CurrentDragedItem != null) {
					spriteBatch.Draw(CurrentDragedItem.back, CurrentDragedItem.Position + CurrentDragedPosition, Color.White * 0.45f);
					spriteBatch.Draw(CurrentDragedItem.Item.Texture, CurrentDragedItem.Position + CurrentDragedPosition, Color.White);
				}
			}
		}

		public override void Update(GameTime gameTime) {
			if (IsActiv && PerformUpdate) {
				ObjektManager.Update(gameTime);
				foreach (IControl control in CustomControls.Select(c => c.Item2)) {
					control.Update(gameTime);
				}

				foreach (ItemSlot item in ItemSlots) {
					item.Update(gameTime);
				}

				if (currentToolTip != null) {
					currentToolTip.Position = VirtualGamePad.GetClickPoint() - new Vector2(currentToolTip.GetDimension().Width / 2, -10);
					if (currentToolTip.GetDimension().BottomLeft.Y > Resolution.TargetHeight) {
						currentToolTip.Position = VirtualGamePad.GetClickPoint() - new Vector2(currentToolTip.GetDimension().Width / 2, currentToolTip.GetDimension().Height + 10);
					}
				}
			}
		}

		public override void Initialize() {

		}

		public override void LoadContent() {

		}

		public override void AddCustomControl(string key, IControl control, bool renderingFirst) {
			base.AddCustomControl(key, control, renderingFirst);

			if (key == "back") {
				Position = new Vector2(control.GetDimension().X, control.GetDimension().Y);
				Inventory.Position = new Vector2(Position.X + (control.GetDimension().Width / 2 - Inventory.GetDimension().Width / 2), control.GetDimension().Height - Inventory.GetDimension().Height - 10f);

				Dictionary<string, Rectangle> tmpBoxes = new Dictionary<string, Rectangle>();
				foreach (KeyValuePair<string, Rectangle> box in boxes) {
					tmpBoxes.Add(box.Key, new Rectangle((int)(box.Value.X + Position.X), (int)(box.Value.Y + Position.Y), box.Value.Width, box.Value.Height));
				}
				boxes = tmpBoxes;

				List<ItemSlot> tmpItemSlots = new List<ItemSlot>();
				foreach (ItemSlot item in ItemSlots) {
					switch (item.SlotType) {
						case ItemType.Body:
							ItemSlot body = new ItemSlot(boxes["Body"], item.SlotType);
							tmpItemSlots.Add(body);
							body.SetItem(item.Item);
							break;
						case ItemType.Head:
							ItemSlot head = new ItemSlot(boxes["Helm"], item.SlotType);
							tmpItemSlots.Add(head);
							head.SetItem(item.Item);
							break;
						case ItemType.Schild:
							ItemSlot schild = new ItemSlot(boxes["Schild"], item.SlotType);
							tmpItemSlots.Add(schild);
							schild.SetItem(item.Item);
							break;
						case ItemType.Shoe:
							ItemSlot shoe = new ItemSlot(boxes["Shoe"], item.SlotType);
							tmpItemSlots.Add(shoe);
							shoe.SetItem(item.Item);
							break;
						case ItemType.Weapon:
							ItemSlot weapon = new ItemSlot(boxes["Sword"], item.SlotType);
							tmpItemSlots.Add(weapon);
							weapon.SetItem(item.Item);
							break;
						default: throw new Exception(item.SlotType.ToString());
					}
				}

				ItemSlots = tmpItemSlots;
			}
		}

		#region tooltip
		Dictionary<Item, ToolTipText> ToolTips = new Dictionary<Item, ToolTipText>();
		ToolTipText currentToolTip;
		private void SetToolTip(Item item) {
			if (ToolTips.ContainsKey(item)) {
				currentToolTip = ToolTips[item];
			} else {
				ToolTips.Add(item, new ToolTipText(item, Vector2.Zero, GameRef.Content.Load<SpriteFont>(@"Fonts\Areal11.tk"), false));
				currentToolTip = ToolTips[item];
			}
		}
		#endregion

		public void Reset(CorePlayer player) {
			this.Player = player;

			RemoveEvents();
			ToolTips.Clear();

			ItemSlots = new List<ItemSlot>();
			ItemSlots.Add(new ItemSlot(boxes["Helm"], ItemType.Head));
			ItemSlots.Add(new ItemSlot(boxes["Sword"], ItemType.Weapon));
			ItemSlots.Add(new ItemSlot(boxes["Body"], ItemType.Body));
			ItemSlots.Add(new ItemSlot(boxes["Schild"], ItemType.Schild));
			ItemSlots.Add(new ItemSlot(boxes["Shoe"], ItemType.Shoe));

			foreach (KeyValuePair<ItemType, Item> item in Player.UsedItems) {
				ItemSlots.Single(i => i.SlotType == item.Key).SetItem(item.Value);
			}

			Vector2 invPosition = Inventory.Position;
			Inventory = (InventoryControl)Player.Inventory;
			AddCustomControl("inventory", Inventory, false);
			Inventory.Position = invPosition;

			int inventoryUpgradeCount = Player.Owner.ShopSkills.Where(s => s.Typ == BuyableSkillTyps.InventoryExtension && s.Buyed).Count();
			Inventory.SetInventoryDimension(4 + 4 * inventoryUpgradeCount, 6);
		}

		public void CreateEvents() {
			foreach (ItemSlot slot in ItemSlots) {
				slot.DoubleClicked += new EventHandler<ItemSlotEvents>(slot_DoubleClicked);
				slot.EndDrag += new EventHandler<ItemSlotEvents>(slot_EndDrag);
				slot.BeginDrag += new EventHandler<ItemSlotEvents>(slot_BeginDrag);
				slot.MouseHoverBegin += new EventHandler<ItemSlotEvents>(slot_MouseHoverBegin);
				slot.MouseHoverEnd += new EventHandler<ItemSlotEvents>(slot_MouseHoverEnd);
			}

			Inventory.ItemDoubleClicked += new EventHandler<InventoryEvent>(Inventory_ItemDoubleClicked);
			Inventory.ItemBeginDrag += new EventHandler<InventoryEvent>(Inventory_ItemBeginDrag);
			Inventory.ItemEndDrag += new EventHandler<InventoryEvent>(Inventory_ItemEndDrag);
			Inventory.ItemMouseHoverBegin += new EventHandler<InventoryEvent>(Inventory_ItemMouseHoverBegin);
			Inventory.ItemMouseHoverEnd += new EventHandler<InventoryEvent>(Inventory_ItemMouseHoverEnd);

			int inventoryUpgradeCount = Player.Owner.ShopSkills.Where(s => s.Typ == BuyableSkillTyps.InventoryExtension && s.Buyed).Count();
			Inventory.SetInventoryDimension(4 + 4 * inventoryUpgradeCount, 6);

			Log.Add("In InventoryScreen", LogType.Debug);
			foreach (var item in Player.UsedItems.Where(s => s.Value != null)) {
				Log.Add("UsedItem: " + item.ToString(), LogType.Debug);
			}

			foreach (var item in Player.Inventory.Items.Select(s => s.Item)) {
				Log.Add("InvItem: " + item.ToString(), LogType.Debug);
			}
		}

		public void RemoveEvents() {
			foreach (ItemSlot slot in ItemSlots) {
				slot.DoubleClicked -= slot_DoubleClicked;
				slot.EndDrag -= slot_EndDrag;
				slot.BeginDrag -= slot_BeginDrag;
				slot.MouseHoverBegin -= slot_MouseHoverBegin;
				slot.MouseHoverEnd -= slot_MouseHoverEnd;
			}

			Inventory.ItemDoubleClicked -= Inventory_ItemDoubleClicked;
			Inventory.ItemBeginDrag -= Inventory_ItemBeginDrag;
			Inventory.ItemEndDrag -= Inventory_ItemEndDrag;
			Inventory.ItemMouseHoverBegin -= Inventory_ItemMouseHoverBegin;
			Inventory.ItemMouseHoverEnd -= Inventory_ItemMouseHoverEnd;
		}
	}
}

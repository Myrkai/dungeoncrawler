﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GUI.Screens;
using DungeonCrawler;
using SharpDX.Toolkit;
using SharpDX.Toolkit.Graphics;
using GUI.Interface;
using DungeonCrawler.CoreElements;
using SharpDX;
using GUI.Controls;
using Core.BattleSystem;
using RPG.Sprites;
using Core;
using Core.Items.Interface;
using RPG.Items;
using DungeonCrawler.Controls;
using Core.Enums;
using DungeonCrawler.Sound;

namespace GUI.Screens {

	internal enum FightState {
		MoveForward,
		PerformAttack,
		ShowDamage,
		MoveBackward
	}

	public class BattleScreen : BaseScreen<Game1> {

		private CorePlayer Player { get; set; }
		private CorePlayer RealPlayer { get; set; }
		private CoreMonster Monster { get; set; }
		private List<BattleStep> FullBattle { get; set; }

		private Texture2D BattleBackground { get; set; }
		private Texture2D TextBackground { get; set; }
		private Vector2 OffsetBG { get; set; }
		private Vector2 OffsetText { get; set; }

		private ProgressbarControl PlayerHealth { get; set; }
		private ProgressbarControl MonsterHealth { get; set; }

		private bool AutoFight { get; set; }
		private FightState FightState { get; set; }
		private Vector2 StartPositionMonster { get; set; }
		private Vector2 StartPositionPlayer { get; set; }

		const float MoveDistance = 35f;
		const float MoveSpeed = 120f;
		const float ShowDamageDuration = 0.5f;

		private float Timer { get; set; }
		private SpriteFont Font { get; set; }
		private SpriteFont BattleHistoryFont { get; set; }
		private List<string> BattleHistory { get; set; }

		private bool ShowEndBattle { get; set; }
		private Texture2D EndBattleScreen { get; set; }
		private int ExpAfterBattle { get; set; }
		private uint GoldAfterBattle { get; set; }
		private bool ItemWasDroped { get; set; }
		private bool ItemIsInInventory { get; set; }

		public BattleScreen(Game1 game, ScreenManager manager, CorePlayer player, CoreMonster monster)
			: base(game, manager) {
			RealPlayer = (CorePlayer)player;
			Player = (CorePlayer)player.Clone();
			Monster = monster;
			AutoFight = false;
			FightState = FightState.MoveForward;
			Timer = 0f;
			ShowEndBattle = false;

			ExpAfterBattle = 0;
			GoldAfterBattle = 0;
			ItemWasDroped = false;
			ItemIsInInventory = false;

			Font = GameRef.Content.Load<SpriteFont>(@"Fonts\Areal28.tk");
			BattleHistoryFont = GameRef.Content.Load<SpriteFont>(@"Fonts\Areal12.tk");
			BattleHistory = new List<string>();

			StartPositionMonster = Vector2.Zero;
			StartPositionPlayer = Vector2.Zero;

			BattleBackground = TextureHelper.CreateRoundedRectangleTexture(GameRef.GraphicsDevice, 600, 400, 5, 50, 0, new List<Color>() { Color.Gray * 0.7f }, new List<Color>() { Color.Black }, 0f, 0f);
			TextBackground = TextureHelper.CreateRoundedRectangleTexture(GameRef.GraphicsDevice, 600, 200, 5, 50, 0, new List<Color>() { Color.Blue * 0.7f }, new List<Color>() { Color.White }, 0f, 0f);
			EndBattleScreen = TextureHelper.CreateRoundedRectangleTexture(GameRef.GraphicsDevice, 600, 100, 5, 25, 0, new List<Color>() { Color.Gray * 0.85f }, new List<Color>() { Color.Black }, 0f, 0f);
			OffsetBG = new Vector2(0, 125f);
			OffsetText = new Vector2(0, -320f);

			Texture2D emptyHealth = TextureHelper.CreateTextureWidthBoarderAndSmooth(500, 16, Color.Gray, 2, Color.Black, 0, false, GameRef.GraphicsDevice);
			if (player != null) {
				Texture2D fullHealth = TextureHelper.CreateTextureWidthBoarderAndSmooth(500, 16, Color.Blue, 2, Color.Black, 0, false, GameRef.GraphicsDevice);
				float healthPercentage = Player.CurrentHP / Player.MaxHP;
				PlayerHealth = new ProgressbarControl(500, 16, Resolution.Size / 2 - BattleBackground.Size() / 2 - OffsetBG + new Vector2(50f, 355f), fullHealth, emptyHealth, healthPercentage, 2.47f);
			}

			if (monster != null) {
				Texture2D fullHealthMonster = TextureHelper.CreateTextureWidthBoarderAndSmooth(500, 16, Color.Red, 2, Color.Black, 0, false, GameRef.GraphicsDevice);
				float healthPercentageMonster = Monster.CurrentHP / Monster.MaxHP;
				MonsterHealth = new ProgressbarControl(500, 16, Resolution.Size / 2 - BattleBackground.Size() / 2 - OffsetBG + new Vector2(50f, 20f), fullHealthMonster, emptyHealth, healthPercentageMonster, 2.47f);
			}

			VirtualGamePad.ignoreOneFrame = true;
		}

		public void Restart(CorePlayer player, CoreMonster monster, List<BattleStep> fullBattle) {
			BattleHistory.Clear();
			AutoFight = false;
			ShowEndBattle = false;
			FightState = FightState.MoveForward;

			ExpAfterBattle = 0;
			GoldAfterBattle = 0;
			ItemWasDroped = false;
			ItemIsInInventory = false;

			RealPlayer = (CorePlayer)player;
			Player = (CorePlayer)player.Clone();
			Monster = monster;
			FullBattle = fullBattle;

			if (player != null) {
				Texture2D fullHealth = TextureHelper.CreateTextureWidthBoarderAndSmooth(500, 16, Color.Blue, 2, Color.Black, 0, false, GameRef.GraphicsDevice);
				Texture2D emptyHealth = TextureHelper.CreateTextureWidthBoarderAndSmooth(500, 16, Color.Gray, 2, Color.Gray, 0, false, GameRef.GraphicsDevice);
				float healthPercentage = Player.CurrentHP / Player.MaxHP;
				PlayerHealth = new ProgressbarControl(500, 16, Resolution.Size / 2 - BattleBackground.Size() / 2 - OffsetBG + new Vector2(50f, 355f), fullHealth, emptyHealth, healthPercentage, 2f);

				Player.PlayerAnimation.SetDirection(AnimationDirection.UP);
				Player.PlayerAnimation.DoAnimationLooping = true;

				StartPositionPlayer = Resolution.Size / 2 - Player.GetDimension().Size() / 2 - new Vector2(0, 5);
				Player.SetPosition(StartPositionPlayer);
			}

			if (monster != null) {
				Texture2D fullHealthMonster = TextureHelper.CreateTextureWidthBoarderAndSmooth(500, 16, Color.Red, 2, Color.Black, 0, false, GameRef.GraphicsDevice);
				Texture2D emptyHealth = TextureHelper.CreateTextureWidthBoarderAndSmooth(500, 16, Color.Gray, 2, Color.Gray, 0, false, GameRef.GraphicsDevice);
				float healthPercentageMonster = Monster.CurrentHP / Monster.MaxHP;
				MonsterHealth = new ProgressbarControl(500, 16, Resolution.Size / 2 - BattleBackground.Size() / 2 - OffsetBG + new Vector2(50f, 20f), fullHealthMonster, emptyHealth, healthPercentageMonster, 2f);

				float factor = 125f / Monster.Texture.Height;
				Vector2 dimensionSize = new Vector2(Monster.Texture.Width * factor, Monster.Texture.Height * factor);
				Monster.SetPosition(Resolution.Size / 2 - dimensionSize / 2 - new Vector2(0, 220));

				StartPositionMonster = Resolution.Size / 2 - dimensionSize / 2 - new Vector2(0, 220);
			}

			VirtualGamePad.ignoreOneFrame = true;
		}

		public override void Draw(SpriteBatch spriteBatch) {
			if (IsActiv && PerformDraw && Monster != null && Player != null) {
				foreach (IControl control in CustomControls.Where(c => c.Item3).Select(c => c.Item2)) {
					control.Draw(spriteBatch);
				}

				if (!ShowEndBattle) {
					spriteBatch.Draw(BattleBackground, Resolution.Size / 2 - BattleBackground.Size() / 2 - OffsetBG, Color.White);
					spriteBatch.Draw(TextBackground, Resolution.Size / 2 - BattleBackground.Size() / 2 - OffsetText, Color.White);
					PlayerHealth.Draw(spriteBatch);
					MonsterHealth.Draw(spriteBatch);

					float factor = 125f / Monster.Texture.Height;
					spriteBatch.Draw(Monster.Texture, new RectangleF(Monster.Position.X, Monster.Position.Y, Monster.Texture.Width * factor, Monster.Texture.Height * factor), Color.White);
					spriteBatch.Draw(Player.PlayerAnimation.Texture, Player.Position, Player.PlayerAnimation.CurrentTextureRectangle(), Color.White);

					if (FightState == FightState.ShowDamage && Timer <= ShowDamageDuration) {
						string s = "";
						if (FullBattle[0].Direction == FightDirection.EnermyToPlayer) {
							s = Math.Round(FullBattle[0].Damage * (1f - Player.GetDamageReduce(Monster.Level) / 100f), 0).ToString();
						} else if (FullBattle[0].Direction == FightDirection.PlayerToEnermy) {
							s = Math.Round(FullBattle[0].Damage * (1f - Monster.GetDamageReduce(Player.Level) / 100f), 0).ToString();
						} else {
							throw new Exception();
						}

						if (FullBattle[0].Missing) {
							s = "Miss";
						}
						spriteBatch.DrawString(Font, s, Resolution.Size / 2 - OffsetBG - Font.MeasureString(s) / 2 + new Vector2(0, 45f), Color.Red);
					}

					Vector2 battleHistoryBackgroundPosition = Resolution.Size / 2 - BattleBackground.Size() / 2 - OffsetText + new Vector2(45f, 10f);
					for (int i = 0; i < BattleHistory.Count; i++) {
						spriteBatch.DrawString(BattleHistoryFont, BattleHistory[i], battleHistoryBackgroundPosition + new Vector2(0, 20 * i), Color.White);
					}
				} else {
					Vector2 endScreenPosition = Resolution.Size / 2 - EndBattleScreen.Size() / 2;
					spriteBatch.Draw(EndBattleScreen, endScreenPosition, Color.White);
					endScreenPosition += new Vector2(25, 10);

					spriteBatch.DrawString(BattleHistoryFont, "EXP: " + ExpAfterBattle.ToString("n0"), endScreenPosition, Color.White);
					spriteBatch.DrawString(BattleHistoryFont, "Gold: " + GoldAfterBattle.ToString("n0"), endScreenPosition + new Vector2(0, 25), Color.White);
					if (ItemWasDroped && ItemIsInInventory) {
						spriteBatch.DrawString(BattleHistoryFont, "Der fallengelassene Gegenstand wurde dem Inventar hinzugefügt.", endScreenPosition + new Vector2(0, 50), Color.White);
					} else if (ItemWasDroped && !ItemIsInInventory) {
						spriteBatch.DrawString(BattleHistoryFont, "Der fallengelassene Gegenstand ging verloren, weil das Inventar zu voll ist.", endScreenPosition + new Vector2(0, 50), Color.White);
					}
				}

				foreach (IControl control in CustomControls.Where(c => !c.Item3).Select(c => c.Item2)) {
					control.Draw(spriteBatch);
				}
			}
		}

		/* 
		 * GameModes:
		 * RoundAttack - Nach jeder Kampfrunde wird eins abgezogen
		 * TimeAttack - Solange der BattleScreen vorhanden ist geht die Zeit runter
		 */

		Boolean allowSkip = false;
		Boolean closeBattle = false;
		Boolean hasSkiped = false;
		public override void Update(GameTime gameTime) {
			if (IsActiv && PerformUpdate && Monster != null && Player != null) {
				ObjektManager.Update(gameTime);
				Player.Update(gameTime);

				if (Player.GetGameMode() == GameModes.TimeAttack) {
					RealPlayer.RedurceBattleTime((int)gameTime.ElapsedGameTime.TotalMilliseconds);
				}

				foreach (IControl control in CustomControls.Select(c => c.Item2)) {
					control.Update(gameTime);
				}

				if (VirtualGamePad.ClickBegin() && !AutoFight) {
					AutoFight = true;
					VirtualGamePad.ignoreOneFrame = true;
					return;
				}

				// schließe den Kampfbildschirm
				if (VirtualGamePad.ClickReleased() && ShowEndBattle && closeBattle) {
					Player.Owner.Save();
					ScreenManager.Instance.RemoveScreen();
					ScreenManager.Instance.GetScreen(Game1.GAMESCREEN).SetDoUpdate(true);
				} else if (VirtualGamePad.ClickReleased() && ShowEndBattle && !closeBattle) {
					closeBattle = true;
				}

				//Kampflogik ausführen
				if (AutoFight) {
					//Simuliere die Kampfrunden
					if (FullBattle.Count > 0) {

						if (VirtualGamePad.ClickBegin() && allowSkip) {
							SkipBattle(); // Buggy
							hasSkiped = true;
						} else if (VirtualGamePad.ClickReleased() && !allowSkip) {
							allowSkip = true;
						}

						switch (FightState) {
							case FightState.MoveForward:
								if (FullBattle[0].Direction == FightDirection.EnermyToPlayer) {
									Monster.SetPosition(Monster.Position + new Vector2(0, 1f) * MoveSpeed * (float)gameTime.ElapsedGameTime.TotalSeconds);

									if (Math.Abs(Monster.Position.Y - StartPositionMonster.Y) > MoveDistance) {
										FightState = FightState.PerformAttack;
									}
								} else if (FullBattle[0].Direction == FightDirection.PlayerToEnermy) {
									Player.SetPosition(Player.Position + new Vector2(0, -1f) * MoveSpeed * (float)gameTime.ElapsedGameTime.TotalSeconds);

									if (Math.Abs(Player.Position.Y - StartPositionPlayer.Y) > MoveDistance) {
										FightState = FightState.PerformAttack;
									}
								} else {
									throw new Exception();
								}
								break;
							case FightState.PerformAttack:
								FightState = FightState.ShowDamage;
								Timer = 0f;
								if (FullBattle[0].Direction == FightDirection.EnermyToPlayer) {
									MediaManager.Instance.Play("hit1");
								} else if (FullBattle[0].Direction == FightDirection.PlayerToEnermy) {
									MediaManager.Instance.Play("hit2");
								}
								break;
							case FightState.ShowDamage:
								Timer += (float)gameTime.ElapsedGameTime.TotalSeconds;
								if (Timer > ShowDamageDuration) {
									FightState = FightState.MoveBackward;

									if (FullBattle[0].Direction == FightDirection.EnermyToPlayer) {
										if (!FullBattle[0].Missing) {
											Player.SetCurrentHealth(Player.CurrentHP - FullBattle[0].Damage * (1f - Player.GetDamageReduce(Monster.Level) / 100f));

											float healthPercentage = Player.CurrentHP / Player.MaxHP;
											PlayerHealth.ProgressFilled = Math.Max(healthPercentage, 0f);

											BattleHistory.Add(Monster.Name + " greift mit " + Math.Round(FullBattle[0].Damage * (1f - Player.GetDamageReduce(Monster.Level) / 100f), 0).ToString() + " Schaden an");
										} else {
											BattleHistory.Add(Monster.Name + " verfehlt");
										}

										foreach (SkillEffect effect in FullBattle[0].ItemEffects) {
											switch (effect.TargetType) {
												case TargetType.Owner:
													switch (effect.EffectType) {
														case EffectType.Heal:
															Monster.CurrentHP = Math.Min(Monster.MaxHP, Monster.CurrentHP + effect.Value);
															BattleHistory.Add(Monster.Name + " heilt sich um " + effect.Value.ToString());
															break;
														case EffectType.Damage:
															Monster.CurrentHP -= effect.Value;
															BattleHistory.Add(Monster.Name + " verletzt sich selbst um " + effect.Value.ToString());
															break;
														case EffectType.BattleTimeUp:
															break;
														case EffectType.BattleRoundUp:
															break;
														default:
															throw new Exception();
													}
													break;
												case TargetType.Target:
													switch (effect.EffectType) {
														case EffectType.Heal:
															Player.CurrentHP = Math.Min(Monster.MaxHP, Monster.CurrentHP + effect.Value);
															BattleHistory.Add(Monster.Name + " heilt " + Player.Name + " um " + effect.Value.ToString());
															break;
														case EffectType.Damage:
															Player.CurrentHP -= effect.Value;
															BattleHistory.Add(Monster.Name + " verursacht " + effect.Value.ToString() + " zusätzlichen Schaden");
															break;
														case EffectType.BattleTimeUp:
															RealPlayer.AddBattleTime(effect.Value);
															BattleHistory.Add(Player.Name + " erhält " + effect.Value.ToString() + "min zusätzliche Kampfzeit");
															break;
														case EffectType.BattleRoundUp:
															RealPlayer.AddBattleRound(effect.Value);
															BattleHistory.Add(Player.Name + " erhält " + effect.Value.ToString() + " zusätzliche Kampfrunden");
															break;
														default:
															throw new Exception();
													}
													break;
												default: throw new Exception();
											}
										}
									} else if (FullBattle[0].Direction == FightDirection.PlayerToEnermy) {
										if (!FullBattle[0].Missing) {
											Monster.SetCurrentHP(Monster.CurrentHP - FullBattle[0].Damage * (1f - Monster.GetDamageReduce(Player.Level) / 100f));

											float healthPercentageMonster = Monster.CurrentHP / Monster.MaxHP;
											MonsterHealth.ProgressFilled = Math.Max(healthPercentageMonster, 0f);

											BattleHistory.Add(Player.Name + " greift mit " + Math.Round(FullBattle[0].Damage * (1f - Player.GetDamageReduce(Monster.Level) / 100f), 0).ToString() + " Schaden an");
										} else {
											BattleHistory.Add(Player.Name + " verfehlt");
										}

										foreach (SkillEffect effect in FullBattle[0].ItemEffects) {
											switch (effect.TargetType) {
												case TargetType.Owner:
													switch (effect.EffectType) {
														case EffectType.Heal:
															Player.CurrentHP = Math.Min(Player.MaxHP, Player.CurrentHP + effect.Value);
															BattleHistory.Add(Player.Name + " heilt sich um " + effect.Value.ToString());
															break;
														case EffectType.Damage:
															Player.CurrentHP -= effect.Value;
															BattleHistory.Add(Player.Name + " verletzt sich selbst um " + effect.Value.ToString());
															break;
														case EffectType.BattleTimeUp:
															RealPlayer.AddBattleTime(effect.Value);
															BattleHistory.Add(Player.Name + " erhält " + effect.Value.ToString() + "min zusätzliche Kampfzeit");
															break;
														case EffectType.BattleRoundUp:
															RealPlayer.AddBattleRound(effect.Value);
															BattleHistory.Add(Player.Name + " erhält " + effect.Value.ToString() + " zusätzliche Kampfrunden");
															break;
														default:
															throw new Exception();
													}
													break;
												case TargetType.Target:
													switch (effect.EffectType) {
														case EffectType.Heal:
															Monster.CurrentHP = Math.Min(Monster.MaxHP, Monster.CurrentHP + effect.Value);
															BattleHistory.Add(Player.Name + " heilt " + Monster.Name + " um " + effect.Value.ToString());
															break;
														case EffectType.Damage:
															Monster.CurrentHP -= effect.Value;
															BattleHistory.Add(Player.Name + " verursacht " + effect.Value.ToString() + " zusätzlichen Schaden");
															break;
														case EffectType.BattleTimeUp:
															break;
														case EffectType.BattleRoundUp:
															break;
														default:
															throw new Exception();
													}
													break;
												default: throw new Exception();
											}
										}
									} else {
										throw new Exception();
									}
								}
								break;
							case FightState.MoveBackward:
								if (FullBattle[0].Direction == FightDirection.EnermyToPlayer) {
									Monster.SetPosition(Monster.Position + new Vector2(0, -1f) * MoveSpeed * (float)gameTime.ElapsedGameTime.TotalSeconds);

									if (StartPositionMonster.Y >= Monster.Position.Y) {
										FightState = FightState.MoveForward;
										Monster.SetPosition(StartPositionMonster);
										FullBattle.RemoveAt(0);
									}
								} else if (FullBattle[0].Direction == FightDirection.PlayerToEnermy) {
									Player.SetPosition(Player.Position + new Vector2(0, +1f) * MoveSpeed * (float)gameTime.ElapsedGameTime.TotalSeconds);

									if (StartPositionPlayer.Y <= Player.Position.Y) {
										FightState = FightState.MoveForward;
										Player.SetPosition(StartPositionPlayer);
										FullBattle.RemoveAt(0);
									}
								} else {
									throw new Exception();
								}
								break;
							default:
								throw new Exception(FightState.ToString());
						}
					} else { //Alle Kampfrunden wurden verarbeitet und der Kampf ist zu ende
						if (!ShowEndBattle) {
							if (Player.GetCurrentHP() <= 0f || hasSkiped && Monster.GetCurrentHP() > Player.GetCurrentHP()) { // TODO Kampfübersprungen hat einen Bug. Dieser ist unbekannt und ich hab keine Lust ihn zu suchen
								//erstmal nichts zu tun

								RealPlayer.SetCurrentHealth(0f);
								int expGained;
								uint goldGaind;

								try {
									IItem item = Engine.Instance.EndBattle(Monster, out expGained, out goldGaind);
								} catch (ApplicationException) {
									Player.Owner.Save();
									ScreenManager.Instance.RemoveScreen();
									ScreenManager.Instance.GetScreen(Game1.GAMESCREEN).SetDoUpdate(false);
									ScreenManager.Instance.AddScreen(Game1.INGAMEMENU, false);
									var ingameMenu = (MainMenuScreen<Game1>)ScreenManager.Instance.GetScreen(Game1.INGAMEMENU);
									var width = "Game Over".Width(GameRef.Content.Load<SpriteFont>(@"Fonts\Areal26.tk"));
									ingameMenu.SetTitle("Game Over", new Vector2((Resolution.TargetWidth - width) / 2, 50), GameRef.Content.Load<SpriteFont>(@"Fonts\Areal26.tk"), Color.White);
									((UpdateControl)ingameMenu.CustomControls.Single(c => c.Item1 == "update").Item2).updateAction = null;
									return;
								}
							} else if (Monster.GetCurrentHP() <= 0f || hasSkiped && Monster.GetCurrentHP() <= Player.GetCurrentHP()) {
								int expGained;
								uint goldGaind;

								if (Monster.IsBoss) { //nach einem Bosskampf wird die nächste ebene geladen
									RealPlayer.SetLayer(Player.GetLayer() + 1);
									Engine.Instance.CreateNewMap(new Vector2(100f, 100f));

									Engine.Instance.CurrentMap.Zones.ForEach(z => z.CurrentLayer = RealPlayer.GetLayer());
									Engine.Instance.CurrentMap.BossZone.CurrentLayer = RealPlayer.GetLayer();

									Engine.Instance.ResetCamera();

									//Spieler muss die ausgewählte Ebenen Stufe auch abgeschlossen haben um den Run erfolgreich abzuschließen.
									if (RealPlayer.GetGameModeAddons().Contains(GameModeAddons.Hardcore) && RealPlayer.GetLayersToBeat() > RealPlayer.GetLayer()) {
										throw new Exception("Player has won the Hardcore Mode");
									}

									((EngineControl)((GameScreen<Game1>)ScreenManager.Instance.GetScreen(Game1.GAMESCREEN)).CustomControls.Where(s => s.Item1 == "engine").First().Item2).Reset();
								}

								IItem item;
								try {
									item = Engine.Instance.EndBattle(Monster, out expGained, out goldGaind);
								} catch (ApplicationException) {
									Player.Owner.Save();
									ScreenManager.Instance.RemoveScreen();
									ScreenManager.Instance.GetScreen(Game1.GAMESCREEN).SetDoUpdate(false);
									ScreenManager.Instance.AddScreen(Game1.INGAMEMENU, false);
									var ingameMenu = (MainMenuScreen<Game1>)ScreenManager.Instance.GetScreen(Game1.INGAMEMENU);
									var width = "Game Over".Width(GameRef.Content.Load<SpriteFont>(@"Fonts\Areal26.tk"));
									ingameMenu.SetTitle("Game Over", new Vector2((Resolution.TargetWidth - width) / 2, 50), GameRef.Content.Load<SpriteFont>(@"Fonts\Areal26.tk"), Color.White);
									((UpdateControl)ingameMenu.CustomControls.Single(c => c.Item1 == "update").Item2).updateAction = null;
									return;
								}

								ShowEndBattle = true;
								ItemWasDroped = item != null;
								if (ItemWasDroped) {
									ItemIsInInventory = Player.Inventory.AddItem((Item)item);
								} else {
									ItemIsInInventory = false;
								}

								ExpAfterBattle = expGained;
								GoldAfterBattle = goldGaind;
							}
						}
					}
				}
			}

			//Die letzten 9 Elemente der Liste werden behalten
			BattleHistory = Enumerable.Reverse(BattleHistory).Take(9).Reverse().ToList();
		}

		private void SkipBattle() {
			while (FullBattle.Count > 1) {
				if (FullBattle[0].Direction == FightDirection.PlayerToEnermy) {
					if (!FullBattle[0].Missing) {
						Monster.SetCurrentHP(Monster.CurrentHP - FullBattle[0].Damage * (1f - Monster.GetDamageReduce(Player.Level) / 100f));
					}

					foreach (SkillEffect effect in FullBattle[0].ItemEffects) {
						switch (effect.TargetType) {
							case TargetType.Owner:
								switch (effect.EffectType) {
									case EffectType.Heal:
										Player.CurrentHP = Math.Min(Player.MaxHP, Player.CurrentHP + effect.Value);
										break;
									case EffectType.Damage:
										Player.CurrentHP -= effect.Value;
										break;
								}
								break;
							case TargetType.Target:
								switch (effect.EffectType) {
									case EffectType.Heal:
										Monster.CurrentHP = Math.Min(Monster.MaxHP, Monster.CurrentHP + effect.Value);
										break;
									case EffectType.Damage:
										Monster.CurrentHP -= effect.Value;
										break;
								}
								break;
							default: throw new Exception();
						}
					}
				} else {
					if (!FullBattle[0].Missing) {
						Player.SetCurrentHealth(Player.CurrentHP - FullBattle[0].Damage * (1f - Player.GetDamageReduce(Monster.Level) / 100f));
					}

					foreach (SkillEffect effect in FullBattle[0].ItemEffects) {
						switch (effect.TargetType) {
							case TargetType.Owner:
								switch (effect.EffectType) {
									case EffectType.Heal:
										Monster.CurrentHP = Math.Min(Monster.MaxHP, Monster.CurrentHP + effect.Value);
										break;
									case EffectType.Damage:
										Monster.CurrentHP -= effect.Value;
										break;
								}
								break;
							case TargetType.Target:
								switch (effect.EffectType) {
									case EffectType.Heal:
										Player.CurrentHP = Math.Min(Monster.MaxHP, Monster.CurrentHP + effect.Value);
										break;
									case EffectType.Damage:
										Player.CurrentHP -= effect.Value;
										break;
								}
								break;
							default: throw new Exception();
						}
					}
				}
				FullBattle.RemoveAt(0);
			}
		}

		public void Reset(CorePlayer player) {
			this.Player = player;

			Restart(player, null, null);
		}

		public override void Initialize() {

		}

		public override void LoadContent() {

		}

		public override void AddCustomControl(string key, IControl control, bool renderingFirst) {
			base.AddCustomControl(key, control, renderingFirst);
		}
	}
}

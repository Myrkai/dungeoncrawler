﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GUI.Screens;
using SharpDX.Toolkit.Graphics;
using SharpDX.Toolkit;
using GUI;
using DungeonCrawler.CoreElements;
using GUI.Controls;
using SharpDX.Toolkit.Content;
using SharpDX;
using RPG.Chars.Enum;
using DungeonCrawler;
using GUI.Interface;
using RPG.Items;
using SideProjekt.Mechanic.Log;

namespace GUI.Screens {
	public class DetailsScreen : BaseScreen<Game1> {

		private List<Tuple<string, string>> textboxesSources;
		private Dictionary<string, Rectangle> textBoxes;

		private List<KeyValuePair<string, string>> Informations { get; set; }
		private CorePlayer player;
		private ContentManager content;

		public DetailsScreen(Game1 game, ScreenManager manager, ContentManager content)
			: base(game, manager) {
			this.content = content;

			textboxesSources = new List<Tuple<string, string>>();
			textboxesSources.Add(new Tuple<string, string>("14,17,628,705", "FullTextBox"));

			textBoxes = new Dictionary<string, Rectangle>();
			foreach (Tuple<string, string> box in textboxesSources) {
				int[] cord = box.Item1.Split(',').Select(b => Convert.ToInt32(b)).ToArray();
				textBoxes.Add(box.Item2, new Rectangle((int)(cord[0] * (Resolution.Scale - Resolution.ScaleAddon4_3)), (int)(cord[1] * (Resolution.Scale - Resolution.ScaleAddon4_3)), (int)((cord[2] - cord[0]) * (Resolution.Scale - Resolution.ScaleAddon4_3)), (int)((cord[3] - cord[1]) * (Resolution.Scale - Resolution.ScaleAddon4_3))));
			}
		}

		public override void Draw(SpriteBatch spriteBatch) {
			if (IsActiv && PerformDraw) {
				foreach (IControl control in CustomControls.Where(c => c.Item3).Select(c => c.Item2)) {
					control.Draw(spriteBatch);
				}

				foreach (IControl control in CustomControls.Where(c => !c.Item3).Select(c => c.Item2)) {
					control.Draw(spriteBatch);
				}
			}
		}

		public override void Update(GameTime gameTime) {
			if (IsActiv && PerformUpdate) {
				ObjektManager.Update(gameTime);
				foreach (IControl control in CustomControls.Select(c => c.Item2)) {
					control.Update(gameTime);
				}
			}
		}

		public override void Initialize() {

		}

		public override void LoadContent() {

		}

		public void CreateInformations(CorePlayer player) {
			this.player = player;

			int borderTop = 0;

			//Hinzufügen von Details
			AddCustomControl("*stats*", new LinkControl("*Alle Statuswerte*", new Vector2(textBoxes["FullTextBox"].X + 3, textBoxes["FullTextBox"].Y + 3 + borderTop), content.Load<SpriteFont>(@"Fonts\Areal9.tk"), Color.White, null), false);
			borderTop += 13;

			foreach (KeyValuePair<StatType, float> type in player.Stats) {
				AddCustomControl(type.Key.ToString(), new LinkControl(type.Key.ToString(), new Vector2(textBoxes["FullTextBox"].X + 3, textBoxes["FullTextBox"].Y + 3 + borderTop), content.Load<SpriteFont>(@"Fonts\Areal9.tk"), Color.White, null), false);
				AddCustomControl("value" + type.Key.ToString(), new LinkControl(type.Value.ToString(), new Vector2(textBoxes["FullTextBox"].X + 3 + 150, textBoxes["FullTextBox"].Y + 3 + borderTop), content.Load<SpriteFont>(@"Fonts\Areal9.tk"), Color.White, null), false);

				borderTop += 13;
			}

			AddCustomControl("statPoints", new LinkControl("StatPoints", new Vector2(textBoxes["FullTextBox"].X + 3, textBoxes["FullTextBox"].Y + 3 + borderTop), content.Load<SpriteFont>(@"Fonts\Areal9.tk"), Color.White, null), false);
			AddCustomControl("valuestatPoints", new LinkControl(player.StatPoints.ToString(), new Vector2(textBoxes["FullTextBox"].X + 3 + 150, textBoxes["FullTextBox"].Y + 3 + borderTop), content.Load<SpriteFont>(@"Fonts\Areal9.tk"), Color.White, null), false);
			borderTop += 13;
			AddCustomControl("statPointsLevelUp", new LinkControl("StatPointsOnLevelUp", new Vector2(textBoxes["FullTextBox"].X + 3, textBoxes["FullTextBox"].Y + 3 + borderTop), content.Load<SpriteFont>(@"Fonts\Areal9.tk"), Color.White, null), false);
			AddCustomControl("valuestatPointsLevelUp", new LinkControl(player.StatPointsPerLevelUp.ToString(), new Vector2(textBoxes["FullTextBox"].X + 3 + 150, textBoxes["FullTextBox"].Y + 3 + borderTop), content.Load<SpriteFont>(@"Fonts\Areal9.tk"), Color.White, null), false);
			borderTop += 13;
			AddCustomControl("damageReduce", new LinkControl("DamageReduce(Level " + player.Level.ToString() + ")", new Vector2(textBoxes["FullTextBox"].X + 3, textBoxes["FullTextBox"].Y + 3 + borderTop), content.Load<SpriteFont>(@"Fonts\Areal9.tk"), Color.White, null), false);
			AddCustomControl("valuedamageReduce", new LinkControl(player.GetDamageReduce(player.Level).ToString(), new Vector2(textBoxes["FullTextBox"].X + 3 + 150, textBoxes["FullTextBox"].Y + 3 + borderTop), content.Load<SpriteFont>(@"Fonts\Areal9.tk"), Color.White, null), false);
			borderTop += 13;
			AddCustomControl("Gold", new LinkControl("Gold", new Vector2(textBoxes["FullTextBox"].X + 3, textBoxes["FullTextBox"].Y + 3 + borderTop), content.Load<SpriteFont>(@"Fonts\Areal9.tk"), Color.White, null), false);
			AddCustomControl("valueGold", new LinkControl(player.Gold.ToString(), new Vector2(textBoxes["FullTextBox"].X + 3 + 150, textBoxes["FullTextBox"].Y + 3 + borderTop), content.Load<SpriteFont>(@"Fonts\Areal9.tk"), Color.White, null), false);

			//hinzufügen von Fähigkeiten
			borderTop = 0;
			AddCustomControl("*used*", new LinkControl("*Ausgerüstete Fähigkeiten*", new Vector2(textBoxes["FullTextBox"].X + 3 + 425, textBoxes["FullTextBox"].Y + 3 + borderTop), content.Load<SpriteFont>(@"Fonts\Areal9.tk"), Color.White, null), false);
			borderTop += 13;

			foreach (var skill in player.ClassType.StatBonus.Where(s => s.Value > 0f).Select(s => new { Title = s.Key.ToString(), Value = s.Value })) {
				AddCustomControl(skill.Title + skill.Value.ToString() + "class", new LinkControl(skill.Title + ": +" + (skill.Value * 100f).ToString() + "%", new Vector2(textBoxes["FullTextBox"].X + 3 + 425, textBoxes["FullTextBox"].Y + 3 + borderTop), content.Load<SpriteFont>(@"Fonts\Areal9.tk"), Color.White, null), false);

				borderTop += 13;
			}

			foreach (Skill skill in player.CurrentSkills.OrderBy(e => e.AffectType).ThenBy(e => e.Type).ThenBy(e => e.Level).ThenBy(e => e.Value)) {
				AddCustomControl(skill.Title + skill.Value.ToString() + "Used", new LinkControl(skill.Title.Replace("#value", skill.Value.ToString()), new Vector2(textBoxes["FullTextBox"].X + 3 + 425, textBoxes["FullTextBox"].Y + 3 + borderTop), content.Load<SpriteFont>(@"Fonts\Areal9.tk"), Color.White, null), false);

				borderTop += 13;
			}

			borderTop = 0;
			AddCustomControl("*avaiable*", new LinkControl("*verfügbare Fähigkeiten*", new Vector2(textBoxes["FullTextBox"].X + 3 + 240, textBoxes["FullTextBox"].Y + 3 + borderTop), content.Load<SpriteFont>(@"Fonts\Areal9.tk"), Color.White, null), false);
			borderTop += 13;

			foreach (var skill in player.AvaiableSkills.OrderBy(e => e.AffectType).ThenBy(e => e.Type).ThenBy(e => e.Level).ThenBy(e => e.Value).Select(s => new { Title = s.Title, Value = s.Value}).Distinct().ToList()) {
				AddCustomControl(skill.Title + skill.Value.ToString(), new LinkControl(skill.Title.Replace("#value", skill.Value.ToString()), new Vector2(textBoxes["FullTextBox"].X + 3 + 240, textBoxes["FullTextBox"].Y + 3 + borderTop), content.Load<SpriteFont>(@"Fonts\Areal9.tk"), Color.White, null), false);

				borderTop += 13;
			}

			////höhster Wert je Type und Level
			//foreach (Skill skill in player.AvaiableSkills.OrderBy(e => e.AffectType).ThenBy(e => e.Type).ThenBy(e => e.Level).ThenByDescending(e => e.Value).GroupBy(e => new { e.AffectType, e.Type, e.Level }).Select(e => e.First())) {
			//    AddCustomControl(skill.Title + skill.Value + "test".ToString(), new LinkControl(skill.Title.Replace("#value", skill.Value.ToString()), new Vector2(textBoxes["FullTextBox"].X + 3 + 350, textBoxes["FullTextBox"].Y + 3 + borderTop), content.Load<SpriteFont>(@"Fonts\Areal9.tk"), Color.White, null), false);

			//    borderTop += 13;
			//}
		}

		public void RefreshInformations() {
			foreach (Tuple<string, IControl, bool> control in base.CustomControls.Where(c => c.Item2.GetType() == typeof(LinkControl)).ToList()) {
				base.CustomControls.Remove(control);
			}
			ObjektManager.Reset();
			CreateInformations(player);

			Log.Add("In DetailsScreen", LogType.Debug);
			foreach (var item in player.UsedItems.Where(s => s.Value != null)) {
				Log.Add("UsedItem: " + item.ToString(), LogType.Debug);
			}

			foreach (var item in player.Inventory.Items.Select(s => s.Item)) {
				Log.Add("InvItem: " + item.ToString(), LogType.Debug);
			}
		}
	}
}

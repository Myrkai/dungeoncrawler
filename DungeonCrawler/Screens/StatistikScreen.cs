﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GUI.Screens;
using SharpDX.Toolkit.Graphics;
using SharpDX.Toolkit;
using GUI;
using DungeonCrawler.CoreElements;
using GUI.Controls;
using SharpDX.Toolkit.Content;
using SharpDX;
using RPG.Chars.Enum;
using DungeonCrawler;
using GUI.Interface;

namespace GUI.Screens {
	public class StatistikScreen : BaseScreen<Game1> {

		private List<Tuple<string, string>> textboxesSources;
		private Dictionary<string, Rectangle> textBoxes;

		private List<KeyValuePair<string, string>> Informations { get; set; }
		private CorePlayer player;
		private ContentManager content;

		public StatistikScreen(Game1 game, ScreenManager manager, ContentManager content)
			: base(game, manager) {
			this.content = content;

			textboxesSources = new List<Tuple<string, string>>();
			textboxesSources.Add(new Tuple<string, string>("14,17,628,705", "FullTextBox"));

			textBoxes = new Dictionary<string, Rectangle>();
			foreach (Tuple<string, string> box in textboxesSources) {
				int[] cord = box.Item1.Split(',').Select(b => Convert.ToInt32(b)).ToArray();
				textBoxes.Add(box.Item2, new Rectangle((int)(cord[0] * (Resolution.Scale - Resolution.ScaleAddon4_3)), (int)(cord[1] * (Resolution.Scale - Resolution.ScaleAddon4_3)), (int)((cord[2] - cord[0]) * (Resolution.Scale - Resolution.ScaleAddon4_3)), (int)((cord[3] - cord[1]) * (Resolution.Scale - Resolution.ScaleAddon4_3))));
			}
		}

		public override void Draw(SpriteBatch spriteBatch) {
			if (IsActiv && PerformDraw) {
				foreach (IControl control in CustomControls.Where(c => c.Item3).Select(c => c.Item2)) {
					control.Draw(spriteBatch);
				}

				foreach (IControl control in CustomControls.Where(c => !c.Item3).Select(c => c.Item2)) {
					control.Draw(spriteBatch);
				}
			}
		}

		public override void Update(GameTime gameTime) {
			if (IsActiv && PerformUpdate) {
				ObjektManager.Update(gameTime);
				foreach (IControl control in CustomControls.Select(c => c.Item2)) {
					control.Update(gameTime);
				}
			}
		}

		public override void Initialize() {

		}

		public override void LoadContent() {

		}

		public void CreateInformations(CorePlayer player) {
			this.player = player;

			int borderTop = 0;
			foreach (KeyValuePair<StatisticType, UInt64> type in player.Statistics) {
				AddCustomControl(type.Key.ToString(), new LinkControl(type.Key.ToString(), new Vector2(textBoxes["FullTextBox"].X + 3, textBoxes["FullTextBox"].Y + 3 + borderTop), content.Load<SpriteFont>(@"Fonts\Areal12.tk"), Color.White, null), false);
				AddCustomControl("value" + type.Key.ToString(), new LinkControl(type.Value.ToString(), new Vector2(textBoxes["FullTextBox"].X + 3 + 170, textBoxes["FullTextBox"].Y + 3 + borderTop), content.Load<SpriteFont>(@"Fonts\Areal12.tk"), Color.White, null), false);

				borderTop += 16;
			}
		}

		public void RefreshInformations() {
			CreateInformations(player);
		}
	}
}

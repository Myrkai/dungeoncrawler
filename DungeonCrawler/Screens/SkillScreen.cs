﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GUI.Screens;
using GUI;
using SharpDX.Toolkit.Graphics;
using GUI.Interface;
using SharpDX.Toolkit;
using DungeonCrawler;
using DungeonCrawler.Controls;
using SharpDX;
using RPG.Chars;
using DungeonCrawler.CoreElements;
using RPG.Items;
using DungeonCrawler.Controls.Skills;
using DungeonCrawler.CoreElements.Shop.Enum;
using SideProjekt.Mechanic.Log;

namespace GUI.Screens {
	/// <summary>
	/// Wie werden Skills aufgerüsten?
	/// Das SkillListControl listet sämtliche Fähigkeiten auf die der Spieler besitzt.
	/// Klick der Spieler doppelt auf ein SkillListeControlEntity so wird der dahinter befindliche Skill via Event über SkillScren an das SkillGridControl übertragen.
	/// Hier werden alle Skills angezeigt und können per Drag & Drop ausgerüstet werden beim Spieler.
	/// Hierbei werden nur Skills ausgerüstet die sich vollständig auf dem Grid befinden
	/// </summary>
	public class SkillScreen : BaseScreen<Game1> {

		private SkillListControl SkillListControl { get; set; }
		private SkillGridControl SkillGridControl { get; set; }
		private SelectedSkillControl CurrentSkill { get; set; }
		private CorePlayer Player { get; set; }

		public SkillScreen(Game1 game, ScreenManager manager, CorePlayer player) : base(game, manager) {
			Player = player;
		}

		public override void Draw(SpriteBatch spriteBatch) {
			if (IsActiv && PerformDraw) {
				foreach (IControl control in CustomControls.Where(c => c.Item3).Select(c => c.Item2)) {
					control.Draw(spriteBatch);
				}

				foreach (IControl control in CustomControls.Where(c => !c.Item3).Select(c => c.Item2)) {
					control.Draw(spriteBatch);
				}

				CurrentSkill.Draw(spriteBatch);
			}
		}

		public override void Update(GameTime gameTime) {
			if (IsActiv && PerformUpdate) {
				ObjektManager.Update(gameTime);
				foreach (IControl control in CustomControls.Select(c => c.Item2)) {
					control.Update(gameTime);
				}
			}
		}

		public override void Initialize() {

		}

		public override void LoadContent() {

		}

		public override void AddCustomControl(string key, IControl control, bool renderingFirst) {
			base.AddCustomControl(key, control, renderingFirst);

			if (control.GetType() == typeof(SkillListControl)) {
				((SkillListControl)control).HasDoubleClicked += new EventHandler<SkillListEventArgs>(SkillListControl_HasDoubleClicked);
				SkillListControl = (SkillListControl)control;
				SkillListControl.VisibleChanged += new EventHandler(SkillListControl_VisibleChanged);

				CurrentSkill = new SelectedSkillControl(new Vector2(SkillListControl.GetDimension().TopRight.X, SkillListControl.GetDimension().TopRight.Y), GameRef.Content);
				base.AddCustomControl("current", CurrentSkill, false);
			}

			if (control.GetType() == typeof(SkillGridControl)) {
				SkillGridControl = (SkillGridControl)control;
				SkillGridControl.SkillCompleteInGrid += new EventHandler<SkillGridEventArgs>(SkillGridControl_SkillCompleteInGrid);
				SkillGridControl.SkillNotInGrid += new EventHandler<SkillGridEventArgs>(SkillGridControl_SkillNotInGrid);
				SkillGridControl.SkillDeleted += new EventHandler<SkillGridEventArgs>(SkillGridControl_SkillDeleted);
				SkillGridControl.SkillSelected += new EventHandler<SkillGridEventArgs>(SkillGridControl_SkillSelected);
			}
		}

		void SkillGridControl_SkillSelected(object sender, SkillGridEventArgs e) {
			if (e == null) {
				CurrentSkill.Skill = null;
			} else {
				CurrentSkill.Skill = e.Skill;
			}
		}

		void SkillGridControl_SkillDeleted(object sender, SkillGridEventArgs e) {
			foreach (Skill skill in Player.AvaiableSkills.Where(s => s.Type == e.Skill.Type)) {
				SkillListControl.AddSkillControl(new SkillListEntityControl(skill, GameRef.Content.Load<SpriteFont>(@"Fonts\Areal12.tk"), GameRef.Content.Load<SpriteFont>(@"Fonts\Areal8Normal.tk"), new Vector2(50f, 50f)));
			}
			SkillListControl.ForceRefreshSkillEntityPositions();
		}

		void SkillListControl_VisibleChanged(object sender, EventArgs e) {
			SkillGridControl.DeleteArea = SkillListControl.GetDimension();
			CurrentSkill.Position = new Vector2(SkillListControl.GetDimension().TopRight.X, 4f);
			if (CustomControls.Where(c => c.Item1 == "background").FirstOrDefault() != null) {
				CurrentSkill.Resize(CustomControls.Where(c => c.Item1 == "background").FirstOrDefault().Item2.GetDimension().Width - SkillListControl.GetDimension().TopRight.X - 4);
			}
		}

		void SkillGridControl_SkillNotInGrid(object sender, SkillGridEventArgs e) {
			if (Player.CurrentSkills.Contains(e.Skill)) {
				Player.RemoveUseSkill(e.Skill);
				Player.RefreshAllStatValue();
				Player.ActiveSkillsFromGrid.Remove(e.Control);
			}
		}

		void SkillGridControl_SkillCompleteInGrid(object sender, SkillGridEventArgs e) {
			if (!Player.CurrentSkills.Contains(e.Skill)) {
				Player.AddUseSkill(e.Skill);
				Player.RefreshAllStatValue();
				Player.ActiveSkillsFromGrid.Add(e.Control);
			}
		}

		void SkillListControl_HasDoubleClicked(object sender, SkillListEventArgs e) {
			SkillListControl.RemoveSkillType(e.SelectedSkill.Type);
			SkillGridControl.AddSkill(e.SelectedSkill, new Vector2(SkillGridControl.GetGridDimension().BottomLeft.X, SkillGridControl.GetGridDimension().BottomLeft.Y + 10f));
		}

		public void Reset(CorePlayer player) {
			this.Player = player;

			foreach (var skill in player.ActiveSkillsFromGrid) {
				SkillGridControl.Skills.Add(skill);
			}

			Refresh();
		}

		public void Refresh() {
			if (SkillListControl != null && Player != null && SkillGridControl != null) {
				SkillListControl.RemoveAll();
				Player.AvaiableSkills.OrderBy(e => e.AffectType).ThenBy(e => e.Type).ThenBy(e => e.Level).ThenByDescending(e => e.Value).GroupBy(e => new { e.AffectType, e.Type, e.Level }).Select(e => e.First()).ToList().ForEach(e => SkillListControl.AddSkillControl(new SkillListEntityControl(e, GameRef.Content.Load<SpriteFont>(@"Fonts\Areal12.tk"), GameRef.Content.Load<SpriteFont>(@"Fonts\Areal8Normal.tk"), new Vector2(50f, 50f))));
				SkillListControl.offset = 0;
				SkillListControl.RefreshSkillEntityPositions();

				CurrentSkill.Skill = null;

				foreach (SkillGridEntityControl skill in SkillGridControl.Skills.Where(s => Player.CurrentSkills.FirstOrDefault(t => t.Equals(s.Skill)) == null).ToList()) {
					SkillGridControl.Remove(skill);
					Player.ActiveSkillsFromGrid.Remove(skill);
				}

				foreach (SkillGridEntityControl skill in SkillGridControl.Skills) {
					SkillListControl.RemoveSkillType(skill.Skill.Type);
				}

				int gridUpgradeCount = Player.Owner.ShopSkills.Where(s => s.Typ == BuyableSkillTyps.SkillGridExtension && s.Buyed).Count();
				SkillGridControl.SetGridDimension(4 + gridUpgradeCount, 4 + gridUpgradeCount);

				Log.Add("In SkillScreen", LogType.Debug);
				foreach (var item in Player.UsedItems.Where(s => s.Value != null)) {
					Log.Add("UsedItem: " + item.ToString(), LogType.Debug);
				}

				foreach (var item in Player.Inventory.Items.Select(s => s.Item)) {
					Log.Add("InvItem: " + item.ToString(), LogType.Debug);
				}
			}
		}
	}
}

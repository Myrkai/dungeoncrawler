﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpDX.Toolkit;
using SharpDX.Toolkit.Graphics;
using DungeonCrawler;
using GUI.Interface;
using DungeonCrawler.CoreElements;
using RPG.Items.Inventory;
using DungeonCrawler.Controls;
using SharpDX;
using Core.Items;
using RPG.Items;
using GUI.Controls;
using DungeonCrawler.CoreElements.Shop.Enum;
using RPG.Chars.Enum;
using Core;

namespace GUI.Screens {
	public class ShopScreen : BaseScreen<Game1> {

		private CorePlayer Player { get; set; }
		private InventoryControl PlayerInventory { get; set; }
		private InventoryControl ShopInventory { get; set; }
		private Vector2 Position { get; set; }

		private InventoryPosition CurrentDragedItem { get; set; }
		private Vector2 CurrentDragedPosition { get; set; }
		private ButtonControl ButtonControl { get; set; }

		private Random Random { get; set; }
		private ItemManager ItemManager { get; set; }

		public ShopScreen(Game1 game, ScreenManager manager, CorePlayer player, ItemManager itemManager, Random rnd)
			: base(game, manager) {
			Player = player;
			Random = rnd;
			ItemManager = itemManager;

			ShopInventory = new InventoryControl(24, 12, 24, 12);
			PlayerInventory = (InventoryControl)Player.Inventory;

			AddCustomControl("PlayerInventory", PlayerInventory, false);
			AddCustomControl("ShopInventory", ShopInventory, false);

			RefreshItems();

			Position = Vector2.Zero;
		}

		#region ShopInventoryEvents
		void ShopInventory_ItemMouseHoverEnd(object sender, InventoryEvent e) {
			currentShopToolTip = null;
		}

		void ShopInventory_ItemMouseHoverBegin(object sender, InventoryEvent e) {
			if (CurrentDragedItem == null) {
				SetShopToolTip(e.Item);
			}
		}

		void ShopInventory_ItemBeginDrag(object sender, InventoryEvent e) {
			CurrentDragedItem = e.InventoryPosition;
			CurrentDragedPosition = ShopInventory.Position;
			currentShopToolTip = null;
		}

		void ShopInventory_ItemEndDrag(object sender, InventoryEvent e) {
			CurrentDragedItem = null;
			currentShopToolTip = null;

			if (Player.Gold >= e.Item.Cost && PlayerInventory.GetActiveDimension().Intersects((e.Position + ShopInventory.Position).ToRectangle())) {
				if (PlayerInventory.AddItem(e.Item)) {
					Player.Gold -= e.Item.Cost;
					ShopInventory.RemoveItem(e.Item);
				}
			}
		}

		void ShopInventory_ItemDoubleClicked(object sender, InventoryEvent e) {
			CurrentDragedItem = null;
			currentShopToolTip = null;

			//prüfen ob Spieler genügend Gold besitz
			if (Player.Gold >= e.Item.Cost) {
				// prüfen ob genügend Platz im Inventar ist
				if (PlayerInventory.AddItem(e.Item)) {
					//  wenn ja, Geld abziehen, Item aus shop entfernen, item spieler hinzufügen
					Player.Gold -= e.Item.Cost;
					ShopInventory.RemoveItem(e.Item);
				}//  wenn nein, tue nix
			}
		}
		#endregion

		#region PlayerInventoryEvents
		void PlayerInventory_ItemMouseHoverEnd(object sender, InventoryEvent e) {
			currentInvToolTip = null;
		}

		void PlayerInventory_ItemMouseHoverBegin(object sender, InventoryEvent e) {
			if (CurrentDragedItem == null) {
				SetInvToolTip(e.Item);
			}
		}

		void PlayerInventory_ItemBeginDrag(object sender, InventoryEvent e) {
			CurrentDragedItem = e.InventoryPosition;
			CurrentDragedPosition = PlayerInventory.Position;
			currentInvToolTip = null;
		}

		void PlayerInventory_ItemEndDrag(object sender, InventoryEvent e) {
			CurrentDragedItem = null;

			//prüfen ob item über shopinventar ist
			// wenn ja item entfernen
			if (ShopInventory.GetDimension().Intersects((e.Position + PlayerInventory.Position).ToRectangle())) {
				currentInvToolTip = null;
				CurrentDragedItem = null;
				PlayerInventory.RemoveItem(e.Item);
				//Player.Gold += 10000000; //only for testing... selled items dont give gold
			}// wenn nein tue nix

		}

		void PlayerInventory_ItemDoubleClicked(object sender, InventoryEvent e) {
			currentInvToolTip = null;
			CurrentDragedItem = null;
			PlayerInventory.RemoveItem(e.Item);
			//Player.Gold += 10000000; //only for testing... selled items dont give gold
		}
		#endregion

		public override void Draw(SpriteBatch spriteBatch) {
			if (IsActiv && PerformDraw) {
				foreach (IControl control in CustomControls.Where(c => c.Item3).Select(c => c.Item2)) {
					control.Draw(spriteBatch);
				}

				foreach (IControl control in CustomControls.Where(c => !c.Item3).Select(c => c.Item2)) {
					control.Draw(spriteBatch);
				}

				//werden immer über allem anderen gezeichnet
				if (currentShopToolTip != null) {
					currentShopToolTip.Draw(spriteBatch);
				} else if (currentInvToolTip != null) {
					currentInvToolTip.Draw(spriteBatch);
				}

				if (CurrentDragedItem != null) {
					spriteBatch.Draw(CurrentDragedItem.back, CurrentDragedItem.Position + CurrentDragedPosition, Color.White * 0.45f);
					spriteBatch.Draw(CurrentDragedItem.Item.Texture, CurrentDragedItem.Position + CurrentDragedPosition, Color.White);
				}
			}
		}

		public override void Update(GameTime gameTime) {
			if (IsActiv && PerformUpdate) {
				if (((ButtonControl)CustomControls.Where(s => s.Item1 == "Gold").First().Item2).Value != "Gold: " + String.Format("{0:n0}", Player.Gold)) {
					((ButtonControl)CustomControls.Where(s => s.Item1 == "Gold").First().Item2).SetText("Gold: " + String.Format("{0:n0}", Player.Gold));
					((ButtonControl)CustomControls.Where(s => s.Item1 == "Gold").First().Item2).SetOffSetPositionText(new Vector2(10, ((ButtonControl)CustomControls.Where(s => s.Item1 == "Gold").First().Item2).OffSetPositionText.Y));
				}

				ObjektManager.Update(gameTime);
				foreach (IControl control in CustomControls.Select(c => c.Item2)) {
					control.Update(gameTime);
				}

				if (currentShopToolTip != null) {
					currentShopToolTip.Position = VirtualGamePad.GetClickPoint() - new Vector2(currentShopToolTip.GetDimension().Width / 2, -10);
					if (currentShopToolTip.GetDimension().BottomLeft.Y > Resolution.TargetHeight) {
						currentShopToolTip.Position = VirtualGamePad.GetClickPoint() - new Vector2(currentShopToolTip.GetDimension().Width / 2, currentShopToolTip.GetDimension().Height + 10);
					}
				} else if (currentInvToolTip != null) {
					currentInvToolTip.Position = VirtualGamePad.GetClickPoint() - new Vector2(currentInvToolTip.GetDimension().Width / 2, -10);
					if (currentInvToolTip.GetDimension().BottomLeft.Y > Resolution.TargetHeight) {
						currentInvToolTip.Position = VirtualGamePad.GetClickPoint() - new Vector2(currentInvToolTip.GetDimension().Width / 2, currentInvToolTip.GetDimension().Height + 10);
					}
				}
			}
		}

		public override void Initialize() {

		}

		public override void LoadContent() {

		}

		public override void AddCustomControl(string key, IControl control, bool renderingFirst) {
			base.AddCustomControl(key, control, renderingFirst);

			if (key == "background") {
				Position = new Vector2(control.GetDimension().X, control.GetDimension().Y);
				PlayerInventory.Position = new Vector2(Position.X + (control.GetDimension().Width / 2 - PlayerInventory.GetDimension().Width / 2), control.GetDimension().Height - PlayerInventory.GetDimension().Height - 10f);
				ShopInventory.Position = new Vector2(Position.X + (control.GetDimension().Width / 2 - ShopInventory.GetDimension().Width / 2), 10f);
			}
		}

		#region tooltip
		Dictionary<Item, ToolTipText> ShopToolTips = new Dictionary<Item, ToolTipText>();
		ToolTipText currentShopToolTip;
		private void SetShopToolTip(Item item) {
			if (ShopToolTips.ContainsKey(item)) {
				currentShopToolTip = ShopToolTips[item];
			} else {
				ShopToolTips.Add(item, new ToolTipText(item, Vector2.Zero, GameRef.Content.Load<SpriteFont>(@"Fonts\Areal11.tk"), true));
				currentShopToolTip = ShopToolTips[item];
			}
		}

		Dictionary<Item, ToolTipText> InvToolTips = new Dictionary<Item, ToolTipText>();
		ToolTipText currentInvToolTip;
		private void SetInvToolTip(Item item) {
			if (InvToolTips.ContainsKey(item)) {
				currentInvToolTip = InvToolTips[item];
			} else {
				InvToolTips.Add(item, new ToolTipText(item, Vector2.Zero, GameRef.Content.Load<SpriteFont>(@"Fonts\Areal11.tk"), false));
				currentInvToolTip = InvToolTips[item];
			}
		}
		#endregion

		public void RefreshItems() {
			ShopInventory.Clear();

			int itemCount = Random.Next(15, 40);
			List<Item> tmpList = new List<Item>();
			for (int i = 0; i < itemCount; i++) {
				//tmpList.Add((Item)itemManager.GenerateItem(player.Level));
				float magic = 0f;
				if (Player.Stats[StatType.MagicFind] > 1f) {
					magic = Player.Stats[StatType.MagicFind] / 100f;
				} else {
					magic = Player.Stats[StatType.MagicFind];
				}

				if (Engine.Instance.Randomizer.NextFloat(1, 11) <= 1f * (1f + magic)) { // 10 % aller Fälle + X
					tmpList.Add((Item)ItemManager.GenerateItem(Player.Level + 10, Player, Player.Stats[StatType.MagicFind]));
				} else {
					tmpList.Add((Item)ItemManager.GenerateItem(Player.Level, Player, Player.Stats[StatType.MagicFind]));
				}
			}
			foreach (Item item in tmpList.OrderBy(t => t.Type)) {
				ShopInventory.AddItem(item);
			}
		}

		public void Reset(CorePlayer player) {
			this.Player = player;

			RemoveEvents();
			ShopToolTips.Clear();
			InvToolTips.Clear();

			Vector2 shopPos = ShopInventory.Position;
			Vector2 playerPos = PlayerInventory.Position;
			ShopInventory = new InventoryControl(24, 12, 24, 12);
			PlayerInventory = (InventoryControl)Player.Inventory;
			ShopInventory.Position = shopPos;
			PlayerInventory.Position = playerPos;

			AddCustomControl("PlayerInventory", PlayerInventory, false);
			AddCustomControl("ShopInventory", ShopInventory, false);

			int itemCount = Random.Next(15, 40);
			List<Item> tmpList = new List<Item>();
			for (int i = 0; i < itemCount; i++) {
				tmpList.Add((Item)ItemManager.GenerateItem(player.Level, player, player.Stats[StatType.MagicFind]));
			}
			foreach (Item item in tmpList.OrderBy(t => t.Type)) {
				ShopInventory.AddItem(item);
			}

			int inventoryUpgradeCount = Player.Owner.ShopSkills.Where(s => s.Typ == BuyableSkillTyps.InventoryExtension && s.Buyed).Count();
			PlayerInventory.SetInventoryDimension(4 + 4 * inventoryUpgradeCount, 6);
		}

		public void CreateEvents() {
			ShopInventory.ItemDoubleClicked += new EventHandler<InventoryEvent>(ShopInventory_ItemDoubleClicked);
			ShopInventory.ItemBeginDrag += new EventHandler<InventoryEvent>(ShopInventory_ItemBeginDrag);
			ShopInventory.ItemEndDrag += new EventHandler<InventoryEvent>(ShopInventory_ItemEndDrag);
			ShopInventory.ItemMouseHoverBegin += new EventHandler<InventoryEvent>(ShopInventory_ItemMouseHoverBegin);
			ShopInventory.ItemMouseHoverEnd += new EventHandler<InventoryEvent>(ShopInventory_ItemMouseHoverEnd);

			PlayerInventory.ItemDoubleClicked += new EventHandler<InventoryEvent>(PlayerInventory_ItemDoubleClicked);
			PlayerInventory.ItemBeginDrag += new EventHandler<InventoryEvent>(PlayerInventory_ItemBeginDrag);
			PlayerInventory.ItemEndDrag += new EventHandler<InventoryEvent>(PlayerInventory_ItemEndDrag);
			PlayerInventory.ItemMouseHoverBegin += new EventHandler<InventoryEvent>(PlayerInventory_ItemMouseHoverBegin);
			PlayerInventory.ItemMouseHoverEnd += new EventHandler<InventoryEvent>(PlayerInventory_ItemMouseHoverEnd);

			int inventoryUpgradeCount = Player.Owner.ShopSkills.Where(s => s.Typ == BuyableSkillTyps.InventoryExtension && s.Buyed).Count();
			PlayerInventory.SetInventoryDimension(4 + 4 * inventoryUpgradeCount, 6);
		}

		public void RemoveEvents() {
			ShopInventory.ItemDoubleClicked -= ShopInventory_ItemDoubleClicked;
			ShopInventory.ItemBeginDrag -= ShopInventory_ItemBeginDrag;
			ShopInventory.ItemEndDrag -= ShopInventory_ItemEndDrag;
			ShopInventory.ItemMouseHoverBegin -= ShopInventory_ItemMouseHoverBegin;
			ShopInventory.ItemMouseHoverEnd -= ShopInventory_ItemMouseHoverEnd;

			PlayerInventory.ItemDoubleClicked -= PlayerInventory_ItemDoubleClicked;
			PlayerInventory.ItemBeginDrag -= PlayerInventory_ItemBeginDrag;
			PlayerInventory.ItemEndDrag -= PlayerInventory_ItemEndDrag;
			PlayerInventory.ItemMouseHoverBegin -= PlayerInventory_ItemMouseHoverBegin;
			PlayerInventory.ItemMouseHoverEnd -= PlayerInventory_ItemMouseHoverEnd;
		}
	}
}

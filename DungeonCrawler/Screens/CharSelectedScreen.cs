﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GUI.Screens;
using SharpDX.Toolkit.Graphics;
using SharpDX.Toolkit;
using GUI.Interface;
using GUI;
using SharpDX;
using DungeonCrawler;
using GUI.Controls;
using RPG.Chars;
using Core.Enums;
using DungeonCrawler.CoreElements;
using SideProjekt.Mechanic.Log;
using RPG.Sprites;
using Core;

namespace GUI.Screens {
	public class CharSelectedScreen : BaseScreen<Game1> {

		private PlayerAccount Account { get; set; }
		private const float Scale = 2f;
		private int CurrentIndex { get; set; }

		private SpriteFont Font { get; set; }
		private Vector2 Offset { get; set; }

		public CharSelectedScreen(Game1 game, ScreenManager manager, PlayerAccount account)
			: base(game, manager) {
			Account = account;
			CurrentIndex = 0;

			Font = GameRef.Content.Load<SpriteFont>(@"Fonts\Areal48.tk");

			for (int i = 0; i < Account.GetAvaiablePlayerCount(); i++) {
				Account.GetPlayer(i).PlayerAnimation.SetDirection(AnimationDirection.DOWN);
			}
		}

		public override void Draw(SpriteBatch spriteBatch) {
			if (IsActiv && PerformDraw) {
				foreach (IControl control in CustomControls.Where(c => c.Item3).Select(c => c.Item2)) {
					control.Draw(spriteBatch);
				}

				Vector2 center = Resolution.Size / 2f - new Vector2(16f * Scale, 16f * Scale + 100);
				float rotateAmount = 360f / Account.GetAvaiablePlayerCount();

				// Aktuell ausgewählter Char wird sehr gross im Kreis angezeigt
				Account.GetPlayer(CurrentIndex).PlayerAnimation.Draw(spriteBatch, center - new Vector2(16f * Scale, 16f * Scale), Scale * 2);

				// Name des ausgewählten Chars schreiben
				spriteBatch.DrawString(Font, Account.GetPlayer(CurrentIndex).Name, Offset, Color.Black);

				// Anzeige aller Chars in Form eines Ringes
				for (int i = 0; i < Account.GetAvaiablePlayerCount(); i++) {
					int index = i + CurrentIndex;
					if (index >= Account.GetAvaiablePlayerCount()) {
						index -= Account.GetAvaiablePlayerCount();
					}

					float scaleAddon = (Math.Abs((i * rotateAmount) - 180f) / 360f) + 0.5f;

					Account.GetPlayer(index).PlayerAnimation.Draw(spriteBatch, MathHelper.RotateAPoint(center + new Vector2(0, 200f), 0f + (i * rotateAmount), center), Scale * scaleAddon, scaleAddon);
				}

				foreach (IControl control in CustomControls.Where(c => !c.Item3).Select(c => c.Item2)) {
					control.Draw(spriteBatch);
				}
			}
		}

		public override void Update(GameTime gameTime) {
			if (IsActiv && PerformUpdate) {
				ObjektManager.Update(gameTime);

				foreach (IControl control in CustomControls.Select(c => c.Item2)) {
					control.Update(gameTime);
				}

				Account.GetPlayer(CurrentIndex).PlayerAnimation.Update(gameTime);
			}
		}

		public override void Initialize() {

		}

		public override void LoadContent() {

		}

		public override void AddCustomControl(string key, IControl control, bool renderingFirst) {
			base.AddCustomControl(key, control, renderingFirst);
		}

		public void NextCharacter() {
			CurrentIndex = (CurrentIndex + 1) % Account.GetAvaiablePlayerCount();

			ButtonControl PreviousCharButton = (ButtonControl)CustomControls.Where(s => s.Item1 == "PreviousCharButton").First().Item2;
			ButtonControl NextCharButton = (ButtonControl)CustomControls.Where(s => s.Item1 == "NextCharButton").First().Item2;
			float freeSpace = NextCharButton.Position.X - (PreviousCharButton.Position.X + PreviousCharButton.GetDimension().Width);

			Vector2 stringDimension = Font.MeasureString(Account.GetPlayer(CurrentIndex).Name);
			Offset = new Vector2(PreviousCharButton.Position.X - 5 + PreviousCharButton.GetDimension().Width, PreviousCharButton.Position.Y - 15) + new Vector2((freeSpace - stringDimension.X) / 2f, 0f);
		}

		public void PreviousCharacter() {
			CurrentIndex = CurrentIndex - 1 < 0 ? Account.GetAvaiablePlayerCount() - 1 : CurrentIndex - 1;

			ButtonControl PreviousCharButton = (ButtonControl)CustomControls.Where(s => s.Item1 == "PreviousCharButton").First().Item2;
			ButtonControl NextCharButton = (ButtonControl)CustomControls.Where(s => s.Item1 == "NextCharButton").First().Item2;
			float freeSpace = NextCharButton.Position.X - (PreviousCharButton.Position.X + PreviousCharButton.GetDimension().Width);

			Vector2 stringDimension = Font.MeasureString(Account.GetPlayer(CurrentIndex).Name);
			Offset = new Vector2(PreviousCharButton.Position.X - 5 + PreviousCharButton.GetDimension().Width, PreviousCharButton.Position.Y - 15) + new Vector2((freeSpace - stringDimension.X) / 2f, 0f);
		}

		public void ChooseCharacter() {
			CorePlayer player = Account.GetPlayer(CurrentIndex);

			if (player.GetGameModeAddons().Contains(GameModeAddons.Hardcore)) {
				//Todo: auswahl wieviele Ebenen der Spieler schaffen möchte
				Log.Add("Todo: auswahl wieviele Ebenen der Spieler schaffen möchte");
				player.SetLayersToBeat(10);
			}

			if (player.GetGameModeAddons().Contains(GameModeAddons.Ironman)) {
				//Todo: verschiebe alle Gegenstände in die Bank
				Log.Add("Todo: verschiebe alle Gegenstände in die Bank");
			}

			// Todo: Auswahl ob der Spieler den Spielstand fortsetzen will
			Boolean continune = GameOptions.autoContinue;
			if (continune && player.GetLayer() > 1) {
				player.SetNextBattleTime(Engine.Instance.Randomizer);
			} else {
				player.Reset(Engine.Instance.Randomizer);
			}

			GameRef.ResetScreens(player);
			ScreenManagerRef.Transfer(Game1.GAMESCREEN);

			Log.Add("In CharSelectScreen", LogType.Debug);
			foreach (var item in player.UsedItems.Where(s => s.Value != null)) {
				Log.Add("UsedItem: " + item.ToString(), LogType.Debug);
			}

			foreach (var item in player.Inventory.Items.Select(s => s.Item)) {
				Log.Add("InvItem: " + item.ToString(), LogType.Debug);
			}
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GUI.Screens;
using SharpDX.Toolkit.Graphics;
using SharpDX.Toolkit;
using GUI.Interface;
using GUI;
using DungeonCrawler.CoreElements;
using GUI.Controls;
using SharpDX;
using DungeonCrawler.CoreElements.Shop.Enum;
using DungeonCrawler;
using DungeonCrawler.CoreElements.Shop;
using SideProjekt.Mechanic.Log;

namespace GUI.Screens {
	public class UpgradesScreen : BaseScreen<Game1> {
		private CorePlayer Player { get; set; }
		private Vector2 Position { get; set; }

		private ImageControl FilterBackground { get; set; }
		private SelectBoxControl CurrentFilterSelectbox { get; set; }
		private CheckBoxControl OnlyBuyedCheckbox { get; set; }
		private LinkControl CurrentFilterTitle { get; set; }
		private LinkControl OnlyBuyedTitle { get; set; }

		private TableControl UpgradesTable { get; set; }
		private ButtonControl BeforeIndex { get; set; }
		private ButtonControl NextIndex { get; set; }
		private ButtonControl CurrentGold { get; set; }

		public UpgradesScreen(Game1 game, ScreenManager manager, CorePlayer player)
			: base(game, manager) {
			Player = player;
			Position = Vector2.Zero;

			//Alle Elemente werden ohne Position angelegt, die Final Positon wird erst bestimmt wenn das Hintergrundbild hinzugefügt wird.
			//Grund: das Hintergrundbild ist rechtsbündig und dessen Position kann sich je nach Auflösung ändern.
			List<string> filterSetting = new List<string>() { "All", "AllSorted" };
			filterSetting.AddRange(BuyableSkillTyps.BankInventoryExtension.EnumToList<BuyableSkillTyps>());
			CurrentFilterSelectbox = new SelectBoxControl(filterSetting, GameRef.Content.Load<SpriteFont>(@"Fonts\Areal12.tk"), Color.White, 100);

			OnlyBuyedCheckbox = new CheckBoxControl(GameRef.Content.Load<SpriteFont>(@"Fonts\Areal12.tk"), Color.White, Vector2.Zero);
			OnlyBuyedCheckbox.SetValue(true);

			CurrentFilterTitle = new LinkControl("Upgrades-Filter:", Vector2.Zero, GameRef.Content.Load<SpriteFont>(@"Fonts\Areal12.tk"), Color.White, null);
			OnlyBuyedTitle = new LinkControl("Zeige gekaufte Upgrades:", Vector2.Zero, GameRef.Content.Load<SpriteFont>(@"Fonts\Areal12.tk"), Color.White, null);

			FilterBackground = new ImageControl(TextureHelper.CreateTextureWidthBoarder(10, 10, Color.Black, 2, Color.DarkGray, GameRef.GraphicsDevice), Vector2.Zero, 0.8f);
			UpgradesTable = new TableControl(Vector2.Zero, TextureHelper.CreateTextureWidthBoarder(10, 10, Color.Black, 2, Color.DarkGray, GameRef.GraphicsDevice));

			BeforeIndex = new ButtonControl("<<", Vector2.Zero, GameRef.Content.Load<SpriteFont>(@"Fonts\Areal12.tk"), TextureHelper.CreateTextureWidthBoarder(60, 40, Color.Black, 2, Color.White, GameRef.GraphicsDevice), Color.White, () => UpgradesTable.Index = Math.Max(0, UpgradesTable.Index - 1), Color.White * 0.8f);
			NextIndex = new ButtonControl(">>", Vector2.Zero, GameRef.Content.Load<SpriteFont>(@"Fonts\Areal12.tk"), TextureHelper.CreateTextureWidthBoarder(60, 40, Color.Black, 2, Color.White, GameRef.GraphicsDevice), Color.White, () => UpgradesTable.Index = Math.Min(UpgradesTable.MaxIndex, UpgradesTable.Index + 1), Color.White * 0.8f);
			CurrentGold = new ButtonControl("Gold: " + String.Format("{0:n0}", Player.Gold), Vector2.Zero, GameRef.Content.Load<SpriteFont>(@"Fonts\Areal12.tk"), TextureHelper.CreateTextureWidthBoarder(200, 40, Color.Black, 2, Color.White, GameRef.GraphicsDevice), Color.White, null, Color.White * 0.8f);

			AddCustomControl("FilterBackground", FilterBackground, false);
			AddCustomControl("currentFilterTitle", CurrentFilterTitle, false);
			AddCustomControl("currentFilter", CurrentFilterSelectbox, false);
			AddCustomControl("OnlyBuyedTitle", OnlyBuyedTitle, false);
			AddCustomControl("OnlyBuyedCheckbox", OnlyBuyedCheckbox, false);
			AddCustomControl("UpgradesTable", UpgradesTable, false);
			AddCustomControl("before", BeforeIndex, false);
			AddCustomControl("next", NextIndex, false);
			AddCustomControl("gold", CurrentGold, false);
		}

		void UpgradesTable_EntitySelected(object sender, TableControlEventArgs e) {
			if (e.Data.Control.Value != "gekauft" && Player.Gold >= Convert.ToUInt32(e.Data.Control.Value.Replace(".", ""))) {
				Player.Owner.ShopSkills.Where(s => s.Skill.ToString() == e.Data.Control.Title).First().Buyed = true;
				Player.Gold -= Convert.ToUInt32(e.Data.Control.Value.Replace(".", ""));
				RefreshFilter();
			}
		}

		void CurrentFilterSelectbox_ValueChanged(object sender, EventArgs e) {
			RefreshFilter();
		}

		void OnlyBuyedCheckbox_ValueChanged(object sender, EventArgs e) {
			RefreshFilter();
		}

		public override void Draw(SpriteBatch spriteBatch) {
			if (IsActiv && PerformDraw) {
				foreach (IControl control in CustomControls.Where(c => c.Item3).Select(c => c.Item2)) {
					control.Draw(spriteBatch);
				}

				foreach (IControl control in CustomControls.Where(c => !c.Item3).Select(c => c.Item2)) {
					control.Draw(spriteBatch);
				}
			}
		}

		public override void Update(GameTime gameTime) {
			if (IsActiv && PerformUpdate) {
				ObjektManager.Update(gameTime);
				foreach (IControl control in CustomControls.Select(c => c.Item2)) {
					control.Update(gameTime);
				}

				if ("Gold: " + String.Format("{0:n0}", Player.Gold) != CurrentGold.Text.Text) {
					CurrentGold.SetText("Gold: " + String.Format("{0:n0}", Player.Gold));
				}
			}
		}

		public override void Initialize() {

		}

		public override void LoadContent() {

		}

		public override void AddCustomControl(string key, IControl control, bool renderingFirst) {
			base.AddCustomControl(key, control, renderingFirst);

			if (key == "background") {
				Position = new Vector2(control.GetDimension().X, control.GetDimension().Y);

				FilterBackground.Texture = TextureHelper.CreateTextureWidthBoarder(control.GetDimension().Width - 20, 53, Color.Black, 2, Color.White, GameRef.GraphicsDevice);
				FilterBackground.Position = Position + new Vector2(10f, 10f);

				CurrentFilterTitle.Position = Position + new Vector2(17f, 15f);
				CurrentFilterSelectbox.SetPosition(CurrentFilterTitle.Position + new Vector2(CurrentFilterTitle.GetDimension().Width + 10f, 0f));
				OnlyBuyedTitle.Position = new Vector2(CurrentFilterTitle.GetDimension().TopLeft.X, CurrentFilterTitle.GetDimension().BottomLeft.Y + 5f);
				OnlyBuyedCheckbox.SetPosition(OnlyBuyedTitle.Position + new Vector2(OnlyBuyedTitle.GetDimension().Width + 10f, 0f));

				UpgradesTable.Position = new Vector2(FilterBackground.Position.X, FilterBackground.GetDimension().BottomLeft.Y + 50f);
				UpgradesTable.Background = TextureHelper.CreateTextureWidthBoarder(control.GetDimension().Width - 20, control.GetDimension().Height - (int)UpgradesTable.Position.Y - 10, Color.Black, 2, Color.White, GameRef.GraphicsDevice);

				BeforeIndex.SetPosition(new Vector2(FilterBackground.GetDimension().X, FilterBackground.GetDimension().BottomLeft.Y + 5f));
				NextIndex.SetPosition(new Vector2(FilterBackground.GetDimension().X + UpgradesTable.Background.Width - NextIndex.Texture.Width, FilterBackground.GetDimension().BottomLeft.Y + 5f));
				CurrentGold.SetPosition(new Vector2(FilterBackground.GetDimension().X + UpgradesTable.Background.Width / 2 - CurrentGold.Texture.Width / 2, FilterBackground.GetDimension().BottomLeft.Y + 5f));

				RefreshFilter();
			}
		}

		private void RefreshFilter() {
			UpgradesTable.ClearEntity();

			if (CurrentFilterSelectbox.Text == "All") {
				if (OnlyBuyedCheckbox.GetValue() == true) {
					foreach (ShopSkill skill in Player.Owner.ShopSkills) {
						UpgradesTable.AddEntity(skill.Skill.ToString(), new TableEntityControl(skill.Skill.ToString(), skill.Buyed ? "gekauft" : String.Format("{0:n0}", skill.Cost), GameRef.Content.Load<SpriteFont>(@"Fonts\Areal12.tk"), Color.White, UpgradesTable.Background.Width - 20f));
					}
				} else {
					foreach (ShopSkill skill in Player.Owner.ShopSkills.Where(s => s.Buyed == false)) {
						UpgradesTable.AddEntity(skill.Skill.ToString(), new TableEntityControl(skill.Skill.ToString(), skill.Buyed ? "gekauft" : String.Format("{0:n0}", skill.Cost), GameRef.Content.Load<SpriteFont>(@"Fonts\Areal12.tk"), Color.White, UpgradesTable.Background.Width - 20f));
					}
				}
			} if (CurrentFilterSelectbox.Text == "AllSorted") {
				if (OnlyBuyedCheckbox.GetValue() == true) {
					foreach (ShopSkill skill in Player.Owner.ShopSkills.OrderBy(s => s.Cost)) {
						UpgradesTable.AddEntity(skill.Skill.ToString(), new TableEntityControl(skill.Skill.ToString(), skill.Buyed ? "gekauft" : String.Format("{0:n0}", skill.Cost), GameRef.Content.Load<SpriteFont>(@"Fonts\Areal12.tk"), Color.White, UpgradesTable.Background.Width - 20f));
					}
				} else {
					foreach (ShopSkill skill in Player.Owner.ShopSkills.Where(s => s.Buyed == false).OrderBy(s => s.Cost)) {
						UpgradesTable.AddEntity(skill.Skill.ToString(), new TableEntityControl(skill.Skill.ToString(), skill.Buyed ? "gekauft" : String.Format("{0:n0}", skill.Cost), GameRef.Content.Load<SpriteFont>(@"Fonts\Areal12.tk"), Color.White, UpgradesTable.Background.Width - 20f));
					}
				}
			} else {
				if (OnlyBuyedCheckbox.GetValue() == true) {
					foreach (ShopSkill skill in Player.Owner.ShopSkills.Where(s => s.Typ.ToString() == CurrentFilterSelectbox.Text)) {
						UpgradesTable.AddEntity(skill.Skill.ToString(), new TableEntityControl(skill.Skill.ToString(), skill.Buyed ? "gekauft" : String.Format("{0:n0}", skill.Cost), GameRef.Content.Load<SpriteFont>(@"Fonts\Areal12.tk"), Color.White, UpgradesTable.Background.Width - 20f));
					}
				} else {
					foreach (ShopSkill skill in Player.Owner.ShopSkills.Where(s => s.Typ.ToString() == CurrentFilterSelectbox.Text && s.Buyed == false)) {
						UpgradesTable.AddEntity(skill.Skill.ToString(), new TableEntityControl(skill.Skill.ToString(), skill.Buyed ? "gekauft" : String.Format("{0:n0}", skill.Cost), GameRef.Content.Load<SpriteFont>(@"Fonts\Areal12.tk"), Color.White, UpgradesTable.Background.Width - 20f));
					}
				}
			}

			Log.Add("In UpgradesScreen", LogType.Debug);
			foreach (var item in Player.UsedItems.Where(s => s.Value != null)) {
				Log.Add("UsedItem: " + item.ToString(), LogType.Debug);
			}

			foreach (var item in Player.Inventory.Items.Select(s => s.Item)) {
				Log.Add("InvItem: " + item.ToString(), LogType.Debug);
			}
		}

		public void Reset(CorePlayer player) {
			Player = player;

			RemoveEvents();
			RefreshFilter();
		}

		public void AddEvents() {
			CurrentFilterSelectbox.ValueChanged += new EventHandler(CurrentFilterSelectbox_ValueChanged);
			OnlyBuyedCheckbox.ValueChanged += new EventHandler(OnlyBuyedCheckbox_ValueChanged);
			UpgradesTable.EntitySelected += new EventHandler<TableControlEventArgs>(UpgradesTable_EntitySelected);
		}

		public void RemoveEvents() {
			CurrentFilterSelectbox.ValueChanged -= CurrentFilterSelectbox_ValueChanged;
			OnlyBuyedCheckbox.ValueChanged -= OnlyBuyedCheckbox_ValueChanged;
			UpgradesTable.EntitySelected -= UpgradesTable_EntitySelected;
		}
	}
}

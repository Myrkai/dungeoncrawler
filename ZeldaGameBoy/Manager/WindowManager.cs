﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZeldaGameBoy.Components;
using SharpDX.Toolkit;
using SharpDX.Toolkit.Graphics;

namespace ZeldaGameBoy {
	public class WindowManager {
		private Dictionary<string, Window> Windows { get; set; }

		private static WindowManager _instance { get; set; }
		public static WindowManager Instance { get { if (_instance == null) _instance = new WindowManager(); return _instance; } }

		private WindowManager() {
			Windows = new Dictionary<string, Window>();
		}

		public void NewWindow(string id, Window window) {
			if (!Windows.ContainsKey(id)) {
				Windows.Add(id, window);
			}
		}

		public void RemoveAll() {
			Windows.Clear();
		}

		public Window GetWindow(string id) {
			return Windows.ContainsKey(id) ? Windows[id] : null;
		}

		public bool Contains(string id) {
			return Windows.ContainsKey(id);
		}

		public void Update(GameTime gameTime) {
			foreach (Window window in Windows.Values) {
				if (window.Active && !window.Done) {
					window.Update(gameTime);
				}
			}

			Windows = Windows.Where(s => !s.Value.Done).ToDictionary(s => s.Key, s => s.Value);
		}

		public void Draw(SpriteBatch spriteBatch) {
			foreach (Window window in Windows.Values) {
				if (window.Active && !window.Done) {
					window.Draw(spriteBatch);
				}
			}
		}
	}
}

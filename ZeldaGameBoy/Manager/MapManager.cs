﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tiledmap = TiledMap.TiledMap;
using SharpDX.Toolkit.Graphics;
using SharpDX.Toolkit;

namespace ZeldaGameBoy {
	public class MapManager {
		private Dictionary<string, Map> Maps { get; set; }
		public Map CurrentMap { get; private set; }

		public MapManager() {
			Maps = new Dictionary<string, Map>();
		}

		public void AddMap(string name, Map map) {
			if (Maps.ContainsKey(name)) {
				Maps[name] = map;
			} else {
				Maps.Add(name, map);
			}
		}

		public bool SwitchMap(string name) {
			if (Maps.ContainsKey(name)) {
				if (CurrentMap != null) {
					BaseObject player = CurrentMap.Player;
					CurrentMap.Player = null;
					CurrentMap = Maps[name];
					CurrentMap.Player = player;
				} else {
					CurrentMap = Maps[name];
				}
				
				return true;
			} else {
				return false;
			}
		}

		public void Draw(SpriteBatch spritebatch) {
			CurrentMap.Draw(spritebatch);
			CurrentMap.GetEntities().Where(b => CurrentMap.Cam.Rectangle.Intersects(b.GetComponent<Animation>(ComponentType.Animation).PositionRec.AddVector(CurrentMap.Cam.Position))).ToList().ForEach(b => b.Draw(spritebatch));
			CurrentMap.Player.Draw(spritebatch);
		}

		public void Update(GameTime gametime) {
			CurrentMap.Update(gametime);
			CurrentMap.GetEntities().ForEach(b => b.Update(gametime));
			CurrentMap.GetEntities().Where(b => CurrentMap.Cam.Rectangle.Intersects(b.GetComponent<Animation>(ComponentType.Animation).PositionRec.AddVector(CurrentMap.Cam.Position))).ToList().ForEach(b => b.Update(gametime));
			CurrentMap.Player.Update(gametime);
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpDX.Toolkit.Content;
using NLua;

namespace ZeldaGameBoy {
	public class LuaScriptManager {
		private Lua CurrentScript { get; set; }

		private static LuaScriptManager _Instance { get; set; }
		public static LuaScriptManager Instance { get { if (_Instance == null) _Instance = new LuaScriptManager(); return _Instance; } }
		
		private ContentManager Content { get; set; }
		private MapManager MapManager { get; set; }

		public void Initialize(ContentManager content, MapManager mapManager) {
			Content = content;
			MapManager = mapManager;

			CurrentScript = new Lua();
			CurrentScript.RegisterFunction("ShowMessage", this, this.GetType().GetMethod("NewMessageWindow"));
			CurrentScript.RegisterFunction("RaiseMessageEvent", this, this.GetType().GetMethod("NewMessageEvent"));
		}

		public void RunScript(string script) {
			CurrentScript.DoString(script);
		}

		#region LuaFunctions
		public void NewMessageWindow(string id, string message) {
			WindowManager.Instance.NewWindow(id, new MessageWindow(Content, message));
		}

		public void NewMessageEvent(string message) {
			EventManager.Instance.AddEvent(new GameEventMessage(message));
		}
		#endregion
	}
}

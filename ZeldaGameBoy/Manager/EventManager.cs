﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpDX.Toolkit;
using SharpDX.Toolkit.Graphics;
using SharpDX.Toolkit.Content;

namespace ZeldaGameBoy {
	public class EventManager {
		private Queue<IGameEvent> GameEvents { get; set; }
		private IGameEvent CurrentEvent { get; set; }
		private ContentManager Content { get; set; }

		private static EventManager _instance { get; set; }
		public static EventManager Instance { get { if (_instance == null) _instance = new EventManager(); return _instance; } }

		private EventManager() {
			GameEvents = new Queue<IGameEvent>();
		}

		public void Init(ContentManager content) {
			Content = content;
		}

		public void AddEvent(IGameEvent gameEvent) {
			GameEvents.Enqueue(gameEvent);
		}

		public void Update(GameTime gameTime) {
			if (CurrentEvent != null) {
				CurrentEvent.Update(gameTime);

				if (CurrentEvent.Done && GameEvents.Count > 0) {
					CurrentEvent = GameEvents.Dequeue();
					CurrentEvent.Initialize(Content);
				}
			} else {
				if (GameEvents.Count > 0) {
					CurrentEvent = GameEvents.Dequeue();
					CurrentEvent.Initialize(Content);
				}
			}
		}

		public void Draw(SpriteBatch spriteBatch) {
			if (CurrentEvent != null) {
				CurrentEvent.Draw(spriteBatch);
			}
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpDX.Toolkit.Input;
using SharpDX.Toolkit;
using SharpDX.Toolkit.Graphics;
using SharpDX;

namespace ZeldaGameBoy {
	public enum GameKeys {
		Up,
		Down,
		Left,
		Right,
		Exit,
		A,
		B,
		Start,
		Select
	}

	public enum GameKeysState {
		//Button is in a none state.
		None = 0,
		// The button is being pressed.
		Down = 1,
		// The button was pressed since last frame.
		Pressed = 2,
		//The button was released since last frame.
		Released = 4
	}

	public static class VirtualGamePad {
		static MouseManager mouseManager;
		static MouseState mouseState;
		static int screenWidth;
		static int screenHeight;
		static KeyboardManager keyBoardManager;
		static KeyboardState keyBoardState;
		public static bool ignoreOneFrame = false;
		public static bool SuspentInputs = false;

		const Keys ExitKey = Keys.Escape;
		const Keys UpKey = Keys.Up;
		const Keys DownKey = Keys.Down;
		const Keys LeftKey = Keys.Left;
		const Keys RightKey = Keys.Right;
		const Keys AKey = Keys.S;
		const Keys BKey = Keys.A;
		const Keys StartKey = Keys.Enter;
		const Keys SelectKey = Keys.Space;

		public static void Initialize(Game game, GraphicsDevice device) {
			mouseManager = new MouseManager(game);
			keyBoardManager = new KeyboardManager(game);

			var backbuffer = device.BackBuffer;
			screenWidth = backbuffer.Width;
			screenHeight = backbuffer.Height;
		}

		public static void Update(GameTime gameTime) {
			mouseState = mouseManager.GetState();
			keyBoardState = keyBoardManager.GetState();
			if (ignoreOneFrame) {
				ignoreOneFrame = false;
			}
		}

		public static bool ClickReleased() {
			return mouseState.LeftButton.Released;
		}

		public static bool ClickBegin() {
			return mouseState.LeftButton.Pressed;
		}

		public static bool ClickHolded() {
			return mouseState.LeftButton.Down;
		}

		public static Vector2 GetMousePoint() {
			return new Vector2(mouseState.X * screenWidth, mouseState.Y * screenHeight);
		}

		public static bool GetGameKeyState(GameKeys gameKey, GameKeysState state, bool forceKeyget = false) {
			if (!ignoreOneFrame && !SuspentInputs || forceKeyget) {
				switch (gameKey) {
					case GameKeys.Exit:
						if (keyBoardState.IsKeyDown(ExitKey) && state == GameKeysState.Down) {
							return true;
						}

						if (keyBoardState.IsKeyPressed(ExitKey) && state == GameKeysState.Pressed) {
							return true;
						}

						if (keyBoardState.IsKeyReleased(ExitKey) && state == GameKeysState.Released) {
							return true;
						}
						break;
					case GameKeys.Up:
						if (keyBoardState.IsKeyDown(UpKey) && state == GameKeysState.Down) {
							return true;
						}

						if (keyBoardState.IsKeyPressed(UpKey) && state == GameKeysState.Pressed) {
							return true;
						}

						if (keyBoardState.IsKeyReleased(UpKey) && state == GameKeysState.Released) {
							return true;
						}
						break;
					case GameKeys.Down:
						if (keyBoardState.IsKeyDown(DownKey) && state == GameKeysState.Down) {
							return true;
						}

						if (keyBoardState.IsKeyPressed(DownKey) && state == GameKeysState.Pressed) {
							return true;
						}

						if (keyBoardState.IsKeyReleased(DownKey) && state == GameKeysState.Released) {
							return true;
						}
						break;
					case GameKeys.Left:
						if (keyBoardState.IsKeyDown(LeftKey) && state == GameKeysState.Down) {
							return true;
						}

						if (keyBoardState.IsKeyPressed(LeftKey) && state == GameKeysState.Pressed) {
							return true;
						}

						if (keyBoardState.IsKeyReleased(LeftKey) && state == GameKeysState.Released) {
							return true;
						}
						break;
					case GameKeys.Right:
						if (keyBoardState.IsKeyDown(RightKey) && state == GameKeysState.Down) {
							return true;
						}

						if (keyBoardState.IsKeyPressed(RightKey) && state == GameKeysState.Pressed) {
							return true;
						}

						if (keyBoardState.IsKeyReleased(RightKey) && state == GameKeysState.Released) {
							return true;
						}
						break;
					case GameKeys.A:
						if (keyBoardState.IsKeyDown(AKey) && state == GameKeysState.Down) {
							return true;
						}

						if (keyBoardState.IsKeyPressed(AKey) && state == GameKeysState.Pressed) {
							return true;
						}

						if (keyBoardState.IsKeyReleased(AKey) && state == GameKeysState.Released) {
							return true;
						}
						break;
					case GameKeys.B:
						if (keyBoardState.IsKeyDown(BKey) && state == GameKeysState.Down) {
							return true;
						}

						if (keyBoardState.IsKeyPressed(BKey) && state == GameKeysState.Pressed) {
							return true;
						}

						if (keyBoardState.IsKeyReleased(BKey) && state == GameKeysState.Released) {
							return true;
						}
						break;
					case GameKeys.Start:
						if (keyBoardState.IsKeyDown(StartKey) && state == GameKeysState.Down) {
							return true;
						}

						if (keyBoardState.IsKeyPressed(StartKey) && state == GameKeysState.Pressed) {
							return true;
						}

						if (keyBoardState.IsKeyReleased(StartKey) && state == GameKeysState.Released) {
							return true;
						}
						break;
					case GameKeys.Select:
						if (keyBoardState.IsKeyDown(SelectKey) && state == GameKeysState.Down) {
							return true;
						}

						if (keyBoardState.IsKeyPressed(SelectKey) && state == GameKeysState.Pressed) {
							return true;
						}

						if (keyBoardState.IsKeyReleased(SelectKey) && state == GameKeysState.Released) {
							return true;
						}
						break;
				}
			}

			return false;
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpDX;
using SharpDX.Toolkit.Graphics;
using SharpDX.Toolkit.Content;
using SharpDX.Toolkit;

namespace ZeldaGameBoy {
	public abstract class Window {
		public Vector2 Position { get; set; }
		public int Width { get; set; }
		public int Height { get; set; }

		protected SpriteFont Font { get; set; }
		protected Texture2D Texture { get; set; }
		protected Color FontColor { get; set; }
		protected Color BackgroundColor { get; set; }

		public bool Done { get; set; }
		public bool Active { get; set; }
		public float Opacity { get; set; }

		protected ContentManager Content { get; private set; }

		protected Window(ContentManager content) {
			Position = new Vector2(5, 5);
			Width = 150;
			Height = 40;

			Font = content.Load<SpriteFont>(@"Fonts\Verdana10.tk"); ;
			Texture = content.Load<Texture2D>(@"GUI\white_background.png");
			FontColor = Color.White;
			BackgroundColor = Color.Black;

			Done = false;
			Active = true;
			Opacity = 1f;

			Content = content;
		}

		public virtual void LoadContent() {
		
		}

		public abstract void Update(GameTime gameTime);
		public abstract void Reset();

		public virtual void Draw(SpriteBatch spriteBatch) {
			spriteBatch.Draw(Texture, new RectangleF(Position.X, Position.Y, Width, Height), BackgroundColor * Opacity);
		}
	}
}

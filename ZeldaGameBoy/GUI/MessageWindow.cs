﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpDX.Toolkit;
using SharpDX.Toolkit.Graphics;
using SharpDX.Toolkit.Content;
using SharpDX;

namespace ZeldaGameBoy {
	public class MessageWindow : Window {
		private List<string> TextSites { get; set; }
		private int SiteIndex { get; set; } //Lässt die Seiten umblättern
		private int CharIndex { get; set; } //lässt den Text, nach und nach erscheinen
		private bool WritingData { get; set; } //ob der Textgerade dabei ist auf dem Bildschirm geschrieben zu werden
		private string CurrentText { get; set; } //aktueller Textfortschritt von der aktuellen Seite

		private float Timer { get; set; }
		public float CharWritingDelay { get; set; } //Abstand zwischen dem Schreiben eines Buchstabens

		public MessageWindow(ContentManager content, string text)
			: base(content) {
			TextSites = SplittLinesToSites(SplitWordsToLines(text));
			SiteIndex = 0;
			CharIndex = 0;
			WritingData = true;

			Timer = 0f;
			CharWritingDelay = 0.05f;

			CurrentText = "";
		}

		private List<string> SplitWordsToLines(string text) {
			List<string> lines = new List<string>();

			string line = "";
			foreach (string s in text.Split(' ')) {
				if (Font.MeasureString(line + " " + s).X < Width - 10) {
					line = line + " " + s;
				} else {
					lines.Add(line);
					line = s;
				}
			}

			lines.Add(line);
			return lines;
		}

		private List<string> SplittLinesToSites(List<string> lines) {
			List<string> sites = new List<string>();

			string site = "";
			foreach (string line in lines) {
				if (site == "") {
					site = line;
				} else {
					if (Font.MeasureString(site + Environment.NewLine + line).Y < Height - 8) {
						site = site + Environment.NewLine + line;
					} else {
						sites.Add(site.Trim());
						site = line;
					}
				}
			}

			sites.Add(site.Trim());
			return sites;
		}

		public override void Update(GameTime gameTime) {
			if (Done || !Active) return;

			//Schreibt den Text wie eine Schreibmaschine auf dem Bildschirm
			if (WritingData) {
				Timer += (float)gameTime.ElapsedGameTime.TotalSeconds;
				if (Timer >= CharWritingDelay) {
					Timer -= CharWritingDelay;

					if (CharIndex < TextSites[SiteIndex].Length) {
						CurrentText += TextSites[SiteIndex].ToCharArray()[CharIndex];
						CharIndex++;
					} else {
						WritingData = false;
					}
				}
			} else {
				if (VirtualGamePad.GetGameKeyState(GameKeys.A, GameKeysState.Released, true)) {
					SiteIndex++;
					WritingData = true;
					CurrentText = "";
					Timer = 0f;
					CharIndex = 0;
				}

				if (SiteIndex >= TextSites.Count) {
					Done = true;
				}
			}
		}

		public override void Reset() {

		}

		public override void Draw(SpriteBatch spriteBatch) {
			if (Done || !Active) return;
			base.Draw(spriteBatch);
			spriteBatch.DrawString(Font, CurrentText, Position + new Vector2(5, 4), FontColor);
		}

		public override void LoadContent() {
			base.LoadContent();
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpDX.Toolkit.Graphics;
using SharpDX.Toolkit;
using SharpDX;

namespace ZeldaGameBoy {
	public class RandomMovementAI : Component {

		float timer;
		float maxTimeMovement;
		Random rnd;
		Vector2 movedirection;
		AnimationDirection animationDirection;

		public RandomMovementAI(float frequenz) {
			timer = 0f;
			maxTimeMovement = frequenz;
			rnd = new Random(Guid.NewGuid().GetHashCode());
			movedirection = Vector2.Zero;
			animationDirection = AnimationDirection.NULL;
		}

		public override ComponentType ComponentType {
			get { return ComponentType.AI; }
		}

		public override void Update(GameTime gameTime) {
			Animation sprite = GetComponent<Animation>(ComponentType.Animation);
			timer += (float)gameTime.ElapsedGameTime.TotalSeconds;

			if (sprite != null) {
				if (timer > maxTimeMovement) {
					timer = 0f;
					//change movedirection
					int direction = rnd.Next(0, 6);
					switch (direction) {
						case 0:
							movedirection = new Vector2(0, 1);
							animationDirection = AnimationDirection.DOWN;
							break;
						case 1:
							movedirection = new Vector2(0, -1);
							animationDirection = AnimationDirection.UP;
							break;
						case 2:
							movedirection = new Vector2(1, 0);
							animationDirection = AnimationDirection.RIGHT;
							break;
						case 3:
							movedirection = new Vector2(-1, 0);
							animationDirection = AnimationDirection.LEFT;
							break;
						case 4:
						case 5:
							movedirection = new Vector2(0, 0);
							break;
					}
				}

				Collision collision = GetComponent<Collision>(ComponentType.Collision);
				if (collision != null && animationDirection != AnimationDirection.NULL) {
					sprite.SetDirection(animationDirection);
					sprite.Position += collision.GetMoveDirectionFromTileCollision(movedirection, (float)gameTime.ElapsedGameTime.TotalSeconds);
				}

				if (movedirection == Vector2.Zero) {
					sprite.AnimationState = AnimationState.Standing;
				} else {
					sprite.AnimationState = AnimationState.Walking;
				}
			}
		}

		public override void Draw(SpriteBatch spritebatch) {
			
		}
	}
}

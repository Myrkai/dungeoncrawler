﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpDX.Toolkit.Graphics;
using SharpDX.Toolkit;
using SharpDX;

namespace ZeldaGameBoy {
	public class OctorocAI : Component {

		private MapManager MapManager { get; set; }
		private BaseObject Player { get; set; }
		private Bullet BaseBullet { get; set; }
		private List<Bullet> Bullets { get; set; }

		private float Timer { get; set; }
		private float Delay { get; set; }

		public OctorocAI(MapManager mapManager, BaseObject player, Bullet baseBullet, float shootDelay) {
			MapManager = mapManager;
			Player = player;
			Delay = shootDelay;
			Bullets = new List<Bullet>();
			BaseBullet = baseBullet;
		}

		public override ComponentType ComponentType {
			get { return ComponentType.EnemyOctoroc; }
		}

		public override void Update(GameTime gameTime) {
			Timer += (float)gameTime.ElapsedGameTime.TotalSeconds;

			if (Timer > Delay) {
				Timer -= Delay;

				Animation sprite = GetComponent<Animation>(ComponentType.Animation);
				Animation playerAni = Player.GetComponent<Animation>(ComponentType.Animation);
				bool canShoot = false;
				if (sprite != null && playerAni != null) {
					Vector2 shootDirection = Vector2.Zero;
					switch (sprite.CurrentAnimationDirection) { 
						case AnimationDirection.UP:
							shootDirection = new Vector2(0, -1);
							if (playerAni.Position.Y < sprite.Position.Y) {
								canShoot = true;
							}
							break;
						case AnimationDirection.DOWN:
							shootDirection = new Vector2(0, 1);
							if (playerAni.Position.Y > sprite.Position.Y) {
								canShoot = true;
							}
							break;
						case AnimationDirection.LEFT:
							shootDirection = new Vector2(-1, 0);
							if (playerAni.Position.X < sprite.Position.X) {
								canShoot = true;
							}
							break;
						case AnimationDirection.RIGHT:
							shootDirection = new Vector2(1, 0);
							if (playerAni.Position.X > sprite.Position.X) {
								canShoot = true;
							}
							break;
					}
					
					if (canShoot) {
						Bullet b = BaseBullet.Clone();
						b.Position = sprite.Position + (sprite.Size - b.Size) / 2;
						b.MoveDirection = shootDirection;
						Bullets.Add(b);
					}
				}
			}

			Bullets.ForEach(b => b.Update(gameTime));
			Bullets = Bullets.Where(b => b.Active).ToList();
		}

		public override void Draw(SpriteBatch spritebatch) {
			Bullets.ForEach(b => b.Draw(spritebatch));
		}
	}
}

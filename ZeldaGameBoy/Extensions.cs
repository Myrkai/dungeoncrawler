﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpDX.Toolkit.Graphics;
using SharpDX;

namespace ZeldaGameBoy {
	public static class Extensions {
		public static Vector2 Size(this Texture2D texture) {
			return new Vector2(texture.Width, texture.Height);
		}

		/// <summary>
		/// Bestimmen ob 2 Rechtecke sich zu einem Bestimmten Prozentsatz überschneiden.
		/// Hierbei muss die höhe und die weite größer als der Prozentsatz sein.
		/// Ausgegangen wird davon das beide Rechtecke gleich groß sind. 
		/// Sind diese Unterschiedlich dann können merkwürdige Dinge passieren.
		/// </summary>
		public static bool Interacts(this RectangleF rectangle, RectangleF target, float amount) {
			RectangleF overlapping = RectangleF.Intersect(rectangle, target);
			return overlapping.Width > rectangle.Width * amount && overlapping.Height > rectangle.Height * amount;
		}

		/// <summary>
		/// Bestimmen ob 2 Rechtecke sich zu einem Bestimmten Prozentsatz überschneiden.
		/// Hierbei muss die höhe und die weite größer als der Prozentsatz sein.
		/// Ausgegangen wird davon das beide Rechtecke gleich groß sind. 
		/// Sind diese Unterschiedlich dann können merkwürdige Dinge passieren.
		/// </summary>
		public static bool Interacts(this Rectangle rectangle, Rectangle target, float amount) {
			Rectangle overlapping = Rectangle.Intersect(rectangle, target);
			return overlapping.Width > rectangle.Width * amount && overlapping.Height > rectangle.Height * amount;
		}

		public static Rectangle AddVector(this Rectangle rec, Vector2 position) {
			return new Rectangle((int)(rec.X + position.X), (int)(rec.Y + position.Y), rec.Width, rec.Height);
		}

		/// <summary>
		/// // make red 50% lighter:
		///	Color.Red.Lerp( Color.White, 0.5 );
		///	// make red 75% darker:
		///	Color.Red.Lerp( Color.Black, 0.75 );
		///	// make white 10% bluer:
		///	Color.White.Lerp( Color.Blue, 0.1 );
		/// </summary>
		/// <param name="colour"></param>
		/// <param name="to">auf welche Farbe soll geändert werden</param>
		/// <param name="amount">um wieviel Prozent soll sich diese Farbe ändern</param>
		/// <returns></returns>
		public static Color Lerp(this Color colour, Color to, float amount) {
			// start colours as lerp-able floats
			float sr = colour.R, sg = colour.G, sb = colour.B;

			// end colours as lerp-able floats
			float er = to.R, eg = to.G, eb = to.B;

			// lerp the colours to get the difference
			byte r = (byte)sr.Lerp(er, amount),
				 g = (byte)sg.Lerp(eg, amount),
				 b = (byte)sb.Lerp(eb, amount);

			// return the new colour
			return new Color(r, g, b);
		}

		public static float Lerp(this float start, float end, float amount) {
			float difference = end - start;
			float adjusted = difference * amount;
			return start + adjusted;
		}

		public static Color Mix(this Color color, Color backColor, float amount = 0.5f) {
			float hue = color.GetHue() * (1f - amount) + backColor.GetHue() * amount;
			float sat = color.GetSaturation() * (1f - amount) + backColor.GetSaturation() * amount;
			float lum = color.GetBrightness() * (1f - amount) + backColor.GetBrightness() * amount;

			return HSLtoRGB(hue / 2, sat / 2, lum / 2);
		}

		private static Color HSLtoRGB(double h, double s, double l) {
			if (s == 0) {
				// achromatic color (gray scale)
				return new Color(
					Convert.ToInt32(Double.Parse(String.Format("{0:0.00}",
						l * 255.0))),
					Convert.ToInt32(Double.Parse(String.Format("{0:0.00}",
						l * 255.0))),
					Convert.ToInt32(Double.Parse(String.Format("{0:0.00}",
						l * 255.0)))
					);
			} else {
				double q = (l < 0.5) ? (l * (1.0 + s)) : (l + s - (l * s));
				double p = (2.0 * l) - q;

				double Hk = h / 360.0;
				double[] T = new double[3];
				T[0] = Hk + (1.0 / 3.0);
				T[1] = Hk;
				T[2] = Hk - (1.0 / 3.0);

				for (int i = 0; i < 3; i++) {
					if (T[i] < 0) T[i] += 1.0;
					if (T[i] > 1) T[i] -= 1.0;

					if ((T[i] * 6) < 1) {
						T[i] = p + ((q - p) * 6.0 * T[i]);
					} else if ((T[i] * 2.0) < 1) {
						T[i] = q;
					} else if ((T[i] * 3.0) < 2) {
						T[i] = p + (q - p) * ((2.0 / 3.0) - T[i]) * 6.0;
					} else T[i] = p;
				}

				return new Color(
					Convert.ToInt32(Double.Parse(String.Format("{0:0.00}",
						T[0] * 255.0))),
					Convert.ToInt32(Double.Parse(String.Format("{0:0.00}",
						T[1] * 255.0))),
					Convert.ToInt32(Double.Parse(String.Format("{0:0.00}",
						T[2] * 255.0)))
					);
			}
		}
	}
}

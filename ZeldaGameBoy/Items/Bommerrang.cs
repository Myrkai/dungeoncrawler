﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpDX.Toolkit.Content;
using SharpDX.Toolkit.Graphics;
using SharpDX.Toolkit;
using SharpDX;

namespace ZeldaGameBoy {
	internal enum MoveState {
		Front,
		Back
	}

	public class Bommerrang : Item {
		private MapManager MapManager { get; set; }
		private Vector2 MoveDirection { get; set; }
		private MoveState MoveState { get; set; }
		private float Speed { get; set; }

		public Bommerrang(MapManager mapManager)
			: base() {
			MapManager = mapManager;
			MoveDirection = Vector2.Zero;
			Speed = 100f;
		}

		public override void Action() {
			if (!Active) {
				Animation playerAni = Owner.GetComponent<Animation>(ComponentType.Animation);
				Animation animation = GetComponent<Animation>(ComponentType.Animation);
				if (playerAni != null && animation != null) {
					switch (playerAni.CurrentAnimationDirection) {
						case AnimationDirection.DOWN:
							MoveDirection = new Vector2(0, 1);
							animation.SetDirection(AnimationDirection.DOWN);
							break;
						case AnimationDirection.LEFT:
							MoveDirection = new Vector2(-1, 0);
							animation.SetDirection(AnimationDirection.LEFT);
							break;
						case AnimationDirection.RIGHT:
							MoveDirection = new Vector2(1, 0);
							animation.SetDirection(AnimationDirection.RIGHT);
							break;
						case AnimationDirection.UP:
							MoveDirection = new Vector2(0, -1);
							animation.SetDirection(AnimationDirection.UP);
							break;
					}

					Active = true;
					animation.Position = playerAni.Position;
					animation.AnimationState = AnimationState.Walking;
					MoveState = MoveState.Front;
				}
			}
		}

		public override void LoadContent(Equipment owner, ContentManager content) {
			base.LoadContent(owner, content);
			AddComponent(new Animation(content.Load<Texture2D>(@"Items\Bummerrang.png"), 4, 4, 0.15f, Vector2.Zero));
			AddComponent(new Collision(MapManager, Speed));
			GetComponent<Animation>(ComponentType.Animation).SetAnimationConfiguration(AnimationDirection.RIGHT, AnimationDirection.DOWN, AnimationDirection.UP, AnimationDirection.LEFT);
			GuiTexture = content.Load<Texture2D>(@"Items\bummerrangGUI.png");
		}

		public override void Draw(SpriteBatch spriteBatch) {
			base.Draw(spriteBatch);
		}

		public override void Update(GameTime gameTime) {
			base.Update(gameTime);

			if (Active) {
				Collision collision = GetComponent<Collision>(ComponentType.Collision);
				Animation animation = GetComponent<Animation>(ComponentType.Animation);
				Animation player = Owner.GetComponent<Animation>(ComponentType.Animation);
				if (collision != null && animation != null) {
					animation.Update(gameTime);
					Vector2 direct = collision.GetMoveDirectionFromTileCollision(MoveDirection, (float)gameTime.ElapsedGameTime.TotalSeconds);
					if (direct == Vector2.Zero) {
						MoveState = MoveState.Back;
					}

					if (MoveState == MoveState.Front) {
						animation.Position += direct;
					} else {
						Vector2 newVec = ((animation.Position + animation.Size / 2) - (player.Position + player.Size / 2));
						newVec.Normalize();
						animation.Position += newVec * -1 * Speed * (float)gameTime.ElapsedGameTime.TotalSeconds;
					}

					if (MoveState == MoveState.Back && player.PositionRec.Intersects(animation.PositionRec)) {
						Active = false;
						MoveDirection = Vector2.Zero;
					}
				}
			}
		}
	}
}

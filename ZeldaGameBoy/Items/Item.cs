﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpDX.Toolkit.Graphics;
using SharpDX;
using SharpDX.Toolkit.Content;

namespace ZeldaGameBoy {
	public abstract class Item : BaseObject {
		protected Texture2D GuiTexture { get; set; }
		protected Equipment Owner { get; set; }
		public int ItemID { get; set; }
		public bool Active { get; set; }
		public Vector2 MenuPosition { get; set; }

		public abstract void Action();
		public virtual void LoadContent(Equipment owner, ContentManager content) {
			Owner = owner;
		}

		public void DrawGUI(SpriteBatch spritebatch, Rectangle rectangle) {
			if (GuiTexture != null) {
				spritebatch.Draw(GuiTexture, (RectangleF)rectangle, Color.White);
			}
		}

		public void DrawMenu(SpriteBatch spritebatch) {
			if (GuiTexture != null) {
				spritebatch.Draw(GuiTexture, new RectangleF(MenuPosition.X, MenuPosition.Y, 13, 13), Color.White);
			}
		}
	}
}

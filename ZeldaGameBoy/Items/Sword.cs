﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpDX;
using SharpDX.Toolkit.Graphics;
using SharpDX.Toolkit.Content;
using SharpDX.Toolkit;
using ZeldaGameBoy.Components;

namespace ZeldaGameBoy {
	public class Sword : Item {
		private MapManager MapManager { get; set; }
		private float Speed { get; set; }
		private Vector2[] AnimationStepOffset { get; set; }
		private int Counter { get; set; }

		public Sword(MapManager mapManager)
			: base() {
			MapManager = mapManager;
			Speed = 50f;
			Counter = 0;
		}

		public override void Action() {
			if (!Active) {
				Animation playerAni = Owner.GetComponent<Animation>(ComponentType.Animation);
				Animation animation = GetComponent<Animation>(ComponentType.Animation);
				// ja Link ist rechts und linkhänder, je nachdem ob er nach oben oder unten zuschlägt
				if (playerAni != null && animation != null) {
					playerAni.SwitchAnimation("link_sword");
					playerAni.LookAnimationDirection = true;

					switch (playerAni.CurrentAnimationDirection) {
						case AnimationDirection.DOWN:
							animation.SetDirection(AnimationDirection.DOWN);
							AnimationStepOffset = new Vector2[] { new Vector2(-14, 0), new Vector2(-13, 13), new Vector2(-2, 16) };
							break;
						case AnimationDirection.LEFT:
							animation.SetDirection(AnimationDirection.LEFT);
							AnimationStepOffset = new Vector2[] { new Vector2(0, -14), new Vector2(-14, -14), new Vector2(-15, 1) };
							break;
						case AnimationDirection.RIGHT:
							animation.SetDirection(AnimationDirection.RIGHT);
							AnimationStepOffset = new Vector2[] { new Vector2(0, -14), new Vector2(14, -14), new Vector2(14, 0) };
							break;
						case AnimationDirection.UP:
							animation.SetDirection(AnimationDirection.UP);
							AnimationStepOffset = new Vector2[] { new Vector2(12, 0), new Vector2(10, -15), new Vector2(-1, -16) };
							break;
					}

					Active = true;
					VirtualGamePad.SuspentInputs = true;
					animation.Position = playerAni.Position + AnimationStepOffset[animation.CurrentAnimationStepIndex];
					animation.AnimationState = AnimationState.Walking;
				}
			}
		}

		public override void LoadContent(Equipment owner, ContentManager content) {
			base.LoadContent(owner, content);
			AddComponent(new Animation(content.Load<Texture2D>(@"Items\sword.png"), 3, 4, 0.15f, Vector2.Zero));
			AddComponent(new Collision(MapManager, Speed));
			GetComponent<Animation>(ComponentType.Animation).SetAnimationConfiguration(AnimationDirection.UP, AnimationDirection.DOWN, AnimationDirection.RIGHT, AnimationDirection.LEFT);
			GuiTexture = content.Load<Texture2D>(@"Items\swordGUI.png");
		}

		public override void Draw(SpriteBatch spriteBatch) {
			base.Draw(spriteBatch);
		}

		public override void Update(GameTime gameTime) {
			base.Update(gameTime);

			if (Active) {
				Collision collision = GetComponent<Collision>(ComponentType.Collision);
				Animation animation = GetComponent<Animation>(ComponentType.Animation);
				Animation player = Owner.GetComponent<Animation>(ComponentType.Animation);
				if (collision != null && animation != null) {
					animation.Update(gameTime);
					animation.Position = player.Position + AnimationStepOffset[animation.CurrentAnimationStepIndex];

					if (Counter != animation.CurrentAnimationStepIndex) {
						Counter++;
					}

					if (Counter == animation.AnimationStepsPerAnimation) {
						Active = false;
						VirtualGamePad.SuspentInputs = false;
						animation.AnimationState = AnimationState.Standing;
						Counter = 0;
						player.SwitchAnimationToDefault();
						player.LookAnimationDirection = false;
					}
				}

				BaseObject enermy = MapManager.CurrentMap.GetEnermies().Where(s => s.GetComponent<Animation>(ComponentType.Animation).PositionRec.Interacts(animation.PositionRec, 0.1f)).FirstOrDefault();
				if (enermy != null) {
					Damage enermyCollision = enermy.GetComponent<Damage>(ComponentType.Damage);
					if (enermyCollision != null) {
						enermyCollision.GetHittet(player.CurrentAnimationDirection, player._baseObject);
					}
				}
			}
		}
	}
}

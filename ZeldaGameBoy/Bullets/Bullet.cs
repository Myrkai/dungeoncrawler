﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpDX.Toolkit.Graphics;
using SharpDX;
using SharpDX.Toolkit;

namespace ZeldaGameBoy {
	public class Bullet {
		private Texture2D Texture { get; set; }
		public Vector2 Position { get; set; }
		public Vector2 MoveDirection { get; set; }
		private float Speed { get; set; }
		public Rectangle Rectangle { get { return new Rectangle((int)Position.X, (int)Position.Y, Texture.Width, Texture.Height); } }
		public bool Active { get; set; }
		private MapManager MapManager { get; set; }
		private BaseObject Player { get; set; }
		public Vector2 Size { get { return new Vector2(Texture.Width, Texture.Height); } }

		public Bullet(Texture2D texture, float speed, MapManager mapmanager, BaseObject player) {
			Texture = texture;
			Position = Vector2.Zero;
			MoveDirection = Vector2.Zero;
			Speed = speed;
			Active = true;
			MapManager = mapmanager;
			Player = player;
		}

		public void Update(GameTime gametime) {
			if (!Active) {
				return;
			}

			Position += MoveDirection * Speed * (float)gametime.ElapsedGameTime.TotalSeconds;

			Animation playerAni = Player.GetComponent<Animation>(ComponentType.Animation);
			if (playerAni != null && playerAni.CurrentTextureRectangle().Intersects(this.Rectangle)) {
				Active = false;
			}

			if (Rectangle.X < 0 || Rectangle.Y < 0 || Rectangle.TopLeft.X > MapManager.CurrentMap.MapWidthPx || Rectangle.BottomLeft.Y > MapManager.CurrentMap.MapHeightPx) {
				Active = false;
			}

			if (!IsWalkable(Rectangle, MapManager.CurrentMap)) {
				Active = false;
			}
		}

		private bool IsWalkable(RectangleF rec, Map map) {
			int cellWidthPx = map.MapWidthPx / map.CollisitionLayer.Width;
			int cellHeightPx = map.MapHeightPx / map.CollisitionLayer.Height;

			bool topLeft = map.CollisitionLayer.GetCell((int)(rec.TopLeft.X / cellWidthPx), (int)(rec.TopLeft.Y / cellHeightPx));
			bool topRight = map.CollisitionLayer.GetCell((int)(rec.TopRight.X / cellWidthPx), (int)(rec.TopRight.Y / cellHeightPx));
			bool bottomLeft = map.CollisitionLayer.GetCell((int)(rec.BottomLeft.X / cellWidthPx), (int)(rec.BottomLeft.Y / cellHeightPx));
			bool bottomRight = map.CollisitionLayer.GetCell((int)(rec.BottomRight.X / cellWidthPx), (int)(rec.BottomRight.Y / cellHeightPx));

			return topLeft && topRight && bottomLeft && bottomRight;
		}

		public void Draw(SpriteBatch spritebatch) {
			if (!Active) {
				return;
			}

			spritebatch.Draw(Texture, Position, Color.White);
		}

		public Bullet Clone() {
			Bullet tmp = new Bullet(Texture, Speed, MapManager, Player);
			tmp.Position = Position;
			tmp.MoveDirection = MoveDirection;
			tmp.Active = Active;

			return tmp;
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpDX.Toolkit;
using SharpDX.Toolkit.Graphics;
using SharpDX;
using ZeldaGameBoy.Components;
using TiledMap;
using GUI;
using ZeldaGameBoy.Screens;
using SharpDX.Toolkit.Content;

namespace ZeldaGameBoy {
	public class Game1 : Game {

		/*
		 * https://www.youtube.com/watch?v=x5rMXzEgfqE&list=PLD2t5VOqzPm-IwgQwgmXpmLajQlcCwvBD&index=1
		 * https://github.com/Meeii/SpeedCodingZelda/tree/master/LetsCreateZelda
		 * http://www.spriters-resource.com/game_boy/L.html
		 * http://www.spriters-resource.com/game_boy/legendofzeldalinksawakening/
		 */

		public GraphicsDeviceManager Graphics { get; private set; }
		public SpriteBatch SpriteBatch { get; private set; }

		private int ZoomFactor { get; set; }
		private Matrix Matrix { get; set; }

		public Game1()
			: base() {
			string b = "💩";

			Graphics = new GraphicsDeviceManager(this);
			Content.RootDirectory = "Content";

			ZoomFactor = 1;
			Matrix = Matrix.Scaling(ZoomFactor);

			Graphics.PreferredBackBufferWidth = 160 * ZoomFactor;
			Graphics.PreferredBackBufferHeight = 144 * ZoomFactor;
		}

		protected override void Initialize() {
			base.Initialize();

			VirtualGamePad.Initialize(this, GraphicsDevice);
			EventManager.Instance.Init(Content);
			StatsFactory.Initialize();
			DeathAnimationFactory.Initialise(Content);
		}

		protected override void LoadContent() {
			base.LoadContent();

			SpriteBatch = new SpriteBatch(GraphicsDevice);
			ScreenManager.Instance.RegisterScreen("gamescreen", new GameScreen(this, ScreenManager.Instance));
			ScreenManager.Instance.RegisterScreen("introscreen", new IntroScreen(this, ScreenManager.Instance));
			ScreenManager.Instance.RegisterScreen("inventory", new Inventory(this, ScreenManager.Instance, null));
			ScreenManager.Instance.RegisterScreen("worldmap", new WorldmapOverviewScreen(this, ScreenManager.Instance, null));
			ScreenManager.Instance.Transfer("introscreen");
		}

		protected override void Update(GameTime gameTime) {
			base.Update(gameTime);

			VirtualGamePad.Update(gameTime);
			ScreenManager.Instance.Update(gameTime);
		}

		protected override void Draw(GameTime gameTime) {
			base.Draw(gameTime);

			GraphicsDevice.Clear(GraphicsDevice.BackBuffer, new Color(248, 248, 168));

			SpriteBatch.Begin(SpriteSortMode.Immediate, GraphicsDevice.BlendStates.AlphaBlend, GraphicsDevice.SamplerStates.PointClamp, null, null, null, Matrix);
			ScreenManager.Instance.Draw(SpriteBatch);
			SpriteBatch.End();
		}

		protected override void UnloadContent() {
			base.UnloadContent();
		}
	}
}

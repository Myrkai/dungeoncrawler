﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpDX;
using SharpDX.Toolkit.Content;
using SharpDX.Toolkit.Graphics;

namespace ZeldaGameBoy {
	public static class DeathAnimationFactory {
		private static ContentManager Content { get; set; }

		public static void Initialise(ContentManager content) {
			Content = content;
		}

		public static BaseObject GetDeathAnimationObject(DeathAnimation animation, Vector2 position) {
			switch (animation) {
				case DeathAnimation.Exposion:
					BaseObject tmp = new BaseObject();
					tmp.AddComponent(new Animation(Content.Load<Texture2D>(@"Sprites\death.png"), 3, 4, 0.2f, position));
					tmp.AddComponent(new DeathEffect());
					tmp.GetComponent<Animation>(ComponentType.Animation).AnimationState = AnimationState.Walking;
					return tmp;
				default:
					throw new NotImplementedException(animation.ToString());
			}
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml.Serialization;

namespace ZeldaGameBoy {
	public static class StatsFactory {
		private static List<Stats> Stats = new List<Stats>();

		public static void Initialize() {
			if (File.Exists(@"Content\stats.xml")) {
				FileStream fs = new FileStream(@"Content\stats.xml", FileMode.Open);
				List<Stats> stats;

				XmlSerializer xml = new XmlSerializer(typeof(List<Stats>));
				stats = (List<Stats>)xml.Deserialize(fs);
				fs.Close();

				Stats = stats;
			} else {
				Stats.Add(new Stats("Link", 6, 6, 1, 0, 0));
				Stats.Add(new Stats("Octoroc", 1, 1, 1, 0, 0));

				TextWriter writer = new StreamWriter(@"Content\stats.xml");

				XmlSerializer xml = new XmlSerializer(typeof(List<Stats>));
				xml.Serialize(writer, Stats);

				writer.Close();
			}
		}

		public static Stats GetStats(string statID) {
			return Stats.Where(s => s.StatsId == statID).First().Clone();
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GUI;
using GUI.Interface;
using SharpDX.Toolkit.Graphics;
using SharpDX.Toolkit;

namespace ZeldaGameBoy {
	public class IntroScreen : GUI.Screens.TitleScreen<Game1> {
		public IntroScreen(Game1 gameRef, ScreenManager screenManager)
			: base(gameRef, screenManager) {
			base.SetBackground(GameRef.Content.Load<Texture2D>(@"Background\Intro.png"));
		}

		public override void AddCustomControl(string key, IControl control, bool renderingFirst) {
			base.AddCustomControl(key, control, renderingFirst);
		}

		public override void Draw(SpriteBatch spriteBatch) {
			base.Draw(spriteBatch);
		}

		public override void Initialize() {
			base.Initialize();
		}

		public override void LoadContent() {
			base.LoadContent();
		}

		public override void Update(GameTime gameTime) {
			base.Update(gameTime);
			if (VirtualGamePad.GetGameKeyState(GameKeys.Exit, GameKeysState.Released)) {
				ScreenManager.Instance.Transfer("gamescreen");
			}
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GUI;
using GUI.Interface;
using SharpDX.Toolkit.Graphics;
using SharpDX.Toolkit;
using SharpDX;
using System.Threading;
using NLua;
namespace ZeldaGameBoy.Screens {
	public class Inventory : GUI.Screens.GameScreen<Game1> {

		private Texture2D Inventory1 { get; set; }
		private Texture2D Inventory2 { get; set; }
		private bool ShowFirstBackground { get; set; }
		private float BackgroundChangeTimer { get; set; }
		private float BackgroundChangeInterval { get; set; }
		public BaseObject Player { get; set; }

		private Texture2D WhitePixel { get; set; }
		private ColorChanger ColorChanger { get; set; }

		private Texture2D SlotSelector { get; set; }
		private int SelectorIndex { get; set; }

		// Draw GUI, ja ist Copy & Paste
		private Texture2D ItemSlotAIcon { get; set; }
		private Texture2D ItemSlotBIcon { get; set; }
		private Texture2D RupieIcon { get; set; }
		private Texture2D FullHeartIcon { get; set; }
		private Texture2D HalfHeartIcon { get; set; }
		private Texture2D EmptyHeartIcon { get; set; }
		private Texture2D Font { get; set; }

		public Inventory(Game1 gameRef, ScreenManager screenManager, BaseObject player)
			: base(gameRef, screenManager) {
			Inventory1 = gameRef.Content.Load<Texture2D>(@"Items\Inventory1.png");
			Inventory2 = gameRef.Content.Load<Texture2D>(@"Items\Inventory2.png");
			ShowFirstBackground = true;
			BackgroundChangeTimer = 0f;
			BackgroundChangeInterval = 0.5f;
			Player = player;

			WhitePixel = gameRef.Content.Load<Texture2D>(@"GUI\white_background.png");
			ColorChanger = new ColorChanger(Color.Blue, Color.Red, Color.Orange, Color.Green);

			SlotSelector = gameRef.Content.Load<Texture2D>(@"Items\SlotSelection.png");
			SelectorIndex = 0;

			ItemSlotAIcon = gameRef.Content.Load<Texture2D>(@"GUI\ASlotIcon.png");
			ItemSlotBIcon = gameRef.Content.Load<Texture2D>(@"GUI\BSlotIcon.png");
			RupieIcon = gameRef.Content.Load<Texture2D>(@"GUI\RupiIcon.png");
			FullHeartIcon = gameRef.Content.Load<Texture2D>(@"GUI\FullHeartIcon.png");
			HalfHeartIcon = gameRef.Content.Load<Texture2D>(@"GUI\HalfHeartIcon.png");
			EmptyHeartIcon = gameRef.Content.Load<Texture2D>(@"GUI\EmtpyHeartIcon.png");
			Font = gameRef.Content.Load<Texture2D>(@"Fonts\Schrift2.png");
		}

		public override void AddCustomControl(string key, IControl control, bool renderingFirst) {
			base.AddCustomControl(key, control, renderingFirst);
		}

		public override void Draw(SpriteBatch spriteBatch) {
			base.Draw(spriteBatch);
			spriteBatch.Draw(ShowFirstBackground ? Inventory1 : Inventory2, Vector2.Zero, Color.White);
			DrawGUI(spriteBatch);
			DrawInstruments(spriteBatch);
			DrawSelector(spriteBatch);
			Player.GetComponent<Equipment>(ComponentType.Equipment).DrawMenuGui(spriteBatch);
		}

		public override void Initialize() {
			base.Initialize();
		}

		public override void LoadContent() {
			base.LoadContent();
		}

		public override void Update(GameTime gameTime) {
			base.Update(gameTime);
			BackgroundChangeTimer += (float)gameTime.ElapsedGameTime.TotalSeconds;

			ColorChanger.Update(gameTime);

			if (VirtualGamePad.GetGameKeyState(GameKeys.Start, GameKeysState.Released)) {
				ScreenManager.Instance.Transfer("gamescreen");
				//LuaScriptManager.Instance.RunScript("ShowMessage('Test', 'Hallo du hast das Inventar verlassen und zusätzlich bin ich nur ein Lua-Script.')");
				//LuaScriptManager.Instance.RunScript("RaiseMessageEvent('Hallo du hast das Inventar verlassen und zusätzlich bin ich nur ein Lua-Script.')");

				return;
			}

			if (VirtualGamePad.GetGameKeyState(GameKeys.A, GameKeysState.Released)) {
				Player.GetComponent<Equipment>(ComponentType.Equipment).SwitchEquipment(ItemSlot.A, SelectorIndex % 2 == 0 ? new Vector2(11, 26 + (SelectorIndex * 12)) : new Vector2(41, 26 + ((SelectorIndex - 1) * 12)));
			}

			if (VirtualGamePad.GetGameKeyState(GameKeys.B, GameKeysState.Released)) {
				Player.GetComponent<Equipment>(ComponentType.Equipment).SwitchEquipment(ItemSlot.B, SelectorIndex % 2 == 0 ? new Vector2(11, 26 + (SelectorIndex * 12)) : new Vector2(41, 26 + ((SelectorIndex - 1) * 12)));
			}

			if (VirtualGamePad.GetGameKeyState(GameKeys.Up, GameKeysState.Released)) {
				SelectorIndex -= 2;
			}

			if (VirtualGamePad.GetGameKeyState(GameKeys.Down, GameKeysState.Released)) {
				SelectorIndex += 2;
			}

			if (VirtualGamePad.GetGameKeyState(GameKeys.Left, GameKeysState.Released)) {
				SelectorIndex -= 1;
			}

			if (VirtualGamePad.GetGameKeyState(GameKeys.Right, GameKeysState.Released)) {
				SelectorIndex += 1;
			}

			if (SelectorIndex < 0) {
				SelectorIndex = SelectorIndex % 2 == 0 ? 8 : 9;
			}

			if (SelectorIndex > 9) {
				SelectorIndex = SelectorIndex % 2 == 0 ? 0 : 1;
			}

			if (BackgroundChangeTimer > BackgroundChangeInterval) {
				BackgroundChangeTimer -= BackgroundChangeInterval;
				ShowFirstBackground = !ShowFirstBackground;
			}
		}

		private void DrawSelector(SpriteBatch spriteBatch) {
			if (SelectorIndex % 2 == 0) {
				int index = SelectorIndex;
				spriteBatch.Draw(SlotSelector, new Vector2(7, 26 + (index * 12)), Color.White);
			} else {
				int index = SelectorIndex - 1;
				spriteBatch.Draw(SlotSelector, new Vector2(37, 26 + (index * 12)), Color.White);
			}
		}

		private void DrawInstruments(SpriteBatch spriteBatch) {
			spriteBatch.Draw(WhitePixel, new RectangleF(96, 64, 16, 16), ColorChanger.Color); //8
			spriteBatch.Draw(WhitePixel, new RectangleF(104, 72, 8, 8), new Color(248, 248, 168)); //8 Text
			DrawText("8", spriteBatch, new Vector2(106, 74));

			spriteBatch.Draw(WhitePixel, new RectangleF(120, 64, 16, 16), ColorChanger.Color); //1
			spriteBatch.Draw(WhitePixel, new RectangleF(128, 72, 8, 8), new Color(248, 248, 168)); //1 Text
			DrawText("1", spriteBatch, new Vector2(130, 74));

			spriteBatch.Draw(WhitePixel, new RectangleF(136, 80, 16, 16), ColorChanger.Color); //2
			spriteBatch.Draw(WhitePixel, new RectangleF(144, 88, 8, 8), new Color(248, 248, 168)); //2 Text
			DrawText("2", spriteBatch, new Vector2(146, 90));

			spriteBatch.Draw(WhitePixel, new RectangleF(136, 104, 16, 16), ColorChanger.Color); //3
			spriteBatch.Draw(WhitePixel, new RectangleF(144, 112, 8, 8), new Color(248, 248, 168)); //3 Text
			DrawText("3", spriteBatch, new Vector2(146, 114));

			spriteBatch.Draw(WhitePixel, new RectangleF(120, 120, 16, 16), ColorChanger.Color); //4
			spriteBatch.Draw(WhitePixel, new RectangleF(128, 128, 8, 8), new Color(248, 248, 168)); //4 Text
			DrawText("4", spriteBatch, new Vector2(130, 130));

			spriteBatch.Draw(WhitePixel, new RectangleF(96, 120, 16, 16), ColorChanger.Color); //5
			spriteBatch.Draw(WhitePixel, new RectangleF(104, 128, 8, 8), new Color(248, 248, 168)); //5 Text
			DrawText("5", spriteBatch, new Vector2(106, 130));

			spriteBatch.Draw(WhitePixel, new RectangleF(80, 104, 16, 16), ColorChanger.Color); //6
			spriteBatch.Draw(WhitePixel, new RectangleF(88, 112, 8, 8), new Color(248, 248, 168)); //6 Text
			DrawText("6", spriteBatch, new Vector2(90, 114));

			spriteBatch.Draw(WhitePixel, new RectangleF(80, 80, 16, 16), ColorChanger.Color); //7
			spriteBatch.Draw(WhitePixel, new RectangleF(88, 88, 8, 8), new Color(248, 248, 168)); //7 Text
			DrawText("7", spriteBatch, new Vector2(90, 90));
		}

		#region Draw GUI From Player
		private void DrawGUI(SpriteBatch spritebatch) {
			Vector2 offset = new Vector2(0, 127);

			spritebatch.Draw(ItemSlotBIcon, new Vector2(1, 129) - offset, Color.White);
			spritebatch.Draw(ItemSlotAIcon, new Vector2(43, 129) - offset, Color.White);
			spritebatch.Draw(RupieIcon, new Vector2(83, 129) - offset, Color.White);

			Stats stats = Player.GetComponent<Stats>(ComponentType.Stats);
			if (stats != null) {
				DrawNumber(stats.Rupee.ToString("000"), spritebatch, offset);
				DrawLife(stats.CurrentHealth, stats.MaxHealth, spritebatch, offset);
			} else {
				DrawNumber("000", spritebatch, offset);
				DrawLife(0, 6, spritebatch, offset);
			}

			Equipment equipment = Player.GetComponent<Equipment>(ComponentType.Equipment);
			if (equipment != null) {
				if (equipment.EquipedInSlot(ItemSlot.B)) {
					equipment.DrawGui(spritebatch, ItemSlot.B, new Rectangle(11, 130 - (int)offset.Y, 12, 12));
				}

				if (equipment.EquipedInSlot(ItemSlot.A)) {
					equipment.DrawGui(spritebatch, ItemSlot.A, new Rectangle(52, 130 - (int)offset.Y, 12, 12));
				}
			}
		}

		private void DrawNumber(string text, SpriteBatch batch, Vector2 offset) {
			Vector2 position = new Vector2(83, 138) - offset;
			foreach (char z in text.ToCharArray()) {
				int number = Convert.ToInt16(z) - 48;
				batch.Draw(Font, position, new Rectangle(4 * number, 0, 4, 5), Color.Black);
				position += new Vector2(5, 0);
			}
		}

		private void DrawText(string text, SpriteBatch batch, Vector2 position) {
			foreach (char z in text.ToCharArray()) {
				int number = Convert.ToInt16(z) - 48;
				batch.Draw(Font, position, new Rectangle(4 * number, 0, 4, 5), Color.Black);
				position += new Vector2(5, 0);
			}
		}

		private void DrawLife(int lifeamount, int maxlife, SpriteBatch batch, Vector2 offset) {
			if (maxlife % 2 == 1) {
				throw new Exception("maxlife muss durch 2 Teilbar sein (2 Leben sind ein Herz)");
			}

			Vector2 position = new Vector2(103, 129) - offset;
			int fullHearts = lifeamount / 2;
			int halfHearts = lifeamount % 2;
			int emptyHearts = (maxlife / 2) - fullHearts - halfHearts;
			int heartsDrawed = 0;

			for (int i = 0; i < fullHearts; i++) {
				batch.Draw(FullHeartIcon, position, Color.White);
				heartsDrawed++;
				position += new Vector2(8, 0);
				if (heartsDrawed == 7) {
					position = new Vector2(103, 137);
				}
			}

			for (int i = 0; i < halfHearts; i++) {
				batch.Draw(HalfHeartIcon, position, Color.White);
				heartsDrawed++;
				position += new Vector2(8, 0);
				if (heartsDrawed == 7) {
					position = new Vector2(103, 137);
				}
			}

			for (int i = 0; i < emptyHearts; i++) {
				batch.Draw(EmptyHeartIcon, position, Color.White);
				heartsDrawed++;
				position += new Vector2(8, 0);
				if (heartsDrawed == 7) {
					position = new Vector2(103, 137);
				}
			}
		}
		#endregion
	}
}

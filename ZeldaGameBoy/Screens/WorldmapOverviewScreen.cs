﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GUI.Screens;
using SharpDX.Toolkit;
using SharpDX.Toolkit.Graphics;
using GUI.Interface;
using GUI;
using SharpDX;
using TiledMap;
using SharpDX.Toolkit.Content;

namespace ZeldaGameBoy.Screens {
	class OverworldMapObjects {
		private Dictionary<Vector2, Texture2D> _objects;
		private Texture2D _mapIconDungeonTexture;
		private Texture2D _mapIconOwlTexture;
		private Texture2D _mapIconQuestionTexture;
		private Texture2D _mapIconShopTexture;


		public OverworldMapObjects() {
			_objects = new Dictionary<Vector2, Texture2D>();
		}


		public void LoadContent(ContentManager content) {
			_mapIconDungeonTexture = content.Load<Texture2D>(@"Worldmap\map_icon_dungeon.png");
			_mapIconOwlTexture = content.Load<Texture2D>(@"Worldmap\map_icon_owl.png");
			_mapIconQuestionTexture = content.Load<Texture2D>(@"Worldmap\map_icon_question.png");
			_mapIconShopTexture = content.Load<Texture2D>(@"Worldmap\map_icon_shop.png");

			_objects.Add(new Vector2(0, 1), _mapIconDungeonTexture);
			_objects.Add(new Vector2(1, 2), _mapIconOwlTexture);
		}


		public void Draw(SpriteBatch spriteBatch, Vector2 cursorPosition) {
			var key = new Vector2((int)cursorPosition.X, (int)cursorPosition.Y);
			if (_objects.ContainsKey(key)) {
				spriteBatch.Draw(_objects[key], new Rectangle(100, 100, 30, 30), Color.White);
			}
		}
	}

	public class WorldmapOverviewScreen : GameScreen<Game1> {

		public BaseObject Player { get; set; }
		private Texture2D MapTexture { get; set; }
		private Texture2D FirstCursorTexture { get; set; }
		private Texture2D SecondCursorTexture { get; set; }
		private Texture2D BlockedTexture { get; set; }
		private Texture2D PlayerIconTexture { get; set; }

		private float Timer { get; set; }
		private float TimerInterval { get; set; }
		private bool ShowFirstCursor { get; set; }

		private Vector2 CursorPosition { get; set; }
		private Vector2 PlayerPosition { get; set; }
		private Camera PlayerCamera { get { return Player.GetComponent<CameraMovement>(ComponentType.CameraMovement).Camera; } }
		private OverworldMapObjects OverworldMapObjects;

		public WorldmapOverviewScreen(Game1 gameRef, ScreenManager screenManager, BaseObject player)
			: base(gameRef, screenManager) {
			Player = player;

			MapTexture = GameRef.Content.Load<Texture2D>(@"Worldmap\overworld_map.png");
			FirstCursorTexture = GameRef.Content.Load<Texture2D>(@"Worldmap\map_cursor_1.png");
			SecondCursorTexture = GameRef.Content.Load<Texture2D>(@"Worldmap\map_cursor_2.png");
			BlockedTexture = GameRef.Content.Load<Texture2D>(@"Worldmap\map_block.png");
			PlayerIconTexture = GameRef.Content.Load<Texture2D>(@"Worldmap\map_player_icon.png");

			Timer = 0f;
			TimerInterval = 2f;

			CursorPosition = Vector2.Zero;
			OverworldMapObjects = new OverworldMapObjects();
			OverworldMapObjects.LoadContent(GameRef.Content);
		}

		public override void AddCustomControl(string key, IControl control, bool renderingFirst) {
			base.AddCustomControl(key, control, renderingFirst);
		}

		public override void Initialize() {
			base.Initialize();

			CursorPosition = PlayerPosition = new Vector2(PlayerCamera.Position.X / PlayerCamera.Width, PlayerCamera.Position.Y / PlayerCamera.Height);
			Player.GetComponent<Stats>(ComponentType.Stats).VisitiedOverworldCells.Where(v => v.X == CursorPosition.X && v.Y == CursorPosition.Y).First().Visited = true;
		}

		public override void LoadContent() {
			base.LoadContent();
		}

		public override void Update(GameTime gameTime) {
			base.Update(gameTime);

			Timer += (float)gameTime.ElapsedGameTime.TotalSeconds;

			if (Timer > TimerInterval) {
				Timer -= TimerInterval;
				ShowFirstCursor = !ShowFirstCursor;
			}

			if (VirtualGamePad.GetGameKeyState(GameKeys.Select, GameKeysState.Released)) {
				ScreenManager.Instance.Transfer("gamescreen");
			} else {
				if (VirtualGamePad.GetGameKeyState(GameKeys.Up, GameKeysState.Released)) {
					CursorPosition = new Vector2(CursorPosition.X, Math.Max(0, CursorPosition.Y - 1));
				}

				if (VirtualGamePad.GetGameKeyState(GameKeys.Down, GameKeysState.Released)) {
					CursorPosition = new Vector2(CursorPosition.X, Math.Min(15, CursorPosition.Y + 1));
				}

				if (VirtualGamePad.GetGameKeyState(GameKeys.Left, GameKeysState.Released)) {
					CursorPosition = new Vector2(Math.Max(0, CursorPosition.X - 1), CursorPosition.Y);
				}

				if (VirtualGamePad.GetGameKeyState(GameKeys.Right, GameKeysState.Released)) {
					CursorPosition = new Vector2(Math.Min(15, CursorPosition.X + 1), CursorPosition.Y);
				}
			}
		}

		public override void Draw(SpriteBatch spriteBatch) {
			base.Draw(spriteBatch);

			spriteBatch.Draw(MapTexture, Vector2.Zero, Color.White);
			foreach (var info in Player.GetComponent<Stats>(ComponentType.Stats).VisitiedOverworldCells.Where(s => !s.Visited)) {
				spriteBatch.Draw(BlockedTexture, new Vector2((int)(16 + info.X * 8), (int)(8 + info.Y * 8)), Color.White);
			}

			spriteBatch.Draw(ShowFirstCursor ? FirstCursorTexture : SecondCursorTexture, new Rectangle((int)(3 + CursorPosition.X * 8), (int)(CursorPosition.Y * 8 - 5), 36, 36), Color.White);
			spriteBatch.Draw(PlayerIconTexture, new Rectangle((int)(18 + PlayerPosition.X * 8), (int)(PlayerPosition.Y * 8 + 10), 6, 6), ShowFirstCursor ? Color.White : Color.Red);
			OverworldMapObjects.Draw(spriteBatch, CursorPosition);
		}
	}
}

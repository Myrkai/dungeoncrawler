﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GUI.Interface;
using SharpDX.Toolkit.Graphics;
using SharpDX.Toolkit;
using GUI;
using ZeldaGameBoy.Components;
using TiledMap;
using SharpDX;

namespace ZeldaGameBoy {
	public class GameScreen : GUI.Screens.GameScreen<Game1> {
		private MapManager MapManager { get; set; }

		public GameScreen(Game1 gameRef, ScreenManager screenManager)
			: base(gameRef, screenManager) {

			// Player
			var Player = new BaseObject();
			MapManager = new MapManager();
			LuaScriptManager.Instance.Initialize(GameRef.Content, MapManager);

			Player.AddComponent(new PlayerInput());
			Player.AddComponent(new Animation(GameRef.Content.Load<Texture2D>(@"Sprites\link-schild.png"), 2, 4, 0.2f, new Vector2(20, 20)));
			Player.AddComponent(new Collision(MapManager, 40f));
			Player.AddComponent(new CameraMovement(MapManager));
			Player.AddComponent(new Equipment(gameRef.Content));
			Player.AddComponent(new Components.GUI(gameRef.Content));
			Player.AddComponent(StatsFactory.GetStats("Link"));
			Player.AddComponent(new Damage(MapManager, 0.75f, Color.Red, 0.2f));

			Player.GetComponent<Equipment>(ComponentType.Equipment).AddItem(0, new Bommerrang(MapManager));
			Player.GetComponent<Equipment>(ComponentType.Equipment).AddItem(1, new Sword(MapManager));
			Player.GetComponent<Equipment>(ComponentType.Equipment).EquipItemInSlot(0, ItemSlot.A);
			Player.GetComponent<Equipment>(ComponentType.Equipment).EquipItemInSlot(1, ItemSlot.B);

			Player.GetComponent<Animation>(ComponentType.Animation).AddAddionalTexture("link_sword", new Animation(GameRef.Content.Load<Texture2D>(@"Sprites\link_sword_hold2.png"), 3, 4, 0.15f, new Vector2(20, 20)));
			Player.GetComponent<Animation>(ComponentType.Animation).SetAnimationConfiguration(AnimationDirection.LEFT, AnimationDirection.DOWN, AnimationDirection.UP, AnimationDirection.RIGHT);
			Player.GetComponent<Animation>(ComponentType.Animation).SwitchAnimation("link_sword");
			Player.GetComponent<Animation>(ComponentType.Animation).SetAnimationConfiguration(AnimationDirection.UP, AnimationDirection.DOWN, AnimationDirection.RIGHT, AnimationDirection.LEFT);
			Player.GetComponent<Animation>(ComponentType.Animation).SwitchAnimationToDefault();

			// NPCs
			BaseObject NPC = new BaseObject();
			NPC.AddComponent(new RandomMovementAI(1.5f));
			NPC.AddComponent(new Animation(GameRef.Content.Load<Texture2D>(@"Sprites\marin.png"), 2, 4, 0.2f, new Vector2(2 * 16, 3 * 16)));
			NPC.AddComponent(new Collision(MapManager, 10f));
			NPC.GetComponent<Animation>(ComponentType.Animation).SetAnimationConfiguration(AnimationDirection.LEFT, AnimationDirection.DOWN, AnimationDirection.UP, AnimationDirection.RIGHT);

			BaseObject NPC2 = new BaseObject();
			NPC2.AddComponent(new RandomMovementAI(1.5f));
			NPC2.AddComponent(new Animation(GameRef.Content.Load<Texture2D>(@"Sprites\marin.png"), 2, 4, 0.2f, new Vector2(2 * 16, 13 * 16)));
			NPC2.AddComponent(new Collision(MapManager, 10f));
			NPC2.GetComponent<Animation>(ComponentType.Animation).SetAnimationConfiguration(AnimationDirection.LEFT, AnimationDirection.DOWN, AnimationDirection.UP, AnimationDirection.RIGHT);

			// Enemies
			BaseObject Enemy = new BaseObject();
			Enemy.AddComponent(new RandomMovementAI(1.5f));
			Enemy.AddComponent(new Animation(GameRef.Content.Load<Texture2D>(@"Sprites\octoroc.png"), 2, 4, 0.2f, new Vector2(3 * 16, 2 * 16)));
			Enemy.AddComponent(new Collision(MapManager, 10f));
			Enemy.AddComponent(new OctorocAI(MapManager, Player, new Bullet(GameRef.Content.Load<Texture2D>(@"Bullets\octoroc.bullet.png"), 60f, MapManager, Player), 1f));
			Enemy.GetComponent<Animation>(ComponentType.Animation).SetAnimationConfiguration(AnimationDirection.LEFT, AnimationDirection.DOWN, AnimationDirection.UP, AnimationDirection.RIGHT);
			Enemy.AddComponent(new Damage(MapManager, 0.75f, Color.Red, 0.2f, true));
			Enemy.AddComponent(StatsFactory.GetStats("Octoroc"));

			BaseObject Enemy2 = new BaseObject();
			Enemy2.AddComponent(new RandomMovementAI(1.5f));
			Enemy2.AddComponent(new Animation(GameRef.Content.Load<Texture2D>(@"Sprites\octoroc.png"), 2, 4, 0.2f, new Vector2(4 * 16, 2 * 16)));
			Enemy2.AddComponent(new Collision(MapManager, 10f));
			Enemy2.AddComponent(new OctorocAI(MapManager, Player, new Bullet(GameRef.Content.Load<Texture2D>(@"Bullets\octoroc.bullet.png"), 60f, MapManager, Player), 1f));
			Enemy2.GetComponent<Animation>(ComponentType.Animation).SetAnimationConfiguration(AnimationDirection.LEFT, AnimationDirection.DOWN, AnimationDirection.UP, AnimationDirection.RIGHT);
			Enemy2.AddComponent(new Damage(MapManager, 0.75f, Color.Red, 0.2f, true));
			Enemy2.AddComponent(StatsFactory.GetStats("Octoroc"));

			// Maps
			MapManager.AddMap("Malon-Farm", new Map(@"Content\Maps\Malon-Farm.tmx", GameRef.Content, @"Tilesets", new Camera(Vector2.Zero, 160, 16 * 8)));
			MapManager.AddMap("Malon-Farm-Animation", new Map(@"Content\Maps\Malon-Farm-Animation.tmx", GameRef.Content, @"Tilesets", new Camera(Vector2.Zero, 160, 16 * 8)));
			MapManager.AddMap("Overworld", new Map(@"Content\Maps\overworld.tmx", GameRef.Content, @"Tilesets", new Camera(Vector2.Zero, 160, 16 * 8)));
			MapManager.SwitchMap("Overworld");
			MapManager.CurrentMap.Player = Player;
			MapManager.CurrentMap.AddEntity(NPC, true);
			MapManager.CurrentMap.AddEntity(NPC2, true);
			MapManager.CurrentMap.AddEntity(Enemy, false);
			MapManager.CurrentMap.AddEntity(Enemy2, false);
		}

		public override void AddCustomControl(string key, IControl control, bool renderingFirst) {
			base.AddCustomControl(key, control, renderingFirst);
		}

		public override void Draw(SpriteBatch spriteBatch) {
			base.Draw(spriteBatch);
			MapManager.Draw(spriteBatch);
			WindowManager.Instance.Draw(spriteBatch);
			EventManager.Instance.Draw(spriteBatch);
		}

		public override void Initialize() {
			base.Initialize();
		}

		public override void LoadContent() {
			base.LoadContent();
		}

		public override void Update(GameTime gameTime) {
			base.Update(gameTime);
			MapManager.Update(gameTime);
			WindowManager.Instance.Update(gameTime);
			EventManager.Instance.Update(gameTime);
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpDX.Toolkit.Content;
using SharpDX.Toolkit;
using SharpDX.Toolkit.Graphics;
using SharpDX;

namespace ZeldaGameBoy {
	public class TileEvent {
		public bool Done { get; private set; } //Event ist beendet und kann nicht nochmal gestartet werden
		public bool Active { get; private set; } //Event läuft gerade
		public IGameEvent GameEvent { get; private set; }
		private ContentManager Content { get; set; }
		public RectangleF Dimension { get; private set; }
		public bool IsPlayerOnly { get; set; }
		private BaseObject Entity { get; set; }

		public TileEvent(IGameEvent gameEvent, ContentManager content, RectangleF dimension) {
			Done = false;
			Active = false;
			Content = content;
			Dimension = dimension;
			IsPlayerOnly = true;
			GameEvent = gameEvent;
		}

		public void Update(GameTime gameTime) {
			
		}

		public void Draw(SpriteBatch spriteBatch) {

		}

		public void Restart() {
			if (!Done && !Active) {
				Active = true;
				EventManager.Instance.AddEvent(GameEvent);
			}
		}

		public void Break() {
			if (!Done) {
				Active = false;
			}
		}

		public void Resume() {
			if (!Done) {
				Active = true;
			}
		}

		public void Finish() {
			if (!Done) {
				Done = true;
			}
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpDX.Toolkit;
using SharpDX.Toolkit.Graphics;
using SharpDX.Toolkit.Content;

namespace ZeldaGameBoy {
	public interface IGameEvent {
		bool Done { get; set; }
		GameEventType EventType { get; }
		void Initialize(ContentManager content);
		void Update(GameTime gameTime);
		void Draw(SpriteBatch spriteBatch);
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpDX.Toolkit;
using SharpDX.Toolkit.Graphics;
using SharpDX.Toolkit.Content;

namespace ZeldaGameBoy {
	public class GameEventMessage : IGameEvent {
		public bool Done { get; set; }
		public string Text { get; private set; }

		public GameEventType EventType {
			get { return GameEventType.Message; }
		}

		public GameEventMessage(string text) {
			Text = text;
		}

		public void Initialize(ContentManager content) {
			MessageWindow window = new MessageWindow(content, Text);
			WindowManager.Instance.NewWindow("gameEventMessage", window);
			VirtualGamePad.SuspentInputs = true;
			Done = false; 
		}

		public void Update(GameTime gameTime) {
			if (!WindowManager.Instance.Contains("gameEventMessage")) {
				Done = true;
				VirtualGamePad.SuspentInputs = false;
			}
		}

		public void Draw(SpriteBatch spriteBatch) {
			
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Threading;

namespace ZeldaGameBoy {
	static class Program {
		/// <summary>
		/// Der Haupteinstiegspunkt für die Anwendung.
		/// </summary>
		[STAThread]
		static void Main() {
			using (var si = new SingleInstance()) {
				if (si.IsRunning()) {
					MessageBox.Show("Programm läuft bereits.", "Kein Start", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				} else {
					using (Game1 game = new Game1()) {
						game.Run();
					}
				}
			}
		}

		#region Nested type: SingleInstance

		/// <summary>
		/// Class to detect when another instance of the app is running.
		/// </summary>
		private class SingleInstance : IDisposable {

			//our Mutex member variable
			private Mutex _mutex;

			public SingleInstance() {
				_mutex = new Mutex(true, "10a1ebcb-4f71-419c-0815-cfc033cf30d2");
			}

			#region IDisposable Members

			public void Dispose() {
				if (null == _mutex) return;
				try {
					_mutex.ReleaseMutex();
				} catch (ApplicationException) {
					// Occurs if we didn't own the mutex - ie, for second instance.
				}
				_mutex.Close();
				_mutex = null;
			}
			#endregion
			public bool IsRunning() {
				return !_mutex.WaitOne(10, true);
			}
		}
		#endregion
	}
}

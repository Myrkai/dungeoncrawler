﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpDX;
using SharpDX.Toolkit;

namespace ZeldaGameBoy {
	public class ColorChanger {
		private List<Color> Colors { get; set; }
		private Color Start { get; set; } // Blue -> Red -> Yellow -> Green
		private Color End { get; set; }

		private float CurrentChangeAmount { get; set; }

		private int CurrentColorIndexStart { get; set; }
		private int CurrentColorIndexEnd { get; set; }

		public Color Color { get { return Start.Lerp(End, CurrentChangeAmount); } }

		public ColorChanger(params Color[] colors) {
			Colors = new List<Color>();
			foreach (Color c in colors) {
				Colors.Add(c);
			}

			if (Colors.Count < 2) {
				throw new Exception("Es müssen mindestens 2 Farben angegeben werden.");
			}

			Start = Colors[0];
			End = Colors[1];
			CurrentChangeAmount = 0f;

			CurrentColorIndexStart = 0;
			CurrentColorIndexEnd = 1;
		}

		public void Update(GameTime gameTime) {
			CurrentChangeAmount += 0.03f;

			if (CurrentChangeAmount >= 1f) {
				CurrentChangeAmount = 0f;
				CurrentColorIndexStart++;
				CurrentColorIndexEnd++;

				if (CurrentColorIndexStart > Colors.Count - 1) {
					CurrentColorIndexStart = 0;
				}

				if (CurrentColorIndexEnd > Colors.Count - 1) {
					CurrentColorIndexEnd = 0;
				}

				Start = Colors[CurrentColorIndexStart];
				End = Colors[CurrentColorIndexEnd];
			}
		}
	}
}

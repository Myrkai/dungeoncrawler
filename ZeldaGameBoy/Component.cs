﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpDX.Toolkit.Graphics;
using SharpDX.Toolkit;

namespace ZeldaGameBoy {
	public abstract class Component {
		internal BaseObject _baseObject;
		public abstract ComponentType ComponentType { get; }

		public void Initialize(BaseObject baseObject) {
			_baseObject = baseObject;
		}

		public string GetOwnerId() {
			if (_baseObject == null)
				return "";
			return _baseObject.Id;
		}

		public void RemoveMe() {
			_baseObject.RemoveComponent(this);
		}

		public void KillBaseObject() {
			_baseObject.Kill = true;
		}

		public TComponent GetComponent<TComponent>(ComponentType componentType) where TComponent : Component {
			return _baseObject == null ? null : _baseObject.GetComponent<TComponent>(componentType);
		}

		public abstract void Update(GameTime gameTime);
		public abstract void Draw(SpriteBatch spritebatch);

		public virtual void Initialize() { }

		public virtual void Uninitalize() { }
	}
}

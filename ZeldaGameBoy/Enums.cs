﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZeldaGameBoy {
	public enum ComponentType {
		Sprite,
		Input,
		Animation,
		Collision,
		AI,
		CameraMovement,
		Equipment,
		GUI,
		Stats,
		Damage,
		DeathEffect,
		EnemyOctoroc
	}

	public enum AnimationDirection {
		UP = 3,
		DOWN = 0,
		LEFT = 1,
		RIGHT = 2,
		NULL = -1
	}

	public enum AnimationState { 
		Walking,
		Standing
	}

	public enum ItemSlot {
		A,
		B
	}

	public enum GameEventType { 
		Message,
		TileEvent
	}

	public enum DeathAnimation {
		Link,
		Exposion
	}
}

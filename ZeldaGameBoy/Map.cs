﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpDX.Toolkit.Content;
using TiledMap;
using SharpDX.Toolkit;

namespace ZeldaGameBoy {
	public class Map : TiledMap.TiledMap {
		public List<TileEvent> TileEvents { get; set; }
		private List<BaseObject> Entities { get; set; }
		private List<BaseObject> NPCs { get; set; }
		private List<BaseObject> Enermies { get; set; }
		public BaseObject Player { get; set; }

		public List<BaseObject> GetEntities() {
			return Entities;
		}

		public List<BaseObject> GetNPCs() {
			return NPCs;
		}

		public List<BaseObject> GetEnermies() {
			return Enermies;
		}

		public void AddEntity(BaseObject entity, bool isNPC) {
			Entities.Add(entity);
			if (isNPC) {
				NPCs.Add(entity);
			} else {
				Enermies.Add(entity);
			}
		}

		public void AddEntities(List<BaseObject> entities, bool isNPC) {
			Entities.AddRange(entities);
			if (isNPC) {
				NPCs.AddRange(entities);
			} else {
				Enermies.AddRange(entities);
			}
		}

		public Map(string path, ContentManager content, string tileSetPathFolder)
			: base(path, content, tileSetPathFolder) {
			TileEvents = new List<TileEvent>();
			ConvertTiledEvents();
			Entities = new List<BaseObject>();
			NPCs = new List<BaseObject>();
			Enermies = new List<BaseObject>();
		}

		public Map(string path, ContentManager content, string tileSetPathFolder, Camera cam)
			: base(path, content, tileSetPathFolder, cam) {
			TileEvents = new List<TileEvent>();
			ConvertTiledEvents();
			Entities = new List<BaseObject>();
			NPCs = new List<BaseObject>();
			Enermies = new List<BaseObject>();
		}

		private void ConvertTiledEvents() {
			foreach (MapObject mapObject in base.Events) {
				switch (mapObject.Type) {
					case "MessageEvent":
						TileEvents.Add(new TileEvent(new GameEventMessage(mapObject.Name), Content, mapObject.Dimension));
						break;
					default:
						throw new Exception(mapObject.Type + "is not supported");
				}
			}
		}

		public override void Update(GameTime gametime) {
			base.Update(gametime);
			Entities = Entities.Where(e => !e.Kill).ToList();
			Enermies = Enermies.Where(e => !e.Kill).ToList();
			NPCs = NPCs.Where(e => !e.Kill).ToList();
		}
	}
}

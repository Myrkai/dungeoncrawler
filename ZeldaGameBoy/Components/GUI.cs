﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpDX.Toolkit;
using SharpDX.Toolkit.Graphics;
using SharpDX.Toolkit.Content;
using SharpDX;

namespace ZeldaGameBoy.Components {
	public class GUI : Component {
		private ContentManager Content { get; set; }

		private Texture2D ItemSlotAIcon { get; set; }
		private Texture2D ItemSlotBIcon { get; set; }
		private Texture2D RupieIcon { get; set; }
		private Texture2D FullHeartIcon { get; set; }
		private Texture2D HalfHeartIcon { get; set; }
		private Texture2D EmptyHeartIcon { get; set; }
		private Texture2D Font { get; set; }

		public GUI(ContentManager content) {
			Content = content;

			ItemSlotAIcon = Content.Load<Texture2D>(@"GUI\ASlotIcon.png");
			ItemSlotBIcon = Content.Load<Texture2D>(@"GUI\BSlotIcon.png");
			RupieIcon = Content.Load<Texture2D>(@"GUI\RupiIcon.png");
			FullHeartIcon = Content.Load<Texture2D>(@"GUI\FullHeartIcon.png");
			HalfHeartIcon = Content.Load<Texture2D>(@"GUI\HalfHeartIcon.png");
			EmptyHeartIcon = Content.Load<Texture2D>(@"GUI\EmtpyHeartIcon.png");
			Font = Content.Load<Texture2D>(@"Fonts\Schrift2.png");
		}

		public override ComponentType ComponentType {
			get { return ComponentType.GUI; }
		}

		public override void Update(GameTime gameTime) {
			
		}

		public override void Draw(SpriteBatch spritebatch) {
			spritebatch.Draw(ItemSlotBIcon, new Vector2(2, 129), Color.White);
			spritebatch.Draw(ItemSlotAIcon, new Vector2(43, 129), Color.White);
			spritebatch.Draw(RupieIcon, new Vector2(83, 129), Color.White);

			Stats stats = GetComponent<Stats>(ComponentType.Stats);
			if (stats != null) {
				DrawNumber(stats.Rupee.ToString("000"), spritebatch);
				DrawLife(stats.CurrentHealth, stats.MaxHealth, spritebatch);
			} else {
				DrawNumber("000", spritebatch);
				DrawLife(0, 6, spritebatch);
			}

			Equipment equipment = GetComponent<Equipment>(ComponentType.Equipment);
			if (equipment != null) {
				if (equipment.EquipedInSlot(ItemSlot.B)) {
					equipment.DrawGui(spritebatch, ItemSlot.B, new Rectangle(11, 130, 12, 12));
				}

				if (equipment.EquipedInSlot(ItemSlot.A)) {
					equipment.DrawGui(spritebatch, ItemSlot.A, new Rectangle(52, 130, 12, 12));
				}
			}
		}

		private void DrawNumber(string text, SpriteBatch batch) {
			Vector2 position = new Vector2(83, 138);
			foreach (char z in text.ToCharArray()) {
				int number = Convert.ToInt16(z) - 48;
				batch.Draw(Font, position, new Rectangle(4 * number, 0, 4, 5), Color.Black); 
				position += new Vector2(5, 0);
			}
		}

		private void DrawLife(int lifeamount, int maxlife, SpriteBatch batch) {
			if (maxlife % 2 == 1) {
				throw new Exception("maxlife muss durch 2 Teilbar sein (2 Leben sind ein Herz)");
			}

			Vector2 position = new Vector2(103, 129);
			int fullHearts = lifeamount / 2;
			int halfHearts = lifeamount % 2;
			int emptyHearts = (maxlife / 2) - fullHearts - halfHearts;
			int heartsDrawed = 0;

			for (int i = 0; i < fullHearts; i++) {
				batch.Draw(FullHeartIcon, position, Color.White);
				heartsDrawed++;
				position += new Vector2(8, 0);
				if (heartsDrawed == 7) {
					position = new Vector2(103, 137);
				}
			}

			for (int i = 0; i < halfHearts; i++) {
				batch.Draw(HalfHeartIcon, position, Color.White);
				heartsDrawed++;
				position += new Vector2(8, 0);
				if (heartsDrawed == 7) {
					position = new Vector2(103, 137);
				}
			}

			for (int i = 0; i < emptyHearts; i++) {
				batch.Draw(EmptyHeartIcon, position, Color.White);
				heartsDrawed++;
				position += new Vector2(8, 0);
				if (heartsDrawed == 7) {
					position = new Vector2(103, 137);
				}
			}
		}
	}
}

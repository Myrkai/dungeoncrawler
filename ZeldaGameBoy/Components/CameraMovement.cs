﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpDX.Toolkit;
using SharpDX.Toolkit.Graphics;
using SharpDX;
using TiledMap;

namespace ZeldaGameBoy {
	public class CameraMovement : Component {

		private MapManager MapManager { get; set; }
		private float Speed = 240f;
		private Vector2 MoveVector;
		private Vector2 Target;
		private float Distance;
		public bool IsMoving { get; set; }
		public Camera Camera { get { return MapManager.CurrentMap.Cam; } }

		public CameraMovement(MapManager mapManager) {
			MapManager = mapManager;
			MoveVector = Vector2.Zero;
			IsMoving = false;
		}

		public override ComponentType ComponentType {
			get { return ComponentType.CameraMovement; }
		}

		public override void Update(GameTime gameTime) {
			Animation player = GetComponent<Animation>(ComponentType.Animation);
			if (player != null) {
				RectangleF playerRec = new RectangleF(player.Position.X + (player.CurrentTextureRectangle().Width * 0.4f / 2f), player.Position.Y + (player.CurrentTextureRectangle().Height * 0.5f), player.CurrentTextureRectangle().Width * 0.6f, player.CurrentTextureRectangle().Height * 0.5f);

				if (MoveVector != Vector2.Zero) {
					IsMoving = true;
					MapManager.CurrentMap.TileEvents.Where(s => s.Active).ToList().ForEach(s => s.Break()); //bei Bildwechseln werden alle Activen Events zurückgesetzt

					Camera.Position += MoveVector * Speed * (float)gameTime.ElapsedGameTime.TotalSeconds;
					player.Position -= MoveVector * Speed * 0.9f * (float)gameTime.ElapsedGameTime.TotalSeconds;
					MapManager.CurrentMap.GetEntities().ForEach(b => b.GetComponent<Animation>(ComponentType.Animation).Position -= MoveVector * Speed * (float)gameTime.ElapsedGameTime.TotalSeconds);

					if (Camera.Position.Y > Target.Y && MoveVector.Y == 1 || Camera.Position.Y <= Target.Y && MoveVector.Y == -1) {
						Camera.Position = Target;
						MoveVector = Vector2.Zero;

						GetComponent<Stats>(ComponentType.Stats).VisitiedOverworldCells.Where(v => v.X == (int)(Camera.Position.X / Camera.Width) && v.Y == (int)(Camera.Position.Y / Camera.Height)).First().Visited = true;
					} else if (Camera.Position.X > Target.X && MoveVector.X == 1 || Camera.Position.X <= Target.X && MoveVector.X == -1) {
						Camera.Position = Target;
						MoveVector = Vector2.Zero;

						GetComponent<Stats>(ComponentType.Stats).VisitiedOverworldCells.Where(v => v.X == (int)(Camera.Position.X / Camera.Width) && v.Y == (int)(Camera.Position.Y / Camera.Height)).First().Visited = true;
					}
				} else {
					IsMoving = false;

					if (playerRec.Bottom + Camera.Position.Y >= Camera.Rectangle.BottomLeft.Y - 0.5f && Camera.Rectangle.Bottom < MapManager.CurrentMap.MapHeightPx) {
						MoveVector = new Vector2(0, 1);
						Distance = 8f * 16f;
					}

					if ((int)playerRec.Top + Camera.Position.Y <= Camera.Rectangle.TopLeft.Y && Camera.Position.Y > 0) {
						MoveVector = new Vector2(0, -1);
						Distance = 8f * 16f;
					}

					if (playerRec.Right + Camera.Position.X >= Camera.Rectangle.TopRight.X - 0.5f && Camera.Rectangle.Right < MapManager.CurrentMap.MapWidthPx) {
						MoveVector = new Vector2(1, 0);
						Distance = 10f * 16f;
					}

					if ((int)playerRec.Left + Camera.Position.X <= Camera.Rectangle.TopLeft.X && Camera.Position.X > 0) {
						MoveVector = new Vector2(-1, 0);
						Distance = 10f * 16f;
					}

					Target = Camera.Position + MoveVector * Distance;
				}
			}
		}

		public override void Draw(SpriteBatch spritebatch) {

		}
	}
}

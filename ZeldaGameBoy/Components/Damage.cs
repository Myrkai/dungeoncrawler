﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpDX.Toolkit;
using SharpDX.Toolkit.Graphics;
using SharpDX;

namespace ZeldaGameBoy.Components {
	public class Damage : Component {

		private MapManager MapManager { get; set; }
		private bool IsHitted { get; set; }
		private float DamageIntervalTimer { get; set; }
		private float DamageInterval { get; set; }
		private Color DamageColor { get; set; }
		private Color AnimationStartColor { get; set; }

		private float BlinkTimer { get; set; }
		private float BlinkInterval { get; set; }
		private Color CurrentBlinkColor { get; set; }

		private float MoveDistance { get; set; }
		private Vector2 AutoMoveDirection { get; set; }
		private bool DoAutoMove { get; set; }
		private float AutoMoveTimer { get; set; }
		private bool IsEnermy { get; set; }

		public Damage(MapManager mapManager, float damageInterval, Color damageColor, float blinkInterval, bool isEnermy = false) {
			MapManager = mapManager;
			DamageInterval = damageInterval;
			DamageIntervalTimer = 0f;
			DamageColor = damageColor;
			Animation player = GetComponent<Animation>(ComponentType.Animation);
			AnimationStartColor = player != null ? player.Color : Color.White;
			BlinkInterval = blinkInterval;
			BlinkTimer = 0f;
			CurrentBlinkColor = DamageColor;
			MoveDistance = 12f;
			IsEnermy = isEnermy;
		}

		public override ComponentType ComponentType {
			get { return ComponentType.Damage; }
		}

		public override void Update(GameTime gameTime) {
			Animation player = GetComponent<Animation>(ComponentType.Animation);
			Collision playerCollision = GetComponent<Collision>(ComponentType.Collision);
			if (playerCollision == null) {
				return;
			}

			if (DoAutoMove) {
				if (!IsEnermy) {
					VirtualGamePad.SuspentInputs = true;
				}
				AutoMoveTimer += (float)gameTime.ElapsedGameTime.TotalSeconds;
				if (AutoMoveTimer > 0.25f) {
					DoAutoMove = false;
					VirtualGamePad.SuspentInputs = false;
				} else {
					if (playerCollision.IsWalkable(player.PositionRec.AddVector(AutoMoveDirection * (float)gameTime.ElapsedGameTime.TotalSeconds * 80f))) {
						player.Position += AutoMoveDirection * (float)gameTime.ElapsedGameTime.TotalSeconds * 60f;
					}
				}
			}

			if (IsHitted) {
				DamageIntervalTimer += (float)gameTime.ElapsedGameTime.TotalSeconds;
				BlinkTimer += (float)gameTime.ElapsedGameTime.TotalSeconds;

				if (BlinkTimer > BlinkInterval) {
					BlinkTimer = 0f;
					CurrentBlinkColor = CurrentBlinkColor == DamageColor ? AnimationStartColor : DamageColor;
				}

				Animation playerSprite = GetComponent<Animation>(ComponentType.Animation);
				if (playerSprite != null) {
					playerSprite.Color = CurrentBlinkColor;
				}

				if (DamageIntervalTimer > DamageInterval) {
					IsHitted = false;
					DamageIntervalTimer = 0f;
					if (playerSprite != null) {
						playerSprite.Color = AnimationStartColor;
					}
				}
				return;
			}

			if (player != null) {
				if (IsEnermy) {
					return;
				}

				BaseObject enermy = MapManager.CurrentMap.GetEnermies().Where(s => s.GetComponent<Animation>(ComponentType.Animation).PositionRec.Interacts(player.PositionRec, 0.1f)).FirstOrDefault();
				if (enermy != null) {
					Animation enermyAnimation = enermy.GetComponent<Animation>(ComponentType.Animation);
					IsHitted = true;
					DoAutoMove = true;
					AutoMoveTimer = 0f;
					RedurceHealth(enermy);

					if (player.AnimationState == AnimationState.Walking) {
						switch (player.CurrentAnimationDirection) {
							case AnimationDirection.DOWN:
								if (playerCollision.IsWalkable(player.PositionRec.AddVector(new Vector2(0, -1) * MoveDistance))) {
									AutoMoveDirection = new Vector2(0, -1);
								}
								break;
							case AnimationDirection.LEFT:
								if (playerCollision.IsWalkable(player.PositionRec.AddVector(new Vector2(1, 0) * MoveDistance))) {
									AutoMoveDirection = new Vector2(1, 0);
								}
								break;
							case AnimationDirection.RIGHT:
								if (playerCollision.IsWalkable(player.PositionRec.AddVector(new Vector2(-1, 0) * MoveDistance))) {
									AutoMoveDirection = new Vector2(-1, 0);
								}
								break;
							case AnimationDirection.UP:
								if (playerCollision.IsWalkable(player.PositionRec.AddVector(new Vector2(0, 1) * MoveDistance))) {
									AutoMoveDirection = new Vector2(0, 1);
								}
								break;
						}
					} else {
						switch (enermyAnimation.CurrentAnimationDirection) {
							case AnimationDirection.DOWN:
								if (playerCollision.IsWalkable(player.PositionRec.AddVector(new Vector2(0, 1) * MoveDistance))) {
									AutoMoveDirection = new Vector2(0, 1);
								}
								break;
							case AnimationDirection.LEFT:
								if (playerCollision.IsWalkable(player.PositionRec.AddVector(new Vector2(-1, 0) * MoveDistance))) {
									AutoMoveDirection = new Vector2(-1, 0);
								}
								break;
							case AnimationDirection.RIGHT:
								if (playerCollision.IsWalkable(player.PositionRec.AddVector(new Vector2(1, 0) * MoveDistance))) {
									AutoMoveDirection = new Vector2(1, 0);
								}
								break;
							case AnimationDirection.UP:
								if (playerCollision.IsWalkable(player.PositionRec.AddVector(new Vector2(0, -1) * MoveDistance))) {
									AutoMoveDirection = new Vector2(0, -1);
								}
								break;
						}
					}
				}
			}
		}

		public void GetHittet(AnimationDirection direction, BaseObject enemy) {
			if (IsHitted) {
				return;
			}

			Collision playerCollision = GetComponent<Collision>(ComponentType.Collision);
			Animation player = GetComponent<Animation>(ComponentType.Animation);
			IsHitted = true;
			DoAutoMove = true;
			AutoMoveTimer = 0f;
			RedurceHealth(enemy);

			switch (direction) {
				case AnimationDirection.DOWN:
					if (playerCollision.IsWalkable(player.PositionRec.AddVector(new Vector2(0, 1) * MoveDistance))) {
						AutoMoveDirection = new Vector2(0, 1);
					}
					break;
				case AnimationDirection.LEFT:
					if (playerCollision.IsWalkable(player.PositionRec.AddVector(new Vector2(-1, 0) * MoveDistance))) {
						AutoMoveDirection = new Vector2(-1, 0);
					}
					break;
				case AnimationDirection.RIGHT:
					if (playerCollision.IsWalkable(player.PositionRec.AddVector(new Vector2(1, 0) * MoveDistance))) {
						AutoMoveDirection = new Vector2(1, 0);
					}
					break;
				case AnimationDirection.UP:
					if (playerCollision.IsWalkable(player.PositionRec.AddVector(new Vector2(0, -1) * MoveDistance))) {
						AutoMoveDirection = new Vector2(0, -1);
					}
					break;
			}
		}

		private void RedurceHealth(BaseObject enermy) {
			Stats stats = GetComponent<Stats>(ComponentType.Stats);
			Stats enermyStats = enermy.GetComponent<Stats>(ComponentType.Stats);

			if (stats != null && enermyStats != null && !stats.IsDeath) {
				stats.CurrentHealth -= (Math.Max(0, enermyStats.Attack - stats.Defence));

				if(stats.CurrentHealth == 0) {
					MapManager.CurrentMap.AddEntity(DeathAnimationFactory.GetDeathAnimationObject(DeathAnimation.Exposion, stats._baseObject.GetComponent<Animation>(ComponentType.Animation).Position), true);
				}
			}
		}

		public override void Draw(SpriteBatch spritebatch) {

		}
	}
}

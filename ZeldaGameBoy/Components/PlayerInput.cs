﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpDX.Toolkit;
using SharpDX.Toolkit.Graphics;
using SharpDX;
using GUI;
using System.Threading;
using ZeldaGameBoy.Screens;

namespace ZeldaGameBoy.Components {
	public class PlayerInput : Component {
		public override ComponentType ComponentType {
			get { return ComponentType.Input; }
		}

		public PlayerInput() {

		}

		public override void Update(GameTime gameTime) {
			Vector2 direction = Vector2.Zero;
			AnimationDirection animationDirection = AnimationDirection.NULL;
			CameraMovement camera = GetComponent<CameraMovement>(ComponentType.CameraMovement);
			if (camera == null || camera.IsMoving) {
				return;
			}

			if (VirtualGamePad.GetGameKeyState(GameKeys.Start, GameKeysState.Released)) {
				((Inventory)ScreenManager.Instance.GetScreen("inventory")).Player = _baseObject;
				ScreenManager.Instance.Transfer("inventory");
				return;
			}

			if (VirtualGamePad.GetGameKeyState(GameKeys.Select, GameKeysState.Released)) {
				((WorldmapOverviewScreen)ScreenManager.Instance.GetScreen("worldmap")).Player = _baseObject;
				((WorldmapOverviewScreen)ScreenManager.Instance.GetScreen("worldmap")).Initialize();
				ScreenManager.Instance.Transfer("worldmap");
				return;
			}

			// Entwerden nur ein Item oder alle Richtungen gleichzeit möglich
			if (VirtualGamePad.GetGameKeyState(GameKeys.A, GameKeysState.Pressed)) {
				Equipment playerEquip = GetComponent<Equipment>(ComponentType.Equipment);
				if (playerEquip != null) {
					if (playerEquip.EquipedInSlot(ItemSlot.A)) {
						playerEquip.FireItem(ItemSlot.A);
					}
				}
			} else if (VirtualGamePad.GetGameKeyState(GameKeys.B, GameKeysState.Pressed)) {
				Equipment playerEquip = GetComponent<Equipment>(ComponentType.Equipment);
				if (playerEquip != null) {
					if (playerEquip.EquipedInSlot(ItemSlot.B)) {
						playerEquip.FireItem(ItemSlot.B);
					}
				}
			} else {
				if (VirtualGamePad.GetGameKeyState(GameKeys.Up, GameKeysState.Down)) {
					direction = new Vector2(direction.X, -1);
					animationDirection = AnimationDirection.UP;
				}

				if (VirtualGamePad.GetGameKeyState(GameKeys.Down, GameKeysState.Down)) {
					direction = new Vector2(direction.X, 1);
					animationDirection = AnimationDirection.DOWN;
				}

				if (VirtualGamePad.GetGameKeyState(GameKeys.Left, GameKeysState.Down)) {
					direction = new Vector2(-1, direction.Y);
					animationDirection = AnimationDirection.LEFT;
				}

				if (VirtualGamePad.GetGameKeyState(GameKeys.Right, GameKeysState.Down)) {
					direction = new Vector2(1, direction.Y);
					animationDirection = AnimationDirection.RIGHT;
				}
			}

			Collision collision = GetComponent<Collision>(ComponentType.Collision);
			if (collision != null) {
				direction = collision.GetMoveDirectionFromTileCollision(direction, (float)gameTime.ElapsedGameTime.TotalSeconds);
			}

			if (direction != Vector2.Zero && animationDirection != AnimationDirection.NULL) {
				Animation sprite = GetComponent<Animation>(ComponentType.Animation);
				if (sprite != null) {
					sprite.Position += direction;
					sprite.AnimationState = AnimationState.Walking;
					sprite.SetDirection(animationDirection);
					collision.RaiseTileEvents(sprite.PositionRec, true);
				}
			} else {
				Animation sprite = GetComponent<Animation>(ComponentType.Animation);
				if (sprite != null) {
					sprite.AnimationState = AnimationState.Standing;
				}
			}
		}

		public override void Draw(SpriteBatch spritebatch) {

		}
	}
}

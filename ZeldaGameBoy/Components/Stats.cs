﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpDX.Toolkit.Graphics;
using SharpDX.Toolkit;
using SharpDX;

namespace ZeldaGameBoy {
	public class OverworldCellInformation {
		public int X;
		public int Y;
		public bool Visited;
	}

	public class Stats : Component {

		public List<OverworldCellInformation> VisitiedOverworldCells { get; set; }
		public string StatsId { get; set; }
		public int MaxHealth { get; set; }
		public int CurrentHealth { get; set; }
		public int Attack { get; set; }
		public int Defence { get; set; }
		private int _Rupee { get; set; }
		public int Rupee {
			get { return _Rupee; }
			set { _Rupee = MathUtil.Clamp(value, 0, 999); }
		}
		public bool IsDeath { get { return CurrentHealth <= 0; } }

		public Stats() {
			VisitiedOverworldCells = new List<OverworldCellInformation>();
			for (int x = 0; x < 16; x++) {
				for (int y = 0; y < 16; y++) {
					VisitiedOverworldCells.Add(new OverworldCellInformation() { X = x, Y = y, Visited = false });
				}
			}
		}

		public Stats(string statsID, int maxHealth, int health, int attack, int defence, int rupee)
			: this() {
			StatsId = statsID;
			MaxHealth = maxHealth;
			CurrentHealth = health;
			Attack = attack;
			Defence = defence;
			Rupee = rupee;
		}

		public Stats(Stats stats)
			: this() {
			StatsId = stats.StatsId;
			MaxHealth = stats.MaxHealth;
			CurrentHealth = stats.CurrentHealth;
			Attack = stats.Attack;
			Defence = stats.Defence;
			Rupee = stats.Rupee;
			VisitiedOverworldCells = stats.VisitiedOverworldCells;
		}

		public override ComponentType ComponentType {
			get { return ComponentType.Stats; }
		}

		public override void Update(GameTime gameTime) {
			if (IsDeath) {
				_baseObject.Kill = true;
			}
		}

		public override void Draw(SpriteBatch spritebatch) {

		}

		public Stats Clone() {
			Stats tmp = new Stats();
			tmp.StatsId = StatsId;
			tmp.MaxHealth = MaxHealth;
			tmp.CurrentHealth = CurrentHealth;
			tmp.Attack = Attack;
			tmp.Defence = Defence;
			tmp.Rupee = Rupee;

			return tmp;
		}
	}
}

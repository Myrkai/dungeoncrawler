﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpDX.Toolkit;
using SharpDX.Toolkit.Graphics;

namespace ZeldaGameBoy {
	public class DeathEffect : Component {
		
		private int Counter;

		public DeathEffect() {
			Counter = 0;
		}

		public override ComponentType ComponentType {
			get { return ComponentType.DeathEffect; }
		}

		public override void Update(GameTime gameTime) {
			Animation animation = GetComponent<Animation>(ComponentType.Animation);
			if(animation != null) {
				if(Counter != animation.CurrentAnimationStepIndex) {
					Counter++;
				}

				if(Counter >= animation.AnimationStepsPerAnimation) {
					_baseObject.Kill = true;
				}
			}
		}

		public override void Draw(SpriteBatch spritebatch) {
			
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpDX.Toolkit;
using SharpDX.Toolkit.Graphics;
using SharpDX;

namespace ZeldaGameBoy {
	public class Animation : Component {

		public Texture2D Texture { get; private set; }
		public int AnimationStepsPerAnimation { get; private set; }
		public int AnimationCount { get; private set; }
		public float AnimationSpeed { get; private set; }

		private AnimationDirection _CurrentAnimationDirection { get; set; }
		public AnimationDirection CurrentAnimationDirection {
			get { if (OtherAnimation != null) { return OtherAnimation.CurrentAnimationDirection; } else { return _CurrentAnimationDirection; } }
			set { if (OtherAnimation != null) { OtherAnimation.CurrentAnimationDirection = value; } else { _CurrentAnimationDirection = value; } }
		}
		public AnimationState AnimationState { get; set; }
		public int CurrentAnimationStepIndex { get; private set; }

		private float Timer { get; set; }

		private int spriteSingleWidth;
		private int spriteSingleHeight;
		public int AnimationStopIndex { get; set; }
		public Vector2 Position { get; set; }
		public Vector2 Size { get { return new Vector2(spriteSingleWidth, spriteSingleHeight); } }
		public Rectangle PositionRec { get { return new Rectangle((int)Position.X, (int)Position.Y, spriteSingleWidth, spriteSingleHeight); } }

		public Color Color { get; set; }

		private Dictionary<AnimationDirection, int> AnimationIndexes;
		private Dictionary<string, Animation> Animations;
		private Animation OtherAnimation;
		public bool LookAnimationDirection { get; set; }

		/// <summary>
		/// Eine Animierte Figur. Default der Animationen sind: DOWN, LEFT, RIGHT, UP
		/// </summary>
		/// <param name="texture">Das jeweilioge Bild mit allen Animationsphasen</param>
		/// <param name="imagesPerAni">Wieviele Bilder besitzt die Animation (Pro Animation)</param>
		/// <param name="animationCount">Wieviele verschiedene Animationen gibt es</param>
		/// <param name="animationspeed">Wie schnell wird die Animation abgespielt</param>
		public Animation(Texture2D texture, int imagesPerAni, int animationCount, float animationspeed, Vector2 position) {
			Texture = texture;
			AnimationStepsPerAnimation = imagesPerAni;
			AnimationCount = animationCount;
			AnimationSpeed = animationspeed;

			CurrentAnimationDirection = AnimationDirection.DOWN;
			AnimationState = AnimationState.Standing;
			CurrentAnimationStepIndex = 0;

			Timer = 0f;
			AnimationStopIndex = 0;

			spriteSingleWidth = Texture.Width / imagesPerAni;
			spriteSingleHeight = Texture.Height / animationCount;

			AnimationIndexes = new Dictionary<AnimationDirection, int>();
			AnimationIndexes.Add(AnimationDirection.DOWN, (int)AnimationDirection.DOWN);
			AnimationIndexes.Add(AnimationDirection.RIGHT, (int)AnimationDirection.RIGHT);
			AnimationIndexes.Add(AnimationDirection.LEFT, (int)AnimationDirection.LEFT);
			AnimationIndexes.Add(AnimationDirection.UP, (int)AnimationDirection.UP);

			Position = position;

			Color = Color.White;

			Animations = new Dictionary<string, Animation>();
		}

		public void AddAddionalTexture(string key, Animation texture) {
			if (!Animations.ContainsKey(key)) {
				Animations.Add(key, texture);
			}
		}

		public void SwitchAnimation(string key) {
			if (Animations.ContainsKey(key)) {
				OtherAnimation = Animations[key];
				OtherAnimation.AnimationState = AnimationState.Standing;
				OtherAnimation.CurrentAnimationStepIndex = AnimationStopIndex;
				OtherAnimation.SetDirection(_CurrentAnimationDirection);
			}
		}

		public void SwitchAnimationToDefault() {
			OtherAnimation = null;
			AnimationState = AnimationState.Standing;
			CurrentAnimationStepIndex = AnimationStopIndex;
		}

		public Rectangle CurrentTextureRectangle() {
			if (OtherAnimation != null) {
				return new Rectangle(spriteSingleWidth * OtherAnimation.CurrentAnimationStepIndex, spriteSingleHeight * OtherAnimation.AnimationIndexes[CurrentAnimationDirection], spriteSingleWidth, spriteSingleHeight);
			} else {
				return new Rectangle(spriteSingleWidth * CurrentAnimationStepIndex, spriteSingleHeight * AnimationIndexes[CurrentAnimationDirection], spriteSingleWidth, spriteSingleHeight);
			}
		}

		public override void Update(GameTime gameTime) {
			if (OtherAnimation != null) {
				OtherAnimation.Update(gameTime);
			} else {
				Timer += (float)gameTime.ElapsedGameTime.TotalSeconds;

				if (Timer > AnimationSpeed) {
					Timer -= AnimationSpeed;
					CurrentAnimationStepIndex += 1;

					if (AnimationState == AnimationState.Walking) {
						if (CurrentAnimationStepIndex >= AnimationStepsPerAnimation) {
							CurrentAnimationStepIndex = 0;
						}
					} else if (AnimationState == AnimationState.Standing) {
						CurrentAnimationStepIndex = AnimationStopIndex;
					}
				}
			}
		}

		public void SetDirection(AnimationDirection d) {
			if (LookAnimationDirection) {
				return;
			}

			if (OtherAnimation != null) {
				OtherAnimation.SetDirection(d);
			} else {
				if (CurrentAnimationDirection != d) {
					CurrentAnimationDirection = d;
					ResetAnimation();
				}
			}
		}

		public void ResetAnimation() {
			if (OtherAnimation != null) {
				OtherAnimation.ResetAnimation();
			} else {
				CurrentAnimationStepIndex = 0;
			}
		}

		/// <summary>
		/// Hier wird angegeben welche Animationen sich im Bild befinden. 
		/// Betrachtet wird dabei das Bild von oben(first) nach unten(four).
		/// RPG Maker-Sprites sind immer (Down, Right, Left, Up).
		/// </summary>
		public void SetAnimationConfiguration(AnimationDirection first, AnimationDirection second, AnimationDirection third, AnimationDirection four) {
			if (OtherAnimation != null) {
				OtherAnimation.SetAnimationConfiguration(first, second, third, four);
			} else {
				AnimationIndexes[first] = 0;
				AnimationIndexes[second] = 1;
				AnimationIndexes[third] = 2;
				AnimationIndexes[four] = 3;
			}
		}

		public override ComponentType ComponentType {
			get { return ComponentType.Animation; }
		}

		public override void Draw(SpriteBatch spritebatch) {
			if (OtherAnimation != null) {
				spritebatch.Draw(OtherAnimation.Texture, Position, CurrentTextureRectangle(), Color);
			} else {
				spritebatch.Draw(Texture, Position, CurrentTextureRectangle(), Color);
			}
		}
	}
}

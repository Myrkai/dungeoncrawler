﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpDX.Toolkit;
using SharpDX.Toolkit.Graphics;
using SharpDX.Toolkit.Content;
using SharpDX;

namespace ZeldaGameBoy {
	public class Equipment : Component {

		private List<Item> Items { get; set; }
		private Dictionary<ItemSlot, Item> EquipedItems { get; set; }
		private ContentManager Content { get; set; }

		public Equipment(ContentManager content) {
			Items = new List<Item>();
			EquipedItems = new Dictionary<ItemSlot, Item>();
			Content = content;
		}

		public void AddItem(int id, Item item) {
			item.ItemID = id;
			Items.Add(item);
			item.LoadContent(this, Content);
		}

		public void EquipItemInSlot(int id, ItemSlot itemSlot) {
			Item item = Items.FirstOrDefault(i => i.ItemID == id);
			if (item != null) {
				if (EquipedItems.ContainsKey(itemSlot)) {
					EquipedItems[itemSlot] = item;
				} else {
					EquipedItems.Add(itemSlot, item);
				}

				item.MenuPosition = new Vector2(-1, -1);
			}
		}

		public void FireItem(ItemSlot itemSlot) {
			if (EquipedItems.ContainsKey(itemSlot)) {
				if (!EquipedItems[itemSlot].Active)
					EquipedItems[itemSlot].Action();
			}
		}

		public override ComponentType ComponentType {
			get { return ComponentType.Equipment; }
		}

		public override void Update(GameTime gameTime) {
			foreach (Item item in EquipedItems.Values) {
				if (item.Active)
					item.Update(gameTime);
			}
		}

		public override void Draw(SpriteBatch spritebatch) {
			foreach (Item item in EquipedItems.Values) {
				if (item.Active)
					item.Draw(spritebatch);
			}
		}

		public void DrawGui(SpriteBatch spriteBatch, ItemSlot itemSlot, Rectangle rectangle) {
			if (EquipedItems.ContainsKey(itemSlot)) {
				EquipedItems[itemSlot].DrawGUI(spriteBatch, rectangle);
			}
		}

		public bool EquipedInSlot(ItemSlot itemSlot) {
			if (EquipedItems.ContainsKey(itemSlot)) {
				return EquipedItems[itemSlot] != null;
			}
			return false;
		}

		public void UnEquipInSlot(ItemSlot itemSlot, Vector2 cursorPosition) {
			if (EquipedItems.ContainsKey(itemSlot)) {
				EquipedItems[itemSlot].MenuPosition = cursorPosition;
				EquipedItems.Remove(itemSlot);
			}
		}

		public void DrawMenuGui(SpriteBatch spriteBatch) {
			foreach (Item item in Items) {
				if (EquipedItems.ContainsValue(item)) {
					continue;
				} else {
					item.DrawMenu(spriteBatch);
				}
			}
		}

		public void SwitchEquipment(ItemSlot itemSlot, Vector2 cursorPosition) {
			Item item = Items.FirstOrDefault(i => i.MenuPosition.Equals(cursorPosition));
			UnEquipInSlot(itemSlot, cursorPosition);
			if (item != null) {
				EquipedItems.Add(itemSlot, item);
				item.MenuPosition = new Vector2(-1, -1);
			}
		}
	}
}

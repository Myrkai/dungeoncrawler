﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpDX.Toolkit.Graphics;
using SharpDX.Toolkit;
using SharpDX;

namespace ZeldaGameBoy.Components {
	public class Sprite : Component {
		private Texture2D _texture;
		public int Width { get { return _texture.Width; } }
		public int Height { get { return _texture.Height; } }
		public Vector2 Position { get; set; }
		public Color Color { get; set; }

		public Sprite(Texture2D texture, Vector2 position) {
			_texture = texture;
			Position = position;
			Color = Color.White;
		}

		public override ComponentType ComponentType {
			get { return ComponentType.Sprite; }
		}

		public override void Update(GameTime gameTime) {
			
		}

		public override void Draw(SpriteBatch spritebatch) {
			spritebatch.Draw(_texture, Position, Color);
		}
	}
}

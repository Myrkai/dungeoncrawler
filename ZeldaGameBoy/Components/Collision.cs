﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpDX.Toolkit;
using SharpDX.Toolkit.Graphics;
using SharpDX;

namespace ZeldaGameBoy {
	public class Collision : Component {

		private MapManager MapManager { get; set; }
		private float Movementspeed { get; set; }

		public Collision(MapManager mapmanager, float movementspeed) {
			MapManager = mapmanager;
			Movementspeed = movementspeed;
		}

		public Vector2 GetMoveDirectionFromTileCollision(Vector2 rec, float totalSecoandElapseTime) {
			Animation ani = GetComponent<Animation>(ComponentType.Animation);
			Vector2 movedirection = rec;

			if (ani != null) {
				//RectangleF playerRec = new RectangleF(ani.Position.X + 1, ani.Position.Y + 1, ani.CurrentTextureRectangle().Width - 2, ani.CurrentTextureRectangle().Height - 2);
				RectangleF playerRec = new RectangleF(ani.Position.X + (ani.CurrentTextureRectangle().Width * 0.4f / 2f) + MapManager.CurrentMap.Cam.Position.X, ani.Position.Y + (ani.CurrentTextureRectangle().Height * 0.5f) + MapManager.CurrentMap.Cam.Position.Y, ani.CurrentTextureRectangle().Width * 0.6f, ani.CurrentTextureRectangle().Height * 0.5f);
				Map CurrentMap = MapManager.CurrentMap;

				if (!IsWalkable(new RectangleF(playerRec.X + movedirection.X * Movementspeed * totalSecoandElapseTime, playerRec.Y, playerRec.Width, playerRec.Height), CurrentMap)) {
					movedirection = new Vector2(0f, movedirection.Y);
				}

				if (!IsWalkable(new RectangleF(playerRec.X, playerRec.Y + movedirection.Y * Movementspeed * totalSecoandElapseTime, playerRec.Width, playerRec.Height), CurrentMap)) {
					movedirection = new Vector2(movedirection.X, 0f);
				}

				movedirection = movedirection * Movementspeed * totalSecoandElapseTime;

				//bind player in map
				if (ani.Position.X + (ani.CurrentTextureRectangle().Width * 0.4f / 2f) + movedirection.X < 0 || ani.Position.X + movedirection.X + playerRec.Width + (ani.CurrentTextureRectangle().Width * 0.4f / 2f) > CurrentMap.Cam.Width) {
					movedirection = new Vector2(0, movedirection.Y);
				}

				if (ani.Position.Y + (ani.CurrentTextureRectangle().Height * 0.5f) + movedirection.Y < 0 || ani.Position.Y + movedirection.Y + playerRec.Height + (ani.CurrentTextureRectangle().Height * 0.5f) > CurrentMap.Cam.Height) {
					movedirection = new Vector2(movedirection.X, 0);
				}

				return movedirection;
			} else {
				return Vector2.Zero;
			}
		}

		public bool IsWalkable(RectangleF rec) {
			return IsWalkable(rec, MapManager.CurrentMap);
		}

		private bool IsWalkable(RectangleF rec, Map map) {
			int cellWidthPx = map.MapWidthPx / map.CollisitionLayer.Width;
			int cellHeightPx = map.MapHeightPx / map.CollisitionLayer.Height;

			bool topLeft = map.CollisitionLayer.GetCell((int)(rec.TopLeft.X / cellWidthPx), (int)(rec.TopLeft.Y / cellHeightPx));
			bool topRight = map.CollisitionLayer.GetCell((int)(rec.TopRight.X / cellWidthPx), (int)(rec.TopRight.Y / cellHeightPx));
			bool bottomLeft = map.CollisitionLayer.GetCell((int)(rec.BottomLeft.X / cellWidthPx), (int)(rec.BottomLeft.Y / cellHeightPx));
			bool bottomRight = map.CollisitionLayer.GetCell((int)(rec.BottomRight.X / cellWidthPx), (int)(rec.BottomRight.Y / cellHeightPx));

			return topLeft && topRight && bottomLeft && bottomRight;
		}

		public void RaiseTileEvents(RectangleF screenPosition, bool forPlayer) {
			RectangleF tmp = screenPosition;
			tmp.X += MapManager.CurrentMap.Cam.Position.X;
			tmp.Y += MapManager.CurrentMap.Cam.Position.Y;
			TileEvent tileEvent = MapManager.CurrentMap.TileEvents.Where(t => t.Dimension.Interacts(tmp, 0.75f)).FirstOrDefault();
			if (tileEvent != null && tileEvent.IsPlayerOnly == forPlayer) {
				tileEvent.Restart();
			}
		}

		public override ComponentType ComponentType {
			get { return ZeldaGameBoy.ComponentType.Collision; }
		}

		public override void Update(GameTime gameTime) {

		}

		public override void Draw(SpriteBatch spritebatch) {

		}
	}
}

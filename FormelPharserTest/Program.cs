﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MathExprParser;
using MathEval;

namespace FormelPharserTest {
	class Program {
		static void Main(string[] args) {
			string formel = "2*(3+3)";
			Console.WriteLine(formel+ "=" + Parser.Parse(formel).ToString());
			Console.WriteLine("2*a+3*b" + "=" + Parser.Parse("2a+3*b", "a", "2", "b", "3").ToString());

			ExpressionEvaluator calc = new ExpressionEvaluator();
			Console.WriteLine(calc.EvaluateExpression("Log(150,10)")); //<-- dieser Pharser ist genau das was ich benötige. Wow.
			Console.WriteLine(calc.EvaluateExpression("2*a+3*b", "a", "2", "b", "3")); //<-- dieser Pharser ist genau das was ich benötige. Wow.
			Console.WriteLine(calc.EvaluateExpression("Log(a,10)+Log(a,10)+Log(a,10)+Log(a,10)", "a", "150"));
			Console.WriteLine(calc.EvaluateExpression("Log(a,10)*4+a", "a", "150"));
			Console.WriteLine(calc.EvaluateExpression("0&0|1&1", "a", "150"));//boolische algebra

			Console.ReadLine();
		}
	}
}

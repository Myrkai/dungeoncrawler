﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MathExprParser {
	public class Parser {
		/// <summary>
		/// Hier können Variablen in die Formel eingebaut werden. Beispiel ("2*a+3*b", "a", "2", "b", "3") das ergitb 13(2*2+3*3).
		/// </summary>
		/// <param name="input">Formel mit Variablen. Beispiel: "2*a+3*b"</param>
		/// <param name="variablen">Variablen für die Formel angeben. Erste Stelle ist Variable und danach folgt der Wert der Variablen. Beispiel "a", "2", "b", "3"</param>
		/// <returns></returns>
		public static double Parse(string input, params string[] variablen) {
			string s = input;
			for (int i = 0; i < variablen.Length; i=i+2) {
				s = s.Replace(variablen[i], variablen[i + 1]);
			}

			return Parse(s);
		}

		public static double Parse(string input) {
			List<object> Tokens = Scanner.Scan(input);

			for (int i = 0; i < Tokens.Count; i++) {
				if (Tokens[i] is Operand) {
					Operand Op = (Operand)Tokens[i];

					if (Op == Operand.OpenBracket) {
						for (int j = 0; j < Tokens.Count; j++) {
							if (Tokens[j] is Operand) {
								Op = (Operand)Tokens[j];

								if (Op == Operand.OpenBracket) {
									i = j;
								}

								if (Op == Operand.CloseBracket) {
									Tokens[i] = ResolveExpression(Tokens.GetRange(i + 1, j - (i + 1)));
									Tokens.RemoveRange(i + 1, j - (i));
									i = -1;
									break;
								}
							}
						}
					}
				}
			}

			return ResolveExpression(Tokens);
		}

		static internal Double ResolveExpression(List<object> tokens) {
			ResolveExpressionHelper(ref tokens, Operand.Potenzieren);
			ResolveExpressionHelper(ref tokens, Operand.Dividieren);
			ResolveExpressionHelper(ref tokens, Operand.Multiplizieren);
			ResolveExpressionHelper(ref tokens, Operand.Subtrahieren);
			ResolveExpressionHelper(ref tokens, Operand.Addieren);

			if (tokens.Count != 1) {
				//TODO: ParsingException werfen
			}

			return Double.Parse(tokens[0].ToString());
		}

		private static void ResolveExpressionHelper(ref List<object> tokens, Operand OpTarget) {
			for (int i = 0; i < tokens.Count; i++) {
				if (tokens[i] is Operand) {
					Operand Op = (Operand)tokens[i];

					if (Op == OpTarget) {
						tokens[i - 1] = ResolveOperand(tokens[i - 1], Op, tokens[i + 1]);
						tokens.RemoveRange(i, 2);
						i = 0;
					}
				}
			}
		}

		static private double ResolveOperand(object left, Operand op, object right) {
			switch (op) {
				case Operand.Addieren:
					return Double.Parse(left.ToString()) + Double.Parse(right.ToString());
				case Operand.Subtrahieren:
					return Double.Parse(left.ToString()) - Double.Parse(right.ToString());
				case Operand.Dividieren:
					return Double.Parse(left.ToString()) / Double.Parse(right.ToString());
				case Operand.Multiplizieren:
					return Double.Parse(left.ToString()) * Double.Parse(right.ToString());
				case Operand.Potenzieren:
					return Math.Pow(Double.Parse(left.ToString()), Double.Parse(right.ToString()));
				default:
					throw new FormatException("Operand nicht auflösbar.");
			}
		}
	}
}

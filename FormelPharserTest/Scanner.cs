﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MathExprParser {
	public enum Operand {
		Addieren,
		Subtrahieren,
		Dividieren,
		Multiplizieren,
		Potenzieren,
		OpenBracket,
		CloseBracket
	}

	internal class Scanner {
		private static Dictionary<char, Operand> operands = new Dictionary<char, Operand> {
            { '+', Operand.Addieren },
            { '-', Operand.Subtrahieren },
            { '*', Operand.Multiplizieren },
            { '/', Operand.Dividieren },
            { '^', Operand.Potenzieren },
            { '(', Operand.OpenBracket },
            { ')', Operand.CloseBracket }
        };

		static internal List<object> Scan(string input) {
			List<object> Tokens = new List<object>();

			bool Negotate = false;

			if (input.Length < 3) {
				//Werfe ScannerException, String zu kurz.
			}

			for (int index = 0; index < input.Length; index++) {
				if (Char.IsDigit(input[index])) {
					if (Negotate) {
						Tokens.Add(Double.Parse("-" + ScanNumber(input, ref index)));
						Negotate = false;
					} else {
						Tokens.Add(ScanNumber(input, ref index));
					}
				} else if (IsOperator(input[index])) {
					if (Tokens.Count > 0) {
						if (Tokens[Tokens.Count - 1] is Operand) {
							Operand LastOp = (Operand)Tokens[Tokens.Count - 1];

							if (ScanOperator(input[index]) == Operand.Subtrahieren) {
								Negotate = true;
							} else if (ScanOperator(input[index]) == Operand.OpenBracket || LastOp == Operand.CloseBracket) {
								Tokens.Add(ScanOperator(input[index]));
							}
						} else {
							Tokens.Add(ScanOperator(input[index]));
						}
					} else {
						if (ScanOperator(input[index]) == Operand.Subtrahieren) {
							Negotate = true;
						} else if (ScanOperator(input[index]) == Operand.OpenBracket) {
							Tokens.Add(Operand.OpenBracket);
						}
					}
				}
			}

			return Tokens;
		}

		static private Double ScanNumber(string input, ref int index) {
			string temp = "";

			for (int i = index; i < input.Length; i++) {
				if (char.IsDigit(input[i]) || input[i].Equals('.') || input[i].Equals(',')) {
					if (input[i].Equals(',')) {
						temp += '.';
						continue;
					}
					temp += input[i];
					index = i;
				} else {
					break;
				}
			}

			return Double.Parse(temp, System.Globalization.CultureInfo.InvariantCulture);
		}

		static private bool IsOperator(char c) {
			return operands.ContainsKey(c);
		}

		static private Operand ScanOperator(char c) {
			if (operands.ContainsKey(c)) {
				return operands[c];
			}
			throw new FormatException("Dieser Operand ist nicht bekannt.");
		}
	}
}

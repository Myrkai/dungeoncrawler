using System;

namespace MathEval {
	
	public class MathEvalException : ApplicationException {
		private int col;
		
		public MathEvalException(int col, string message) : base(message) {
			this.col = col;
		}
		
		public int Col {
			get {
				return col;
			}
		}
	}
	
	internal enum ErrorCode {
		LeftShiftExpected,
		RightShiftExpected,
		CharConstExpected,
		UnknownSymbolEncountered,
		NumberParseError,
		ConstNumberOverflow,
		UnknownIdentifier,
		OrUsedWithFloat,
		XorUsedWithFloat,
		AndUsedWithFloat,
		LeftShiftUsedWithFloat,
		RightShiftUsedWithFloat,
		IntDivUsedWithFloat,
		RemainderUsedWithFloat,
		ComplementUsedWithFloat,
		FactorialUsedWithFloat,
		FactorialUsedWithNegativeNumber,
		UnexpectedSymbolInFactor,
		OnlyIntegralTypeAllowed,
		UnexpectedSymbol
	}
	
	internal static class Errors {
		
		private static string[] errorMsgs = new string[] {
												"<< operator expected.",
												">> operator expected.",
												"Invalid character constant.",
												"Unknown symbol.",
												"Could not parse number.",
												"Number constant is too large.",
												"Unknown function or constant.",
												"Operator | can only be applied to operands of type Integer.",
												"Operator xor can only be applied to operands of type Integer.",
												"Operator & can only be applied to operands of type Integer.",
												"Operator << can only be applied to operands of type Integer.",
												"Operator >> can only be applied to operands of type Integer.",
												"Operator \\ can only be applied to operands of type Integer.",
												"Operator % can only be applied to operands of type Integer.",
												"Operator ~ can only be applied to operand of type Integer.",
												"Operator ! can only be applied to operand of type Integer.",
												"Cannot compute factorial for negative numbers.",
												"Number, '(' or function expected.",
												"Expression has to be of type integer.",
												"Unexpected Symbol encountered."
												};
		
		public static void Error(int col, ErrorCode code) {
			throw new MathEvalException(col, errorMsgs[(int)code]);
		}
		
		public static void Error(int col, ErrorCode code, object expected) {
			throw new MathEvalException(col, String.Format("{0} Expected: {1}.", errorMsgs[(int)code], expected));
		}
	}
}
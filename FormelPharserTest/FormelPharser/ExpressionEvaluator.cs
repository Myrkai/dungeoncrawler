using System;
using System.Globalization;

namespace MathEval {

	public class ExpressionEvaluator {
		
		private static readonly double root2 = Math.Sqrt(2.0);
		private static readonly double root3 = Math.Sqrt(3.0);
		
		private static Random random = new Random();
		
		private Scanner scanner;
		private Token t;
		private TokenType sym;
		
		public ExpressionEvaluator() {
			scanner = new Scanner();
		}

		/// <summary>
		/// Hier k�nnen Variablen in die Formel eingebaut werden. Beispiel ("2*a+3*b", "a", "2", "b", "3") das ergitb 13(2*2+3*3).
		/// Achtung: 2a, "a", "3"-> ergibt "23", NICHT "2*3". Automatisch * zu erg�nzen wurde nicht implementiert
		/// </summary>
		/// <param name="expr">Formel mit Variablen. Beispiel: "2*a+3*b"</param>
		/// <param name="variables">Variablen f�r die Formel angeben. Erste Stelle ist Variable und danach folgt der Wert der Variablen. Beispiel "a", "2", "b", "3"</param>
		/// <returns></returns>
		public string EvaluateExpression(string expr, params string[] variables) {
			string s = expr.Replace(" ", "");
					
			for (int i = 0; i < variables.Length; i = i + 2) {
				s = s.Replace(variables[i], variables[i + 1]);
			}
			
			scanner.Reset(s);
			return Sentence();
		}

		public string EvaluateExpression(string expr) {
			scanner.Reset(expr);
			return Sentence();
		}
		
		/* Expression ["to" ("hex"|"bin"|"dec"|"oct"|"chr"|"gen"|"fix"|"sci")] */
		private string Sentence() {
			Number n;
			string retVal;
			TokenType formatSpecifier = TokenType.Gen;
			
			Scan();
			n = Expression();
			if (sym != TokenType.EOF) {
				Check(TokenType.To);
				formatSpecifier = sym;
				Scan();
				formatSpecifier = sym;
			}
			
			switch (formatSpecifier) {
				case TokenType.Hex:
					if (n.Kind != NumberType.Integer) {
						throw new ArgumentException("Cannot use Hex format with floating point number.");
					}
					retVal = n.IntegerNumber.ToString("X", NumberFormatInfo.InvariantInfo);
					break;
				
				case TokenType.Oct:
					if (n.Kind != NumberType.Integer) {
						throw new ArgumentException("Cannot use Octal format with floating point number.");
					}
					retVal = Convert.ToString(n.IntegerNumber, 8);
					break;
					
				case TokenType.Bin:
					if (n.Kind != NumberType.Integer) {
						throw new ArgumentException("Cannot use Binary format with floating point number.");
					}
					retVal = Convert.ToString(n.IntegerNumber, 2);
					break;
					
				case TokenType.Chr:
					if (n.IntegerNumber < 0 || n.IntegerNumber > 0xFFFF) {
						throw new ArgumentException("Value too large to convert to char.");
					}
					retVal = String.Format("'{0}'", (char)n.IntegerNumber);
					break;
				
				case TokenType.Fix:
					if (n.Kind != NumberType.FloatingPoint) {
						throw new ArgumentException("Cannot use Fixed format with integer number.");
					}
					retVal = n.FloatingPointNumber.ToString("F", NumberFormatInfo.InvariantInfo);
					break;
				
				case TokenType.Sci:
					if (n.Kind != NumberType.FloatingPoint) {
						throw new ArgumentException("Cannot use Scientific format with integer number.");
					}
					retVal = n.FloatingPointNumber.ToString("E", NumberFormatInfo.InvariantInfo);
					break;
					
				case TokenType.Dec:
				case TokenType.Gen:
					if (n.Kind == NumberType.Integer) {
						retVal = n.IntegerNumber.ToString("G", NumberFormatInfo.InvariantInfo);
					} else {
						retVal = n.FloatingPointNumber.ToString("G", NumberFormatInfo.InvariantInfo);
					}
					break;
					
				default:
					throw new ArgumentException("Format specifier(bin, oct, hex, dec, gen, sci, fix, chr) expected.");
			}
			
			return retVal;
		}
		
		/* XorExpression {"|" XorExpression} */ 
		private Number Expression() {
			Number x, y;
			
			x = XorExpression();
			while (sym == TokenType.Or) {
				int errorPos = t.Col;
				Scan();
				y = XorExpression();
				
				if (x.Kind != NumberType.Integer || y.Kind != NumberType.Integer) {
					Errors.Error(errorPos, ErrorCode.OrUsedWithFloat);
				}
				x.IntegerNumber |= y.IntegerNumber;
			}
			
			return x;
		}
		
		/* AndExpression {"xor" AndExpression} */
		private Number XorExpression() {
			Number x, y;
			
			x = AndExpression();
			while (sym == TokenType.Xor) {
				int errorPos = t.Col;
				Scan();
				y = AndExpression();
				
				if (x.Kind != NumberType.Integer || y.Kind != NumberType.Integer) {
					Errors.Error(errorPos, ErrorCode.XorUsedWithFloat);
				}
				x.IntegerNumber ^= y.IntegerNumber;
			}
			
			return x;
		}
		
		/* ShiftExpression {"&" ShiftExpression} */
		private Number AndExpression() {
			Number x, y;
			
			x = ShiftExpression();
			while (sym == TokenType.And) {
				int errorPos = t.Col;
				Scan();
				y = ShiftExpression();
				
				if (x.Kind != NumberType.Integer || y.Kind != NumberType.Integer) {
					Errors.Error(errorPos, ErrorCode.AndUsedWithFloat);
				}
				x.IntegerNumber &= y.IntegerNumber;
			}
			
			return x;
		}
		
		/* AdditiveExpression {("<<"|">>") AdditiveExpression} */
		private Number ShiftExpression() {
			Number x, y;
			TokenType op;
			
			x = AdditiveExpression();
			while (sym == TokenType.LeftShift || sym == TokenType.RightShift) {
				op = sym;
				int errorPos = t.Col;
				Scan();
				y = AdditiveExpression();
				
				if (x.Kind != NumberType.Integer || y.Kind != NumberType.Integer) {
					ErrorCode c = (op == TokenType.LeftShift) ? ErrorCode.LeftShiftUsedWithFloat
															  : ErrorCode.RightShiftUsedWithFloat;
					Errors.Error(errorPos, c);
				}
				
				if (op == TokenType.LeftShift) {
					x.IntegerNumber <<= (int)(y.IntegerNumber & 0x3F);
				} else {
					x.IntegerNumber = (long)((ulong)x.IntegerNumber >> (int)(y.IntegerNumber & 0x3F));
				}
			}
			
			return x;
		}
		
		/* MultiplicativeExpression {("+"|"-") MultiplicativeExpression} */
		private Number AdditiveExpression() {
			Number n1,n2;
			TokenType op;
			
			n1 = MultiplicativeExpression();
			while (sym == TokenType.Plus || sym == TokenType.Minus) {
				op = sym;
				Scan();
				n2 = MultiplicativeExpression();
				n1 = (op == TokenType.Plus) ? n1 + n2 : n1 - n2;
			}
			
			return n1;
		}
		
		/* ComplementExpression {("*"|"/"|"\"|"%") ComplementExpression} */
		private Number MultiplicativeExpression() {
			Number x, y;
			TokenType op;
			
			x = ComplementExpression();
			while (sym == TokenType.Mult || sym == TokenType.Div || sym == TokenType.IntegerDiv || sym == TokenType.Remainder) {
				op = sym;
				int errorPos = t.Col;
				
				Scan();
				y = ComplementExpression();
				
				switch (op) {
					case TokenType.Mult:
						x *= y;
						break;
					
					case TokenType.Div:
						x /= y;
						break;
					
					case TokenType.IntegerDiv:
						if (x.Kind != NumberType.Integer || y.Kind != NumberType.Integer) {
							Errors.Error(errorPos, ErrorCode.IntDivUsedWithFloat);
						}
						x.IntegerNumber /= y.IntegerNumber;
						break;
					
					case TokenType.Remainder:
						if (x.Kind != NumberType.Integer || y.Kind != NumberType.Integer) {
							Errors.Error(errorPos, ErrorCode.RemainderUsedWithFloat);
						}
						x.IntegerNumber %= y.IntegerNumber;
						break;
				}
			}
			
			return x;
		}
		
		/* ["~"]PowExpression */
		private Number ComplementExpression() {
			if (sym == TokenType.Complement) {
				int errorPos = t.Col;
				Scan();
				Number x = PowExpression();
				
				if (x.Kind != NumberType.Integer) {
					Errors.Error(errorPos, ErrorCode.ComplementUsedWithFloat);
				}
				x.IntegerNumber = ~x.IntegerNumber;
				
				return x;
				
			} else {
				return PowExpression();
			}
		}
		
		/* NegationExpression {"^" NegationExpression} */
		private Number PowExpression() {
			Number n;
			
			n = NegationExpression();
			while (sym == TokenType.Power) {
				Scan();
				n = Number.Pow(n, NegationExpression());
			}
			
			return n;
		}
		
		/* ["-"]FactorialExpression */
		private Number NegationExpression() {
			if (sym == TokenType.Minus) {
				Scan();
				return -FactorialExpression();
				
			} else {
				return FactorialExpression();
			}
		}
		
		/* Factor["!"] */
		private Number FactorialExpression() {
			Number x = Factor();
			
			if (sym == TokenType.Factorial) {
				if (x.Kind != NumberType.Integer) {
					Errors.Error(t.Col, ErrorCode.FactorialUsedWithFloat);
				}
				if (x.IntegerNumber < 0) {
					Errors.Error(t.Col, ErrorCode.FactorialUsedWithNegativeNumber);
				}
				x = Number.Factorial(x);
				
				Scan();
			}
			
			return x;
		}
		
		/* Number | Const | Function | "("Expression")" */
		private Number Factor() {
			Number n;
			
			switch (sym) {
				case TokenType.Number:
					n = t.Number;
					Scan();
					return n;
				
				case TokenType.E:
					Scan();
					return new Number(Math.E);
				
				case TokenType.PI:
					Scan();
					return new Number(Math.PI);
				
				case TokenType.R2:
					Scan();
					return new Number(root2);
				
				case TokenType.R3:
					Scan();
					return new Number(root3);
				
				/* "("Expression")" */
				case TokenType.LeftPar:
					Scan();
					n = Expression();
					Check(TokenType.RightPar);
					return n;
				
				case TokenType.Abs:
					return AbsFunction();
				
				case TokenType.Acos:
					return AcosFunction();
				
				case TokenType.Asin:
					return AsinFunction();
				
				case TokenType.Atan:
					return AtanFunction();
				
				case TokenType.Ceil:
					return CeilFunction();
				
				case TokenType.Cos:
					return CosFunction();
					
				case TokenType.Cosh:
					return CoshFunction();
				
				case TokenType.Exp:
					return ExpFunction();
				
				case TokenType.Floor:
					return FloorFunction();
				
				case TokenType.Log:
					return LogFunction();
				
				case TokenType.Log2:
					return Log2Function();
				
				case TokenType.Log10:
					return Log10Function();
				
				case TokenType.Max:
					return MaxFunction();
				
				case TokenType.Min:
					return MinFunction();
				
				case TokenType.Pow:
					return PowFunction();
				
				case TokenType.Prod:
					return ProdFunction();
				
				case TokenType.Root:
					return RootFunction();
				
				case TokenType.Inv:
					return InvFunction();
				
				case TokenType.Sign:
					return SignFunction();
				
				case TokenType.Sin:
					return SinFunction();
				
				case TokenType.Sinh:
					return SinhFunction();
				
				case TokenType.Sqrt:
					return SqrtFunction();
				
				case TokenType.Sum:
					return SumFunction();
				
				case TokenType.Tan:
					return TanFunction();
				
				case TokenType.Tanh:
					return TanhFunction();
				
				case TokenType.Rand:
					return RandFunction();
				
				case TokenType.RandDouble:
					return RandDoubleFunction();
				
				case TokenType.DegToRad:
					return DegToRadFunction();
				
				case TokenType.RadToDeg:
					return RadToDegFunction();
				
				case TokenType.ToInt:
					return ToIntFunction();
				
				default:
					Errors.Error(t.Col, ErrorCode.UnexpectedSymbolInFactor);
					return new Number();
			}
		}
		
		/* "Abs("Expression")" */
		private Number AbsFunction() {
			Number n;
			
			Scan();
			Check(TokenType.LeftPar);
			n = Expression();
			if (n.Kind == NumberType.Integer) {
				n.IntegerNumber = Math.Abs(n.IntegerNumber);
			} else {
				n.FloatingPointNumber = Math.Abs(n.FloatingPointNumber);
			}
			Check(TokenType.RightPar);
			
			return n;
		}
		
		/* "Acos("Expression")" */
		private Number AcosFunction() {
			Number n;
			
			Scan();
			Check(TokenType.LeftPar);
			n = Math.Acos(Expression());
			Check(TokenType.RightPar);
			
			return n;
		}
		
		/* "Asin("Expression")" */
		private Number AsinFunction() {
			Number n;
			
			Scan();
			Check(TokenType.LeftPar);
			n = Math.Asin(Expression());
			Check(TokenType.RightPar);
			
			return n;
		}
		
		/* "Atan("Expression")" */
		private Number AtanFunction() {
			Number n;
			
			Scan();
			Check(TokenType.LeftPar);
			n = Math.Atan(Expression());
			Check(TokenType.RightPar);
			
			return n;
		}
		
		/* "Ceil("Expression")" */
		private Number CeilFunction() {
			Number n;
			
			Scan();
			Check(TokenType.LeftPar);
			n = Expression();
			if (n.Kind == NumberType.FloatingPoint) {
				n = Math.Ceiling(n);
			}
			Check(TokenType.RightPar);
			
			return n;
		}
		
		/* "Cos("Expression["�"|"rad"]")" */
		private Number CosFunction() {
			Number n;
			
			Scan();
			Check(TokenType.LeftPar);
			n = Expression();
			
			if (sym == TokenType.Deg) {
				Scan();
				ConvertDegToRad(ref n);
			} else if (sym == TokenType.Rad) {
				Scan();
			}
			n = Math.Cos(n);
			Check(TokenType.RightPar);
			
			return n;
		}
		
		/* "Cosh("Expression["�"|"rad"]")" */
		private Number CoshFunction() {
			Number n;
			
			Scan();
			Check(TokenType.LeftPar);
			n = Expression();
			
			if (sym == TokenType.Deg) {
				Scan();
				ConvertDegToRad(ref n);
			} else if (sym == TokenType.Rad) {
				Scan();
			}
			
			n = Math.Cosh(n);
			Check(TokenType.RightPar);
			
			return n;
		}
		
		/* "Exp("Expression")" */
		private Number ExpFunction() {
			Number n;
			
			Scan();
			Check(TokenType.LeftPar);
			n = Math.Exp(Expression());
			Check(TokenType.RightPar);
			
			return n;
		}
		
		/* "Floor("Expression")" */
		private Number FloorFunction() {
			Number n;
			
			Scan();
			Check(TokenType.LeftPar);
			n = Expression();
			if (n.Kind == NumberType.FloatingPoint) {
				n = Math.Floor(n);
			}
			Check(TokenType.RightPar);
			
			return n;
		}
		
		/* "Log("Expression[","Expression]")" */
		private Number LogFunction() {
			Number n;
			
			Scan();
			Check(TokenType.LeftPar);
			n = Expression();
			if (sym == TokenType.Comma) {
				/* "Log("Expression","Expression")" */
				Scan();
				n = Math.Log(n, Expression());
			} else {
				/* "Log("Expression")" */
				n = Math.Log(n);
			}
			Check(TokenType.RightPar);
			
			return n;
		}
		
		/* "Log2("Expression")" */
		private Number Log2Function() {
			Number n;
			
			Scan();
			Check(TokenType.LeftPar);
			n = Math.Log(Expression(), 2.0);
			Check(TokenType.RightPar);
			
			return n;
		}
		
		/* "Log10("Expression")" */
		private Number Log10Function() {
			Number n;
			
			Scan();
			Check(TokenType.LeftPar);
			n = Math.Log10(Expression());
			Check(TokenType.RightPar);
			
			return n;
		}
		
		/* "Max("Expression{","Expression}")" */
		private Number MaxFunction() {
			Number max, n;
		
			Scan();
			Check(TokenType.LeftPar);
			max = Expression();
			
			while (sym == TokenType.Comma) {
				Scan();
				n = Expression();
				if (n > max) {
					max = n;
				}
			}
			Check(TokenType.RightPar);
			
			return max;
		}
		
		/* "Min("Expression{","Expression}")" */
		private Number MinFunction() {
			Number min, n;
		
			Scan();
			Check(TokenType.LeftPar);
			min = Expression();
			
			while (sym == TokenType.Comma) {
				Scan();
				n = Expression();
				if (n < min) {
					min = n;
				}
			}
			Check(TokenType.RightPar);
			
			return min;
		}
		
		/* "Sum("Expression {","Expression}")" */
		private Number SumFunction() {
			Number sum;
		
			Scan();
			Check(TokenType.LeftPar);
			sum = Expression();
			
			while (sym == TokenType.Comma) {
				Scan();
				sum += Expression();
			}
			Check(TokenType.RightPar);
			
			return sum;
		}
		
		/* "Prod("Expression {","Expression}")" */
		private Number ProdFunction() {
			Number prod;
		
			Scan();
			Check(TokenType.LeftPar);
			prod = Expression();
			
			while (sym == TokenType.Comma) {
				Scan();
				prod *= Expression();
			}
			Check(TokenType.RightPar);
			
			return prod;
		}
		
		/* "Pow("Expression","Expression")" */
		private Number PowFunction() {
			Number n;
			
			Scan();
			Check(TokenType.LeftPar);
			n = Expression();
			Check(TokenType.Comma);
			n = Number.Pow(n, Expression());
			Check(TokenType.RightPar);
			
			return n;
		}
		
		/* "Sign("Expression")" */
		private Number SignFunction() {
			Number n;
			
			Scan();
			Check(TokenType.LeftPar);
			n = Expression();
			
			if (n.Kind == NumberType.Integer) {
				n = Math.Sign(n.IntegerNumber);
			} else {
				n = Math.Sign(n);
			}
			Check(TokenType.RightPar);
			
			return n;
		}
		
		/* "Sin("Expression["�"|"rad"]")" */
		private Number SinFunction() {
			Number n;
			
			Scan();
			Check(TokenType.LeftPar);
			n = Expression();
			
			if (sym == TokenType.Deg) {
				Scan();
				ConvertDegToRad(ref n);
			} else if (sym == TokenType.Rad) {
				Scan();
			}
			
			n = Math.Sin(n);
			Check(TokenType.RightPar);
			
			return n;
		}
		
		/* "Sinh("Expression["�"|"rad"]")" */
		private Number SinhFunction() {
			Number n;
			
			Scan();
			Check(TokenType.LeftPar);
			n = Expression();
			
			if (sym == TokenType.Deg) {
				Scan();
				ConvertDegToRad(ref n);
			} else if (sym == TokenType.Rad) {
				Scan();
			}
			
			n = Math.Sinh(n);
			Check(TokenType.RightPar);
			
			return n;
		}
		
		/* "Sqrt("Expression")" */
		private Number SqrtFunction() {
			Number n;
			
			Scan();
			Check(TokenType.LeftPar);
			n = Math.Sqrt(Expression());
			Check(TokenType.RightPar);
			
			return n;
		}
		
		/* "Tan("Expression["�"|"rad"]")" */
		private Number TanFunction() {
			Number n;
			
			Scan();
			Check(TokenType.LeftPar);
			n = Expression();
			
			if (sym == TokenType.Deg) {
				Scan();
				ConvertDegToRad(ref n);
			} else if (sym == TokenType.Rad) {
				Scan();
			}
			n = Math.Tan(n);
			Check(TokenType.RightPar);
			
			return n;
		}
		
		/* "Root("Expression","Expression")" */
		private Number RootFunction() {
			Number n1, n2;
			
			Scan();
			Check(TokenType.LeftPar);
			n1 = Expression();
			Check(TokenType.Comma);
			n2 = Expression();
			n1 = Number.Pow(n1, 1.0 / n2);
			Check(TokenType.RightPar);
			
			return n1;
		}
		
		/* "Inv("Expression")" */
		private Number InvFunction() {
			Number n;
			
			Scan();
			Check(TokenType.LeftPar);
			n = Expression();
			Check(TokenType.RightPar);
			
			return 1.0 / n;
		}
		
		/* "Tanh("Expression["�"|"rad"]")" */
		private Number TanhFunction() {
			Number n;
			
			Scan();
			Check(TokenType.LeftPar);
			n = Expression();
			
			if (sym == TokenType.Deg) {
				Scan();
				ConvertDegToRad(ref n);
			} else if (sym == TokenType.Rad) {
				Scan();
			}
			n = Math.Tanh(n);
			Check(TokenType.RightPar);
			
			return n;
		}
		
		/* "Rand("[Expression[","Epression]]")" */
		private Number RandFunction() {
			Number n1, n2;
			
			Scan();
			Check(TokenType.LeftPar);
			if (sym != TokenType.RightPar) {
				/* "Rand("Expression[","Epression]")" */
				int errorPos = t.Col;
				n1 = Expression();
				if (n1.Kind != NumberType.Integer) {
					Errors.Error(errorPos, ErrorCode.OnlyIntegralTypeAllowed);
				}
				if (sym == TokenType.Comma) {
					/* "Rand("Expression","Epression")" */
					Scan();
					errorPos = t.Col;
					n2 = Expression();
					if (n2.Kind != NumberType.Integer) {
						Errors.Error(errorPos, ErrorCode.OnlyIntegralTypeAllowed);
					}
					n1 = random.Next((int)n1, (int)n2);
				} else {
					/* "Rand("Expression")" */
					n1 = random.Next((int)n1);
				}
			} else {
				/* "Rand()" */
				n1 = random.Next();
			}
			Check(TokenType.RightPar);
			
			return n1;
		}
		
		/* "RandDouble()" */
		private Number RandDoubleFunction() {
			Scan();
			Check(TokenType.LeftPar);
			Check(TokenType.RightPar);
			return random.NextDouble();
		}
		
		/* "Deg2Rad("Expression")" */
		private Number DegToRadFunction() {
			Number n;
			
			Scan();
			Check(TokenType.LeftPar);
			n = Expression();
			ConvertDegToRad(ref n);
			Check(TokenType.RightPar);
			
			return n;
		}
		
		/* "Rad2Deg("Expression")" */
		private Number RadToDegFunction() {
			Number n;
			
			Scan();
			Check(TokenType.LeftPar);
			n = Expression();
			ConvertRadToDeg(ref n);
			Check(TokenType.RightPar);
			
			return n;
		}
		
		/* "ToInt("Expression")" */
		private Number ToIntFunction() {
			Number n;
			
			Scan();
			Check(TokenType.LeftPar);
			n = (long)Expression();
			Check(TokenType.RightPar);
			
			return n;
		}
		
		private void ConvertDegToRad(ref Number val) {
			val *= Math.PI / 180.0;
		}
		
		private void ConvertRadToDeg(ref Number val) {
			val *= 180.0 / Math.PI;
		}
		
		private void Scan() {
			t = scanner.ScanNext();
			sym = t.Kind;
		}
		
		private void Check(TokenType expected) {
			if (sym == expected) {
				Scan();
			} else {
				Errors.Error(t.Col, ErrorCode.UnexpectedSymbol, expected);
			}
		}
	}
}